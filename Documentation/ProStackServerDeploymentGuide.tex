\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb,amsfonts,textcomp}
\usepackage{color}
\usepackage{longtable}
\usepackage{hyperref}
\hypersetup{colorlinks=true, linkcolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue}
\usepackage{graphicx}
\usepackage{epsfig}
\setlength\tabcolsep{1mm}
\renewcommand\arraystretch{1.3}
\begin{document}

\title{ProStack Server Deployment Guide}
\author{Konstantin Kozlov\\
  Department of Computational biology,\\
  Center for Advanced Studies, St.Petersburg State Polytechnical University,\\
  St.Petersburg,\\
  Russian Federation,\\
  Polytechnicheskaya ul., 29\\
  \texttt{kozlov@spbcas.ru}}
\date {\today}
\maketitle


\begin{abstract}

This guide describes the installation and usage of the server component of the ProStack image processing platform on the GNU/Linux operating system.  
The guide is targeted at the administrators and users that need to set up a server for processing of images. It is assumed that a reader has a knowledge of configuration of GNU/Linux, networks and Apache Web Server v.2 at the intermediate level. These terms and actions are not explained here. All paths and locations in the text below refer to the installation of ProStack Server Component on Fedora GNU/Linux 12 from RPM.

\end{abstract}
\newpage
\setcounter{tocdepth}{2}
\renewcommand\contentsname{Table of Contents}
\tableofcontents
\listoffigures
%\listoftables
\newpage
\section{Team}
Konstantin N. Kozlov(kozlov@spbcas.ru), Andrei S. Pisarev(pisarev@spbcas.ru).

The leader of the project Maria G. Samsonova(samson@spbcas.ru).

\section{License}

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHAN\-TABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place {}- Suite 330, Boston, MA 02111{}-1307, USA.

\href{http://www.fsf.org/licensing/licenses/gpl.html}{http://www.fsf.org/licensing/licenses/gpl.html}

\section{System requirements}

The server component of the ProStack image processing platform is implemented as the module named \texttt{mod\_prostack} for Apache Web Server v.2. It is distributed as source code and RPM packages for Fedora GNU/Linux 32-bit. Developers didn't and don't have plans to test the module on OSes other than GNU/Linux.

\section{Architecture}

\begin{figure*}[!h]
\centering\includegraphics[width=15cm]{prostackapache.eps} \caption{ProStack Server Component as \texttt{mod\_prostack}}
\label{fig:prostackapache}
\end{figure*}

The ProStack Server Component \texttt{mod\_prostack} implements an interface for remote execution of modules and scenarios that are available in ProStack image processing platform~(see Fig.\ref{fig:prostackapache}). The communication is done using HTTP(s) protocol. The software uses GET, PUT, POST and DELETE methods to provide equal or extended functionality in comparison to the desktop version. ProStack Server Component is not locked to work with any particular client software. The desktop version of ProStack can be used as client for ProStack Server and more over can run different processing steps on different machines in one scenario.

\section{Installation}

The RPM package prostack-server-*.rpm contains the module for Apache and the image processing routines. These routines are also included in RPM for desktop version of ProStack prostack-main*.rpm. This part is identical in both RPMs. This means that the contents of RPMs overlap but don't conflict so both server and desktop versions may be installed on the same physical computer.

To install ProStack Server install binary prostack-server-*.rpm for your platform. If such package doesn't exist first try to recompile the source rpm package. If that fails or your distribution doesn't support RPM compile the software from source. In the latter case you'll need to install the development environment beforehand.

\section{Server Configuration}

Upon installation the file \texttt{/etc/httpd/conf.d/prostack.conf} is created with the following contents:
\begin{verbatim}
#
# Mod_prostack is a module that embeds the ProStack in apache
#

#LoadModule prostack_module modules/mod_prostack.so

#<Location "/prostack">
#		ProStack On
#		ProStackFullUri "http://prostack.example.local/prostack"
#		ProStackKimonoDb kimono.db
#		ProStackConfDir /var/www/prostack
#		ProStackPortNum 0
#		ProStackUser prostack
#		ProStackTimeout 90
#		ProStackInstall Off
#		ProStackLibrary Off
#</Location>
\end{verbatim}

The following parameters can be adjusted:
\begin{itemize}
\item \textbf{ProStack}--On/Off. Switch ProStack Server Component for this location on or off.
\item \textbf{ProStackFullUri}--"http://prostack.example.local/prostack". The URI that will be included in all responses from the \texttt{mod\_prostack}. In high-availability configuration when several slave servers are behind the reverse proxy this attribute is to be equal the URI of gateway.
\item \textbf{ProStackKimonoDb}--kimono.db. The name of the database with modules.
\item \textbf{ProStackConfDir}--/var/www/prostack. The real path to the directory where the data for \texttt{mod\_prostack} is located.
\item \textbf{ProStackPortNum}--0. If this parameter has non-zero value the name of the scratch directory will include it. It is not really used in the current implementation.
\item \textbf{ProStackUser}--prostack. The user that runs ProStack routines. This is normally equal to the user that runs apache.
\item \textbf{ProStackTimeout}--90. The timeout for routines that are called remotely.
\item \textbf{ProStackInstall}--On/Off. Allow or not to install new modules remotely.
\item \textbf{ProStackLibrary}--On/Off. Allow or not to install scenarios remotely.
\end{itemize}

It is highly recommended to leave the latter two parameters 'Off' and let the administrator to perform those tasks from the command line.

The file \texttt{/etc/httpd.conf} should contain the following string:
\begin{verbatim}
Include conf.d/*.conf
\end{verbatim}

\section{Client Configuration}

\begin{figure*}[!h]
\centering\includegraphics[width=15cm]{addserver.eps} \caption{Client configuration}
\label{fig:addserver}
\end{figure*}

In ProStack main window go to \texttt{Toolbar/Server/Add}. The following dialog will be displayed~\ref{fig:addserver}. Fill in all fields and submit. This will result in the following section in the file \texttt{~user/.bambu/kimono-gui.rc}:
\begin{verbatim}
[server]
name=server
host=prostack.example.local
port=80
address=/prostack
proto=rest
\end{verbatim}

Here the value 'rest' for the parameter 'proto' indicates that unsecure HTTP connection is used. For secure HTTPS connection set this parameter to 'rests'.

\begin{figure*}[!h]
\centering\includegraphics[width=15cm]{selserver.eps} \caption{Select the server}
\label{fig:selserver}
\end{figure*}

To run modules on the remote server select it in the main window~(see Fig.~\ref{fig:selserver}). The list of available operations will be displayed and the use will be able to add them to the scenario. To use local machine again select the 'default' server. Any scenario can include modules from different servers. The user is responsible to make sure that the data is accessible by all servers. \texttt{mod\_prostack} does not provide upload/download functionality. This can be easily achieved by setting up WebDAV or ftp server. ProStack does provide modules such as 'cmove' and 'curlup' to access data remotely. The user can also mount remote repositories under GNU/Linux.

\section{ProStack Server Component and SELinux}

The work is currently being done to test ProStack Server Component with SELinux enabled. In general it is up to the administrator to decide if SELinux is to be enabled. The current version of ProStack Server Component is known to run with SELinux disabled.

\bibliographystyle{plain}
\bibliography{parus}


\end{document}
