/***************************************************************************
                          grf.h  -  description
                             -------------------
    begin                : þÔ× ñÎ× 20 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <tiffio.h>
#include <glib.h>

#ifndef GRF_H
#define GRF_H

typedef int (*GrfCallbackFunc0)(double scale, uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*out_pixels_1, void*user_data);
typedef int (*GrfCallbackFunc)(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data);
typedef int (*GrfCallbackFunc3)(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*in_pixels_3, double*out_pixels_1, void*user_data);

void grf_read_char(char*filename, uint32 *rows, uint32 *columns, unsigned char**pixels);
void grf_write_char(char*filename, uint32 rows, uint32 columns, unsigned char*pixels);
void grf_write_char_rgb(char*filename, uint32 rows, uint32 columns, unsigned char**pixels, int SamplesPerPixel);
void grf_read_dim(char*filename, uint32 *rows, uint32 *columns);
int grf_is_tiff(char*filename);
void grf_read_char_volume(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes);
void grf_read_dim_volume(char*filename, uint32 *rows, uint32 *columns, uint32 *planes);
void grf_write_char_volume(char*filename, unsigned char***voxels, uint32 rows, uint32 columns, uint32 planes);
void grf_read_info(char*filename, uint32 *rows, uint32 *columns, int *bits, int *format);
int grf_read_super(char*filename, uint32 *rows, uint32 *columns, double**pixels);
int grf_read_dim_super(char*filename, uint32 *rows, uint32 *columns);
void grf_write_super(char*filename, uint32 rows, uint32 columns, double*pixels, int FilledPixel);
void grf_read_char_volume_lsm(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes, int channel);
void grf_read_max_char_volume_lsm(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes, int nchannels, int *channels);

int grf_compose_two(char*input_pointer_1, char*input_pointer_2, char*output_pointer_1, GrfCallbackFunc grf_callback, void*user_data);
int grf_compose_rgb(char*input_pointer_1, char*input_pointer_2, char*input_pointer_3, char*output_pointer_1, GrfCallbackFunc3 grf_callback, void*user_data);
int grf_compose_one(char*input_pointer_1, char*output_pointer_1, GrfCallbackFunc0 grf_callback, void*user_data);

void grf_write_char_volume_rgb(char*filename, uint32 rows, uint32 columns, uint32 planes, unsigned char****voxels, int SamplesPerPixel);

void grf_read_char_rgb(char*filename, uint32 *rows, uint32 *columns, unsigned char**pixels);

#endif
