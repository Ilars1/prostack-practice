/***************************************************************************
                          grf.c  -  description
                             -------------------
    begin                : þÔ× ñÎ× 20 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <grf.h>

int  grf_is_tiff(char*filename)
{
	TIFF *tfile;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile ) {
		return 0;
	} else {
		TIFFClose(tfile);
		return 1;
	}
}

void grf_read_char(char*filename, uint32 *rows, uint32 *columns, unsigned char**pixels)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	uint32 value32;
	int palette_length;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);

	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != 0 && (int)photom != 1 && (int)photom != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", filename, (int)photom);
	}
	palette_length = 0;
	if ( (int)photom == 3 ) {
		if ( (int)bps == 16 ) {
			palette_length = 65536 * 3;
		} else if ( (int)bps == 8 ) {
			palette_length = 256 * 3;
		}
		TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
	}
/*	fprintf(stdout, "bps %u\n",bps);
	TIFFGetField(tfile, TIFFTAG_EXTRASAMPLES, &value32);
	fprintf(stdout, "extrasamples %u\n",value32);
	TIFFGetField(tfile, EXTRASAMPLE_ASSOCALPHA, &value32);
	fprintf(stdout, "extrasamples %u\n",value32);
	TIFFGetField(tfile, TIFFTAG_IMAGEDEPTH, &value16);
	fprintf(stdout, "extrasamples %u\n",value16);
*/
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}

	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}

	*columns = (uint32) width;
	*rows = (uint32) height;

	(*pixels) = (unsigned char*)calloc( (*columns) * (*rows) , sizeof(unsigned char) );
	if ( !(*pixels) )
		g_error("grf_read_char can't allocate");

	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
/*	fprintf(stdout, "\n scan line size %d\n buf size %d", TIFFScanlineSize(tfile), sizeof(buf));*/
	if ( !buf )
		g_error("grf_read_char can't allocate");
	if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
		Scale = 4096.0;
	} else {
		Scale = 65536.0;
	}
	if ( (int)bps == 16 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint16*)buf)[column];
					(*pixels)[row*width+column] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
				} else {
					(*pixels)[row*width+column] = (unsigned char)( 255 * (double)((uint16*)buf)[column] / Scale + 0.5 );
				}
			}
		}

	} else if ( (int)bps == 8 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint8*)buf)[column];
					(*pixels)[row*width+column] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
				} else {
					(*pixels)[row*width+column] = (unsigned char)(((uint8*)buf)[column] );
				}
			}
		}

	}
	_TIFFfree(buf);
	TIFFClose(tfile);

}

void grf_read_char_volume(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	int i, j, k;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != 0 && (int)photom != 1 && (int)photom != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", filename, (int)photom);
	}
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16);
	*columns = width;
	*rows = height;
	(*voxels) = (unsigned char***)calloc( (*columns), sizeof(unsigned char**));
	if ( !(*voxels) )
		g_error("grf_read_char_volume can't allocate");
	for ( i = 0; i < (*columns); i++) {
		(*voxels)[i] = (unsigned char**)calloc((*rows), sizeof(unsigned char*));
		if ( !((*voxels)[i]) )
			g_error("grf_read_char_volume can't allocate");
		for ( j = 0; j < (*rows); j++) {
			(*voxels)[i][j] = (unsigned char*)NULL;
		}
	}
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");
	if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
		Scale = 4096.0;
	} else {
		Scale = 65536.0;
	}
	k = 0;
	do {
		TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width )
			g_error("grf_read_char_volume different dimensions for different planes! ");
		palette_length = 0;
		if ( (int)photom == 3 ) {
			if ( (int)bps == 16 ) {
				palette_length = 65536 * 3;
			} else if ( (int)bps == 8 ) {
				palette_length = 256 * 3;
			}
			TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
		}
		for ( i = 0; i < (*columns); i++) {
			for ( j = 0; j < (*rows); j++) {
				(*voxels)[i][j] = (unsigned char*)realloc((*voxels)[i][j], ( k + 1 ) * sizeof(unsigned char) );
				if ( !((*voxels)[i][j]) )
					g_error("grf_read_char_volume can't allocate");
			}
		}
		if ( (int)bps == 16 ) {
			for(row = 0; row < height; row++) {
				TIFFReadScanline(tfile, buf, row, 0);
				for(column = 0; column < width; column++) {
					if ( (int)photom == 3 ) {
						int indexed_color = (int)((uint16*)buf)[column];
						(*voxels)[column][row][k] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
					} else {
						(*voxels)[column][row][k] = (unsigned char)( 255 * (double)((uint16*)buf)[column] / Scale + 0.5 );
					}
				}
			}
		} else if ( (int)bps == 8 ) {
			for(row = 0; row < height; row++) {
				TIFFReadScanline(tfile, buf, row, 0);
				for(column = 0; column < width; column++) {
					if ( (int)photom == 3 ) {
						int indexed_color = (int)((uint8*)buf)[column];
						(*voxels)[column][row][k] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
					} else {
						(*voxels)[column][row][k] = (unsigned char)(((uint8*)buf)[column] );
					}
				}
			}
		}
		k++;
	} while ( TIFFReadDirectory( tfile ) );
	_TIFFfree(buf);
	*planes = k;
	TIFFClose(tfile);
}

void grf_read_dim(char*filename, uint32 *rows, uint32 *columns)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}
	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}
	*columns = width;
	*rows = height;
	TIFFClose(tfile);
}


void grf_read_info(char*filename, uint32 *rows, uint32 *columns, int *bits, int *format)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}
	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_SAMPLEFORMAT, &value16))  {
		fprintf(stdout, "format %d\n", value16);
	}
	*columns = width;
	*rows = height;
	*bits = (int)bps;
	*format = (int)value16;
	TIFFClose(tfile);
}


void grf_write_char(char*filename, uint32 rows, uint32 columns, unsigned char*pixels)
{
	int bits8 = 8;
	TIFF *tfile;
	uint32 row, column;

	tdata_t buf;

	tfile = TIFFOpen(filename, "w");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);

	TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
	TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
	TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
	TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
/*
	TIFFSetField(outfile, TIFFTAG_ARTIST, pChar);
	TIFFSetField(outfile, TIFFTAG_DATETIME, pChar);
	TIFFSetField(outfile, TIFFTAG_HOSTCOMPUTER, pChar);
	TIFFSetField(outfile, TIFFTAG_SOFTWARE, pChar);
	TIFFSetField(outfile, TIFFTAG_IMAGEDESCRIPTION, pChar);
*/

	TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, 1);
/*
	fprintf(stdout, "\n dssize %d\n",TIFFDefaultStripSize(tfile, columns));
*/
	TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
/*
	fprintf(stdout, "planar config %u\n",PLANARCONFIG_CONTIG);
*/
	TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
/*
	fprintf(stdout, "\n ssize %d\n",TIFFScanlineSize(tfile));
*/
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_write_char can't allocate");

	for(row = 0; row < rows; row++) {
		for(column = 0; column < columns; column++) {
/*			((uint16 *)buf)[column] = (uint16)(256 * pixels[row*columns + column]);*/
			((uint8 *)buf)[column] = (uint8)(pixels[row*columns + column]);
		}
		TIFFWriteScanline(tfile, buf, row, 0);
	}

	_TIFFfree(buf);
	TIFFClose(tfile);
}

void grf_write_char_rgb(char*filename, uint32 rows, uint32 columns, unsigned char**pixels, int SamplesPerPixel)
{
	int bits8 = 8;
	TIFF *tfile;
	uint32 row, column, c;
	tdata_t buf;
	int sample;

	if (SamplesPerPixel != 3)
		g_error("write_char_rgb can't hadle %d samples per pixel", SamplesPerPixel);
	tfile = TIFFOpen(filename, "w");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);

	TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
	TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
	TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
	TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
/*
	TIFFSetField(outfile, TIFFTAG_ARTIST, pChar);
	TIFFSetField(outfile, TIFFTAG_DATETIME, pChar);
	TIFFSetField(outfile, TIFFTAG_HOSTCOMPUTER, pChar);
	TIFFSetField(outfile, TIFFTAG_SOFTWARE, pChar);
	TIFFSetField(outfile, TIFFTAG_IMAGEDESCRIPTION, pChar);
*/
	TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, SamplesPerPixel);
/*
	fprintf(stdout, "\n dssize %d\n",TIFFDefaultStripSize(tfile, columns));
*/
	TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
/*
	fprintf(stdout, "planar config %u\n",PLANARCONFIG_CONTIG);
*/
	TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
/*
	fprintf(stdout, "\n ssize %d\n",TIFFScanlineSize(tfile));
*/
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");

	for(row = 0; row < rows; row++) {
		for(column = 0, c = 0; column < columns * SamplesPerPixel; column += SamplesPerPixel, c++) {
			for(sample = 0; sample < SamplesPerPixel; sample++) {
/*			((uint16 *)buf)[column] = (uint16)(256 * pixels[row*columns + column]);*/
				((uint8 *)buf)[column + sample] = (uint8)(pixels[sample][row*columns + c ]);
			}
		}
		TIFFWriteScanline(tfile, buf, row, 0);
	}

	_TIFFfree(buf);
	TIFFClose(tfile);
}

void grf_read_dim_volume(char*filename, uint32 *rows, uint32 *columns, uint32 *planes)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_dim_volume cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
	(*planes) = 0;
	do {
		(*planes)++;
	} while (TIFFReadDirectory(tfile));
	*columns = width;
	*rows = height;
	TIFFClose(tfile);
}

void grf_write_char_volume(char*filename, unsigned char***voxels, uint32 rows, uint32 columns, uint32 planes)
{
	int bits8 = 8;
	TIFF *tfile;
	uint32 row, column, plane;
	tdata_t buf;
	tfile = TIFFOpen(filename, "w");
	if ( !tfile )
		g_error("grf_write_char_volume can't open %s",filename);
	for ( plane = 0; plane < planes - 1; plane++ ) {
		TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
		TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
		TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
		TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
		TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
		TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
		TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, 1);
		TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
		TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		buf = _TIFFmalloc(TIFFScanlineSize(tfile));
		if ( !buf )
			g_error("grf_write_char_volume can't allocate");
		for(row = 0; row < rows; row++) {
			for(column = 0; column < columns; column++) {
				((uint8 *)buf)[column] = (uint8)(voxels[column][row][plane]);
			}
			TIFFWriteScanline(tfile, buf, row, 0);
		}
		TIFFWriteDirectory(tfile);
		TIFFFlush(tfile);
		_TIFFfree(buf);
	}
	plane = planes - 1;
	TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
	TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
	TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
	TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
	TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, 1);
	TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
	TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_write_char_volume can't allocate");
	for(row = 0; row < rows; row++) {
		for(column = 0; column < columns; column++) {
			((uint8 *)buf)[column] = (uint8)(voxels[column][row][plane]);
		}
		TIFFWriteScanline(tfile, buf, row, 0);
	}
	TIFFFlush(tfile);
	_TIFFfree(buf);
	TIFFClose(tfile);
}

void grf_write_char_volume_rgb(char*filename, uint32 rows, uint32 columns, uint32 planes, unsigned char****voxels, int SamplesPerPixel)
{
	int bits8 = 8;
	TIFF *tfile;
	uint32 row, column, plane, c;
	tdata_t buf;
	int sample;
	if (SamplesPerPixel != 3)
		g_error("write_char_volume_rgb can't hadle %d samples per pixel", SamplesPerPixel);
	tfile = TIFFOpen(filename, "w");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	for ( plane = 0; plane < planes - 1; plane++ ) {
		TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
		TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
		TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
		TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
		TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
		TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
		TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, SamplesPerPixel);
		TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
		TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		buf = _TIFFmalloc(TIFFScanlineSize(tfile));
		if ( !buf )
			g_error("grf_write_char_volume_rgb can't allocate");
		for(row = 0; row < rows; row++) {
			for(column = 0, c = 0; column < columns * SamplesPerPixel; column += SamplesPerPixel, c++) {
				for(sample = 0; sample < SamplesPerPixel; sample++) {
					((uint8 *)buf)[column + sample] = (uint8)(voxels[sample][c][row][plane]);
				}
			}
			TIFFWriteScanline(tfile, buf, row, 0);
		}
		TIFFWriteDirectory(tfile);
		TIFFFlush(tfile);
		_TIFFfree(buf);
	}
	plane = planes - 1;
	TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
	TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
	TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
	TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
	TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, SamplesPerPixel);
	TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
	TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_write_char_volume can't allocate");
	for(row = 0; row < rows; row++) {
		for(column = 0, c = 0; column < columns * SamplesPerPixel; column += SamplesPerPixel, c++) {
			for(sample = 0; sample < SamplesPerPixel; sample++) {
				((uint8 *)buf)[column + sample] = (uint8)(voxels[sample][c][row][plane]);
			}
		}
		TIFFWriteScanline(tfile, buf, row, 0);
	}
	TIFFFlush(tfile);
	_TIFFfree(buf);
	TIFFClose(tfile);
}


void grf_read_char_rgb(char*filename, uint32 *rows, uint32 *columns, unsigned char**pixels)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != PHOTOMETRIC_RGB ) {
		g_error("grf_read_char cannot process the file %s - %d means not RGB", filename, (int)photom);
	}
	palette_length = 0;
	if ( (int)photom == 3 ) {
		if ( (int)bps == 16 ) {
			palette_length = 65536 * 3;
		} else if ( (int)bps == 8 ) {
			palette_length = 256 * 3;
		}
		TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
	}
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}

	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}

	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}

	*columns = (uint32) width;
	*rows = (uint32) height;

	(*pixels) = (unsigned char*)calloc( (*columns) * (*rows) * 3 , sizeof(unsigned char) );
	if ( !(*pixels) )
		g_error("grf_read_char can't allocate");

	fprintf(stdout, "\n ssize %d\n",TIFFScanlineSize(tfile));
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");
	if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
		Scale = 4096.0;
	} else {
		Scale = 65536.0;
	}
	if ( (int)bps == 16 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint16*)buf)[3 * column];
					(*pixels)[row*3*width+3 * column] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
					indexed_color = (int)((uint16*)buf)[3 * column + 1];
					(*pixels)[row*3*width+3 * column + 1] = (unsigned char)( 255 * (double)(((uint16*)green)[indexed_color]) / Scale + 0.5 );
					indexed_color = (int)((uint16*)buf)[3 * column + 2];
					(*pixels)[row*3*width+3 * column + 2] = (unsigned char)( 255 * (double)(((uint16*)blue)[indexed_color]) / Scale + 0.5 );
				} else {
					(*pixels)[row*3*width+3 * column] = (unsigned char)( 255 * (double)((uint16*)buf)[3 * column] / Scale + 0.5 );
					(*pixels)[row*3*width+3 * column + 1] = (unsigned char)( 255 * (double)((uint16*)buf)[3 * column + 1] / Scale + 0.5 );
					(*pixels)[row*3*width+3 * column + 2] = (unsigned char)( 255 * (double)((uint16*)buf)[3 * column + 2] / Scale + 0.5 );
				}
			}
		}
	} else if ( (int)bps == 8 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint8*)buf)[3 * column];
					(*pixels)[row*3*width+3 * column] = (unsigned char)( 255 * (double)(((uint16*)red)[indexed_color]) / Scale + 0.5 );
					indexed_color = (int)((uint8*)buf)[3 * column + 1];
					(*pixels)[row*3*width+3 * column + 1] = (unsigned char)( 255 * (double)(((uint16*)green)[indexed_color]) / Scale + 0.5 );
					indexed_color = (int)((uint8*)buf)[3 * column + 2];
					(*pixels)[row*3*width+3 * column + 2] = (unsigned char)( 255 * (double)(((uint16*)blue)[indexed_color]) / Scale + 0.5 );
				} else {
					(*pixels)[row*3 * width+3 * column] = (unsigned char)(((uint8*)buf)[3 * column] );
					(*pixels)[row*3 * width+3 * column + 1] = (unsigned char)(((uint8*)buf)[3 * column + 1] );
					(*pixels)[row*3 * width+3 * column + 2] = (unsigned char)(((uint8*)buf)[3 * column + 2] );
				}
			}
		}
	}
	_TIFFfree(buf);
	TIFFClose(tfile);
}


void grf_write_super(char*filename, uint32 rows, uint32 columns, double*pixels, int FilledPixel)
{
	int bits8;
	TIFF *tfile;
	uint32 row, column;
	tdata_t buf;

	switch (FilledPixel) {
		case 255:
			bits8 = 8;
		break;
		case 4095:
			bits8 = 16;
		break;
		case 65535:
			bits8 = 16;
		break;
		default:
			g_error("grf_write_super: can't handle %d filled pixel", FilledPixel);
	}

	tfile = TIFFOpen(filename, "w");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);

	TIFFSetField(tfile, TIFFTAG_IMAGEWIDTH, (uint32)columns);
	TIFFSetField(tfile, TIFFTAG_IMAGELENGTH, (uint32)rows);
	TIFFSetField(tfile, TIFFTAG_BITSPERSAMPLE, bits8);
	TIFFSetField(tfile, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tfile, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	TIFFSetField(tfile, TIFFTAG_SOFTWARE, "PARUS");
/*
	TIFFSetField(outfile, TIFFTAG_ARTIST, pChar);
	TIFFSetField(outfile, TIFFTAG_DATETIME, pChar);
	TIFFSetField(outfile, TIFFTAG_HOSTCOMPUTER, pChar);
	TIFFSetField(outfile, TIFFTAG_SOFTWARE, pChar);
	TIFFSetField(outfile, TIFFTAG_IMAGEDESCRIPTION, pChar);
*/

	TIFFSetField(tfile, TIFFTAG_SAMPLESPERPIXEL, 1);
/*
	fprintf(stdout, "\n dssize %d\n",TIFFDefaultStripSize(tfile, columns));
*/
	TIFFSetField(tfile, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tfile, columns));
/*
	fprintf(stdout, "planar config %u\n",PLANARCONFIG_CONTIG);
*/
	TIFFSetField(tfile, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
/*
	fprintf(stdout, "\n ssize %d\n",TIFFScanlineSize(tfile));
*/
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");

	switch (FilledPixel) {
		case 255:
			for(row = 0; row < rows; row++) {
				for(column = 0; column < columns; column++) {
					((uint8 *)buf)[column] = (uint8)(pixels[row*columns + column] + 0.5);
				}
				TIFFWriteScanline(tfile, buf, row, 0);
			}
		break;
		case 4095:
			for(row = 0; row < rows; row++) {
				for(column = 0; column < columns; column++) {
					((uint16 *)buf)[column] = (uint16)(pixels[row*columns + column] + 0.5);
				}
				TIFFWriteScanline(tfile, buf, row, 0);
			}
		break;
		case 65535:
			for(row = 0; row < rows; row++) {
				for(column = 0; column < columns; column++) {
					((uint16 *)buf)[column] = (uint16)(pixels[row*columns + column] + 0.5);
				}
				TIFFWriteScanline(tfile, buf, row, 0);
			}
		break;
		default:
			g_error("grf_write_super: can't handle %d filled pixel", FilledPixel);
	}

	_TIFFfree(buf);
	TIFFClose(tfile);
}

int grf_read_super(char*filename, uint32 *rows, uint32 *columns, double**pixels)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	int ret_val;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != 0 && (int)photom != 1 && (int)photom != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", filename, (int)photom);
	}
	palette_length = 0;
	if ( (int)photom == 3 ) {
		if ( (int)bps == 16 ) {
			palette_length = 65536 * 3;
		} else if ( (int)bps == 8 ) {
			palette_length = 256 * 3;
		}
		TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
	}
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}
	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}
	*columns = (uint32) width;
	*rows = (uint32) height;
	(*pixels) = (double*)calloc( (*columns) * (*rows) , sizeof(double) );
	if ( !(*pixels) )
		g_error("grf_read_char can't allocate");
/*	fprintf(stdout, "\n ssize %d\n",TIFFScanlineSize(tfile));*/
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");
	if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
		Scale = 4096.0;
		ret_val = 4095;
	} else {
		Scale = 65536.0;
		ret_val = 65535;
	}
	if ( (int)bps == 8 && (int)photom != 3 ) {
		Scale = 256.0;
		ret_val = 255;
	}
	if ( (int)bps == 16 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint16*)buf)[column];
					(*pixels)[row * width + column] = (double)( ((uint16*)red)[indexed_color] );
				} else {
					(*pixels)[row*width+column] = (double)( ((uint16*)buf)[column] );
				}
			}
		}
	} else if ( (int)bps == 8 ) {
		for(row = 0; row < height; row++) {
			TIFFReadScanline(tfile, buf, row, 0);
			for(column = 0; column < width; column++) {
				if ( (int)photom == 3 ) {
					int indexed_color = (int)((uint8*)buf)[column];
					(*pixels)[row * width + column] = (double)( ((uint16*)red)[indexed_color] );
				} else {
					(*pixels)[row*width+column] = (double)( ((uint8*)buf)[column] );
				}
			}
		}
	}
	_TIFFfree(buf);
	TIFFClose(tfile);
	return ret_val;
}

int grf_read_dim_super(char*filename, uint32 *rows, uint32 *columns)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	int ret_val;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
/*
	fprintf(stdout, "bits per sample %u\n",bps);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
/*
	fprintf(stdout, "width %u\n",width);
*/
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
/*
	fprintf(stdout, "height %u\n",height);
*/
	if ( TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16) ) {
/*		fprintf(stdout, "compression %u\n",value16);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar))  {
/*		fprintf(stdout, "artist %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar)) {
/*		fprintf(stdout, "date time %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar)) {
		fprintf(stdout, "host %s\n",pChar);
	}
	if(TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar))  {
/*		fprintf(stdout, "soft %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar))  {
/*		fprintf(stdout, "description %s\n",pChar);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32))  {
/*		fprintf(stdout, "rows per strip %u\n",value32);*/
	}
	if(TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16))  {
/*		fprintf(stdout, "planar config %d\n",value16);*/
	}
	*columns = width;
	*rows = height;
	TIFFClose(tfile);
	if ( (int)bps == 16 ) {
		if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
			ret_val = 4095;
		} else {
			ret_val = 65535;
		}
	} else {
		ret_val = 255;
	}
	return ret_val;
}

void grf_read_char_volume_lsm(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes, int channel)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	int i, j, k;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != 0 && (int)photom != 1 && (int)photom != 3 ) {
		g_error("grf_read_char_volume_lsm cannot process the file %s - %d means not white zero, black zero or indexed", filename, (int)photom);
	}
	if( (int)photom == 3 ) {
		g_error("grf_read_char_volume_lsm cannot process the file %s - %d means indexed - Not implemented", filename, (int)photom);
	}
	palette_length = 0;
	if ( (int)photom == 3 ) {
		if ( (int)bps == 16 ) {
			palette_length = 65536 * 3;
		} else if ( (int)bps == 8 ) {
			palette_length = 256 * 3;
		}
		TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
	}
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16);
	*columns = width;
	*rows = height;
	(*voxels) = (unsigned char***)calloc( (*columns), sizeof(unsigned char**));
	if ( !(*voxels) )
		g_error("grf_read_char_volume can't allocate");
	for ( i = 0; i < (*columns); i++) {
		(*voxels)[i] = (unsigned char**)calloc((*rows), sizeof(unsigned char*));
		if ( !((*voxels)[i]) )
			g_error("grf_read_char_volume can't allocate");
		for ( j = 0; j < (*rows); j++) {
			(*voxels)[i][j] = (unsigned char*)NULL;
		}
	}
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");
	k = 0;
	do {
		TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width ) {
			g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
		} else {
			for ( i = 0; i < (*columns); i++) {
				for ( j = 0; j < (*rows); j++) {
					(*voxels)[i][j] = (unsigned char*)realloc((*voxels)[i][j], ( k + 1 ) * sizeof(unsigned char) );
					if ( !((*voxels)[i][j]) )
						g_error("grf_read_char_volume can't allocate");
				}
			}
			if ( (int)bps == 16 ) {
				if ( (envptr = getenv("GRF_ASSUME_12_BITS")) )
					Scale = 4096.0;
				else
					Scale = 65536.0;
				for(row = 0; row < height; row++) {
					TIFFReadScanline(tfile, buf, row, channel);
					for(column = 0; column < width; column++) {
						(*voxels)[column][row][k] = (unsigned char)( 255 * (double)((uint16*)buf)[column] / Scale + 0.5 );
					}
				}
			} else if ( (int)bps == 8 ) {
				for(row = 0; row < height; row++) {
					TIFFReadScanline(tfile, buf, row, channel);
					for(column = 0; column < width; column++) {
						(*voxels)[column][row][k] = (unsigned char)(((uint8*)buf)[column] );
					}
				}
			}
			k++;
		}
	} while ( TIFFReadDirectory( tfile ) );
	_TIFFfree(buf);
	*planes = k;
	TIFFClose(tfile);
}

void grf_read_max_char_volume_lsm(char*filename, unsigned char****voxels, uint32 *rows, uint32 *columns, uint32 *planes, int nchannels, int *channels)
{
	TIFF *tfile;
	uint32 width, height;
	uint16 value16, bps, photom;
	uint16* red;
	uint16* green;
	uint16* blue;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t buf;
	uint32 row, column;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	double Scale;
	int i, j, k, c, ch;
	unsigned char dot;
	tfile = TIFFOpen(filename, "r");
	if ( !tfile )
		g_error("grf_read_char can't open %s",filename);
	TIFFGetField(tfile, TIFFTAG_BITSPERSAMPLE, &bps);
	if( (int)bps != 16 && (int)bps != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", filename, (int)bps);
	}
	TIFFGetField(tfile, TIFFTAG_PHOTOMETRIC, &photom);
	if( (int)photom != 0 && (int)photom != 1 && (int)photom != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", filename, (int)photom);
	}
	if( (int)photom == 3 ) {
		g_error("grf_read_char_volume_lsm cannot process the file %s - %d means indexed - Not implemented", filename, (int)photom);
	}
	palette_length = 0;
	if ( (int)photom == 3 ) {
		if ( (int)bps == 16 ) {
			palette_length = 65536 * 3;
		} else if ( (int)bps == 8 ) {
			palette_length = 256 * 3;
		}
		TIFFGetField(tfile, TIFFTAG_COLORMAP, &red, &green, &blue);
	}
	TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(tfile, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(tfile, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(tfile, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(tfile, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(tfile, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(tfile, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(tfile, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(tfile, TIFFTAG_PLANARCONFIG, &value16);
	*columns = width;
	*rows = height;
	(*voxels) = (unsigned char***)calloc( (*columns), sizeof(unsigned char**));
	if ( !(*voxels) )
		g_error("grf_read_char_volume can't allocate");
	for ( i = 0; i < (*columns); i++) {
		(*voxels)[i] = (unsigned char**)calloc((*rows), sizeof(unsigned char*));
		if ( !((*voxels)[i]) )
			g_error("grf_read_char_volume can't allocate");
		for ( j = 0; j < (*rows); j++) {
			(*voxels)[i][j] = (unsigned char*)NULL;
		}
	}
	buf = _TIFFmalloc(TIFFScanlineSize(tfile));
	if ( !buf )
		g_error("grf_read_char can't allocate");
	k = 0;
	do {
		TIFFGetField(tfile, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(tfile, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width ) {
			g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
		} else {
			for ( i = 0; i < (*columns); i++) {
				for ( j = 0; j < (*rows); j++) {
					(*voxels)[i][j] = (unsigned char*)realloc((*voxels)[i][j], ( k + 1 ) * sizeof(unsigned char) );
					if ( !((*voxels)[i][j]) )
						g_error("grf_read_char_volume can't allocate");
				}
			}
			if ( (int)bps == 16 ) {
				if ( (envptr = getenv("GRF_ASSUME_12_BITS")) )
					Scale = 4096.0;
				else
					Scale = 65536.0;
				for(row = 0; row < height; row++) {
					c = 0;
					ch = channels[c];
					TIFFReadScanline(tfile, buf, row, ch);
					for(column = 0; column < width; column++) {
						(*voxels)[column][row][k] = (unsigned char)( 255 * (double)((uint16*)buf)[column] / Scale + 0.5 );
					}
					for ( c = 1; c < nchannels; c++ ) {
						ch = channels[c];
						TIFFReadScanline(tfile, buf, row, ch);
						for(column = 0; column < width; column++) {
							dot = (unsigned char)( 255 * (double)((uint16*)buf)[column] / Scale + 0.5 );
							if ( dot > (*voxels)[column][row][k] )
								(*voxels)[column][row][k] = dot;
						}
					}
				}
			} else if ( (int)bps == 8 ) {
				for(row = 0; row < height; row++) {
					c = 0;
					ch = channels[c];
					TIFFReadScanline(tfile, buf, row, ch);
					for(column = 0; column < width; column++) {
						(*voxels)[column][row][k] = (unsigned char)(((uint8*)buf)[column] );
					}
					for ( c = 1; c < nchannels; c++ ) {
						ch = channels[c];
						TIFFReadScanline(tfile, buf, row, ch);
						for(column = 0; column < width; column++) {
							dot = (unsigned char)(((uint8*)buf)[column] );
							if ( dot > (*voxels)[column][row][k] )
								(*voxels)[column][row][k] = dot;
						}
					}
				}
			}
			k++;
		}
	} while ( TIFFReadDirectory( tfile ) );
	_TIFFfree(buf);
	*planes = k;
	TIFFClose(tfile);
}

int grf_compose_two(char*input_pointer_1, char*input_pointer_2, char*output_pointer_1, GrfCallbackFunc grf_callback, void*user_data)
{
	TIFF *in_tfile_1, *in_tfile_2, *out_tfile_1;
	uint32 width, height, columns, rows;
	uint16 value16, bps_1, bps_2, photom_1, photom_2;
	uint16* red_1;
	uint16* green_1;
	uint16* blue_1;
	uint16* red_2;
	uint16* green_2;
	uint16* blue_2;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t in_buf_1, in_buf_2, out_buf_1;
	uint32 row, column;
	int i, j, k, ret_val, done, done_1, done_2;
	double *in_pixels_1, *in_pixels_2, *out_pixels_1;
	in_tfile_1 = TIFFOpen(input_pointer_1, "r");
	if ( !in_tfile_1 )
		g_error("grf_read_char can't open %s", input_pointer_1);
	TIFFGetField(in_tfile_1, TIFFTAG_BITSPERSAMPLE, &bps_1);
	if( (int)bps_1 != 16 && (int)bps_1 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_1, (int)bps_1);
	}
	TIFFGetField(in_tfile_1, TIFFTAG_PHOTOMETRIC, &photom_1);
	if( (int)photom_1 != 0 && (int)photom_1 != 1 && (int)photom_1 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_1, (int)photom_1);
	}
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(in_tfile_1, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_1, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_1, TIFFTAG_PLANARCONFIG, &value16);
	in_tfile_2 = TIFFOpen(input_pointer_2, "r");
	if ( !in_tfile_2 )
		g_error("grf_read_char can't open %s", input_pointer_2);
	TIFFGetField(in_tfile_2, TIFFTAG_BITSPERSAMPLE, &bps_2);
	if( (int)bps_2 != 16 && (int)bps_2 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_2, (int)bps_2);
	}
	TIFFGetField(in_tfile_2, TIFFTAG_PHOTOMETRIC, &photom_2);
	if( (int)photom_2 != 0 && (int)photom_2 != 1 && (int)photom_2 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_2, (int)photom_2);
	}
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGEWIDTH, &columns);
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGELENGTH, &rows);
	TIFFGetField(in_tfile_2, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_2, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_2, TIFFTAG_PLANARCONFIG, &value16);
	if ( bps_1 != bps_2 || columns != width || rows != height ) {
		g_error("grf_compose_two images have different properties");
	}
	in_pixels_1 = (double*)calloc( columns , sizeof(double) );
	if ( !in_pixels_1 )
		g_error("grf_read_char can't allocate");
	in_buf_1 = _TIFFmalloc(TIFFScanlineSize(in_tfile_1));
	if ( !in_buf_1 )
		g_error("grf_read_char can't allocate");
	in_pixels_2 = (double*)calloc( columns , sizeof(double) );
	if ( !in_pixels_2 )
		g_error("grf_read_char can't allocate");
	in_buf_2 = _TIFFmalloc(TIFFScanlineSize(in_tfile_2));
	if ( !in_buf_2 )
		g_error("grf_read_char can't allocate");
	out_tfile_1 = TIFFOpen(output_pointer_1, "w");
	if ( !out_tfile_1 )
		g_error("grf_read_char can't open %s", output_pointer_1);
	out_pixels_1 = (double*)calloc( columns , sizeof(double) );
	if ( !out_pixels_1 )
		g_error("grf_read_char can't allocate");
	k = 0;
	done = 1;
	do {
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width ) {
			g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
		} else {
			TIFFGetField(in_tfile_2, TIFFTAG_IMAGELENGTH, &row);
			TIFFGetField(in_tfile_2, TIFFTAG_IMAGEWIDTH, &column);
			if ( row != height || column != width ) {
				g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
			} else {
				palette_length = 0;
				if ( (int)photom_1 == 3 ) {
					if ( (int)bps_1 == 16 ) {
						palette_length = 65536 * 3;
					} else if ( (int)bps_1 == 8 ) {
						palette_length = 256 * 3;
					}
					TIFFGetField(in_tfile_1, TIFFTAG_COLORMAP, &red_1, &green_1, &blue_1);
				}
				if ( (int)photom_2 == 3 ) {
					if ( (int)bps_2 == 16 ) {
						palette_length = 65536 * 3;
					} else if ( (int)bps_2 == 8 ) {
						palette_length = 256 * 3;
					}
					TIFFGetField(in_tfile_2, TIFFTAG_COLORMAP, &red_2, &green_2, &blue_2);
				}
				TIFFSetField(out_tfile_1, TIFFTAG_IMAGEWIDTH, (uint32)columns);
				TIFFSetField(out_tfile_1, TIFFTAG_IMAGELENGTH, (uint32)rows);
				TIFFSetField(out_tfile_1, TIFFTAG_BITSPERSAMPLE, bps_1);
				TIFFSetField(out_tfile_1, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
				TIFFSetField(out_tfile_1, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
				TIFFSetField(out_tfile_1, TIFFTAG_SOFTWARE, "PARUS");
				TIFFSetField(out_tfile_1, TIFFTAG_SAMPLESPERPIXEL, 1);
				TIFFSetField(out_tfile_1, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out_tfile_1, columns));
				TIFFSetField(out_tfile_1, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				out_buf_1 = _TIFFmalloc(TIFFScanlineSize(out_tfile_1));
				if ( !out_buf_1 )
					g_error("grf_read_char can't allocate");
				if ( (int)bps_1 == 16 ) {
					for(row = 0; row < height; row++) {
						TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint16*)in_buf_1)[column];
								in_pixels_1[column] = (double)( ((uint16*)red_1)[indexed_color] );
							} else {
								in_pixels_1[column] = (double)((uint16*)in_buf_1)[column];
							}
						}
						TIFFReadScanline(in_tfile_2, in_buf_2, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint16*)in_buf_2)[column];
								in_pixels_2[column] = (double)( ((uint16*)red_2)[indexed_color] );
							} else {
								in_pixels_2[column] = (double)((uint16*)in_buf_2)[column];
							}
						}
						ret_val = grf_callback(k, row, rows, columns, bps_1, in_pixels_1, in_pixels_2, out_pixels_1, user_data);
						for( column = 0; column < columns; column++) {
							((uint16 *)out_buf_1)[column] = (uint16)(out_pixels_1[column] + 0.5);
						}
						TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
					}
				} else if ( (int)bps_1 == 8 ) {
					for(row = 0; row < height; row++) {
						TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint8*)in_buf_1)[column];
								in_pixels_1[column] = (double)( ((uint16*)red_1)[indexed_color] );
							} else {
								in_pixels_1[column] = (double)((uint8*)in_buf_1)[column];
							}
						}
						TIFFReadScanline(in_tfile_2, in_buf_2, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint8*)in_buf_2)[column];
								in_pixels_2[column] = (double)( ((uint16*)red_2)[indexed_color] );
							} else {
								in_pixels_2[column] = (double)((uint8*)in_buf_2)[column];
							}
						}
						ret_val = grf_callback(k, row, rows, columns, bps_1, in_pixels_1, in_pixels_2, out_pixels_1, user_data);
						for( column = 0; column < columns; column++) {
							((uint8 *)out_buf_1)[column] = (uint8)(out_pixels_1[column] + 0.5);
						}
						TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
					}
				}
				k++;
				done_1 = TIFFReadDirectory( in_tfile_1 );
				done_2 = TIFFReadDirectory( in_tfile_2 );
				if (done_1 == 1 && done_2 == 1) {
					TIFFWriteDirectory(out_tfile_1);
					TIFFFlush(out_tfile_1);
					done = 0;
				} else if (done_1 == 0 && done_2 == 0) {
					TIFFFlush(out_tfile_1);
					done = 1;
				} else if (done_1 == 1 && done_2 == 0) {
					TIFFSetDirectory( in_tfile_2, 0 );
					TIFFWriteDirectory(out_tfile_1);
					TIFFFlush(out_tfile_1);
					done = 0;
				} else if (done_1 == 0 && done_2 == 1) {
					TIFFFlush(out_tfile_1);
					done = 1;
				}
				_TIFFfree(out_buf_1);
			}
		}
	} while ( done == 0 );
	_TIFFfree(in_buf_1);
	TIFFClose(in_tfile_1);
	_TIFFfree(in_buf_2);
	TIFFClose(in_tfile_2);
	TIFFClose(out_tfile_1);
	return k;
}

int grf_compose_rgb(char*input_pointer_1, char*input_pointer_2, char*input_pointer_3, char*output_pointer_1, GrfCallbackFunc3 grf_callback, void*user_data)
{
	TIFF *in_tfile_1, *in_tfile_2, *in_tfile_3, *out_tfile_1;
	uint32 width, height, columns, rows;
	uint16 value16, bps_1, bps_2, bps_3, photom_1, photom_2, photom_3;
	uint16 *red_1, *red_2, *red_3;
	uint16 *green_1, *green_2, *green_3;
	uint16 *blue_1, *blue_2, *blue_3;
	int palette_length;
	uint32 value32;
	char *pChar;
	tdata_t in_buf_1, in_buf_2, in_buf_3, out_buf_1;
	uint32 row, column;
	int i, j, k, ret_val, done;
	double *in_pixels_1, *in_pixels_2, *in_pixels_3, *out_pixels_1;
	in_tfile_1 = TIFFOpen(input_pointer_1, "r");
	if ( !in_tfile_1 )
		g_error("grf_read_char can't open %s", input_pointer_1);
	TIFFGetField(in_tfile_1, TIFFTAG_BITSPERSAMPLE, &bps_1);
	if( (int)bps_1 != 16 && (int)bps_1 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_1, (int)bps_1);
	}
	TIFFGetField(in_tfile_1, TIFFTAG_PHOTOMETRIC, &photom_1);
	if( (int)photom_1 != 0 && (int)photom_1 != 1 && (int)photom_1 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_1, (int)photom_1);
	}
	palette_length = 0;
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(in_tfile_1, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_1, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_1, TIFFTAG_PLANARCONFIG, &value16);
	in_tfile_2 = TIFFOpen(input_pointer_2, "r");
	if ( !in_tfile_2 )
		g_error("grf_read_char can't open %s", input_pointer_2);
	TIFFGetField(in_tfile_2, TIFFTAG_BITSPERSAMPLE, &bps_2);
	if( (int)bps_2 != 16 && (int)bps_2 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_2, (int)bps_2);
	}
	TIFFGetField(in_tfile_2, TIFFTAG_PHOTOMETRIC, &photom_2);
	if( (int)photom_2 != 0 && (int)photom_2 != 1 && (int)photom_2 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_2, (int)photom_2);
	}
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGEWIDTH, &columns);
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGELENGTH, &rows);
	TIFFGetField(in_tfile_2, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_2, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_2, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_2, TIFFTAG_PLANARCONFIG, &value16);
	if ( bps_1 != bps_2 || columns != width || rows != height ) {
		g_error("grf_compose_two images have different properties");
	}
	in_tfile_3 = TIFFOpen(input_pointer_3, "r");
	if ( !in_tfile_3 )
		g_error("grf_read_char can't open %s", input_pointer_3);
	TIFFGetField(in_tfile_3, TIFFTAG_BITSPERSAMPLE, &bps_3);
	if( (int)bps_3 != 16 && (int)bps_3 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_3, (int)bps_3);
	}
	TIFFGetField(in_tfile_3, TIFFTAG_PHOTOMETRIC, &photom_3);
	if( (int)photom_3 != 0 && (int)photom_3 != 1 && (int)photom_3 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_3, (int)photom_3);
	}
	TIFFGetField(in_tfile_3, TIFFTAG_IMAGEWIDTH, &columns);
	TIFFGetField(in_tfile_3, TIFFTAG_IMAGELENGTH, &rows);
	TIFFGetField(in_tfile_3, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_3, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_3, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_3, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_3, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_3, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_3, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_3, TIFFTAG_PLANARCONFIG, &value16);
	if ( bps_1 != bps_3 || columns != width || rows != height ) {
		g_error("grf_compose_two images have different properties");
	}
	in_pixels_1 = (double*)calloc( columns , sizeof(double) );
	if ( !in_pixels_1 )
		g_error("grf_read_char can't allocate");
	in_buf_1 = _TIFFmalloc(TIFFScanlineSize(in_tfile_1));
	if ( !in_buf_1 )
		g_error("grf_read_char can't allocate");
	in_pixels_2 = (double*)calloc( columns , sizeof(double) );
	if ( !in_pixels_2 )
		g_error("grf_read_char can't allocate");
	in_buf_2 = _TIFFmalloc(TIFFScanlineSize(in_tfile_2));
	if ( !in_buf_2 )
		g_error("grf_read_char can't allocate");
	in_pixels_3 = (double*)calloc( columns , sizeof(double) );
	if ( !in_pixels_3 )
		g_error("grf_read_char can't allocate");
	in_buf_3 = _TIFFmalloc(TIFFScanlineSize(in_tfile_3));
	if ( !in_buf_3 )
		g_error("grf_read_char can't allocate");
	out_tfile_1 = TIFFOpen(output_pointer_1, "w");
	if ( !out_tfile_1 )
		g_error("grf_read_char can't open %s", output_pointer_1);
	out_pixels_1 = (double*)calloc( 3 * columns , sizeof(double) );
	if ( !out_pixels_1 )
		g_error("grf_read_char can't allocate");
	k = 0;
	done = 1;
	do {
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width ) {
			g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
		} else {
			TIFFGetField(in_tfile_2, TIFFTAG_IMAGELENGTH, &row);
			TIFFGetField(in_tfile_2, TIFFTAG_IMAGEWIDTH, &column);
			if ( row != height || column != width ) {
				g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
			} else {
				if ( (int)photom_1 == 3 ) {
					if ( (int)bps_1 == 16 ) {
						palette_length = 65536 * 3;
					} else if ( (int)bps_1 == 8 ) {
						palette_length = 256 * 3;
					}
					TIFFGetField(in_tfile_1, TIFFTAG_COLORMAP, &red_1, &green_1, &blue_1);
				}
				if ( (int)photom_2 == 3 ) {
					TIFFGetField(in_tfile_2, TIFFTAG_COLORMAP, &red_2, &green_2, &blue_2);
				}
				if ( (int)photom_3 == 3 ) {
					TIFFGetField(in_tfile_3, TIFFTAG_COLORMAP, &red_3, &green_3, &blue_3);
				}
				TIFFSetField(out_tfile_1, TIFFTAG_IMAGEWIDTH, (uint32)columns);
				TIFFSetField(out_tfile_1, TIFFTAG_IMAGELENGTH, (uint32)rows);
				TIFFSetField(out_tfile_1, TIFFTAG_BITSPERSAMPLE, bps_1);
				TIFFSetField(out_tfile_1, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
				TIFFSetField(out_tfile_1, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
				TIFFSetField(out_tfile_1, TIFFTAG_SOFTWARE, "PARUS");
				TIFFSetField(out_tfile_1, TIFFTAG_SAMPLESPERPIXEL, 3);
				TIFFSetField(out_tfile_1, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out_tfile_1, columns));
				TIFFSetField(out_tfile_1, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				out_buf_1 = _TIFFmalloc(TIFFScanlineSize(out_tfile_1));
				if ( !out_buf_1 )
					g_error("grf_read_char can't allocate");
				if ( (int)bps_1 == 16 ) {
					for(row = 0; row < height; row++) {
						TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint16*)in_buf_1)[column];
								in_pixels_1[column] = (double)( ((uint16*)red_1)[indexed_color] );
							} else {
								in_pixels_1[column] = (double)((uint16*)in_buf_1)[column];
							}
						}
						TIFFReadScanline(in_tfile_2, in_buf_2, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_2 == 3 ) {
								int indexed_color = (int)((uint16*)in_buf_2)[column];
								in_pixels_2[column] = (double)( ((uint16*)red_2)[indexed_color] );
							} else {
								in_pixels_2[column] = (double)((uint16*)in_buf_2)[column];
							}
						}
						TIFFReadScanline(in_tfile_3, in_buf_3, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_3 == 3 ) {
								int indexed_color = (int)((uint16*)in_buf_3)[column];
								in_pixels_3[column] = (double)( ((uint16*)red_3)[indexed_color] );
							} else {
								in_pixels_3[column] = (double)((uint16*)in_buf_3)[column];
							}
						}
						ret_val = grf_callback(k, row, rows, columns, bps_1, in_pixels_1, in_pixels_2, in_pixels_3, out_pixels_1, user_data);
						for( column = 0; column < 3 * columns; column++) {
							((uint16 *)out_buf_1)[column] = (uint16)(out_pixels_1[column] + 0.5);
						}
						TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
					}
				} else if ( (int)bps_1 == 8 ) {
					for(row = 0; row < height; row++) {
						TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_1 == 3 ) {
								int indexed_color = (int)((uint8*)in_buf_1)[column];
								in_pixels_1[column] = 255.0 * (double)( ((uint16*)red_1)[indexed_color] ) / 65536.0;
							} else {
								in_pixels_1[column] = (double)((uint8*)in_buf_1)[column];
							}
						}
						TIFFReadScanline(in_tfile_2, in_buf_2, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_2 == 3 ) {
								int indexed_color = (int)((uint8*)in_buf_2)[column];
								in_pixels_2[column] = 255.0 * (double)( ((uint16*)red_2)[indexed_color] ) / 65536.0;
							} else {
								in_pixels_2[column] = (double)((uint8*)in_buf_2)[column];
							}
						}
						TIFFReadScanline(in_tfile_3, in_buf_3, row, 0);
						for( column = 0; column < width; column++) {
							if ( (int)photom_3 == 3 ) {
								int indexed_color = (int)((uint8*)in_buf_3)[column];
								in_pixels_1[column] = 255.0 * (double)( ((uint16*)red_3)[indexed_color] ) / 65536.0;
							} else {
								in_pixels_3[column] = (double)((uint8*)in_buf_3)[column];
							}
						}
						ret_val = grf_callback(k, row, rows, columns, bps_1, in_pixels_1, in_pixels_2, in_pixels_3, out_pixels_1, user_data);
						for( column = 0; column < 3 * columns; column++) {
							((uint8 *)out_buf_1)[column] = (uint8)(out_pixels_1[column] + 0.5);
						}
						TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
					}
				}
				k++;
				if ( TIFFReadDirectory( in_tfile_1 ) && TIFFReadDirectory( in_tfile_2 ) && TIFFReadDirectory( in_tfile_3 ) ) {
					TIFFWriteDirectory(out_tfile_1);
					TIFFFlush(out_tfile_1);
					done = 0;
				} else {
					TIFFFlush(out_tfile_1);
					done = 1;
				}
				_TIFFfree(out_buf_1);
			}
		}
	} while ( done == 0 );
	_TIFFfree(in_buf_1);
	TIFFClose(in_tfile_1);
	_TIFFfree(in_buf_2);
	TIFFClose(in_tfile_2);
	_TIFFfree(in_buf_3);
	TIFFClose(in_tfile_3);
	TIFFClose(out_tfile_1);
	return k;
}

int grf_compose_one(char*input_pointer_1, char*output_pointer_1, GrfCallbackFunc0 grf_callback, void*user_data)
{
	TIFF *in_tfile_1, *out_tfile_1;
	uint32 width, height;
	uint16 value16, bps_1, photom_1;
	uint16* red_1;
	uint16* green_1;
	uint16* blue_1;
	int palette_length;
	double Scale, max_intensity;
	uint32 value32;
	char *pChar;
	tdata_t in_buf_1, out_buf_1;
	uint32 row, column;
	int i, j, k, ret_val, done;
	double *in_pixels_1, *out_pixels_1;
	char*envptr; /* to find out if we deal with 12 bit ...*/
	in_tfile_1 = TIFFOpen(input_pointer_1, "r");
	if ( !in_tfile_1 )
		g_error("grf_read_char can't open %s", input_pointer_1);
	TIFFGetField(in_tfile_1, TIFFTAG_BITSPERSAMPLE, &bps_1);
	if( (int)bps_1 != 16 && (int)bps_1 != 8 ) {
		g_error("grf_read_char cannot process the file %s - bits per sample should be 16 or 8 not %d", input_pointer_1, (int)bps_1);
	}
	TIFFGetField(in_tfile_1, TIFFTAG_PHOTOMETRIC, &photom_1);
	if( (int)photom_1 != 0 && (int)photom_1 != 1 && (int)photom_1 != 3 ) {
		g_error("grf_read_char cannot process the file %s - %d means not white zero, black zero or indexed", input_pointer_1, (int)photom_1);
	}
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &height);
	TIFFGetField(in_tfile_1, TIFFTAG_COMPRESSION, &value16);
	TIFFGetField(in_tfile_1, TIFFTAG_ARTIST, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_DATETIME, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_HOSTCOMPUTER, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_SOFTWARE, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_IMAGEDESCRIPTION, &pChar);
	TIFFGetField(in_tfile_1, TIFFTAG_ROWSPERSTRIP, &value32);
	TIFFGetField(in_tfile_1, TIFFTAG_PLANARCONFIG, &value16);
	in_pixels_1 = (double*)calloc( width , sizeof(double) );
	if ( !in_pixels_1 )
		g_error("grf_read_char can't allocate");
	in_buf_1 = _TIFFmalloc(TIFFScanlineSize(in_tfile_1));
	if ( !in_buf_1 )
		g_error("grf_read_char can't allocate");
	out_tfile_1 = TIFFOpen(output_pointer_1, "w");
	if ( !out_tfile_1 )
		g_error("grf_read_char can't open %s", output_pointer_1);
	out_pixels_1 = (double*)calloc( width , sizeof(double) );
	if ( !out_pixels_1 )
		g_error("grf_read_char can't allocate");
	k = 0;
	done = 1;
	if ( (envptr = getenv("GRF_ASSUME_12_BITS")) ) {
		Scale = 4096.0;
	} else {
		Scale = 65536.0;
	}
	if ( (int)bps_1 == 8 ) {
		max_intensity = 255.0;
	} else {
		max_intensity = 65535.0;
	}
	do {
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGELENGTH, &row);
		TIFFGetField(in_tfile_1, TIFFTAG_IMAGEWIDTH, &column);
		if ( row != height || column != width ) {
			g_warning("grf_read_char_volume_lsm different dimensions for different planes! skipping");
		} else {
			palette_length = 0;
			if ( (int)photom_1 == 3 ) {
				if ( (int)bps_1 == 16 ) {
					palette_length = 65536 * 3;
				} else if ( (int)bps_1 == 8 ) {
					palette_length = 256 * 3;
				}
				TIFFGetField(in_tfile_1, TIFFTAG_COLORMAP, &red_1, &green_1, &blue_1);
			}
			TIFFSetField(out_tfile_1, TIFFTAG_IMAGEWIDTH, (uint32)width);
			TIFFSetField(out_tfile_1, TIFFTAG_IMAGELENGTH, (uint32)height);
			TIFFSetField(out_tfile_1, TIFFTAG_BITSPERSAMPLE, bps_1);
			TIFFSetField(out_tfile_1, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
			TIFFSetField(out_tfile_1, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
			TIFFSetField(out_tfile_1, TIFFTAG_SOFTWARE, "PARUS");
			TIFFSetField(out_tfile_1, TIFFTAG_SAMPLESPERPIXEL, 1);
			TIFFSetField(out_tfile_1, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out_tfile_1, width));
			TIFFSetField(out_tfile_1, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
			out_buf_1 = _TIFFmalloc(TIFFScanlineSize(out_tfile_1));
			if ( !out_buf_1 )
				g_error("grf_read_char can't allocate");
			if ( (int)bps_1 == 16 ) {
				for(row = 0; row < height; row++) {
					TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
					for( column = 0; column < width; column++) {
						if ( (int)photom_1 == 3 ) {
							int indexed_color = (int)((uint16*)in_buf_1)[column];
							in_pixels_1[column] = max_intensity * (double)( ((uint16*)red_1)[indexed_color] ) / Scale;
						} else {
							in_pixels_1[column] = max_intensity * (double)((uint16*)in_buf_1)[column] / Scale;
						}
					}
					ret_val = grf_callback(max_intensity, k, row, height, width, bps_1, in_pixels_1, out_pixels_1, user_data);
					for( column = 0; column < width; column++) {
						((uint16 *)out_buf_1)[column] = (uint16)(out_pixels_1[column] + 0.5);
					}
					TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
				}
			} else if ( (int)bps_1 == 8 ) {
				for(row = 0; row < height; row++) {
					TIFFReadScanline(in_tfile_1, in_buf_1, row, 0);
					for( column = 0; column < width; column++) {
						if ( (int)photom_1 == 3 ) {
							int indexed_color = (int)((uint8*)in_buf_1)[column];
							in_pixels_1[column] = max_intensity * (double)(((uint16*)red_1)[indexed_color]) / Scale;
						} else {
							in_pixels_1[column] = (double)((uint8*)in_buf_1)[column];
						}
					}
					ret_val = grf_callback(max_intensity, k, row, height, width, bps_1, in_pixels_1, out_pixels_1, user_data);
					for( column = 0; column < width; column++) {
						((uint8 *)out_buf_1)[column] = (uint8)(out_pixels_1[column] + 0.5);
					}
					TIFFWriteScanline(out_tfile_1, out_buf_1, row, 0);
				}
			}
			k++;
			if ( TIFFReadDirectory( in_tfile_1 ) ) {
				TIFFWriteDirectory(out_tfile_1);
				TIFFFlush(out_tfile_1);
				done = 0;
			} else {
				TIFFFlush(out_tfile_1);
				done = 1;
			}
			_TIFFfree(out_buf_1);
		}
	} while ( done == 0 );
	_TIFFfree(in_buf_1);
	TIFFClose(in_tfile_1);
	TIFFClose(out_tfile_1);
	return k;
}

