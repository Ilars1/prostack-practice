/***************************************************************************
                          CharPixel.h  -  description
                             -------------------
    begin                : óÒÄ ñÎ× 26 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <math.h>

#include <grf.h>
#include <ImageInfo.h>
#include <Pattern.h>

#ifndef CharIMAGE_H
#define CharIMAGE_H

#define FilledPixel 255
#define EmptyPixel 0

#define MY_PI_CONST 3.14159265358979323846264338327950

#ifndef MAX_RECORD
#define MAX_RECORD 255
#endif

#define CharImageFields \
  uint32 number;  \
  uint32 columns;   \
  uint32 rows;   \
  unsigned char*pixels;

typedef struct CharImage {
	CharImageFields
} CharImage;

typedef struct Centroid {
	double x;
	double y;
} Centroid;

typedef struct Blob {
	uint32*nuclear_pixels;
	uint32 Nnuclear_pixels;
	uint32*energid_pixels;
	uint32 Nenergid_pixels;
	int label;
	Centroid*cm;
} Blob;

typedef struct BlobList {
	Blob*blob;
	struct BlobList*next;
} BlobList;

typedef struct BlobInfo {
	int MaxSize;
	int MinSize;
	uint32 MaxBlobs;
} BlobInfo;

typedef enum BlobType {
	only_nuclei,
	nuclei_and_energids
} BlobType;

typedef struct BlobImage {
	uint32 NBlobs;
	uint32 columns;
	uint32 rows;
	BlobList*list;
	BlobType type;
} BlobImage;

typedef struct PList {
	struct PList*next;
	uint32 index;
} PList;

void CharImageSetVerbose(int arg);
Attributes*CharAnalyze(unsigned char*data, uint32 dim);
void CharImageSetThresholdMultiplyer(double arg);
CharImage*CharImageCreate(char*infile);
void CharImageWrite(  CharImage*di,char*infile);
CharImage*CharImageComposeMax(CharImage*di,CharImage*di_j);
CharImage*CharImageComposePlus(CharImage*di,CharImage*di_j);
void CharImageDelete(CharImage*di);
void CharImageThreshold(CharImage*dp,double threshold);
CharImage*CharImageClone(CharImage*p);
CharImage*CharImageMedian(CharImage*dp,int MedianFlag, int Hflag);
int com1(const void*a,const void*s);
CharImage*CharImageDilation(CharImage*dp);
CharImage*CharImageErosion(CharImage*dp);
ImageMoments*CharImageFindMoments(CharImage*dp);
CharImage*CharImageRotate(CharImage*di, double theta);
void CharImageColumnShear(CharImage*di, double shear,uint32 ColumnsBorderWidth, uint32 offset);
void CharImageRowShear(CharImage*di, double shear, uint32 coffset, uint32 roffset);
CharImage*CharImageBorder(CharImage*dp,uint32 ColumnsToAdd,uint32 RowsToAdd,uint32 ColumnsBorderWidth,uint32 RowsBorderWidth);
CharImage*CharImageDistance(CharImage*dp);
CharImage*CharImageHistogramEquilize(CharImage*dp, uint32 RowStart, uint32 ColumnStart, uint32 RowStop, uint32 ColumnStop);
CharImage*CharImageScaleIntensity(CharImage*di, double scale);
CharImage*CharImageAverage(char*ID, char*extension);
CharImage*CharImageComposePlusN(char*ID, char*extension);
CharImage*CharImageComposeMaxN(char*ID, char*extension);
CharImage*CharImageAdd(CharImage*di,CharImage*di_j);
CharImage*CharImageReverseColumn(CharImage*dp);
CharImage*CharImageReverseRow(CharImage*dp);
CharImage*CharImageTranspose(CharImage*dp);
ImageFeatureHalfAxis*CharImageFindFeatureHalfAxis(CharImage*di);
CharImage*CharImageEdge(CharImage*dp);
CharImage*CharImageCrop(CharImage*dp,uint32 ColumnsToDelete,uint32 RowsToDelete,uint32 ColumnsBorderWidth,uint32 RowsBorderWidth);
CharImage*CharImageCropFeature(CharImage*dp, uint32*UpperOffset, uint32*LowerOffset, uint32*LeftOffset, uint32*RightOffset);
CharImage*CharImageCropGeometry(CharImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);
CharImage*CharImageErase(CharImage*dp, double RowsPercent, int RowSign, double ColumnsPercent, int ColumnSign);
CharImage*CharImageFill(CharImage*dp);
CharImage*CharImageInvert(CharImage*di);
CharImage*CharImageInvertWithMask(CharImage*di,CharImage*mask);
CharImage*CharImageInvertN(CharImage*di);
CharImage*CharImageInvertNWithMask(CharImage*di,CharImage*mask);
double CharImageThresholdHist(CharImage*dp, double a, double b);
Hist*CharImageHistogram(CharImage*dp, uint32 RowStart, uint32 ColumnStart, uint32 RowStop, uint32 ColumnStop);
CharImage*CharImageEdgeShenCastan(CharImage*dp,double b,unsigned char low, unsigned char high);
CharImage*CharImageBLI(CharImage*dp,CharImage*isef);
CharImage*CharImageISEF(CharImage*dp, double b);
CharImage*Thinning(CharImage*d,uint32 NMAX);
void CharImageSISEF(CharImage*bli, CharImage*isef);
CharImage*CharImageLocalHistogramEquilization(CharImage*cim, uint32 crows,uint32 ccolumns);
CharImage*CharImageMask(CharImage*di,CharImage*maskimage);
CharImage*CharImageCopy(CharImage*p);
unsigned char CharImageMaxLevel(CharImage*p);
unsigned char CharImageMinLevel(CharImage*p);
int ConnectionRow(CharImage*cim,uint32 index,int sign);
int ConnectionColumn(CharImage*cim,uint32 index,int sign);
int Visible(CharImage*di,uint32 index);
CharImage*CharImageCrossMask(CharImage*di,CharImage*mask);
int ConnectionRowEdm(CharImage*cim,uint32 index,int sign,CharImage*edm,unsigned char level);
int ConnectionColumnEdm(CharImage*cim,uint32 index,int sign,CharImage*edm,unsigned char level);
int NoConnection8(CharImage*d,uint32 i);
int NoConnection4(CharImage*d,uint32 i);
CharImage*CharImageWaterShed4(CharImage*di,CharImage*edm);

CharImage*CharImageWaterShed8(CharImage*di,CharImage*edm);
double CharImageThresholdHist1(CharImage*dp);
int NoConnectionEDM(CharImage*d,uint32 index, CharImage*edm,unsigned char level);

CharImage*CharImageFastWaterShed(CharImage*di, int connectivity);
double Cubic(double x);
void CharImageScaleHorizontal(CharImage*ci, double factor, CharImage*resized);
void CharImageScaleVertical(CharImage*ci, double factor, CharImage*resized);
CharImage*CharImageScale(CharImage*ci, double rfactor, double cfactor);
CharImage*CharImageMul(CharImage*di,CharImage*di_j);
CharImage*CharImageFillFeature(CharImage*dp, int connectivity, int can_touch);

HalfImage*CharImageFindHalfImage(CharImage*dp);
unsigned char CharImageHistogramMedianLevel(Hist*h);
int com2(const void*a,const void*s);

BlobImage*blob(BlobInfo*bi, CharImage*wtsd, CharImage*data, int connectivity);
void BlobImagePrint(BlobImage*bi, FILE*fp);
BlobInfo*NewBlobInfo(int MaxSize, int MinSize, uint32 MaxBlobs);


typedef enum BlobDataType {
	mean,
	var,
	stdev,
	muc,
	max,
	min,
	median,
	area,
	varbc
} BlobDataType;

typedef enum ChannelType {
	nuclear,
	energid,
	outnuc
} ChannelType;

typedef enum StructElemType {
	square,
	disk
} StructElemType;

BlobImage*BlobImageRead(FILE*fp);
Pattern*BlobImage2Pattern(BlobImage*bi);
void AddChannel(ChannelType ct, Pattern*pattern, CharImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag);
void AddChannel1(ChannelType ct, Pattern*pattern, CharImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag);
CharImage*BlobImage2CharImage(BlobImage*bi);
CharImage*CharImageNew(uint32 columns, uint32 rows);

int ZeroCrossing(double *isef, int *bli, uint32 row, uint32 column, uint32 rows, uint32 columns);
void ComputeADGRAD(double *isef, int *bli, uint32 rows, uint32 columns, double *adgrad, uint32 window);
void ComputeBLI(double*isef1,  double*isef2, int*bli, uint32 columns, uint32 rows);
void ComputeISEF(double*isef, double b,  double*orig, uint32 columns, uint32 rows);
double AdGrad(double *isef, int *bli, uint32 row, uint32 column, uint32 rows, uint32 columns, uint32 window);
void HysteresisThreshold(CharImage*dpm, double *adgrad, double low, double high, uint32 segment, int connectivity);
int Length(PList*p);
void MarkPlist(PList*seg, CharImage*dpm, unsigned char intensity);
void PListFree(PList*p);
PList*Segment(guint8 *mask, double *adgrad, uint32 index, uint32 rows, uint32 columns, double low, int connectivity, int *length, uint32 max_segment);
void PListGo(CharImage*dp, PList*list, int row, int column, uint32 rows, uint32 columns, double low, double *adgrad, int connectivity);
CharImage*CharImageEdgeShenCastan92(CharImage*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity);

void StructElemGetNElem(FILE*fp, int *nelem);
void StructElemGetMedian(FILE*fp, int *median);
void StructElemGetElem(FILE*fp, int nelem, int *elem);
CharImage*CharImageStructElem(int magnification, char *file);
void StructElem(char*fout, int vsize, int hsize, StructElemType type);
CharImage*CharImageMedianSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageMedianWithSE(CharImage*dp, char *file);
void CharImageWriteRGB(CharImage*r, CharImage*g, CharImage*b, char*infile);

ShapeDescriptor*ShapeDescriptorNew(Blob*b, uint32 rows, uint32 columns);
double ShapeDescriptorSimilarity(ShapeDescriptor*s1, ShapeDescriptor*s2);
ShapeDescriptor*CharImage2ShapeDescriptor(CharImage*ci);
void BlobDelete(Blob*b);

typedef enum CheckShapeType {
	shape_accept,
	shape_reject
} CheckShapeType;

BlobImage*BlobImageCheckShape(BlobImage*bi, ShapeDescriptor*sd, double p, CheckShapeType cst);
CharImage*CharImagePadGeometry(CharImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);


#define WSHED 0
#define INIT -1
#define MASK -2
#define FICTITIOUS -3


void CharImageCheapWaterShed(CharImage*di, int connectivity);


typedef struct CheapList {
	int pixel;
	struct CheapList *next;
	struct CheapList *prev;
} CheapList;

typedef struct CheapFifo {
	CheapList*head;
	CheapList*tail;
	int size;
} CheapFifo;

void CheapFifoDelete(CheapFifo*queue);
CheapFifo*CheapFifoNew();
void CheapFifoAdd(CheapFifo*queue, unsigned int v);
int CheapFifoRemove(CheapFifo*queue);
int compare_indices_plane(const void *a, const void *b);
void CheapFifoAddFictitious(CheapFifo*queue);
int CheapFifoEmpty(CheapFifo*queue);
void CheapListDelete(CheapList*tail);
CharImage*CharImageDryDilation(CharImage*dp);
CharImage*CharImageShapeSelect(CharImage*cv, ShapeDescriptor*sd, double eps, CheckShapeType cstype, int connectivity, int MaxSegment, int MinSegment);
ShapeDescriptor*PList2ShapeDescriptor(PList*list, int columns, int rows);
PList*IntensitySegment(CharImage*cv, unsigned char Intensity, uint32 row, uint32 column, int connectivity, int MaxSegment, int*SegmentLength, int MinSegment);
int IsLabeled(PList*p, uint32 index);
void CharImageLift(CharImage*di);
unsigned char PListMin(PList* edge, CharImage*cv);
unsigned char PListMax(PList* edge, CharImage*cv);
PList*IntensitySegmentEdge(PList*list, CharImage*cv, int connectivity);
CharImage*CharImageRegionalMin(CharImage*cv, int connectivity, int MaxSegment, int MinSegment, int MaxNumSeg);
CharImage*CharImageRegionalMax(CharImage*cv, int connectivity, int MaxSegment, int MinSegment, int MaxNumSeg);
CharImage*CharImageReconstructMin(CharImage*ci, CharImage*regmin, int connectivity);
CharImage*CharImageMaskOnFilled(CharImage*di,CharImage*mask);
CharImage*CharImageReconstructMax(CharImage*ci, CharImage*regmax, int connectivity);
CharImage*CharImageGDilationSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageGErosionSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageGDilationWithSE(CharImage*dp, char *file);
CharImage*CharImageGErosionWithSE(CharImage*dp, char *file);
CharImage*CharImageReconstruct(CharImage*mask, CharImage*marker, int connectivity);
CharImage*CharImageReconstruct1(CharImage*mask, CharImage*marker, int connectivity);
CharImage*CharImageLHeqWithSE(CharImage*dp, char *file);
CharImage*CharImageLHBGWithSE(CharImage*dp, char *file);
CharImage*CharImageLHeqSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageComposeMinusAbs(CharImage*di,CharImage*w);
void CharImageComposeMinus(CharImage*di,CharImage*w,CharImage*vm,CharImage*vp);
CharImage*CharImageCreateRaw(char*name, uint32 columns, uint32 rows, int bps);
CharImage*CharImageContrastStretch(CharImage*di, double a, double b);
CharImage*CharImageContrastStretchRelay(CharImage*di, double a, double b, double paof, double pbof);
CharImage*CharImageContrastTransferRelay(CharImage*di, double a, double b, double paof, double pbof, char *func, double ax, double bx);
CharImage*dummy();
void plot(FILE*fp, CharImage*ci);
CharImage*CharImageReadDim(char*infile);
void HistogramFree(Hist*histogram);
double computeOtsu(double *p, int n);
CharImage*CharImageLOtsuWithSE(CharImage*dp, char *file);
CharImage*CharImageLOtsuSE(CharImage*dp, int vsize, int hsize, StructElemType type);
Hist*CharImageHistogramWithMask(CharImage*dp, CharImage*mask);
CharImage*CharImageHistogramEquilizeWithMask(CharImage*dp, CharImage*mask);
int IntensitySegmentTouchBorder(PList*list, CharImage*cv, int connectivity);
CharImage*CharImageCHolWithSE(CharImage*dp, char *file);
CharImage*CharImageCHolSE(CharImage*dp, int vsize, int hsize, StructElemType type);
PList*IntensitySegmentFresh(CharImage*cv, unsigned char Intensity, uint32 row, uint32 column, int connectivity, int MaxSegment, int*SegmentLength, int MinSegment, unsigned char toPut);
void deletePList(PList*p);
CharImage*CharImageOLLHeqSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageOLLHeqWithSE(CharImage*dp, char *file);
CharImage*CharImageIExpand(CharImage*ci, int rfactor, int cfactor);
void CharImageIExpandVertical(CharImage*ci, int factor, CharImage*resized);
void CharImageIExpandHorizontal(CharImage*ci, int factor, CharImage*resized);
void CharImageCrimminsDespekle(CharImage*ci, int iterations);
CharImage*CharImageIntensityPolyKern(CharImage*di, Kernel *kern);
void CharImageResampleHorizontal(CharImage*ci, double factor, CharImage*resized);
void CharImageResampleVertical(CharImage*ci, double factor, CharImage*resized);
CharImage*CharImageResample(CharImage*ci, double rfactor, double cfactor);
Kernel *KernelFromString(char*string);
CharImage*CharImageSample(CharImage*ci, double rfactor, double cfactor);
void ShapeDescriptorDelete(ShapeDescriptor*sd);
CharImage*CharImageShrink(CharImage*ci, double rfactor, double cfactor);
CharImage*CharImageConditionalGDilationWithSE(CharImage*dp, char *file, CharImage*ci,  int *cond);
double varianceBwClasses(CharImage*image, BlobImage*bi);
CharImage*CharImageChampherDistance(CharImage*dp, int flag);
CharImage*CharImageChampherYDistance(CharImage*dp, int flag);
CharImage*CharImageChampherXDistance(CharImage*dp, int flag);
Pattern*CharImagePPix(CharImage*di, double ac, double bc, double ar, double br, double t0, double tN, char*lineDraw);
CharImage*CharImageCreateEmpty(char*infile);
void CharImageAddColor(CharImage*rci, CharImage*gci, CharImage*bci, CharImage*di, char*color);
void CharImageParseColorString(char*color, unsigned char*Red, unsigned char*Green, unsigned char*Blue);
Pattern*CharImageMPPix(CharImage*di, double t0, double tN, CharImage*mi);
void plot_sp(FILE*fp, CharImage*ci, int ns, char*flag);
void CharImageCompMasks(CharImage*cand,CharImage*mask, char*fn);
void CharImageCreateRGB(char*infile, CharImage**r, CharImage**g, CharImage**b);
CharImage*CharImageMM(CharImage*di,CharImage*dj);
CharImage*CharImageHueSV(CharImage*r, CharImage*g, CharImage*b);
CharImage*CharImageHueSL(CharImage*r, CharImage*g, CharImage*b);
double exp_perona(double x, double K);
double frac_perona(double x, double K);
CharImage*CharImageAnisoDiff(CharImage*ci, int iterations, double lambda, double threshold, char*func);
void plot_round_polar(FILE*fp, CharImage*ci, CharImage*di, char*ostring, double xmean, double ymean);
CharImage*CharImageGrid(CharImage*ci, int ns);
CharImage*CharImageRoPolScr(CharImage*ci, CharImage*di, double rmin, double rmax, double tmin, double tmax, double xmean, double ymean);
CharImage*CharImageLev(CharImage*ci, unsigned char lev);
CharImage*CharImageRoPolGrid(CharImage*ci, int tbin, double theta_eps, double xmean, double ymean);
void cart2pol(double x, double y, double*theta, double*radius);
void BlobImage2Polar(BlobImage*bi, double xmean, double ymean);
CharImage*BlobImageIndex2CharImage(BlobImage*bi, int index);
void BlobImageRoPolPrint(FILE*fp, BlobImage*bi, double xmean, double ymean);

typedef struct SuperImage {
	int filledPixel;
	int emptyPixel;
	uint32 number;
	uint32 rows;
	uint32 columns;
	double*pixels;
	int bits_per_pixel;
} SuperImage;

SuperImage*SuperImageCreate(char*infile);
SuperImage*SuperImageCreateEmpty(char*infile);
SuperImage*SuperImageClone(SuperImage*si);
SuperImage*SuperImageCopy(SuperImage*si);
void SuperImageWrite(SuperImage*si, char*outfile);
SuperImage*SuperImageCompose(SuperImage*di,SuperImage*di_j, char*oper);
SuperImage*SuperImageComposeN(char*ID, char*extension, char*oper);
void AddChannelSuper(ChannelType ct, Pattern*p, SuperImage*image, BlobImage*bi, int channel, char protein, BlobDataType flag);
void SuperImageDelete(SuperImage*di);
double varBwClasses(SuperImage*image, BlobImage*bi);
CharImage*processCharImageByBlobs(CharImage*image, BlobImage*bi);
CharImage*CharImageGDistance(CharImage*p, int maxfringe, char*arg);
double fringe_bright_sum(CharImage*p, int q, int i, int j);
double fringe_bright_max(CharImage*p, int q, int i, int j);
CharImage*CharImageLStretchWithSE(CharImage*dp, char *file, double a, double b);
CharImage*CharImageLStretchSE(CharImage*dp, int vsize, int hsize, StructElemType type, double a, double b);
CharImage*CharImageMatchWithSE(CharImage*dp, char *file);
CharImage*CharImageMatchSE(CharImage*dp, int vsize, int hsize, StructElemType type);
CharImage*CharImageBelt(CharImage*ci, char*beltname, double xmean, double ymean);
SuperImage*BlobImage2SuperImage(BlobImage*bi, Pattern*p, int bits_per_pixel_in, int bits_per_pixel_out);
SuperImage*SuperImageNew(uint32 columns, uint32 rows, int bits_per_pixel);
SuperImage*SuperImageNewLabel(uint32 columns, uint32 rows, int bits_per_pixel, double label);
SuperImage*SuperImageMul(SuperImage*di,SuperImage*di_j);
SuperImage*SuperImageCropGeometry(SuperImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);
SuperImage*SuperImagePadGeometry(SuperImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);
SuperImage*SuperImageRotate(SuperImage*di, double theta);
SuperImage*SuperImageReverseRow(SuperImage*dp);
SuperImage*SuperImageReverseColumn(SuperImage*dp);
SuperImage*SuperImageTranspose(SuperImage*dp);
char*measurements_print(ShapeDescriptor*s, unsigned int flags);
CharImage*CharImageFMedianWithSE(CharImage*dp, int npattern, int*pattern, int rank);
char*pr_get_name_temp();

typedef struct Element {
	uint32 column;
	uint32 row;
	uint32 plane;
} Element;

Element*elemenet_new(uint32 c, uint32 r, uint32 p);
void element_free(Element*arg);
#include <dirent.h>
char*iterate_compose(FILE**listfp, DIR**dirptr, char*ID);

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_matrix.h>

gsl_matrix *psf_matrix_set(int n, double sigma);
SuperImage*super_image_deconvolve_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, int iter_max, double rho, double criterion);
SuperImage*super_image_get_microscope_parameter(double*msigma, SuperImage*si, BlobImage*bi, int *window, double sigma_a, double sigma_b, char*psf_type, int iter_max, double criterion, BlobImage*bimask, int log_flag);
BlobImage*BlobImageCreate(char*file);
char*prostak_get_info_string(char*pointer);
char**prostak_get_info_array(char*pointer);
char*prostak_get_info_string_with_oper(char*pointer, char*oper);
void prostak_put_info_string(char*pointer, char*oper, char*info_string);
void prostak_put_info_array(char*pointer, char**info_array);
void ShapeDescriptorPrint(GKeyFile*gkf, ShapeDescriptor*s, char*grp);
CharImage*char_image_average(char**files, int size);
CharImage*char_image_max(char**files, int n_files);
SuperImage*super_image_compose(char**files, int n_files, char*oper);
void super_image_copy_data(SuperImage*su, SuperImage*data);
SuperImage*super_image_deconvolve_wiener_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double noise_var, double gamma, double alpha);
SuperImage*super_image_deconvolve_tm_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double noise_var, double gamma);
SuperImage*super_image_deconvolve_inv_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, double gamma);
SuperImage*super_image_deconvolve_blind_with_analytical_psf(SuperImage*input, int *window, double sigma, char*psf_type, int iter_max, double rho, double criterion, int iter_blind_max, double **psf_array);
void vvarbc(GKeyFile*gkf, char*output_group, char*class_out_group, char*class_in_group, char*key_out, char*key_in);
GKeyFile*g_key_file_load_from_cmdarg(char*input_pointer, GError**gerror);
CharImage*char_image_mlsreg(CharImage*landmarks_fixed, CharImage*landmarks, CharImage*data, char*type, double alpha);
unsigned char mls_reg_func_affine(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha);
unsigned char mls_reg_func_similar(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha);
unsigned char mls_reg_func_rigid(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha);
void mls_reg_func_common(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, double alpha, double*p_ast_1, double*p_ast_2, double*q_ast_1, double*q_ast_2);
double*char_image_get_landmarks(CharImage*landmarks, int*i);
unsigned char mls_reg_func_get(CharImage*data, double c, double r, double p_ast_1, double p_ast_2, double q_ast_1, double q_ast_2, double mn_11, double mn_12, double mn_21, double mn_22);
SuperImage*super_image_mlsreg(CharImage*landmarks_fixed, CharImage*landmarks, SuperImage*data, char*type, double alpha);
CharImage*CharImageHistogramNormalize(CharImage*dp);

#endif
