/***************************************************************************
 *            prostak_vfuncs.c
 *
 *  Tue Nov  3 14:35:13 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>

#include <grf.h>
#include <CharVolume.h>
#include <CharImage.h>
#ifndef MAX_RECORD
#define MAX_RECORD 255
#endif

int grf_callback_vmax(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data)
{
	uint32 column;
	for ( column = 0; column < columns; column++ ) {
		out_pixels_1[column] = (in_pixels_1[column] > in_pixels_2[column]) ? in_pixels_1[column] : in_pixels_2[column];
	}
	return column;
}

int grf_callback_vavg(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data)
{
	uint32 column;
	for ( column = 0; column < columns; column++ ) {
		out_pixels_1[column] = 0.5 * ( in_pixels_1[column] + in_pixels_2[column] );
	}
	return column;
}

int grf_callback_vmabs(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data)
{
	uint32 column;
	for ( column = 0; column < columns; column++ ) {
		out_pixels_1[column] = fabs ( in_pixels_1[column] - in_pixels_2[column] );
	}
	return column;
}

int grf_callback_vaff(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data)
{
	uint32 column;
	double*coeffs;
	double dot, max_dot;
	if ( (int)in_bps_1 == 8 ) {
		max_dot = 255.0;
	} else if ( (int)in_bps_1 == 16 ) {
		max_dot = 65535.0;
	}
	coeffs = (double*)user_data;
	for ( column = 0; column < columns; column++ ) {
		dot = coeffs[0] * in_pixels_1[column] + coeffs[1] * in_pixels_2[column] + coeffs[2];
		if ( dot < 0 ) {
			dot = 0.0;
		}
		if ( dot > max_dot ) {
			dot = max_dot;
		}
		out_pixels_1[column] = dot;
	}
	return column;
}

int grf_callback_vrgb(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*in_pixels_3, double*out_pixels_1, void*user_data)
{
	uint32 column;
	for ( column = 0; column < columns; column++ ) {
		out_pixels_1[3 * column] = in_pixels_1[column];
		out_pixels_1[3 * column + 1] = in_pixels_2[column];
		out_pixels_1[3 * column + 2] = in_pixels_3[column];
	}
	return column;
}

int prostak_vmax(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	ret_val = grf_compose_two(input_pointers[0], input_pointers[1], output_pointers[0], (GrfCallbackFunc) grf_callback_vmax, NULL);
	return ret_val;
}

int prostak_vrgb(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	ret_val = grf_compose_rgb(input_pointers[0], input_pointers[1], input_pointers[2], output_pointers[0], (GrfCallbackFunc3) grf_callback_vrgb, NULL);
	return ret_val;
}

int prostak_vavg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	ret_val = grf_compose_two(input_pointers[0], input_pointers[1], output_pointers[0], (GrfCallbackFunc) grf_callback_vavg, NULL);
	return ret_val;
}

int prostak_vmabs(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	ret_val = grf_compose_two(input_pointers[0], input_pointers[1], output_pointers[0], (GrfCallbackFunc) grf_callback_vmabs, NULL);
	return ret_val;
}

int grf_callback_vrmbg(double scale, uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*out_pixels_1, void*user_data)
{
	uint32 column;
	double*coeffs;
	double dot, bkgr;
	coeffs = (double*)user_data;
	for ( column = 0; column < columns; column++ ) {
		bkgr = coeffs[0] * coeffs[1] + coeffs[2] * coeffs[3];
		dot = scale * ( in_pixels_1[column] - bkgr ) / ( scale - bkgr );
		if ( dot < 0 ) {
			dot = 0.0;
		}
		out_pixels_1[column] = dot;
	}
	return column;
}

int prostak_vrmbg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	double*coeffs;
	int length;
	char**options;
	GKeyFile*gkf;
	GError*gerror = NULL;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_vrmbg:option error");
	}
	coeffs = (double*)calloc(4, sizeof(double));
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 4 ) {
			g_error("prostak_vrmbg: option error");
		}
		coeffs[0] = atof(options[0]);
		coeffs[1] = atof(options[1]);
		coeffs[2] = atof(options[2]);
		coeffs[3] = atof(options[3]);
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 4 ) {
			g_error("prostak::decsup option error");
		}
		coeffs[0] = atof(options[0]);
		coeffs[2] = atof(options[2]);
		if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
			g_error("prostak_vrmbg:%s", gerror->message);
		}
		shape_descriptor_get_data_field(gkf, options[1], options[3], &coeffs[1], 0, &gerror);
		if ( gerror ) {
			g_error("prostak_vrmbg:%s", gerror->message);
		}
		shape_descriptor_get_data_field(gkf, options[1], options[3], &coeffs[3], 2, &gerror);
		if ( gerror ) {
			g_error("prostak_vrmbg:%s", gerror->message);
		}
	} else {
		g_error("prostak_vrmbg: option error");
	}
	ret_val = grf_compose_one(input_pointers[0], output_pointers[0], (GrfCallbackFunc0) grf_callback_vrmbg, (void*)coeffs);
	return ret_val;
}

int prostak_vaff(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int ret_val;
	double*coeffs;
	int length;
	char**options;
	GError*gerror = NULL;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_vrmbg:option error");
	}
	coeffs = (double*)calloc(3, sizeof(double));
	length = g_strv_length(options);
	if ( length < 3 ) {
		g_error("prostak_vaff: option error");
	}
	coeffs[0] = atof(options[0]);
	coeffs[1] = atof(options[1]);
	coeffs[2] = atof(options[2]);
	ret_val = grf_compose_two(input_pointers[0], input_pointers[1], output_pointers[0], (GrfCallbackFunc) grf_callback_vaff, (void*)coeffs);
	g_free(coeffs);
	return ret_val;
}
