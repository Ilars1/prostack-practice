/***************************************************************************
 *            CharVolume.h
 *
 *  Mon Jun 20 12:38:59 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  aint with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _CHARVOLUME_H
#define _CHARVOLUME_H

#ifdef __cplusplus
extern "C"
{
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>


#include <math.h>
#include <grf.h>
#include <CharImage.h>

typedef struct CharVolume {
	uint32 columns;
	uint32 rows;
	uint32 planes;
	unsigned char ***voxels;
} CharVolume;

typedef struct Nucleus {
	double x;
	double y;
	double z;
	double xradius;
	double yradius;
	double zradius;
	int label;
	struct Nucleus*next;
} Nucleus;

CharVolume*NewCharVolume(uint32 columns, uint32 rows, uint32 planes);
void CharVolumeAddPlane(CharVolume*cv, CharImage*ci, int plane);

typedef struct CharVoxel {
	uint32 index;
	uint32 row;
	uint32 column;
	uint32 plane;
	unsigned char intensity;
	int label;
	int distance;
	struct CharVoxel**neigh;
	int Nneigh;
} CharVoxel;

CharVoxel*NewCharVoxel(uint32 row, uint32 column, uint32 plane, unsigned char intensity);
CharVoxel*NewFictitiousCharVoxel();
int compare_voxels(const void *a, const void *b);

typedef struct CharVoxelStructure {
	CharVoxel**voxels;
	uint32 size;
} CharVoxelStructure;

CharVoxelStructure*NewCharVoxelStructure(CharVolume*cv,  int connectivity );

typedef struct VoxelList {
	CharVoxel*pixel;
	struct VoxelList *next;
	struct VoxelList *prev;
} VoxelList;

typedef struct VoxelFifo {
	VoxelList*head;
	VoxelList*tail;
	int size;
} VoxelFifo;

void VoxelFifoDelete(VoxelFifo*queue);
VoxelFifo*VoxelFifoNew();
void VoxelFifoAdd(VoxelFifo*queue, CharVoxel*v);
CharVoxel*VoxelFifoRemove(VoxelFifo*queue);
void VoxelFifoAddFictitious(VoxelFifo*queue);
int VoxelFifoEmpty(VoxelFifo*queue);
void VoxelListDelete(VoxelList*head);
int CharVoxelAllNeighWSHED(CharVoxel *cp);

void CharVoxelDelete(CharVoxel*cp);
void CharVoxelStructureDelete(CharVoxelStructure*cps);
void VoxelListAdd(VoxelList*plist, CharVoxel*cp);
void VoxelListTour(VoxelList*plist);
VoxelList*InitVoxelList(CharVoxel*cp);
CharImage*CharVolumeGetPlane(CharVolume*cv, int plane);
void CharVolumeDelete(CharVolume*cv);
CharImage*CharVolumeGetRow(CharVolume*cv, int row);
CharImage*CharVolumeGetColumn(CharVolume*cv, int column);

typedef struct Cube {
	int r;
	int c;
	uint32 RowStart;
	uint32 RowEnd;
	uint32 ColumnStart;
	uint32 ColumnEnd;
} Cube;

typedef struct Cubistic {
	uint32 rows;
	uint32 columns;
	int size;
	uint32 number;
	Cube*cubes;
} Cubistic;

Cubistic*Partition(uint32 rows, uint32 columns, int size);
double Discrepance(CharImage*UpperPlane, CharImage*LowerPlane, double lambda, Cubistic*cuby, int connectivity);
CharImage*Interpolate(Cubistic*cuby, CharImage*UpperPlane, CharImage*LowerPlane);
void CubisticDelete(Cubistic*c);
CharImage*CharVolumeNewPlane(CharImage*UpperPlane, CharImage*LowerPlane, int k, double lambda, int connectivity);
double DiscrepanceCube(CharImage*UpperPlane, CharImage*LowerPlane, double lambda, Cubistic*cuby, int connectivity, uint32 n);
CharVolume*CreateCharVolume(char*ID, char*extension);

void CharVolumeFastWaterShed(CharVolume*di, int connectivity);
void CharVolumeCheapWaterShed(CharVolume*di, int connectivity);
void CharVolumeWrite(CharVolume*cv, char*ID, char*extension);
int compare_indices(const void *a, const void *b);
CharVolume*CharVolumeLeftView(CharVolume*cv);
CharVolume*CharVolumeRightView(CharVolume*cv);
CharVolume*CharVolumeStructElem(char *file);
void StructElem3d(char*fout, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeMedianSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeMedianWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeLocalHEqSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeLocalHEqWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeBPLHBGWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeClone(CharVolume*p);
CharVolume*CharVolumeBPDistance(CharVolume*dp, int repetitions);
CharVolume*CharVolumeDistance(CharVolume*dp);

typedef struct PList3d {
	struct PList3d*next;
	int column;
	int row;
	int plane;
	double x;
	double y;
	double theta;
	double radius;
	double val;
	double vald1;
	double vald2;
	int inc;
} PList3d;

CharVolume*CharVolumeEdgeShenCastan(CharVolume*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity);
CharVolume*CharVolumeBPShenCastan92(CharVolume*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity);
void ComputeISEF3d(double***isef, double b, double***orig, uint32 columns, uint32 rows, uint32 planes);
void ComputeADGRAD3d(double ***isef, int ***bli, uint32 rows, uint32 columns, uint32 planes, double ***adgrad, uint32 window);
int ZeroCrossing3d(double ***isef, int ***bli, uint32 row, uint32 column, uint32 plane, uint32 rows, uint32 columns, uint32 planes);
void ComputeBLI3d(double***isef1, double***isef2, int***bli, uint32 columns, uint32 rows, uint32 planes);
double AdGrad3d(double ***isef, int ***bli, uint32 row, uint32 column, uint32 plane, uint32 rows, uint32 columns, uint32 planes, uint32 window);
void HysteresisThreshold3d(CharVolume*dpm, double ***adgrad, double low, double high, uint32 segment, int connectivity);
int Length3d(PList3d*p);
void MarkPlist3d(PList3d*p, CharVolume*dpm, unsigned char intensity);
void PList3dFree(PList3d*p);
CharVolume*CharVolumeDespeckle(CharVolume*cv);
PList3d*CheapSegment3d(CharVolume*dp,double ***adgrad, uint32 row, uint32 column, uint32 plane, double low, int connectivity);
CharVolume*CharVolumeErosion(CharVolume*cv, int connectivity);
CharVolume*CharVolumeDilation(CharVolume*cv, int connectivity);

typedef struct ShapeDescriptor3d {
	double m000;
	double m001;
	double m010;
	double m100;
	double xmean;
	double ymean;
	double zmean;
	double mu002;
	double mu020;
	double mu200;
	double mu110;
	double mu101;
	double mu011;
	double eta002;
	double eta020;
	double eta200;
	double eta110;
	double eta101;
	double eta011;
	double I_1;
	double I_2;
	double I_3;
	double J_1;
	double J_2;
	double J_3;
	double ellipsoid_axis_a;
	double ellipsoid_axis_b;
	double ellipsoid_axis_c;
	int i_min;
	int i_max;
	int j_min;
	int j_max;
	int k_min;
	int k_max;
	int i_mean;
	int j_mean;
	int k_mean;
} ShapeDescriptor3d;

PList3d*IntensitySegment3d(CharVolume*dp, unsigned char Intensity, uint32 row, uint32 column, uint32 plane, int connectivity, uint32 MaxSegment, int *SegmentLength, uint32 MinSegment);
int IsLabeled3d(PList3d*p, uint32 column, uint32 row, uint32 plane);
CharVolume*CharVolumeShapeSelect(CharVolume*cv, ShapeDescriptor3d*sd, double eps, CheckShapeType cstype, int connectivity, uint32 MaxSegment, uint32 MinSegment);
ShapeDescriptor3d*PList3d2ShapeDescriptor3d(PList3d*list);
ShapeDescriptor3d*CharVolume2ShapeDescriptor3d(CharVolume*cv, int connectivity);
double DistanceShape3d(ShapeDescriptor3d*sd, ShapeDescriptor3d*csd);
CharVolume*CharVolumeCopy(CharVolume*cv);
void ShapeDescriptor3dPrint(GKeyFile*gkf, ShapeDescriptor3d*s, char*grp);
void ShapeDescriptor3dDelete (ShapeDescriptor3d*sd);
double DistanceShape3d_1(ShapeDescriptor3d*sd, ShapeDescriptor3d*csd);

typedef enum VolumetricFormat {
	XYZ_VOLUMETRIC_FORMAT,
	VRML_VOLUMETRIC_FORMAT,
	VRML_VOLUMETRIC_FORMAT_SPHERE
} VolumetricFormat;

void CharVolumePrintText(CharVolume*cv, FILE*f, int connectivity, VolumetricFormat format);

typedef void (*VolumetricFormatWriteHeaderType)( void*ptr, FILE*fp);
typedef void (*VolumetricFormatWriteBodyType)( void*ptr, FILE*fp, void* ptr2);
typedef void (*VolumetricFormatWriteFooterType)( void* ptr, FILE*fp);

void xyzWriteHeader( void* ptr, FILE*fp);
void xyzWriteBody( void* ptr, FILE*fp, void* ptr2);
void xyzWriteFooter( void* ptr, FILE*fp);

void vrmlWriteHeader( void* ptr, FILE*fp);
void vrmlWriteBody( void* ptr, FILE*fp, void* ptr2);
void vrmlWriteFooter( void* ptr, FILE*fp);

void vrmlSphereWriteHeader( void* ptr, FILE*fp);
void vrmlSphereWriteBody( void* ptr, FILE*fp, void* ptr2);
void vrmlSphereWriteFooter( void* ptr, FILE*fp);

int plist3dHas(PList3d*list, int c, int r, int po);
void CharVolumeShapeSelectList(CharVolume*cv, ShapeDescriptor3d*sd, double eps, CheckShapeType cstype, int connectivity, uint32 MaxSegment, uint32 MinSegment, FILE*fp);
PList3d*PList3dReduce(PList3d*list, int delta, int connectivity);
int areNeighbors(PList3d*list, int k_1, int k_3, int connectivity);
int plist3dHasAny(PList3d*list, int c, int r, int po);
int areOnePlane(PList3d*list, int v1, int v2, int v3, int v4);
int plist3dCheckTriangle(PList3d*trList, int c, int r, int po, PList3d*list);
CharVolume*CharVolumeReconstruct(CharVolume*mask, CharVolume*marker, int connectivity);
CharVolume*CharVolumeGDilationWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeGDilationSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeGErosionWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeGErosionSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeCreate(char*ID);
CharVolume*CharVolumeCreateChannel(char*ID, int channel);
CharImage*CharVolume2Image(CharVolume*di, char*oper);

CharVolume*CharVolumeMul(CharVolume*di, CharVolume*di_j);
CharVolume*CharVolumeCropGeometry(CharVolume*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);
CharVolume*CharVolumePadGeometry(CharVolume*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset);
CharVolume*CharVolumeRotate(CharVolume*di, double theta);

CharVolume*CharVolumeTranspose(CharVolume*dp);
CharVolume*CharVolumeReverseColumn(CharVolume*dp);
CharVolume*CharVolumeReverseRow(CharVolume*dp);
CharVolume*CharVolumeExpand(CharVolume*ci, double rfactor, double cfactor);
CharVolume*CharVolumeShrink(CharVolume*ci, double rfactor, double cfactor);
CharVolume*CharVolumeResample(CharVolume*ci, double rfactor, double cfactor);
void CharVolumeResampleVertical(CharVolume*ci, double factor, CharVolume*resized);
void CharVolumeResampleHorizontal(CharVolume*ci, double factor, CharVolume*resized);
CharVolume*CharVolumeCHolSE(CharVolume*dp, int vsize, int hsize, int zsize, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeCHolWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeBPCHolWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeConditionalGDilationWithSE(CharVolume*dp, char *file, CharVolume*ci, int*cond);
CharVolume*CharVolumeInvert(CharVolume*cv);
void CharVolumeGWaterShed(CharVolume*di, int connectivity);

CharVolume*CharVolumeFMedianWithSE(CharVolume*dp, int npattern, int*pattern, int rank);
void CharVolumeThreshold(CharVolume*dp, double threshold);
double CharVolumeThresholdHist1(CharVolume*dp);

CharVolume*CharVolumeUnbent(CharVolume*di, int xoffset, int yoffset);
void CharVolumeSetPlane(CharVolume*cv, CharImage*new_plane, int plane);
void StructElem3d_2(char*fout, int vsize, int hsize, int zsize, int width, StructElemType vhtype, StructElemType ztype);
CharVolume*CharVolumeRestack(CharVolume*ci, int step);

#ifndef G_OS_WIN32
#include <gts.h>
void gts_func(gdouble **gts_a, GtsCartesianGrid gts_grid, guint i, gpointer data);
void CharVolumeIsosurface(CharVolume*cv, double max_cost, gdouble minangle, char*output_file, char*format, double scale_column, double scale_row, double scale_plane);
void CharVolumeIsosurfaceFull(CharVolume*cv, double max_cost, gdouble minangle, char*output_file, char*format, double scale_column, double scale_row, double scale_plane, double xx, double yy, double zz, char*hint);
void gts_surface_scale(GtsSurface *surface, double scale_column, double scale_row, double scale_plane);
gint gts_vertex_scale_func(gpointer item, gpointer data);
#endif

CharVolume*CharVolumeMaxChannel(char*ID, int nchannels, int *channels);
CharVolume*CharVolumeTopView(CharVolume*cv);
CharVolume*CharVolumeCUnbent(CharVolume*mask, char*log);
void CharVolumeQu(CharVolume*cv, CharVolume*ci, GKeyFile*gkf, char*tag, int connectivity, int adding);
void CharVolumeQuPrint(GKeyFile*gkf, char*filename);

typedef struct Measurements3d {
	int notempty;
	double mean;
	double stdev;
	double median;
	double max;
	double min;
	double var;
	double muc;
} Measurements3d;

Measurements3d*Plist3d2Measurements3d(int length, PList3d*list, CharVolume*ci);
void Measurements3dDelete (Measurements3d*data);
void Measurements3dPrint(GKeyFile*gkf, Measurements3d*data, char*grp, char*tag);
PList3d*IntensitySegment3dModified(CharVolume*dp, unsigned char Intensity, uint32 row, uint32 column, uint32 plane, int connectivity, uint32 MaxSegment, int *SegmentLength, uint32 MinSegment, unsigned char IntensityModified);
void shape_descriptor_get_data_field(GKeyFile*gkf, char*grp, char*key, double*data, int idex, GError**gerror);
CharVolume*CharVolumeGrid(CharVolume*ci, double ns_rows, double ns_columns, double ns_planes, double xmean, double ymean, double zmean);
CharVolume*CharVolumeGridElliptical(CharVolume*ci, double ns_rows, double ns_columns, double ns_planes, double offset, double xmean, double ymean, double zmean, double eps);
CharVolume*CharVolumeBPHeqM(CharVolume*dp, CharVolume*mask);
ShapeDescriptor3d*shape_descriptor_3d_read(GKeyFile*gkf, char*grp, GError**gerror);
void CharVolumeQuTransform(GKeyFile*gkf, int center, int percent, ShapeDescriptor3d*mask, double minvolume, double maxvolume, GError**gerror);
CharVolume*Isosurface2CharVolume(char*input_file);
CharVolume*CharVolumePump(CharImage*distmap, CharImage*ci);
CharVolume*CharVolumeMultiSlice(CharVolume*distmap, ShapeDescriptor3d*sd, double ns, int direction);
CharVolume*CharVolumeMultiSliceGCP(CharVolume*distmap, ShapeDescriptor3d*sd, double ns, int direction, int ns_direction, double ns_rows, double ns_columns, double ns_planes);
CharVolume*CharVolumeQuMark(CharVolume*cv, int connectivity);
CharVolume*CharVolumeQuMap(GKeyFile*gkf, CharVolume*mask, int connectivity, char*channel, int index, GError**gerror);
void LabelDoubleByPList3d(PList3d*p, double*data, double value, int rows, int columns, int planes);
void char_volume_relabel_blobs(GKeyFile*gkf, CharVolume*mask, int connectivity, CharImage*landmarks_fixed, CharImage*landmarks, char*type, double alpha, GError**gerror);

void char_volume_label_mask(GKeyFile*gkf, CharVolume*mask, int connectivity, char*tag, CharVolume*cv, GError**gerror);
CharVolume*CharVolumeBPHysThresh(CharVolume*dp, double low, double high, uint32 segment, int connectivity);
CharVolume*CharVolumeBPCheapWaterShed(CharVolume*di, int repetitions);

void CharVolumeWriteRGB(CharVolume*r, CharVolume*g, CharVolume*b, char*ID);
CharVolume*CharVolumeCreateEmpty(char*ID);

CharVolume*CharVolumeBPLHeqWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeBPGErosionWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeBPGDilationWithSE(CharVolume*dp, char *file);
CharVolume*CharVolumeBPReconstruct(CharVolume*dp, CharVolume*mask, int connectivity);
CharVolume*CharVolumeBPAnisoDiff(CharVolume*dp, int iterations, double lambda, double threshold, char*func);

CharVolume*CharVolumeBPGCloseWithSE(CharVolume*dp, char *file, int repetitions);
CharVolume*CharVolumeBPGOpenWithSE(CharVolume*dp, char *file, int repetitions);
CharVolume*CharVolumeBPHeq(CharVolume*dp);
CharVolume*CharVolumeBPNormalize(CharVolume*dp);
CharVolume*CharVolumeBPCrimminsDespekle(CharVolume*dp, int repetitions);
void shape_descriptor_get_data_field(GKeyFile*gkf, char*grp, char*key, double*data, int idex, GError**gerror);

CharVolume*CharVolumeBPShapeSelect(CharVolume*dp, ShapeDescriptor*sd, double eps, CheckShapeType cstype, int connectivity, int MaxSegment, int MinSegment);
CharVolume*CharVolumeQuSort(GKeyFile*gkf, CharVolume*mask, int connectivity, char*channel, int index, char*refch, char*newch, int ascending, GError**gerror);
CharVolume* char_volume_dilate_mask(CharVolume*mask, int connectivity, int maxiter, GError**gerror);
CharVolume* char_volume_erode_mask(CharVolume*mask, int connectivity, int maxiter, GError**gerror);

typedef struct VertDat {
	int n_vert;
	int max_vert;
	int*row_vert;
	int*column_vert;
	int cycle;
	int n_points;
} VertDat;

void VertDatPrint(GKeyFile*gkf, VertDat*data, char*grp, char*tag);
void VertDatDelete (VertDat*data);
VertDat*Plist3d2VertDat(int length, PList3d*list, CharVolume*ci, double xmean, double ymean, int ncycles, int max_vert, double alpha);
void CharVolumeVu(CharVolume*cv, CharVolume*ci, GKeyFile*gkf, char*tag, int connectivity, int ncycles, int max_vert, double alpha);
void CharVolumePBreak(CharVolume*di, int repetitions, CharVolume**di_1, CharVolume**di_2);
CharVolume*CharVolumePUnite(CharVolume*di_1, CharVolume*di_2);

#ifdef __cplusplus
}
#endif

#endif /* _CHARVOLUME_H */
