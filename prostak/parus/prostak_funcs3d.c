/***************************************************************************
 *            prostak_funcs3d.c
 *
 *  Sat Oct  3 18:13:10 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <CharImage.h>
#include <CharVolume.h>
#include <ImageInfo.h>
#include <Pattern.h>


void prostak_threshold3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	double threshold;
	char *info_string;
	if ( !ostring )
		g_error("prostak: threshold no threshold rule (-s)");
	di = CreateCharVolume(input_pointers[0], "tif");
	if ( !strncmp(ostring, "otsu", 4) ) {
		threshold = CharVolumeThresholdHist1(di);
	} else if ( !strncmp(ostring, "plain", 5) ) {
		threshold = param;
	} else {
		g_error("prostak threshold unknown rule %s", ostring);
	}
	if ( repetitions ) {
		CharVolumeThreshold(di, threshold);
		CharVolumeWrite(di, output_pointers[0], ".tif");
	}
	info_string = g_strdup_printf("%f", threshold);
	prostak_put_info_string(output_pointers[1], "threshold3d", info_string);
	g_free(info_string);
/*		} else {
			SuperImage*di;
			di = SuperImageComposeN(argv[optind+1],"tif", "max");
			SuperImageWrite(di,argv[optind+2]);
		}*/
}

int prostak_hysteresis_threshold3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, i;
	CharImage*di;
	double threshold_low, threshold_high, ***adgrad;
	int segment;
	int connectivity;
	uint32 row, column, plane;
	sscanf(ostring,"%lf,%lf,%d,%d", &threshold_low, &threshold_high, &segment, &connectivity);
	CharVolume*dp;
	dp = CreateCharVolume(input_pointers[0], "tif");
	adgrad = (double***)calloc(dp->columns, sizeof(double**));
	for ( column = 0; column < dp->columns; column++ ) {
		adgrad[column] = (double**)calloc(dp->rows, sizeof(double*));
		for ( row = 0; row < dp->rows; row++ ) {
			adgrad[column][row] = (double*)calloc(dp->planes, sizeof(double));
			for ( plane = 0; plane < dp->planes; plane++ ) {
				adgrad[column][row][plane] = (double)dp->voxels[column][row][plane];
			}
		}
	}
	HysteresisThreshold3d(dp, adgrad, threshold_low, threshold_high, (uint32) segment, connectivity);
	CharVolumeWrite(dp, output_pointers[0], ".tif");
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			free(adgrad[column][row]);
		}
		free(adgrad[column]);
	}
	free(adgrad);
	return status;
}

int prostak_max3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	CharImage*ci;
	di = CharVolumeCreate(input_pointers[0]);
	ci = CharVolume2Image(di, "max");
	CharImageWrite(ci, output_pointers[0]);
/*		} else {
			SuperImage*di;
			di = SuperImageComposeN(argv[optind+1],"tif", "max");
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_turn3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	double angle_in_radians;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf", &angle_in_radians);
		free(ops);
	} else {
		sscanf(ostring, "%lf", &angle_in_radians);
	}
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeRotate(di, angle_in_radians);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			di = SuperImageCreate(argv[optind+1]);
			di = SuperImageRotate(di, param);
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_reverserow3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeReverseRow(di);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			int i;
			di = SuperImageCreate(argv[optind+1]);
			di = SuperImageReverseRow(di);
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_reversecolumn3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeReverseColumn(di);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			int i;
			di = SuperImageCreate(argv[optind+1]);
			di = SuperImageReverseRow(di);
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_transpose3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeTranspose(di);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			int i;
			di = SuperImageCreate(argv[optind+1]);
			di = SuperImageReverseRow(di);
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_geometry3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
		free(ops);
	} else {
		sscanf(ostring, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	}
	di = CreateCharVolume(input_pointers[0], "tif");
	di = CharVolumeCropGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
			if ( !GeometryIsSet )
				g_error("prostak geometry is not set!");
			di = SuperImageCreate(argv[optind+1]);
			sscanf(ostring,"%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
			di = SuperImageCropGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
			SuperImageWrite(di,argv[optind+2]);
		}*/
	return status;
}

int prostak_pad3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	int status = 0;
	CharVolume*di;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
		free(ops);
	} else {
		sscanf(ostring,"%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	}
	di = CreateCharVolume(input_pointers[0], "tif");
	di = CharVolumePadGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
			if ( !GeometryIsSet )
				g_error("prostak geometry is not set!");
			di = SuperImageCreate(argv[optind+1]);
			sscanf(ostring,"%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
			di = SuperImageCropGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
			SuperImageWrite(di,output_pointers[0]);
		}*/
	return status;
}

int prostak_invert3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeInvert(di);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_shrink3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	if ( param <= 0 || param >= 1)
		g_error("Shrink: param not valid %f", param);
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeShrink(di,param, param);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_expand3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	if ( param <= 1)
		g_error("Expand: param not valid %f", param);
	di = CharVolumeCreate(input_pointers[0]);
	di = CharVolumeResample(di, param, param);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_mul3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
/*		if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {*/
	CharVolume*di;
	CharVolume*maskimage;
	di = CharVolumeCreate(input_pointers[0]);
	maskimage = CharVolumeCreate(input_pointers[1]);
	di = CharVolumeMul(di, maskimage);
	CharVolumeWrite(di, output_pointers[0], ".tif");
/*		} else {
			SuperImage*di;
			di = SuperImageCreate(argv[optind+1]);
			if ( HasMask ) {
				SuperImage*maskimage;
				maskimage = SuperImageCreate(mask);
				di = SuperImageMul(di,maskimage);
			} else {
				g_error("prostak mask's not defined!");
			}
			SuperImageWrite(di,output_pointers[0]);
		}*/
	return status;
}

int prostak_splitlsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*rci;
	rci = CharVolumeCreateChannel(input_pointers[0], 0);
	CharVolumeWrite(rci, output_pointers[0], ".tif");
	CharVolumeDelete(rci);
	rci = CharVolumeCreateChannel(input_pointers[0], 1);
	CharVolumeWrite(rci, output_pointers[1], ".tif");
	CharVolumeDelete(rci);
	rci = CharVolumeCreateChannel(input_pointers[0], 2);
	CharVolumeWrite(rci, output_pointers[2], ".tif");
	CharVolumeDelete(rci);
	return status;
}

int prostak_maxlsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*rci;
	int i, nch, *ichs;
	char**chs;
	chs = g_strsplit(ostring, ",", 10);
	nch = g_strv_length(chs);
	ichs = (int*)calloc(nch, sizeof(int));
	for ( i = 0; i < nch; i++) {
		ichs[i] = atoi(chs[i]);
	}
	g_strfreev(chs);
	rci = CharVolumeMaxChannel(input_pointers[0], nch, ichs);
	free(ichs);
	CharVolumeWrite(rci, output_pointers[0], ".tif");
	CharVolumeDelete(rci);
	return status;
}

int prostak_diglsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*rci;
	rci = CharVolumeCreateChannel(input_pointers[0], repetitions);
	CharVolumeWrite(rci, output_pointers[0], ".tif");
	return status;
}

int prostak_insert3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*UpperPlane;
	CharImage*LowerPlane;
	int size;
	double lambda;
	int connectivity;
	UpperPlane = CharImageCreate(input_pointers[0]);
	LowerPlane = CharImageCreate(input_pointers[0]);
	sscanf(ostring,"%d,%lf,%d",&size, &lambda, &connectivity);
	di = CharVolumeNewPlane(UpperPlane, LowerPlane, size, lambda, connectivity);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_volume_row(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	CharImage*ci;
	cv = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeGetRow(cv, repetitions);
	CharImageWrite(ci, output_pointers[0]);
	return status;
}

int prostak_volume_column(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	CharImage*ci;
	cv = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeGetColumn(cv, repetitions);
	CharImageWrite(ci, output_pointers[0]);
	return status;
}

int prostak_cheap_watershed3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	cv = CreateCharVolume(input_pointers[0], "tif");
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	CharVolumeCheapWaterShed(cv, repetitions);
	CharVolumeWrite(cv, output_pointers[0], ".tif");
	return status;
}

int prostak_rv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	cv = CreateCharVolume(input_pointers[0], "tif");
	cv = CharVolumeRightView(cv);
	CharVolumeWrite(cv, output_pointers[0], ".tif");
	return status;
}

int prostak_lv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	cv = CreateCharVolume(input_pointers[0], "tif");
	cv = CharVolumeLeftView(cv);
	CharVolumeWrite(cv, output_pointers[0], ".tif");
	return status;
}

int prostak_tv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	cv = CreateCharVolume(input_pointers[0], "tif");
	cv = CharVolumeTopView(cv);
	CharVolumeWrite(cv, output_pointers[0], ".tif");
	return status;
}

int prostak_cunbent3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*cv;
	cv = CreateCharVolume(input_pointers[0], "tif");
	cv = CharVolumeCUnbent(cv, input_pointers[1]);
	CharVolumeWrite(cv, output_pointers[0], ".tif");
	return status;
}

int prostak_median3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	FILE*fp;
	int nelem, npattern, median, *pattern;
	fp = fopen(input_pointers[1], "r");
	if ( !fp )
		g_error("prostak: median3d can't open %s", input_pointers[1]);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 3.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeFMedianWithSE(di, npattern, pattern, 2);
	}
	free(pattern);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_unbent3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int xoffset;
	int yoffset;
	sscanf(ostring, "%d,%d", &xoffset, &yoffset);
	di = CreateCharVolume(input_pointers[0], "tif");
	di = CharVolumeUnbent(di, xoffset, yoffset);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_chole3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	di = CreateCharVolume(input_pointers[0], "tif");
	di = CharVolumeCHolWithSE(di, input_pointers[1]);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_lheq3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeLocalHEqWithSE(di, input_pointers[1]);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_gerosion3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeGErosionWithSE(di, input_pointers[1]);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_gdilation3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeGDilationWithSE(di, input_pointers[1]);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_gopen3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeGErosionWithSE(di, input_pointers[1]);
		di = CharVolumeGDilationWithSE(di, input_pointers[1]);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_gclose3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	for ( i = 0; i < repetitions; i++ ) {
		di = CharVolumeGDilationWithSE(di, input_pointers[1]);
		di = CharVolumeGErosionWithSE(di, input_pointers[1]);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_reconstruct3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*regmax;
	regmax = CreateCharVolume(input_pointers[1], "tif");
	di = CreateCharVolume(input_pointers[0], "tif");
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	di = CharVolumeReconstruct(di, regmax, repetitions);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_rec3dbp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*regmax;
	CharImage*ci;
	int plane;
	ci = CharImageCreate(input_pointers[1]);
	di = CreateCharVolume(input_pointers[0], "tif");
	regmax = CharVolumeClone(di);
	sscanf(ostring, "%d", &plane);
	CharVolumeSetPlane (regmax, ci, plane);
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	di = CharVolumeReconstruct(di, regmax, repetitions);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_getp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharImage*ci;
	di = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeGetPlane (di, repetitions);
	CharImageWrite(ci, output_pointers[0]);
	return status;
}

int prostak_setp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharImage*ci;
	di = CreateCharVolume(input_pointers[0], "tif");
	ci = CharImageCreate (input_pointers[1]);
	CharVolumeSetPlane (di, ci, repetitions);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_strel3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int vsize;
	int hsize;
	int zsize;
	char *etype;
	char *ztype;
	StructElemType type;
	StructElemType typez;
	ztype = (char*)calloc(MAX_RECORD, sizeof(char));
	sscanf(ostring, "%d,%d,%d,%s", &vsize, &hsize, &zsize, ztype);
	etype = ztype;
	while ( *ztype != ',' ) *ztype++;
	*ztype++;
	if ( !strncmp(etype,"disk",4) )
		type = disk;
	else if ( !strncmp(etype,"square",6) )
		type = square;
	else
		g_error("prostak: wrong etype %s", etype);
	if ( !strcmp(ztype,"disk") )
		typez = disk;
	else if ( !strcmp(ztype,"square") )
		typez = square;
	else
		g_error("prostak: wrong ztype %s", ztype);
	StructElem3d(input_pointers[0], vsize, hsize, zsize, type, typez);
	return status;
}

int prostak_strel3dw(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int vsize;
	int hsize;
	int zsize;
	int width;
	char **opts;
	StructElemType type;
	StructElemType typez;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	width = atoi(opts[3]);
	vsize = atoi(opts[0]);
	hsize = atoi(opts[1]);
	zsize = atoi(opts[2]);
	if ( !strncmp(opts[4],"disk",4) )
		type = disk;
	else if ( !strncmp(opts[4],"square",6) )
		type = square;
	else
		g_error("prostak: wrong etype %s", opts[4]);
	if ( !strncmp(opts[5],"disk", 4) )
		typez = disk;
	else if ( !strncmp(opts[5],"square",6) )
		typez = square;
	else
		g_error("prostak: wrong ztype %s", opts[5]);
	StructElem3d_2(input_pointers[0], vsize, hsize, zsize, width, type, typez);
	g_strfreev(opts);
	return status;
}

int prostak_vstrel3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ci = CharVolumeStructElem(input_pointers[0]);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_distance3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if (planes > 1) {
		CharVolume*ci;
		ci = CreateCharVolume(input_pointers[0], "tif");
		ci = CharVolumeDistance(ci);
		CharVolumeWrite(ci, output_pointers[0], ".tif");
	} else {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageChampherDistance(di, repetitions);
		CharImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_restack3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ci = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeRestack(ci, repetitions);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_edge3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	double a1, a2;
	double low, high;
	uint32 segment, window;
	int connectivity;
	sscanf(ostring, "%lf,%lf,%lf,%lf,%u,%u,%d", &a1, &a2, &low, &high,&window,&segment,&connectivity);
	ci = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeEdgeShenCastan(ci, a1, a2, low, high, window, segment, connectivity);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_despeckle3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ci = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeDespeckle(ci);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_shape_select3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ShapeDescriptor3d*sd;
	CheckShapeType cst;
	char *cst_string;
	int MaxSegment;
	int MinSegment;
	cst_string = (char*)calloc(MAX_RECORD, sizeof(char));
	sscanf(ostring, "%d,%d,%s", &MaxSegment, &MinSegment, cst_string);
	if ( !strcmp(cst_string, "accept"))
		cst = shape_accept;
	else if ( !strcmp(cst_string, "reject"))
		cst = shape_reject;
	else
		g_error("shape_select3d: wrong rule (-s) %s", cst_string);
	free(cst_string);
	ci = CreateCharVolume(input_pointers[1], "tif");
	sd = CharVolume2ShapeDescriptor3d(ci, repetitions);
	CharVolumeDelete(ci);
	ci = CreateCharVolume(input_pointers[0], "tif");
	ci = CharVolumeShapeSelect(ci, sd, param, cst, repetitions, MaxSegment, MinSegment);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_shape_select_list3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ShapeDescriptor3d*sd;
	CheckShapeType cst;
	char *cst_string;
	int MaxSegment;
	int MinSegment;
	FILE*fp;
	cst_string = (char*)calloc(MAX_RECORD, sizeof(char));
	sscanf(ostring, "%d,%d,%s", &MaxSegment, &MinSegment, cst_string);
	if ( !strcmp(cst_string, "accept"))
		cst = shape_accept;
	else if ( !strcmp(cst_string, "reject"))
		cst = shape_reject;
	else
		g_error("shape_select3d_list: wrong rule (-s) %s", cst_string);
	free(cst_string);
	ci = CreateCharVolume(input_pointers[1], "tif");
	sd = CharVolume2ShapeDescriptor3d(ci, repetitions);
	CharVolumeDelete(ci);
	ci = CreateCharVolume(input_pointers[0], "tif");
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("can't open %s", output_pointers[0]);
/*	CharVolumeShapeSelectList(ci, sd, param, cst, repetitions, MaxSegment, MinSegment, fp);*/
	CharVolumeDelete(ci);
	fclose(fp);
	return status;
}

int prostak_vtxt3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	VolumetricFormat format;
	FILE*f;
	if ( !strcmp(ostring, "xyz") || !strcmp(ostring, "XYZ") )
		format = XYZ_VOLUMETRIC_FORMAT;
	else if ( !strcmp(ostring, "vrml") || !strcmp(ostring, "VRML") )
		format = VRML_VOLUMETRIC_FORMAT;
	else if ( !strcmp(ostring, "vrml_sphere") || !strcmp(ostring, "VRML_SPHERE") )
		format = VRML_VOLUMETRIC_FORMAT_SPHERE;
	else
		g_error("vtxt: wrong format (-s) %s", ostring);
	ci = CreateCharVolume(input_pointers[0], "tif");
	f = fopen(output_pointers[0],"w");
	if ( !f )
		g_error("can't open %s", output_pointers[0]);
	CharVolumePrintText(ci, f, repetitions, format);
	fclose(f);
	return status;
}

#ifndef G_OS_WIN32
int prostak_surf3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	double scale_column, scale_row, scale_plane;
	double max_cost;
	gdouble minangle;
	char*format;
	format = (char*)calloc(MAX_RECORD, sizeof(char));
	sscanf(ostring, "%lf,%lf,%lf,%lf,%lf,%s", &scale_column, &scale_row, &scale_plane, &max_cost, &minangle, format);
	ci = CreateCharVolume(input_pointers[0], "tif");
	CharVolumeIsosurface(ci, max_cost, minangle, output_pointers[0], format, scale_column, scale_row, scale_plane);
	CharVolumeDelete(ci);
	free(format);
	return status;
}

int prostak_surf3dfull(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	double scale_column, scale_row, scale_plane, xx, yy, zz;
	double max_cost;
	gdouble minangle;
	char*format, *hint, **opts;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	scale_column = atof(opts[0]);
	scale_row = atof(opts[1]);
	scale_plane = atof(opts[2]);
	minangle = atof(opts[3]);
	max_cost = atof(opts[4]);
	format = g_strdup(opts[5]);
	xx = atof(opts[6]);
	yy = atof(opts[7]);
	zz = atof(opts[8]);
	hint = g_strdup(opts[9]);
	ci = CreateCharVolume(input_pointers[0], "tif");
	CharVolumeIsosurfaceFull(ci, max_cost, minangle, output_pointers[0], format, scale_column, scale_row, scale_plane, xx, yy, zz, hint);
	CharVolumeDelete(ci);
	free(format);
	free(hint);
	return status;
}

int prostak_surf2vol(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ci = Isosurface2CharVolume(input_pointers[0]);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	CharVolumeDelete(ci);
	return status;
}
#endif

int prostak_pump3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	CharImage*distmap;
	CharImage*data = NULL;
	distmap = CharImageCreate(input_pointers[0]);
	if ( n_input_pointers == 2 ) {
		data = CharImageCreate(input_pointers[1]);
	}
	ci = CharVolumePump(distmap, data);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	CharVolumeDelete(ci);
	return status;
}

int prostak_shape3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*ci;
	ShapeDescriptor3d*sd;
	char*grp, *buf;
	GKeyFile*gkf;
	GError*gerror = NULL;
	gsize length;
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	ci = CreateCharVolume(input_pointers[0], "tif");
	sd = CharVolume2ShapeDescriptor3d(ci, repetitions);
	buf = g_path_get_basename(input_pointers[0]);
	grp = g_strdup_printf("Shape:%s", buf);
	g_free(buf);
	gkf = g_key_file_new();
	ShapeDescriptor3dPrint(gkf, sd, grp);
	g_free(grp);
	buf = (char*)g_key_file_to_data(gkf, &length, &gerror);
	if ( gerror || length == 0 || !buf )
		g_error("Error printing sd");
	if ( !g_file_set_contents(output_pointers[0], buf, (gssize) length, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_free(buf);
	CharVolumeDelete(ci);
	ShapeDescriptor3dDelete (sd);
	g_key_file_free(gkf);
	return status;
}

int prostak_qu3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*mask;
	GKeyFile*gkf;
	GFile*gfile;
	char*contents, *etag_out;
	int length;
	int adding;
	gsize size;
	GError *gerror = NULL;
	mask = CreateCharVolume(input_pointers[1], "tif");
	di = CreateCharVolume(input_pointers[0], "tif");
	gkf = g_key_file_new();
	adding = 0;
	if ( n_input_pointers > 2 && input_pointers[2] ) {
		gfile = g_file_new_for_commandline_arg(input_pointers[2]);
		if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
			if ( gerror ) {
				g_error("prostak_qu3d:%s", gerror->message);
			}
			g_error("prostak_qu3d: unknown error");
		}
		if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, &gerror ) ) {
			g_warning("prostak_qu3d:%s", gerror->message);
			g_error_free(gerror);
			g_free(contents);
			g_free(etag_out);
			g_object_unref(gfile);
		} else {
			adding = 1;
			g_free(contents);
			g_free(etag_out);
			g_object_unref(gfile);
		}
	}
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	CharVolumeQu(mask, di, gkf, ostring, repetitions, adding);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("Error printing sd");
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	if ( !g_file_replace_contents(gfile, contents, size, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	g_object_unref(gfile);
	return status;
}

int prostak_vu(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int ncycles = (int)param;
	CharVolume*di;
	CharVolume*mask;
	GKeyFile*gkf;
	GFile*gfile;
	char*contents, *etag_out, **options, *channel;
	int length, max_vert;
	gsize size;
	GError *gerror = NULL;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_vu option error");
	}
	length = g_strv_length(options);
	if ( length < 3 ) {
		g_error("prostak_vu option error");
	}
	channel = g_strdup(options[0]);
	max_vert = atoi(options[1]);
	double alpha = atof(options[2]);
	g_strfreev(options);
	mask = CreateCharVolume(input_pointers[1], "tif");
	di = CreateCharVolume(input_pointers[0], "tif");
	gkf = g_key_file_new();
	if ( n_input_pointers > 2 && input_pointers[2] ) {
		gfile = g_file_new_for_commandline_arg(input_pointers[2]);
		if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
			if ( gerror ) {
				g_error("prostak_vu:%s", gerror->message);
			}
			g_error("prostak_vu: unknown error");
		}
		if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, &gerror ) ) {
			g_warning("prostak_vu:%s", gerror->message);
			g_error_free(gerror);
			g_free(contents);
			g_free(etag_out);
			g_object_unref(gfile);
		}
	}
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	CharVolumeVu(mask, di, gkf, channel, repetitions, ncycles, max_vert, alpha);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("Error printing sd");
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	if ( !g_file_replace_contents(gfile, contents, size, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	g_object_unref(gfile);
	g_free(channel);
	return status;
}

int prostak_quremask(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*mask;
	GKeyFile*gkf;
	GFile*gfile;
	char*contents, *etag_out;
	int length;
	gsize size;
	GError *gerror = NULL;
	mask = CreateCharVolume(input_pointers[1], "tif"); /* main mask */
	di = CreateCharVolume(input_pointers[0], "tif"); /* submask */
	gkf = g_key_file_new();
	gfile = g_file_new_for_commandline_arg(input_pointers[2]); /* related to submask */
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
		if ( gerror ) {
			g_error("prostak_quremask:%s", gerror->message);
		}
		g_error("prostak_quremask: unknown error");
	}
	if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_warning("prostak_quremask:%s", gerror->message);
		g_error_free(gerror);
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	} else {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	}
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	char_volume_label_mask(gkf, mask, repetitions, ostring, di, &gerror);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("Error quremask");
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	if ( !g_file_replace_contents(gfile, contents, size, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	g_object_unref(gfile);
	return status;
}

int prostak_quresort(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*mask;
	GKeyFile*gkf;
	GFile*gfile;
	char*contents, *etag_out, **options, *channel, *newch, *refch;
	int length, index, ascending;
	gsize size;
	GError *gerror = NULL;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_qumap3d option error");
	}
	length = g_strv_length(options);
	if ( length < 4 ) {
		g_error("prostak_qumap3d option error");
	}
	channel = g_strdup(options[0]);
	index = atoi(options[1]);
	refch = g_strdup(options[2]);
	newch = g_strdup(options[3]);
	ascending = atoi(options[4]);
	g_strfreev(options);
	mask = CreateCharVolume(input_pointers[0], "tif"); /* main mask */
	gkf = g_key_file_new();
	gfile = g_file_new_for_commandline_arg(input_pointers[1]); /* related to submask */
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
		if ( gerror ) {
			g_error("prostak_quresort:%s", gerror->message);
		}
		g_error("prostak_quresort: unknown error");
	}
	if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_warning("prostak_quresort:%s", gerror->message);
		g_error_free(gerror);
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	} else {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	}
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	di = CharVolumeQuSort(gkf, mask, repetitions, channel, index, refch, newch, ascending, &gerror);
	if ( gerror )
		g_error(gerror->message);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("Error quresort");
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	if ( !g_file_replace_contents(gfile, contents, size, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	g_object_unref(gfile);
	CharVolumeWrite(di, output_pointers[1], ".tif");
	return status;
}

int prostak_quthicken(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int connectivity, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, maxiter;
	CharVolume*mask;
	sscanf(ostring, "%d", &maxiter);
	mask = CharVolumeCreate(input_pointers[0]);
	mask = char_volume_dilate_mask(mask, connectivity, maxiter, NULL);
	CharVolumeWrite(mask, output_pointers[0], ".tif");
	return status;
}

int prostak_querode(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int connectivity, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, maxiter;
	CharVolume*mask;
	sscanf(ostring, "%d", &maxiter);
	mask = CharVolumeCreate(input_pointers[0]);
	mask = char_volume_erode_mask(mask, connectivity, maxiter, NULL);
	CharVolumeWrite(mask, output_pointers[0], ".tif");
	return status;
}

int prostak_zscale3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di, *ci;
	int i;
	di = CharVolumeCreate(input_pointers[0]);
	ci = CharVolumeLeftView(di);
	CharVolumeDelete (di);
	if ( param > 1 ) {
		di = CharVolumeResample(ci, 1.0, param);
	} else if ( param > 0 || param <= 1 ) {
		di = CharVolumeShrink(ci, 1.0, param);
	} else {
		g_error("Wrong factor %f", param);
	}
	CharVolumeDelete (ci);
	ci = CharVolumeRightView (di);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	return status;
}

int prostak_qu3d2csv(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	GKeyFile*gkf;
	char*contents, *etag_out;
	int length;
	GError*gerror = NULL;
	GFile*gfile;
	gsize size;
	gfile = g_file_new_for_commandline_arg(input_pointers[0]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, &gerror) ) {
		if ( gerror ) {
			g_error("prostak_qu3d2csv:%s", gerror->message);
		}
		g_error("prostak_qu3d2csv: unknown error");
	}
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_error("prostak_qu3d2csv:%s", gerror->message);
		g_error_free(gerror);
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	} else {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	}
	CharVolumeQuPrint(gkf, output_pointers[0]);
	g_key_file_free(gkf);
	return status;
}

int prostak_apee3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**apee;
	CharVolume*di;
	apee = prostak_get_info_array(input_pointers[1]);
	di = CharVolumeCreate(input_pointers[0]);
	if ( g_str_has_prefix(apee[1], "transpose") ) {
		di = CharVolumeTranspose(di);
	}
	if ( g_str_has_prefix(apee[2], "reversecolumn") ) {
		di = CharVolumeReverseColumn(di);
	}
	if ( g_str_has_prefix(apee[3], "reverserow") ) {
		di = CharVolumeReverseRow(di);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_vvarbc3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	GKeyFile*gkf;
	char*contents;
	char**options,*output_group, *class_1_group, *class_2_group, *key_1, *key_2;
	int length;
	GError*gerror = NULL;
	gsize size;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( length < 5 ) {
		g_error("prostak::decwiener option error");
	}
	output_group = g_strdup(options[0]);
	class_1_group = g_strdup(options[1]);
	key_1 = g_strdup(options[2]);
	class_2_group = g_strdup(options[3]);
	key_2 = g_strdup(options[4]);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[0], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_vvarbc3d:%s", gerror->message);
		} else {
			g_error("prostak_vvarbc3d: error");
		}
	}
	vvarbc(gkf, output_group, class_1_group, class_2_group, key_1, key_2);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("Error printing sd");
	if ( !g_file_set_contents(output_pointers[0], contents, (gssize) size, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	return status;
}

int prostak_grid3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	GKeyFile*gkf;
	char**options,*shape_group, *type;
	double ns_rows, ns_columns, ns_planes;
	double xmean, ymean, zmean, eps, offset;
	GError*gerror = NULL;
	gsize size;
	CharVolume*di;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_grid3d option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 9 ) {
			g_error("prostak_grid3d option error");
		}
		type = g_strdup(options[0]);
		ns_rows = atof(options[1]);
		ns_columns = atof(options[2]);
		ns_planes = atof(options[3]);
		offset = atof(options[4]);
		eps = atof(options[5]);
		xmean = atof(options[4]);
		ymean = atof(options[5]);
		zmean = atof(options[6]);
	} else if ( n_input_pointers == 2 ) {
		if ( length < 7 ) {
			g_error("prostak_grid3d option error");
		}
		type = g_strdup(options[0]);
		ns_rows = atof(options[1]);
		ns_columns = atof(options[2]);
		ns_planes = atof(options[3]);
		offset = atof(options[4]);
		eps = atof(options[5]);
		shape_group = g_strdup(options[6]);
		if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
			if ( gerror ) {
				g_error("prostak_grid3d:%s", gerror->message);
			} else {
				g_error("prostak_grid3d: error");
			}
		}
		shape_descriptor_get_data_field(gkf, shape_group, "xmean", &xmean, 0, &gerror);
		if ( gerror ) {
			g_error("prostak_grid3d:%s", gerror->message);
		}
		shape_descriptor_get_data_field(gkf, shape_group, "ymean", &ymean, 0, &gerror);
		if ( gerror ) {
			g_error("prostak_grid3d:%s", gerror->message);
		}
		shape_descriptor_get_data_field(gkf, shape_group, "zmean", &zmean, 0, &gerror);
		if ( gerror ) {
			g_error("prostak_grid3d:%s", gerror->message);
		}
		g_key_file_free(gkf);
	}
	di = CharVolumeCreate(input_pointers[0]);
	if ( g_str_has_prefix(type, "cartesian") ) {
		di = CharVolumeGrid(di, ns_rows, ns_columns, ns_planes, xmean, ymean, zmean);
	} else if ( g_str_has_prefix(type, "elliptical") ) {
		di = CharVolumeGridElliptical(di, ns_rows, ns_columns, ns_planes, offset, xmean, ymean, zmean, eps);
	} else {
		g_error("prostak_grid3d: wrong type %s", type);
	}
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}

int prostak_qu3dtrans(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*contents;
	char**options,*shape_group;
	int center, percent, length;
	double minvolume, maxvolume;
	ShapeDescriptor3d*mask;
	GKeyFile*gkf;
	gsize size;
	GError *gerror = NULL;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_grid3dtrans option error");
	}
	length = g_strv_length(options);
	if ( length < 5 ) {
		g_error("prostak_grid3dtrans option error");
	}
	shape_group = g_strdup(options[0]);
	center = atoi(options[1]);
	percent = atoi(options[2]);
	minvolume = atof(options[3]);
	maxvolume = atof(options[4]);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_grid3dtrans:%s", gerror->message);
		} else {
			g_error("prostak_grid3dtrans: error");
		}
	}
	mask = shape_descriptor_3d_read(gkf, shape_group, &gerror);
	g_key_file_free(gkf);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[0], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_grid3dtrans:%s", gerror->message);
		} else {
			g_error("prostak_grid3dtrans: error");
		}
	}
	CharVolumeQuTransform(gkf, center, percent, mask, minvolume, maxvolume, &gerror);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("grid3dtrans: Error printing sd");
	if ( !g_file_set_contents(output_pointers[0], contents, (gssize) size, &gerror) )
		g_error("grid3dtrans: Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	return status;
}

int prostak_mslice3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, length;
	GError*gerror = NULL;
	GKeyFile*gkf;
	CharVolume*ci;
	CharVolume*distmap;
	ShapeDescriptor3d*mask;
	int direction;
	double ns;
	char*shape_group, **options;
	distmap = CharVolumeCreate(input_pointers[0]);
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_mslice3d option error");
	}
	length = g_strv_length(options);
	if ( length < 3 ) {
		g_error("prostak_mslice3d option error");
	}
	shape_group = g_strdup(options[0]);
	ns = atof(options[1]);
	direction = atoi(options[2]);
	g_strfreev(options);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_mslice3d:%s", gerror->message);
		} else {
			g_error("prostak_mslice3d: error");
		}
	}
	mask = shape_descriptor_3d_read(gkf, shape_group, &gerror);
	g_key_file_free(gkf);
	ci = CharVolumeMultiSlice(distmap, mask, ns, direction);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	CharVolumeDelete(ci);
	return status;
}

int prostak_mslicegcp3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, length;
	GError*gerror = NULL;
	GKeyFile*gkf;
	CharVolume*ci;
	CharVolume*distmap;
	ShapeDescriptor3d*mask;
	int direction, ns_direction;
	double ns, ns_rows, ns_columns, ns_planes;
	char*shape_group, **options;
	distmap = CharVolumeCreate(input_pointers[0]);
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_mslice3d option error");
	}
	length = g_strv_length(options);
	if ( length < 7 ) {
		g_error("prostak_mslice3d option error");
	}
	shape_group = g_strdup(options[0]);
	ns = atof(options[1]);
	direction = atoi(options[2]);
	ns_direction = atoi(options[3]);
	ns_rows = atof(options[4]);
	ns_columns = atof(options[5]);
	ns_planes = atof(options[6]);
	g_strfreev(options);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_mslice3d:%s", gerror->message);
		} else {
			g_error("prostak_mslice3d: error");
		}
	}
	mask = shape_descriptor_3d_read(gkf, shape_group, &gerror);
	g_key_file_free(gkf);
	ci = CharVolumeMultiSliceGCP(distmap, mask, ns, direction, ns_direction, ns_rows, ns_columns, ns_planes);
	CharVolumeWrite(ci, output_pointers[0], ".tif");
	CharVolumeDelete(ci);
	return status;
}

int prostak_qumark3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*mask;
	int length;
	gsize size;
	GError *gerror = NULL;
	mask = CreateCharVolume(input_pointers[0], "tif");
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak connectivity must be 6 or 26 (-r )!");
	di = CharVolumeQuMark(mask, repetitions);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	CharVolumeDelete(di);
	return status;
}

int prostak_qumap3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options, *channel;
	CharVolume*di;
	CharVolume*mask;
	int length, index;
	gsize size;
	GError *gerror = NULL;
	GKeyFile*gkf;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_qumap3d option error");
	}
	length = g_strv_length(options);
	if ( length < 2 ) {
		g_error("prostak_qumap3d option error");
	}
	channel = g_strdup(options[0]);
	index = atoi(options[1]);
	g_strfreev(options);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_qumap3d:%s", gerror->message);
		} else {
			g_error("prostak_qumap3d: error");
		}
	}
	mask = CreateCharVolume(input_pointers[0], "tif");
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak qumap3d connectivity must be 6 or 26 (-r )!");
	di = CharVolumeQuMap(gkf, mask, repetitions, channel, index, &gerror);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	g_key_file_free(gkf);
	CharVolumeDelete(di);
	return status;
}

int prostak_qurelabel(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options, *type, *contents;
	CharImage*landmarks, *landmarks_fixed;
	CharVolume*mask;
	int length, index;
	double alpha;
	gsize size;
	GError *gerror = NULL;
	GKeyFile*gkf;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak_qurelabel option error");
	}
	length = g_strv_length(options);
	if ( length < 2 ) {
		g_error("prostak_qurelabel option error");
	}
	type = g_strdup(options[0]);
	alpha = atof(options[1]);
	g_strfreev(options);
	if ( ( gkf = g_key_file_load_from_cmdarg(input_pointers[1], &gerror) ) == NULL ) {
		if ( gerror ) {
			g_error("prostak_qurelabel:%s", gerror->message);
		} else {
			g_error("prostak_qurelabel: error");
		}
	}
	mask = CreateCharVolume(input_pointers[0], "tif");
	landmarks_fixed = CharImageCreate(input_pointers[2]);
	landmarks = CharImageCreate(input_pointers[3]);
	if ( repetitions != 6 && repetitions != 26 )
		g_error("prostak qumap3d connectivity must be 6 or 26 (-r )!");
	char_volume_relabel_blobs(gkf, mask, repetitions, landmarks_fixed, landmarks, type, alpha, &gerror);
	contents = (char*)g_key_file_to_data(gkf, &size, &gerror);
	if ( gerror || size == 0 || !contents )
		g_error("grid3dtrans: Error printing sd");
	if ( !g_file_set_contents(output_pointers[0], contents, (gssize) size, &gerror) )
		g_error("grid3dtrans: Error printing sd: %s", gerror->message);
	g_key_file_free(gkf);
	g_free(contents);
	CharVolumeDelete(mask);
	return status;
}

int prostak_pbreak (int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*di_1;
	CharVolume*di_2;
	int i;
	di = CreateCharVolume(input_pointers[0], "tif");
	CharVolumePBreak(di, repetitions, &di_1, &di_2);
	CharVolumeWrite(di_1, output_pointers[0], ".tif");
	CharVolumeWrite(di_2, output_pointers[1], ".tif");
	return status;
}

int prostak_punite (int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharVolume*di;
	CharVolume*di_1;
	CharVolume*di_2;
	int i;
	di_1 = CreateCharVolume(input_pointers[0], "tif");
	di_2 = CreateCharVolume(input_pointers[1], "tif");
	di = CharVolumePUnite(di_1, di_2);
	CharVolumeWrite(di, output_pointers[0], ".tif");
	return status;
}
