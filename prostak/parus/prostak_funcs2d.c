/***************************************************************************
 *            prostak_funcs2d.c
 *
 *  Sat Oct  3 21:28:29 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>

#include <CharImage.h>
#include <CharVolume.h>
#include <ImageInfo.h>
#include <Pattern.h>

int prostak_intense(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageScaleIntensity(di, param);
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_affine(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	Kernel *kern;
	if ( !ostring )
		g_error("prostak: parameters are not set!");
	kern = KernelFromString(ostring);
	di = CharImageCreate(input_pointers[0]);
	di = CharImageIntensityPolyKern(di, kern);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_max(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = char_image_max(input_pointers, n_input_pointers);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = super_image_compose(input_pointers, n_input_pointers, "max");
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_avg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = char_image_average(input_pointers, n_input_pointers);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = super_image_compose(input_pointers, n_input_pointers, "avg");
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_plus(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	di = CharImageComposePlusN(input_pointers[0], "tif");
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_minusabs(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*w;
	di = CharImageCreate(input_pointers[0]);
	w = CharImageCreate(input_pointers[1]);
	di = CharImageComposeMinusAbs(di,w);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_minus(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*w;
	CharImage*vm;
	CharImage*vp;
	di = CharImageCreate(input_pointers[0]);
	w = CharImageCreate(input_pointers[1]);
	vp = CharImageClone(di);
	vm = CharImageClone(di);
	CharImageComposeMinus(di,w,vm,vp);
	CharImageWrite(vm,output_pointers[0]);
	CharImageWrite(vp,output_pointers[1]);
	CharImageDelete(vm);
	CharImageDelete(vp);
	return status;
}

int prostak_drydilation(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	for ( i = 0; i < repetitions; i++ ) {
		di = CharImageDryDilation(di);
	}
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_threshold_blobs(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di, *ci;
	BlobImage*bi;
	FILE*f;
	di = CharImageCreate(input_pointers[0]);
	f = fopen(input_pointers[1], "r");
	if ( !f )
		g_error("prostak: threshold_b can't open %s", input_pointers[1]);
	bi = BlobImageRead(f);
	fclose(f);
	ci = processCharImageByBlobs(di, bi);
	CharImageWrite(ci, output_pointers[0]);
	return status;
}

int prostak_threshold(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double threshold;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	if ( !strncmp(ostring, "histmax", 7) ) {
		double a, b;
		sscanf(&ostring[8],"%lf,%lf", &a, &b);
		threshold = CharImageThresholdHist(di, a, b);
	} else if ( !strncmp(ostring, "otsu", 4) ) {
		threshold = CharImageThresholdHist1(di);
	} else if ( !strncmp(ostring, "paramhalfmean", 13) ) {
		Attributes*dia;
		dia = CharAnalyze(di->pixels,di->number);
		threshold = param * dia->mean / 2;
	} else if ( !strncmp(ostring, "plain", 5) ) {
		threshold = param;
	} else {
		g_error("prostak threshold unknown rule %s", ostring);
	}
	if ( repetitions == 1 ) {
		CharImageThreshold(di, threshold);
		CharImageWrite(di, output_pointers[0]);
	}
	fp = fopen(output_pointers[1], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[1]);
	fprintf(fp,"<threshold>\n");
	fprintf(fp,"%f\n", threshold);
	fprintf(fp,"</threshold>\n");
	fclose(fp);
	return status;
}

int prostak_hysteresis_threshold(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0, i;
	CharImage*di;
	double threshold_low, threshold_high, *buffer;
	int segment;
	int connectivity;
	uint32 rows, columns, planes;
	sscanf(ostring,"%lf,%lf,%d,%d", &threshold_low, &threshold_high, &segment, &connectivity);
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if (planes > 1) {
		CharVolume*dp;
		dp = CreateCharVolume(input_pointers[0], "tif");
		dp = CharVolumeBPHysThresh(dp, threshold_low, threshold_high, (uint32) segment, connectivity);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		di = CharImageCreate(input_pointers[0]);
		buffer = (double*)calloc(di->number, sizeof(double));
		for ( i = 0; i < di->number; i++ ) {
			buffer[i] = (double)(di->pixels[i]);
		}
		HysteresisThreshold(di, buffer, threshold_low, threshold_high, (uint32) segment, connectivity);
		CharImageWrite(di, output_pointers[0]);
		free(buffer);
	}
	return status;
}

int prostak_rotate(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	ShapeDescriptor *sd;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(di);
	di = CharImageRotate(di, sd->rotation_angle);
	CharImageWrite(di, output_pointers[0]);
	fp = fopen(output_pointers[1], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[1]);
	fprintf(fp,"<rotate>\n");
	fprintf(fp,"%9.6f\n", sd->rotation_angle);
	fprintf(fp,"</rotate>\n");
	ShapeDescriptorDelete(sd);
	fclose(fp);
	return status;
}

int prostak_turn(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	double angle_in_radians;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf", &angle_in_radians);
		free(ops);
	} else {
		sscanf(ostring, "%lf", &angle_in_radians);
	}
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageRotate(di, angle_in_radians);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImageRotate(di, angle_in_radians);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_align(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	ImageFeatureHalfAxis*difha;
	int i;
	double dot;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	difha = CharImageFindFeatureHalfAxis(di);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<align>\n");
	if ( difha->Cminus + difha->Cplus < difha->Rminus + difha->Rplus ) {
		fprintf(fp,"transpose\n");
		dot = difha->Cminus;
		difha->Cminus = difha->Rminus;
		difha->Rminus = dot;
		dot = difha->Cplus;
		difha->Cplus = difha->Rplus;
		difha->Rplus = dot;
		dot = difha->Cr;
		difha->Cr = difha->Cc;
		difha->Cc = dot;
	} else {
		fprintf(fp,"-\n");
	}
	if ( difha->Cminus < difha->Cplus ) {
		fprintf(fp,"reversecolumn\n");
		dot = difha->Cminus;
		difha->Cminus = difha->Cplus;
		difha->Cplus = dot;
	} else {
		fprintf(fp,"-\n");
	}
	if ( difha->Rminus > difha->Rplus ) {
		fprintf(fp,"reverserow\n");
		dot = difha->Rminus;
		difha->Rminus = difha->Rplus;
		difha->Rplus = dot;
	} else {
		fprintf(fp,"-\n");
	}
	fprintf(fp,"</align>\n");
	fclose(fp);
	return status;
}

int prostak_chemar(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	HalfImage*hi;
	int i;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	hi = CharImageFindHalfImage(di);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<chemar>\n");
	if ( hi->L < hi->R ) {
		fprintf(fp,"R\n");
	} else {
		fprintf(fp,"L\n");
	}
	fprintf(fp,"%.4f %.4f\n",hi->L, hi->R);
	fprintf(fp,"</chemar>\n");
	fclose(fp);
	free(hi);
	return status;
}

int prostak_reverserow(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageReverseRow(di);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImageReverseRow(di);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_reversecolumn(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageReverseColumn(di);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImageReverseColumn(di);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_transpose(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageTranspose(di);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImageTranspose(di);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_crop(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageCropFeature(di,&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	fp = fopen(output_pointers[1],"w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[1]);
	fprintf(fp,"<crop>\n");
	fprintf(fp,"%u,%u,%u,%u\n",UpperOffset,LowerOffset,LeftOffset,RightOffset);
	fprintf(fp,"</crop>\n");
	fclose(fp);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_halfsizes(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	FILE*fp;
	di = CharImageReadDim(input_pointers[0]);
	UpperOffset = (uint32)( 0.5 * (double)di->rows );
	LowerOffset = di->rows - UpperOffset;
	LeftOffset = (uint32)( 0.5 * (double)di->columns );
	RightOffset = di->columns - LeftOffset;
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<halfsizes>\n");
	fprintf(fp,"%u,%u,%u,%u\n",UpperOffset,LowerOffset,LeftOffset,RightOffset);
	fprintf(fp,"</halfsizes>\n");
	fclose(fp);
	return status;
}

int prostak_geometry(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
		free(ops);
	} else {
		sscanf(ostring, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	}
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImageCropGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImageCropGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_raw(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	uint32 columns, rows;
	int bps;
	if ( !ostring )
		g_error("prostak geometry is not set!");
	sscanf(ostring,"%u,%u,%d",&columns, &rows, &bps);
	di = CharImageCreateRaw(input_pointers[0], columns, rows, bps);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_pad(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	char*ops;
	if ( !ostring ) {
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
		free(ops);
	} else {
		sscanf(ostring, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	}
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		di = CharImagePadGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		di = SuperImagePadGeometry(di,UpperOffset, LowerOffset, LeftOffset, RightOffset);
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_erase(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double RowsPercent;
	double ColumnsPercent;
	int RowSign;
	int ColumnSign;
	char sc, sr;
	if ( !ostring )
		g_error("prostak area to erase is not set!");
	di = CharImageCreate(input_pointers[0]);
	sscanf(ostring,"%lf,%c,%lf,%c",&RowsPercent, &sr, &ColumnsPercent, &sc);
	RowSign = ( sr == '+' ) ? 1 : -1;
	ColumnSign = ( sc == '+' ) ? 1 : -1;
	di = CharImageErase(di,RowsPercent,RowSign, ColumnsPercent, ColumnSign);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_distance(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if (planes > 1) {
		CharVolume*dp;
		dp = CreateCharVolume(input_pointers[0], "tif");
		dp = CharVolumeBPDistance(dp, repetitions);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		di = CharImageCreate(input_pointers[0]);
		di = CharImageChampherDistance(di, repetitions);
		CharImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_distance_g(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	if ( !ostring )
		g_error("gdistance: arg not set");
	di = CharImageCreate(input_pointers[0]);
	di = CharImageGDistance(di, repetitions, ostring);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_distance_x(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageChampherXDistance(di, repetitions);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_distance_y(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageChampherYDistance(di, repetitions);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_gmag(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageEdge(di);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_edgesc(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double a1, a2;
	double low, high;
	uint32 segment, window;
	int connectivity;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	sscanf(ostring, "%lf,%lf,%lf,%lf,%u,%u,%d", &a1, &a2, &low, &high,&window,&segment,&connectivity);
	if (planes > 1) {
		CharVolume*dp;
		dp = CreateCharVolume(input_pointers[0], "tif");
		dp = CharVolumeBPShenCastan92(dp, a1, a2, low, high, window, segment, connectivity);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		di = CharImageCreate(input_pointers[0]);
		di = CharImageEdgeShenCastan92(di, a1, a2, low, high, window, segment, connectivity);
		CharImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_fill(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	if ( repetitions != 4 && repetitions != 8 )
		g_error("prostak connectivity must be 4 or 8 (-r )!");
	di = CharImageFillFeature(di, repetitions, ( ( (int)param + 1 ) % 2 ) );
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_invert(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	if ( n_input_pointers == 2 ) {
		CharImage*maskimage;
		maskimage = CharImageCreate(input_pointers[1]);
		di = CharImageInvertWithMask(di,maskimage);
	} else {
		di = CharImageInvert(di);
	}
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_grid(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageGrid(di, repetitions);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_ropol_grid(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di, *ci, *dj;
	ShapeDescriptor*sd;
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	di = CharImageCreate(input_pointers[1]);
	dj = CharImageRoPolGrid(di, repetitions, param, sd->xmean, sd->ymean);
	ShapeDescriptorDelete(sd);
	CharImageWrite(dj, output_pointers[0]);
	return status;
}

int prostak_invert_n(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	if ( n_input_pointers == 2 ) {
		CharImage*maskimage;
		maskimage = CharImageCreate(input_pointers[1]);
		di = CharImageInvertNWithMask(di,maskimage);
	} else {
		di = CharImageInvertN(di);
	}
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_cross(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*maskimage;
	int i;
	di = CharImageCreate(input_pointers[0]);
	maskimage = CharImageCreate(input_pointers[1]);
	di = CharImageCrossMask(di,maskimage);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_mask(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*maskimage;
	int i;
	di = CharImageCreate(input_pointers[0]);
	maskimage = CharImageCreate(input_pointers[1]);
	di = CharImageMask(di,maskimage);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_pardec(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	BlobImage*bi;
	int window;
	double sigma_a;
	double sigma_b;
	char*psf_type;
	int iter_max;
	double criterion;
	BlobImage*bimask;
	int log_flag;
	FILE*fp;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options || g_strv_length(options) < 6 )
		g_error("prostak::pardec option error");
	sigma_a = atof(options[0]);
	sigma_b = atof(options[1]);
	iter_max = atoi(options[2]);
	criterion = atof(options[3]);
	log_flag = atoi(options[4]);
	psf_type = options[5];
	si = SuperImageCreate(input_pointers[0]);
	bi = BlobImageCreate(input_pointers[1]);
	bimask = BlobImageCreate(input_pointers[2]);
	step_image = super_image_get_microscope_parameter(&msigma, si, bi, &window, sigma_a, sigma_b, psf_type, iter_max, criterion, bimask, log_flag);
	SuperImageWrite(step_image, output_pointers[0]);
	fp = fopen(output_pointers[1], "w");
	if ( !fp )
		g_error("prostak: pardec can't open %s", output_pointers[1]);
	fprintf(fp, "<pardec>\n%.16f,%d\n<\\pardec>", msigma, window);
	fclose(fp);
	g_strfreev(options);
	return status;
}

int prostak_decsup(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	int window;
	char*psf_type, *ops;
	int iter_max;
	double criterion, lambda;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 5 ) {
			g_error("prostak::decsup option error");
		}
		iter_max = atoi(options[0]);
		criterion = atof(options[1]);
		lambda = atof(options[2]);
		psf_type = options[3];
		msigma = atof(options[4]);
		if ( length == 6 ) {
			window = atoi(options[5]);
		} else {
			window = -1;
		}
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 4 ) {
			g_error("prostak::decsup option error");
		}
		iter_max = atoi(options[0]);
		criterion = atof(options[1]);
		lambda = atof(options[2]);
		psf_type = options[3];
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf,%d",&msigma, &window);
		free(ops);
	} else {
		g_error("prostak::decsup option error");
	}
	si = SuperImageCreate(input_pointers[0]);
	step_image = super_image_deconvolve_with_analytical_psf(si, &window, msigma, psf_type, iter_max, lambda, criterion);
	SuperImageWrite(step_image, output_pointers[0]);
	g_strfreev(options);
	return status;
}

int prostak_info(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	FILE*fp;
	uint32 rows;
	uint32 columns;
	int bits;
	int format;
	grf_read_info(input_pointers[0], &rows, &columns, &bits, &format);
	fp = fopen(output_pointers[0],"w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"%s: rows = %u, columns = %u, bits = %d, format = %d\n", output_pointers[0], rows, columns, bits, format);
	fclose(fp);
	return status;
}

int prostak_cnm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*maskimage;
	int i;
	di = CharImageCreate(input_pointers[0]);
	if ( n_input_pointers != 2 ) {
		g_error("prostak mask's not defined!");
	}
	di = CharImageCreate(input_pointers[0]);
	maskimage = CharImageCreate(input_pointers[1]);
	CharImageCompMasks(di,maskimage, output_pointers[0]);
	return status;
}

int prostak_heq(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			mask = CreateCharVolume(input_pointers[1], "tif");
			dp = CharVolumeBPHeqM(dp, mask);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			CharImage*maskimage;
			di = CharImageCreate(input_pointers[0]);
			maskimage = CharImageCreate(input_pointers[1]);
			di = CharImageHistogramEquilizeWithMask(di,maskimage);
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPHeq(dp);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			di = CharImageHistogramEquilize(di,0,0,di->rows,di->columns);
			CharImageWrite(di,output_pointers[0]);
		}
	}
	return status;
}

int prostak_hno(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			mask = CreateCharVolume(input_pointers[1], "tif");
			dp = CharVolumeBPHeqM(dp, mask);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			CharImage*maskimage;
			di = CharImageCreate(input_pointers[0]);
			maskimage = CharImageCreate(input_pointers[1]);
			di = CharImageHistogramEquilizeWithMask(di,maskimage);
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if (planes > 1) {
			CharVolume*dp;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPNormalize(dp);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			di = CharImageHistogramNormalize(di);
			CharImageWrite(di,output_pointers[0]);
		}
	}
	return status;
}

int prostak_lheq(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPLHeqWithSE(dp, input_pointers[1]);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			for ( i = 0; i < repetitions; i++ ) {
				di = CharImageLHeqWithSE(di, input_pointers[1]);
			}
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageLHeqSE(di, vsize, hsize, type);
		}
		CharImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_lhbg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int vsize;
	int hsize;
	char *etype;
	uint32 rows, columns, planes;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*di;
			di = CreateCharVolume(input_pointers[0], "tif");
			di = CharVolumeBPLHBGWithSE(di, input_pointers[1]);
			CharVolumeWrite(di, output_pointers[0], ".tif");
		} else {
			CharImage*di;
			di = CharImageCreate(input_pointers[0]);
			di = CharImageLHBGWithSE(di, input_pointers[1]);
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		g_error("prostak stractural element is not set!");
	}
	return status;
}

int prostak_chole(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	uint32 rows, columns, planes;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*di;
			di = CreateCharVolume(input_pointers[0], "tif");
			di = CharVolumeBPCHolWithSE(di, input_pointers[1]);
			CharVolumeWrite(di, output_pointers[0], ".tif");
		} else {
			CharImage*di;
			di = CharImageCreate(input_pointers[0]);
			di = CharImageCHolWithSE(di, input_pointers[1]);
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		if (planes > 1) {
			g_error("prostak chole stractural element is not set as file!");
		} else {
			CharImage*di;
			di = CharImageCreate(input_pointers[0]);
			di = CharImageCHolSE(di, vsize, hsize, type);
			CharImageWrite(di,output_pointers[0]);
		}
	}
	return status;
}

int prostak_lotsu(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageLOtsuWithSE(di, input_pointers[1]);
		}
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageLOtsuSE(di, vsize, hsize, type);
		}
	}
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_hist(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	Hist*h;
	int i;
	double sum, T;
	FILE*fp;
	di = CharImageCreate(input_pointers[0]);
	h = CharImageHistogram(di,0,0,di->rows,di->columns);
	T = (double)(di->number);
	sum = 0.0;
	fp = fopen(output_pointers[0],"w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	for ( i = 0; i < 256; i++ ) {
		sum += (double)h[i].npi;
		fprintf(fp,"\n %d %u %9.2f", i, h[i].npi, 100.0 * sum/ T);
		free(h[i].pi);
	}
	fclose(fp);
	free(h);
	return status;
}

int prostak_median(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	FILE*fp;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		int nelem, npattern;
		int*pattern;
		fp = fopen(input_pointers[1],"r");
		if ( !fp )
			g_error("parus: CharImageStructElem can't open %s", input_pointers[1]);
		StructElemGetNElem(fp, &nelem);
		StructElemGetMedian(fp, &i);
		pattern = (int*)calloc(nelem, sizeof(int));
		npattern = (int)( (double)nelem / 2.0 );
		StructElemGetElem(fp, nelem, pattern);
		fclose(fp);
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageFMedianWithSE(di, npattern, pattern, 2);
		}
		free(pattern);
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageMedianSE(di, vsize, hsize, type);
		}
	}
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_match(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		di = CharImageCreate(input_pointers[0]);
		di = CharImageMatchWithSE(di, input_pointers[1]);
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		di = CharImageMatchSE(di, vsize, hsize, type);
	}
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_gerosion(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPGErosionWithSE(dp, input_pointers[1]);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			for ( i = 0; i < repetitions; i++ ) {
				di = CharImageGErosionWithSE(di, input_pointers[1]);
			}
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageGErosionSE(di, vsize, hsize, type);
		}
		CharImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_gopen(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPGOpenWithSE(dp, input_pointers[1], repetitions);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			for	( i = 0; i < repetitions; i++ ) {
				di = CharImageGErosionWithSE(di, input_pointers[1]);
				di = CharImageGDilationWithSE(di, input_pointers[1]);
			}
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		g_error("prostak: gopen - stractural element is not set!");
	}
	return status;
}

int prostak_gclose(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPGCloseWithSE(dp, input_pointers[1], repetitions);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			for ( i = 0; i < repetitions; i++ ) {
				di = CharImageGDilationWithSE(di, input_pointers[1]);
				di = CharImageGErosionWithSE(di, input_pointers[1]);
			}
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		g_error("prostak: gclose - stractural element is not set!");
	}
	return status;
}

int prostak_gdilation(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( n_input_pointers == 2 ) {
		if (planes > 1) {
			CharVolume*dp, *mask;
			dp = CreateCharVolume(input_pointers[0], "tif");
			dp = CharVolumeBPGDilationWithSE(dp, input_pointers[1]);
			CharVolumeWrite(dp, output_pointers[0], ".tif");
		} else {
			di = CharImageCreate(input_pointers[0]);
			for ( i = 0; i < repetitions; i++ ) {
				di = CharImageGDilationWithSE(di, input_pointers[1]);
			}
			CharImageWrite(di,output_pointers[0]);
		}
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		for ( i = 0; i < repetitions; i++ ) {
			di = CharImageGDilationSE(di, vsize, hsize, type);
		}
		CharImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_lstretch(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers == 2 ) {
		di = CharImageCreate(input_pointers[0]);
		di = CharImageLStretchWithSE(di, input_pointers[1], 0, 1);
	} else {
		if ( !ostring )
			g_error("prostak stractural element is not set!");
		sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
		if ( !strcmp(etype,"disk") )
			type = disk;
		else if ( !strcmp(etype,"square") )
			type = square;
		else
			g_error("prostak: wrong type");
		di = CharImageCreate(input_pointers[0]);
		di = CharImageLStretchSE(di, vsize, hsize, type, 0, 1);
	}
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_ppix(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double ac, bc, ar, br, t0, tN;
	Pattern *p;
	FILE*fp;
	if ( !ostring )
		g_error("prostak axis is not defined!");
	sscanf(ostring, "%lf,%lf,%lf,%lf,%lf,%lf", &ac, &bc, &ar, &br, &t0, &tN);
	di = CharImageCreate(input_pointers[0]);
	p = CharImagePPix(di, ac, bc, ar, br, t0, tN, output_pointers[1]);
	CharImageDelete(di);
	fp = fopen(output_pointers[0],"w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	Pattern_write(fp, p);
	Pattern_delete(p);
	fclose(fp);
	return status;
}


int prostak_mppix(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di, *mi;
	double t0, tN;
	Pattern *p;
	FILE*fp;
	if ( n_input_pointers != 2 ) {
		g_error("prostak line is not defined!");
	}
	if ( !ostring )
		g_error("prostak parameters are not defined!");
	sscanf(ostring, "%lf,%lf", &t0, &tN);
	di = CharImageCreate(input_pointers[0]);
	mi = CharImageCreate(input_pointers[1]);
	p = CharImageMPPix(di, t0, tN, mi);
	CharImageDelete(di);
	fp = fopen(output_pointers[0],"w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	Pattern_write(fp, p);
	Pattern_delete(p);
	fclose(fp);
	if ( n_output_pointers == 2 )
		CharImageWrite(mi, output_pointers[1]);
	return status;
}

int prostak_anisodiff(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double threshold, lambda;
	char*perr;
	int iter;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	perr = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( !ostring )
		g_error("prostak parameters are not set!");
	sscanf(ostring, "%d,%lf,%lf,%s", &iter, &lambda, &threshold, perr);
	if (planes > 1) {
		CharVolume*dp;
		dp = CreateCharVolume(input_pointers[0], "tif");
		dp = CharVolumeBPAnisoDiff(dp, iter, lambda, threshold, perr);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		di = CharImageCreate(input_pointers[0]);
		di = CharImageAnisoDiff(di, iter, lambda, threshold, perr);
		CharImageWrite(di,output_pointers[0]);
	}
	free(perr);
	return status;
}

int prostak_despeckle(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if (planes > 1) {
		CharVolume*dp;
		dp = CreateCharVolume(input_pointers[0], "tif");
		dp = CharVolumeBPCrimminsDespekle(dp, repetitions);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		CharImage*ci;
		ci = CharImageCreate(input_pointers[0]);
		CharImageCrimminsDespekle(ci, repetitions);
		CharImageWrite(ci, output_pointers[0]);
	}
	return status;
}

int prostak_strel(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int vsize;
	int hsize;
	char *etype;
	StructElemType type;
	if ( !ostring )
		g_error("prostak stractural element is not set!");
	etype = (char*)calloc(MAX_RECORD, sizeof(char));
	sscanf(ostring, "%d,%d,%s", &vsize, &hsize, etype);
	if ( !strcmp(etype,"disk") )
		type = disk;
	else if ( !strcmp(etype,"square") )
		type = square;
	else
		g_error("prostak: wrong type");
	StructElem(input_pointers[0], vsize, hsize, type);
	return status;
}

int prostak_vstrel(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci;
	ci = CharImageStructElem(0, input_pointers[0]);
	CharImageWrite(ci,output_pointers[0]);
	return status;
}

int prostak_scale(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	di = CharImageScale(di,param, param);
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_shrink(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	if ( param <= 0 || param >= 1)
		g_error("Shrink: param not valid %f", param);
	di = CharImageCreate(input_pointers[0]);
	/*di = CharImageResample(di,param, param);
	di = CharImageSample(di, param, param);*/
	di = CharImageShrink(di,param, param);
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_expand(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	if ( param <= 1)
		g_error("Expand: param not valid %f", param);
	di = CharImageCreate(input_pointers[0]);
	di = CharImageResample(di, param, param);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_noop(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_mul(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	if ( n_input_pointers != 2 ) {
		g_error("prostak mask's not defined!");
	}
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*maskimage;
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		maskimage = CharImageCreate(input_pointers[1]);
		di = CharImageMul(di,maskimage);
		CharImageWrite(di,output_pointers[0]);
	} else {
		SuperImage*di;
		SuperImage*maskimage;
		di = SuperImageCreate(input_pointers[0]);
		maskimage = SuperImageCreate(input_pointers[1]);
		di = SuperImageMul(di,maskimage);
		SuperImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_mumu(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*maskimage;
	int i;
	if ( n_input_pointers != 2 ) {
		g_error("prostak mask's not defined!");
	}
	di = CharImageCreate(input_pointers[0]);
	maskimage = CharImageCreate(input_pointers[1]);
	di = CharImageMM(di,maskimage);
	CharImageWrite(di, output_pointers[0]);
	return status;
}

int prostak_hues(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*r;
	CharImage*g;
	CharImage*b;
	CharImage*h;
	int i;
	r = CharImageCreate(input_pointers[0]);
	g = CharImageCreate(input_pointers[1]);
	b = CharImageCreate(input_pointers[2]);
	h = CharImageHueSV(r, g, b);
	CharImageWrite(h, output_pointers[0]);
	return status;
}

int prostak_huel(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*r;
	CharImage*g;
	CharImage*b;
	CharImage*h;
	int i;
	r = CharImageCreate(input_pointers[0]);
	g = CharImageCreate(input_pointers[1]);
	b = CharImageCreate(input_pointers[2]);
	h = CharImageHueSL(r, g, b);
	CharImageWrite(h, output_pointers[0]);
	return status;
}

int prostak_vvarbc(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	FILE*fp;
	int i;
	if ( n_input_pointers != 2 ) {
		g_error("prostak blobs are not defined!");
	}
	fp = fopen(input_pointers[1], "r");
	if ( !fp )
		g_error("prostak: vvarbc can't open %s", input_pointers[1]);
	bi = BlobImageRead(fp);
	fclose(fp);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: vvarbc can't open %s", output_pointers[0]);
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		fprintf(fp,"%10.6f\n", varianceBwClasses(di, bi));
		CharImageDelete(di);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		fprintf(fp,"%10.6f\n", varBwClasses(di, bi));
		SuperImageDelete(di);
	}
	fclose(fp);
	return status;
}

int prostak_blo2pol(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	CharImage*ci;
	ShapeDescriptor*sd;
	FILE*fp;
	if ( n_input_pointers != 2 )
		g_error("blo2pol: mask is not defined (-m)");
	ci = CharImageCreate(input_pointers[1]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: blo2pol can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	BlobImage2Polar(bi, sd->xmean, sd->ymean);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<blo2pol>\n");
	BlobImagePrint(bi, fp);
	fprintf(fp,"<blo2pol>\n");
	fclose(fp);
	return status;
}

int prostak_blob(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*maskimage;
	BlobInfo*blin;
	BlobImage*bi;
	FILE*fp;
	int i;
	di = CharImageCreate(input_pointers[0]);
	maskimage = (CharImage*)NULL;
	if ( n_input_pointers == 2 ) {
		maskimage = CharImageCreate(input_pointers[1]);
	}
	if ( !ostring )
		g_error("prostak window is not set!");
	blin = NewBlobInfo(0, 0, 0);
	sscanf(ostring, "%d,%d,%u", &blin->MinSize, &blin->MaxSize, &blin->MaxBlobs);
	bi = blob(blin, maskimage, di, repetitions);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<blob>\n");
	BlobImagePrint(bi, fp);
	fprintf(fp,"</blob>\n");
	fclose(fp);
	return status;
}

int prostak_solo(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	Pattern *p;
	FILE*fp;
	int i, j;
	char**argvp;
	int argcp;
	argvp = g_strsplit(ostring, ",", MAX_RECORD);
	argcp = g_strv_length(argvp);
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: solo can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	p = BlobImage2Pattern(bi);
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		for ( i = 1, j = 0; i < n_input_pointers; i++, j += 3 ) {
			CharImage*di;
			int channel;
			char protein;
			ChannelType CType;
			BlobDataType BDType;
			channel = i;
			protein = argvp[j + 2][0];
			if ( !strcmp(argvp[j], "nuclear") )
				CType = nuclear;
			else if ( !strcmp(argvp[j], "energid") )
				CType = energid;
			else if ( !strcmp(argvp[j], "outnuc") )
				CType = outnuc;
			if ( !strcmp(argvp[j + 1], "mean") )
				BDType = mean;
			else if ( !strcmp(argvp[j + 1], "median") )
				BDType = median;
			else if ( !strcmp(argvp[j + 1], "muc") )
				BDType = muc;
			else if ( !strcmp(argvp[j + 1], "max") )
				BDType = max;
			else if ( !strcmp(argvp[j + 1], "min") )
				BDType = min;
			else if ( !strcmp(argvp[j + 1], "stdev") )
				BDType = stdev;
			else if ( !strcmp(argvp[j + 1], "var") )
				BDType = var;
			else if ( !strcmp(argvp[j + 1], "area") )
				BDType = area;
			else if ( !strcmp(argvp[j + 1], "varbc") )
				BDType = varbc;
			di = CharImageCreate(input_pointers[i]);
			if ( repetitions == 255 )
				AddChannel1(CType, p, di, bi, channel, protein, BDType);
			else
				AddChannel(CType, p, di, bi, channel, protein, BDType);
			CharImageDelete(di);
		}
	} else {
		for ( i = 1, j = 0; i < n_input_pointers; i++, j += 3 ) {
			SuperImage*di;
			int channel;
			char protein;
			ChannelType CType;
			BlobDataType BDType;
			channel = i;
			protein = argvp[j + 2][0];
			if ( !strcmp(argvp[j], "nuclear") )
				CType = nuclear;
			else if ( !strcmp(argvp[j], "energid") )
				CType = energid;
			else if ( !strcmp(argvp[j], "outnuc") )
				CType = outnuc;
			if ( !strcmp(argvp[j + 1], "mean") )
				BDType = mean;
			else if ( !strcmp(argvp[j + 1], "median") )
				BDType = median;
			else if ( !strcmp(argvp[j + 1], "muc") )
				BDType = muc;
			else if ( !strcmp(argvp[j + 1], "max") )
				BDType = max;
			else if ( !strcmp(argvp[j + 1], "min") )
				BDType = min;
			else if ( !strcmp(argvp[j + 1], "stdev") )
				BDType = stdev;
			else if ( !strcmp(argvp[j + 1], "var") )
				BDType = var;
			else if ( !strcmp(argvp[j + 1], "area") )
				BDType = area;
			else if ( !strcmp(argvp[j + 1], "varbc") )
				BDType = varbc;
			di = SuperImageCreate(input_pointers[i]);
			AddChannelSuper(CType, p, di, bi, channel, protein, BDType);
			SuperImageDelete(di);
		}
	}
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: solo can't open %s", output_pointers[0]);
	Pattern_write(fp, p);
	fclose(fp);
	Pattern_delete(p);
	return status;
}

int prostak_bolb(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	CharImage*ci;
	FILE*fp;
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: bolb can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	ci = BlobImage2CharImage(bi);
	CharImageWrite(ci,output_pointers[0]);
	return status;
}

int prostak_bolb_y(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	SuperImage*si;
	Pattern *p;
	FILE*fp;
	int bits_per_pixel_in, bits_per_pixel_out;
	if ( !ostring )
		g_error("prostak:yblob bits_per_pixel is not set.");
	sscanf(ostring, "%d,%d", &bits_per_pixel_in, &bits_per_pixel_out);
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: ybolb can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	fp = fopen(input_pointers[1], "r");
	if ( !fp )
		g_error("prostak: ybolb can't open %s", input_pointers[1]);
	p = Pattern_create(fp, repetitions);
	fclose(fp);
	si = BlobImage2SuperImage(bi, p, bits_per_pixel_in, bits_per_pixel_out);
	SuperImageWrite(si, output_pointers[0]);
	return status;
}

int prostak_bolind(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	BlobImage*bi;
	CharImage*ci;
	FILE*fp;
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: bolb can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	ci = BlobImageIndex2CharImage(bi, repetitions);
	CharImageWrite(ci,output_pointers[0]);
	return status;
}

int prostak_shape(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci;
	ShapeDescriptor*sd;
	char*grp, *buf;
	GKeyFile*gkf;
	GError*gerror = NULL;
	gsize length;
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	buf = g_path_get_basename(input_pointers[0]);
	grp = g_strdup_printf("Shape:%s", buf);
	g_free(buf);
	gkf = g_key_file_new();
	ShapeDescriptorPrint(gkf, sd, grp);
	g_free(grp);
	buf = (char*)g_key_file_to_data(gkf, &length, &gerror);
	if ( gerror || length == 0 || !buf )
		g_error("Error printing sd");
	if ( !g_file_set_contents(output_pointers[0], buf, (gssize) length, &gerror) )
		g_error("Error printing sd: %s", gerror->message);
	g_free(buf);
	CharImageDelete(ci);
	ShapeDescriptorDelete (sd);
	g_key_file_free(gkf);
	return status;
}

int prostak_round_polar(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci, *di;
	ShapeDescriptor*sd;
	FILE*fp;
	if ( !ostring )
		g_error("prostak parameters are not set!");
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	ci = CharImageCreate(input_pointers[1]);
	di = CharImageCreate(input_pointers[2]);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: can't open %s", output_pointers[0]);
	plot_round_polar(fp, ci, di, ostring, sd->xmean, sd->ymean);
	fclose(fp);
	return status;
}

int prostak_ropol_scr(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci, *di, *dj;
	ShapeDescriptor*sd;
	FILE*fp;
	double rmin, rmax, tmin, tmax;
	if ( !ostring )
		g_error("prostak geometry is not set!");
	sscanf(ostring,"%lf,%lf,%lf,%lf",&rmin, &rmax, &tmin, &tmax);
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	ci = CharImageCreate(input_pointers[1]);
	di = CharImageCreate(input_pointers[2]);
	dj = CharImageRoPolScr(ci, di, rmin, rmax, tmin, tmax, sd->xmean, sd->ymean);
	CharImageWrite(dj, output_pointers[0]);
	return status;
}

int prostak_ropol_print(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci, *di, *dj;
	ShapeDescriptor*sd;
	BlobImage*bi;
	FILE*fp;
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: bolb can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: can't open %s", output_pointers[0]);
	BlobImageRoPolPrint(fp, bi, sd->xmean, sd->ymean);
	fclose(fp);
	return status;
}

int prostak_belt(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci, *di, *dj;
	ShapeDescriptor*sd;
	ci = CharImageCreate(input_pointers[0]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	di = CharImageCreate(input_pointers[1]);
	dj = CharImageBelt(di, input_pointers[2], sd->xmean, sd->ymean);
	CharImageWrite(dj, output_pointers[0]);
	return status;
}

int prostak_lev(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci, *dj;
	ci = CharImageCreate(input_pointers[0]);
	dj = CharImageLev(ci, (unsigned char)repetitions);
	CharImageWrite(dj, output_pointers[0]);
	return status;
}

int prostak_shape_select(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci;
	BlobImage*bi;
	ShapeDescriptor*sd;
	CheckShapeType cst;
	int nblobs_old;
	FILE*fp;
	if ( n_input_pointers != 2 )
		g_error("shape_select: shape not defined (-m)");
	if ( !ostring )
		g_error("shape_select: rule not defined (-s)");
	if ( !strcmp(ostring, "accept"))
		cst = shape_accept;
	else if ( !strcmp(ostring, "reject"))
		cst = shape_reject;
	else
		g_error("shape_select: wrong rule (-s) %s", ostring);
	ci = CharImageCreate(input_pointers[1]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	fp = fopen(input_pointers[0], "r");
	if ( !fp )
		g_error("prostak: shape_select can't open %s", input_pointers[0]);
	bi = BlobImageRead(fp);
	fclose(fp);
	nblobs_old = bi->NBlobs;
	bi = BlobImageCheckShape(bi, sd, param, cst);
	fp = fopen(output_pointers[1], "w");
	if ( !fp )
		g_error("prostak: shape_select can't open %s", output_pointers[1]);
	fprintf(fp,"%d %.16f %.16f %.16f %.16f %.16f %.16f %.16f", nblobs_old - bi->NBlobs, sd->I_1, sd->I_2, sd->I_3, sd->I_4, sd->I_5, sd->I_6, sd->I_7);
	fclose(fp);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: shape_select can't open %s", output_pointers[0]);
	fprintf(fp,"<shape_select>\n");
	BlobImagePrint(bi, fp);
	fprintf(fp,"</shape_select>\n");
	fclose(fp);
	return status;
}

int prostak_shape_select2(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*ci;
	ShapeDescriptor*sd;
	CheckShapeType cst;
	int MaxSegment;
	int MinSegment;
	char*cst_string;
	uint32 rows, columns, planes;
	cst_string = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( n_input_pointers != 2 )
		g_error("shape_select: shape not defined (-m)");
	if ( !ostring )
		g_error("shape_select: rule not defined (-s)");
	sscanf(ostring, "%d,%d,%s", &MaxSegment, &MinSegment, cst_string);
	if ( !strcmp(cst_string, "accept"))
		cst = shape_accept;
	else if ( !strcmp(cst_string, "reject"))
		cst = shape_reject;
	else
		g_error("shape_select: wrong rule (-s) %s", ostring);
	free(cst_string);
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	ci = CharImageCreate(input_pointers[1]);
	sd = CharImage2ShapeDescriptor(ci);
	CharImageDelete(ci);
	if (planes > 1) {
		CharVolume*di;
		di = CreateCharVolume(input_pointers[0], "tif");
		di = CharVolumeBPShapeSelect(di, sd, param, cst, repetitions, MaxSegment, MinSegment);
		CharVolumeWrite(di, output_pointers[0], ".tif");
	} else {
		ci = CharImageCreate(input_pointers[0]);
		ci = CharImageShapeSelect(ci, sd, param, cst, repetitions, MaxSegment, MinSegment);
		CharImageWrite(ci, output_pointers[0]);
	}
	return status;
}

int prostak_movl(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*rci, *gci, *bci;
	char *s, *color;
	char**argvp, **colors;
	int argcp;
	FILE*fp;
	FILE*f;
	int i, n_colors, plane;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	colors = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !colors )
		g_error("prostak: movl wrong colors");
	n_colors = g_strv_length(colors);
	if ( n_colors != n_input_pointers )
		g_error("prostak: movl wrong colors number");
	if (planes > 1) {
		CharVolume*rdi, *gdi, *bdi, *ddi;
		rdi = CharVolumeCreateEmpty(input_pointers[0]);
		gdi = CharVolumeClone(rdi);
		bdi = CharVolumeClone(rdi);
		for ( i = 0; i < n_colors; i++ ) {
			CharImage*di;
			ddi = CharVolumeCreate(input_pointers[i]);
			for ( plane = 0; plane < planes; plane++ ) {
				rci = CharVolumeGetPlane (rdi, plane);
				gci = CharVolumeGetPlane (gdi, plane);
				bci = CharVolumeGetPlane (bdi, plane);
				di = CharVolumeGetPlane (ddi, plane);
				CharImageAddColor(rci, gci, bci, di, colors[i]);
				CharImageDelete(di);
				CharVolumeAddPlane (rdi, rci, plane);
				CharVolumeAddPlane (gdi, gci, plane);
				CharVolumeAddPlane (bdi, bci, plane);
				CharImageDelete(rci);
				CharImageDelete(gci);
				CharImageDelete(bci);
			}
		}
		CharVolumeWriteRGB(rdi, gdi, bdi, output_pointers[0]);
		CharVolumeDelete(rdi);
		CharVolumeDelete(gdi);
		CharVolumeDelete(bdi);
	} else {
		rci = CharImageCreateEmpty(input_pointers[0]);
		gci = CharImageClone(rci);
		bci = CharImageClone(rci);
		for ( i = 0; i < n_colors; i++ ) {
			CharImage*di;
			di = CharImageCreate(input_pointers[i]);
			CharImageAddColor(rci, gci, bci, di, colors[i]);
			CharImageDelete(di);
		}
		CharImageWriteRGB(rci, gci, bci, output_pointers[0]);
		CharImageDelete(rci);
		CharImageDelete(gci);
		CharImageDelete(bci);
	}
	g_strfreev(colors);
	return status;
}

int prostak_splitrgb(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*rci, *gci, *bci;
	CharImageCreateRGB(input_pointers[0], &rci, &gci, &bci);
	CharImageWrite(rci, output_pointers[0]);
	CharImageWrite(gci, output_pointers[1]);
	CharImageWrite(bci, output_pointers[2]);
	return status;
}

int prostak_cwtsd(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	int i;
	uint32 rows, columns, planes;
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if ( repetitions != 4 && repetitions != 8 )
		g_error("prostak connectivity must be 4 or 8 (-r )!");
	if (planes > 1) {
		CharVolume*di;
		di = CreateCharVolume(input_pointers[0], "tif");
		di = CharVolumeBPCheapWaterShed(di, repetitions);
		CharVolumeWrite(di, output_pointers[0], ".tif");
	} else {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		CharImageCheapWaterShed(di, repetitions);
		CharImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_reconstruct(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	CharImage*regmax;
	uint32 rows, columns, planes;
	if ( repetitions != 4 && repetitions != 8 )
		g_error("prostak connectivity must be 4 or 8 (-r )!");
	if ( n_input_pointers != 2 ) {
		g_error("prostak mask's not defined!");
	}
	grf_read_dim_volume(input_pointers[0], &rows, &columns, &planes);
	if (planes > 1) {
		CharVolume*dp, *regmax;
		dp = CreateCharVolume(input_pointers[0], "tif");
		regmax = CreateCharVolume(input_pointers[1], "tif");
		dp = CharVolumeBPReconstruct(dp, regmax, repetitions);
		CharVolumeWrite(dp, output_pointers[0], ".tif");
	} else {
		regmax = CharImageCreate(input_pointers[1]);
		di = CharImageCreate(input_pointers[0]);
		di = CharImageReconstruct(di, regmax, repetitions);
		CharImageWrite(di,output_pointers[0]);
	}
	return status;
}

int prostak_lift(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int i;
	di = CharImageCreate(input_pointers[0]);
	CharImageLift(di);
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_dummy(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	di = dummy();
	CharImageWrite(di,input_pointers[0]);
	return status;
}

int prostak_plot(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	FILE*fp;
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: shape_select can't open %s", output_pointers[0]);
	di = CharImageCreate(input_pointers[0]);
	plot(fp, di);
	fclose(fp);
	return status;
}

int prostak_plot_sp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	FILE*fp;
	if ( !ostring )
		g_error("prostak parameters are not set!");
	di = CharImageCreate(input_pointers[0]);
	if ( repetitions == 0 )
		repetitions = 1;
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak: shape_select can't open %s", output_pointers[0]);
	plot_sp(fp, di, repetitions, ostring);
	fclose(fp);
	return status;
}

int prostak_regionalmin(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int MaxSegment, MinSegment, MaxNumSeg;
	int connectivity;
	if ( !ostring )
		g_error("prostak parameters are not set!");
	sscanf(ostring, "%d,%d,%d,%d", &MaxSegment, &MinSegment, &MaxNumSeg, &connectivity);
	di = CharImageCreate(input_pointers[0]);
	di = CharImageRegionalMin(di, connectivity, MaxSegment, MinSegment, MaxNumSeg);
	CharImageWrite(di, output_pointers[0]);
	return status;
}


int prostak_regionalmax(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	int MaxSegment, MinSegment, MaxNumSeg;
	int connectivity;
	if ( !ostring )
		g_error("prostak parameters are not set!");
	sscanf(ostring, "%d,%d,%d,%d", &MaxSegment, &MinSegment, &MaxNumSeg, &connectivity);
	di = CharImageCreate(input_pointers[0]);
	di = CharImageRegionalMax(di, connectivity, MaxSegment, MinSegment, MaxNumSeg);
	CharImageWrite(di,output_pointers[0]);
	return status;
}

int prostak_contrast(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	CharImage*di;
	double a, b, aof, bof, ax, bx;
	char *str;
	char *func;
	if ( !ostring )
		g_error("prostak geometry is not set!");
	if ( n_input_pointers != 1 )
		g_error("prostak no input!");
	if ( n_output_pointers != 1 )
		g_error("prostak no output!");
	str = (char*)calloc(MAX_RECORD, sizeof(char));
	func = (char*)calloc(MAX_RECORD, sizeof(char));
	di = CharImageCreate(input_pointers[0]);
	if ( !strncmp(ostring, "stretch", 7) ) {
		sprintf(str, "%s", &ostring[8]);
		sscanf(str,"%lf,%lf", &a, &b);
		di = CharImageContrastStretch(di, a, b);
	} else if ( !strncmp(ostring, "relay", 5) ) {
		sprintf(str, "%s", &ostring[6]);
		sscanf(str,"%lf,%lf,%lf,%lf", &a, &b, &aof, &bof);
		di = CharImageContrastStretchRelay(di, a, b, aof, bof);
	} else if ( !strncmp(ostring, "transfer", 8) ) {
		sprintf(str, "%s", &ostring[9]);
		sscanf(str,"%lf,%lf,%lf,%lf,%lf,%lf,%s", &a, &b, &aof, &bof, &ax, &bx, func);
		di = CharImageContrastTransferRelay(di, a, b, aof, bof, func, ax, bx);
	} else {
		g_error("prostak.contrast: %s is a wrong type. Valid are stretch, relay and transfer!", str);
	}
	CharImageWrite(di, output_pointers[0]);
	free(str);
	free(func);
	return status;
}

int prostak_m_ar_x(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*ops;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	uint32 upperOffset, lowerOffset, leftOffset, rightOffset;
	uint32 upper_offset, lower_offset, left_offset, right_offset;
	int dim, g1, g2;
	FILE*fp;
	ops = prostak_get_info_string(input_pointers[0]);
	sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	free(ops);
	sscanf(ostring, "%u,%u,%u,%u",&upperOffset, &lowerOffset, &leftOffset, &rightOffset);
	dim = (int) ( UpperOffset + LowerOffset );
	dim = (int) ( (double)dim * (double)upperOffset + 0.5 );
	g1 = (int) ( 0.5 * (double)dim );
	g2 = dim - g1;
	upper_offset = (uint32)g1;
	lower_offset = (uint32)g2;
	dim = (int) ( LeftOffset + RightOffset );
	dim = (int) ( (double)dim * (double)leftOffset + 0.5 );
	g1 = (int) ( 0.5 * (double)dim );
	g2 = dim - g1;
	left_offset = (uint32)g1;
	right_offset = (uint32)g2;
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<m_ar_x>\n");
	fprintf(fp,"%u,%u,%u,%u\n", upper_offset, lower_offset, left_offset, right_offset);
	fprintf(fp,"</m_ar_x>\n");
	fclose(fp);
	return status;
}

int prostak_m_ar_plus(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*ops;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	uint32 upperOffset, lowerOffset, leftOffset, rightOffset;
	uint32 upper_offset, lower_offset, left_offset, right_offset;
	FILE*fp;
	ops = prostak_get_info_string(input_pointers[0]);
	sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	free(ops);
	sscanf(ostring, "%u,%u,%u,%u",&upperOffset, &lowerOffset, &leftOffset, &rightOffset);
	upper_offset = (uint32)(UpperOffset + upperOffset);
	lower_offset = (uint32)(LowerOffset + lowerOffset);
	left_offset = (uint32)(LeftOffset + leftOffset);
	right_offset = (uint32)(RightOffset + rightOffset);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<m_ar_plus>\n");
	fprintf(fp,"%u,%u,%u,%u\n", upper_offset, lower_offset, left_offset, right_offset);
	fprintf(fp,"</m_ar_plus>\n");
	fclose(fp);
	return status;
}

int prostak_ar_x(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*ops;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	uint32 upper_offset, lower_offset, left_offset, right_offset;
	int dim, g1, g2;
	FILE*fp;
	ops = prostak_get_info_string(input_pointers[0]);
	sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	free(ops);
	dim = (int) ( UpperOffset + LowerOffset );
	dim = (int) ( (double)dim * param + 0.5 );
	g1 = (int) ( 0.5 * (double)dim );
	g2 = dim - g1;
	upper_offset = (uint32)g1;
	lower_offset = (uint32)g2;
	dim = (int) ( LeftOffset + RightOffset );
	dim = (int) ( (double)dim * param + 0.5 );
	g1 = (int) ( 0.5 * (double)dim );
	g2 = dim - g1;
	left_offset = (uint32)g1;
	right_offset = (uint32)g2;
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<ar_x>\n");
	fprintf(fp,"%u,%u,%u,%u\n", upper_offset, lower_offset, left_offset, right_offset);
	fprintf(fp,"</ar_x>\n");
	fclose(fp);
	return status;
}

int prostak_ar_minus(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*ops;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	uint32 upperOffset, lowerOffset, leftOffset, rightOffset;
	uint32 upper_offset, lower_offset, left_offset, right_offset;
	FILE*fp;
	ops = prostak_get_info_string(input_pointers[0]);
	sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	free(ops);
	ops = prostak_get_info_string(input_pointers[1]);
	sscanf(ops, "%u,%u,%u,%u",&upperOffset, &lowerOffset, &leftOffset, &rightOffset);
	free(ops);
	upper_offset = (uint32)(-UpperOffset + upperOffset);
	lower_offset = (uint32)(-LowerOffset + lowerOffset);
	left_offset = (uint32)(-LeftOffset + leftOffset);
	right_offset = (uint32)(-RightOffset + rightOffset);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<ar_minus>\n");
	fprintf(fp,"%u,%u,%u,%u\n", upper_offset, lower_offset, left_offset, right_offset);
	fprintf(fp,"</ar_minus>\n");
	fclose(fp);
	return status;
}

int prostak_ar_plus(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char*ops;
	uint32 UpperOffset, LowerOffset, LeftOffset, RightOffset;
	uint32 upperOffset, lowerOffset, leftOffset, rightOffset;
	uint32 upper_offset, lower_offset, left_offset, right_offset;
	FILE*fp;
	ops = prostak_get_info_string(input_pointers[0]);
	sscanf(ops, "%u,%u,%u,%u",&UpperOffset, &LowerOffset, &LeftOffset, &RightOffset);
	free(ops);
	ops = prostak_get_info_string(input_pointers[1]);
	sscanf(ops, "%u,%u,%u,%u",&upperOffset, &lowerOffset, &leftOffset, &rightOffset);
	free(ops);
	upper_offset = (uint32)(UpperOffset + upperOffset);
	lower_offset = (uint32)(LowerOffset + lowerOffset);
	left_offset = (uint32)(LeftOffset + leftOffset);
	right_offset = (uint32)(RightOffset + rightOffset);
	fp = fopen(output_pointers[0], "w");
	if ( !fp )
		g_error("prostak can't open %s for writing", output_pointers[0]);
	fprintf(fp,"<ar_plus>\n");
	fprintf(fp,"%u,%u,%u,%u\n", upper_offset, lower_offset, left_offset, right_offset);
	fprintf(fp,"</ar_plus>\n");
	fclose(fp);
	return status;
}

int prostak_apsc(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**alighn;
	char**chemar;
	alighn = prostak_get_info_array(input_pointers[0]);
	chemar = prostak_get_info_array(input_pointers[1]);
	if ( g_str_has_prefix(ostring, "A") ) {
		if ( g_str_has_prefix(chemar[1], "R") ) {
			g_free(alighn[2]);
			alighn[2] = g_strdup("reversecolumn");
		} else if ( g_str_has_prefix(chemar[1], "L") ) {
			g_free(alighn[2]);
			alighn[2] = g_strdup("-");
		}
	} else if ( g_str_has_prefix(ostring, "P") ) {
		if ( g_str_has_prefix(chemar[1], "L") ) {
			g_free(alighn[2]);
			alighn[2] = g_strdup("reversecolumn");
		} else if ( g_str_has_prefix(chemar[1], "R") ) {
			g_free(alighn[2]);
			alighn[2] = g_strdup("-");
		}
	}
	prostak_put_info_array(output_pointers[0], alighn);
	return status;
}

int prostak_apee(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**apee;
	apee = prostak_get_info_array(input_pointers[1]);
	if ( !PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 ) {
		CharImage*di;
		di = CharImageCreate(input_pointers[0]);
		if ( g_str_has_prefix(apee[1], "transpose") ) {
			di = CharImageTranspose(di);
		}
		if ( g_str_has_prefix(apee[2], "reversecolumn") ) {
			di = CharImageReverseColumn(di);
		}
		if ( g_str_has_prefix(apee[3], "reverserow") ) {
			di = CharImageReverseRow(di);
		}
		CharImageWrite(di, output_pointers[0]);
	} else {
		SuperImage*di;
		di = SuperImageCreate(input_pointers[0]);
		if ( g_str_has_prefix(apee[1], "transpose") ) {
			di = SuperImageTranspose(di);
		}
		if ( g_str_has_prefix(apee[2], "reversecolumn") ) {
			di = SuperImageReverseColumn(di);
		}
		if ( g_str_has_prefix(apee[3], "reverserow") ) {
			di = SuperImageReverseRow(di);
		}
		SuperImageWrite(di, output_pointers[0]);
	}
	return status;
}

int prostak_decwiener(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	int window;
	char*psf_type, *ops;
	int iter_max;
	double criterion, lambda, gamma, alpha;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 5 ) {
			g_error("prostak::decwiener option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		msigma = atof(options[2]);
		gamma = atof(options[3]);
		alpha = atof(options[4]);
		if ( length == 6 ) {
			window = atoi(options[5]);
		} else {
			window = -1;
		}
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 4 ) {
			g_error("prostak::decsup option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		gamma = atof(options[2]);
		alpha = atof(options[3]);
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf,%d",&msigma, &window);
		free(ops);
	} else {
		g_error("prostak::decsup option error");
	}
	si = SuperImageCreate(input_pointers[0]);
	step_image = super_image_deconvolve_wiener_with_analytical_psf(si, &window, msigma, psf_type, lambda, gamma, alpha);
	SuperImageWrite(step_image, output_pointers[0]);
	g_strfreev(options);
	return status;
}

int prostak_dectm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	int window;
	char*psf_type, *ops;
	int iter_max;
	double criterion, lambda, gamma;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 4 ) {
			g_error("prostak::decwiener option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		msigma = atof(options[2]);
		gamma = atof(options[3]);
		if ( length == 5 ) {
			window = atoi(options[4]);
		} else {
			window = -1;
		}
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 3 ) {
			g_error("prostak::decsup option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		gamma = atof(options[2]);
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf,%d",&msigma, &window);
		free(ops);
	} else {
		g_error("prostak::decsup option error");
	}
	si = SuperImageCreate(input_pointers[0]);
	step_image = super_image_deconvolve_tm_with_analytical_psf(si, &window, msigma, psf_type, lambda, gamma);
	SuperImageWrite(step_image, output_pointers[0]);
	g_strfreev(options);
	return status;
}

int prostak_decinv(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	int window;
	char*psf_type, *ops;
	int iter_max;
	double criterion, lambda, gamma;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 4 ) {
			g_error("prostak::decwiener option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		msigma = atof(options[2]);
		gamma = atof(options[3]);
		if ( length == 5 ) {
			window = atoi(options[4]);
		} else {
			window = -1;
		}
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 3 ) {
			g_error("prostak::decsup option error");
		}
		lambda = atof(options[0]);
		psf_type = options[1];
		gamma = atof(options[2]);
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf,%d",&msigma, &window);
		free(ops);
	} else {
		g_error("prostak::decsup option error");
	}
	si = SuperImageCreate(input_pointers[0]);
	step_image = super_image_deconvolve_inv_with_analytical_psf(si, &window, msigma, psf_type, gamma);
	SuperImageWrite(step_image, output_pointers[0]);
	g_strfreev(options);
	return status;
}

int prostak_decbsup(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	SuperImage*step_image;
	double msigma;
	SuperImage*si;
	int window;
	char*psf_type, *ops;
	int iter_max, iter_blind_max;
	double criterion, lambda;
	int length;
	double*psf_array;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::decsup option error");
	}
	length = g_strv_length(options);
	if ( n_input_pointers == 1 ) {
		if ( length < 6 ) {
			g_error("prostak::decsup option error");
		}
		iter_max = atoi(options[0]);
		iter_blind_max = atoi(options[1]);
		criterion = atof(options[2]);
		lambda = atof(options[3]);
		psf_type = options[4];
		msigma = atof(options[5]);
		if ( length == 7 ) {
			window = atoi(options[6]);
		} else {
			window = -1;
		}
	} else if ( n_input_pointers == 2 ) {
		if ( g_strv_length(options) < 5 ) {
			g_error("prostak::decsup option error");
		}
		iter_max = atoi(options[0]);
		iter_blind_max = atoi(options[1]);
		criterion = atof(options[2]);
		lambda = atof(options[3]);
		psf_type = options[4];
		ops = prostak_get_info_string(input_pointers[1]);
		sscanf(ops, "%lf,%d",&msigma, &window);
		free(ops);
	} else {
		g_error("prostak::decsup option error");
	}
	si = SuperImageCreate(input_pointers[0]);
	psf_array = NULL;
	step_image = super_image_deconvolve_blind_with_analytical_psf(si, &window, msigma, psf_type, iter_max, lambda, criterion, iter_blind_max, &psf_array);
	SuperImageWrite(step_image, output_pointers[0]);
	if ( n_output_pointers == 2 && psf_array) {
		int i, j;
		FILE*fp;
		fp = fopen(output_pointers[1], "w");
		if ( !fp ) {
			g_error("prostak::decbsup file error");
		}
		fprintf( fp, "%d\n", window);
		for ( i = 0; i < 2 * window + 1; i++ ) {
			for ( j = 0; j < 2 * window + 1; j++ ) {
				fprintf( fp, "%16.12f ", psf_array[ i * ( 2 * window + 1 ) + j ]);
			}
			fprintf( fp, "\n");
		}
		fclose( fp );
	}
	g_strfreev(options);
	return status;
}

int prostak_mlsreg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers)
{
	int status = 0;
	char**options;
	CharImage*landmarks_fixed, *landmarks, *data, *registered;
	double alpha;
	char*type, **opions;
	int length;
	options = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !options ) {
		g_error("prostak::mlsreg option error");
	}
	length = g_strv_length(options);
	if ( length < 2 ) {
		g_error("prostak::mlsreg option error");
	}
	type = g_strdup(options[0]);
	alpha = atof(options[1]);
	g_strfreev(options);
	landmarks_fixed = CharImageCreate(input_pointers[0]);
	landmarks = CharImageCreate(input_pointers[1]);
	data = CharImageCreate(input_pointers[2]);
	registered = char_image_mlsreg(landmarks_fixed, landmarks, data, type, alpha);
	if ( !registered ) {
		g_error("Can't register - different number of landmarks or wrong type: %s", type);
	}
	CharImageWrite(registered, output_pointers[0]);
	return status;
}
