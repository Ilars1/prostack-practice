/***************************************************************************
 *            prostak_funcs3d.h
 *
 *  Sat Oct  3 18:13:36 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef PROSTAK_FUNCS3D_H
#define PROSTAK_FUNCS3D_H

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

G_BEGIN_DECLS

void prostak_threshold3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_max3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_turn3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_reverserow3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_reversecolumn3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_transpose3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_geometry3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_pad3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_invert3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_shrink3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_expand3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_mul3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_splitlsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_maxlsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_diglsm(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_insert3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_volume_row(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_volume_column(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_cheap_watershed3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_rv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_lv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_tv3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_cunbent3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_median3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_unbent3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_chole3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_lheq3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_gerosion3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_gdilation3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_reconstruct3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_strel3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vstrel3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_distance3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_edge3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_despeckle3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_shape_select3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_shape_select_list3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vtxt3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_gopen3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_gclose3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_setp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_restack3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
#ifndef G_OS_WIN32
int prostak_surf3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_surf3dfull(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_surf2vol(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
#endif
int prostak_shape3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qu3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vu(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_zscale3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qu3d2csv(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_apee3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vvarbc3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_grid3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qu3dtrans(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_pump3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_mslice3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_mslicegcp3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qumark3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qumap3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_qurelabel(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_quremask(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_hysteresis_threshold3d(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_rec3dbp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_getp(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_strel3dw(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_quresort(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_quthicken(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int connectivity, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_querode(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int connectivity, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_pbreak(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_punite(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);

G_END_DECLS

#endif /* PROSTAK_FUNCS3D_H */
