/***************************************************************************
                          ImageInfo.h  -  description
                             -------------------
    begin                : óÒÄ ñÎ× 26 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

typedef struct Attributes {
  double StDev;
  double mean;
} Attributes;

typedef struct ImageMoments {
	double Mr;
	double Mc;
	double Mrc;
	double Theta;
} ImageMoments;

typedef struct ImageFeatureHalfAxis {
	double Cr;
	double Cc;
	double Cplus;
	double Cminus;
	double Rplus;
	double Rminus;
} ImageFeatureHalfAxis;

typedef struct HalfImage {
	double L;
	double R;
} HalfImage;

typedef struct Kernel {
	int order;
	double*array;
} Kernel;

typedef struct Hist {
	uint32 *pi;
	uint32 npi;
	unsigned char level;
} Hist;

typedef struct ShapeDescriptor {
	double m00;
	double m01;
	double m10;
	double xmean;
	double ymean;
	double xmin;
	double ymin;
	double xmax;
	double ymax;
	double mu00;
	double mu02;
	double mu20;
	double mu11;
	double mu12;
	double mu21;
	double mu30;
	double mu03;
	double eta02;
	double eta20;
	double eta11;
	double eta12;
	double eta21;
	double eta30;
	double eta03;
	double I_1;
	double I_2;
	double I_3;
	double I_4;
	double I_5;
	double I_6;
	double I_7;
	double rotation_angle;
	double mu20_prime;
	double mu02_prime;
	double mu11_prime;
	double ellipse_axis_a;
	double ellipse_axis_b;
} ShapeDescriptor;

#endif
