/***************************************************************************
                          CharPixel.c  -  description
                             -------------------
    begin                : óÒÄ ñÎ× 26 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>
#include <grf.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <CharImage.h>
#include <ImageInfo.h>
/*#define BLI_IMAGE*/


#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
#endif


static int VERBOSE = 0;
static double ThresholdMultiplyer = 1.0;
static CharImage *sv;

GKeyFile*g_key_file_load_from_cmdarg(char*input_pointer, GError**gerror)
{
	GKeyFile*gkf;
	char*contents, *etag_out;
	GFile*gfile;
	gsize size;
	gfile = g_file_new_for_commandline_arg(input_pointer);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &contents, &size, &etag_out, gerror) ) {
		return NULL;
	}
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)contents, size, G_KEY_FILE_NONE, gerror ) ) {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	} else {
		g_free(contents);
		g_free(etag_out);
		g_object_unref(gfile);
	}
	return gkf;
}

char*prostak_get_info_string_with_oper(char*pointer, char*oper)
{
	FILE*fp;
	char*info_string;
	fp = fopen(pointer, "r");
	if ( !fp )
		g_error("get_info_string error %s", pointer);
	fscanf(fp, "<%s>\n", oper);
	info_string = (char*)calloc(MAX_RECORD, sizeof(char));
	fscanf(fp, "%s", info_string);
	fclose(fp);
	return info_string;
}

char*prostak_get_info_string(char*pointer)
{
	FILE*fp;
	char*info_string;
	fp = fopen(pointer, "r");
	if ( !fp )
		g_error("get_info_string error %s", pointer);
	fscanf(fp, "%*s\n");
	info_string = (char*)calloc(MAX_RECORD, sizeof(char));
	fscanf(fp, "%s", info_string);
	fclose(fp);
	return info_string;
}

void prostak_put_info_string(char*pointer, char*oper, char*info_string)
{
	FILE*fp;
	fp = fopen(pointer, "w");
	if ( !fp )
		g_error("put_info_string error %s", pointer);
	fprintf(fp, "<%s>\n", oper);
	fprintf(fp, "%s\n", info_string);
	fprintf(fp, "</%s>\n", oper);
	fclose(fp);
}

char**prostak_get_info_array(char*pointer)
{
	GError*gerror = NULL;
	char**info_array;
	gchar*contents;
	gsize length;
	if ( !g_file_get_contents((const gchar*)pointer, &contents, &length, &gerror ) ) {
		if ( gerror ) {
			g_warning(gerror->message);
			g_error_free(gerror);
		}
		return NULL;
	}
	info_array = g_strsplit(contents, "\n", MAX_RECORD);
	g_free(contents);
	return info_array;
}

void prostak_put_info_array(char*pointer, char**info_array)
{
	GError*gerror = NULL;
	gchar*contents;
	contents = g_strjoinv("\n", info_array);
	if ( !g_file_set_contents((const gchar*)pointer, contents, -1, &gerror ) ) {
		if ( gerror ) {
			g_warning(gerror->message);
			g_error_free(gerror);
		}
	}
	g_free(contents);
}

void CharImageSetVerbose(int arg)
{
	VERBOSE = arg;
}

void CharImageSetThresholdMultiplyer(double arg)
{
	ThresholdMultiplyer = arg;
	g_print("Oops!");
}

CharImage*CharImageCreate(char*infile)
{
	uint32 columns,rows;
	CharImage*di;
	di = (CharImage*)malloc(sizeof(CharImage));
	grf_read_char(infile,&di->rows,&di->columns,&di->pixels);
	di->number = di->columns * di->rows;
	if ( VERBOSE > 0 ) {
		fprintf(stdout,"\nCharImage: xdim = %u ydim = %u", di->columns, di->rows);
	}
	return di;
}


CharImage*CharImageCreateEmpty(char*infile)
{
	uint32 columns,rows;
	CharImage*di;
	di = (CharImage*)malloc(sizeof(CharImage));
	grf_read_dim(infile,&di->rows,&di->columns);
	di->number = di->columns * di->rows;
	if ( VERBOSE > 0 ) {
		fprintf(stdout,"\nCharImage: xdim = %u ydim = %u", di->columns, di->rows);
	}
	di->pixels = (unsigned char*)calloc(di->number, sizeof(unsigned char));
	return di;
}

CharImage*CharImageReadDim(char*infile)
{
	uint32 columns,rows;
	CharImage*di;
	di = (CharImage*)malloc(sizeof(CharImage));
	grf_read_dim(infile,&di->rows,&di->columns);
	di->number = di->columns * di->rows;
	if ( VERBOSE > 0 ) {
		fprintf(stdout,"\nCharImage: xdim = %u ydim = %u", di->columns, di->rows);
	}
	di->pixels = (unsigned char*)NULL;
	return di;
}

void CharImageWrite(  CharImage*di,char*infile)
{
	grf_write_char(infile,di->rows,di->columns,di->pixels);
}

void CharImageWriteRGB(CharImage*r, CharImage*g, CharImage*b, char*infile)
{
	unsigned char **rgb;
	rgb = (unsigned char**)calloc(3, sizeof(unsigned char *) );
	rgb[0] = r->pixels;
	rgb[1] = g->pixels;
	rgb[2] = b->pixels;
	grf_write_char_rgb(infile, r->rows, r->columns, rgb, 3);
	free(rgb);
}

CharImage*CharImageClone(CharImage*p)
{
	CharImage*di;
	int i;
	di = (CharImage*)malloc(sizeof(CharImage));
	di->columns = p->columns;
	di->rows = p->rows;
	di->number = p->columns*p->rows;
	di->pixels = (unsigned char*)calloc(di->number,sizeof(unsigned char));
	for( i = 0; i < p->number; i++) {
		di->pixels[i] = EmptyPixel;
	}
	return di;
}


CharImage*CharImageCopy(CharImage*p)
{
	CharImage*di;
	uint32 i;
	di = (CharImage*)malloc(sizeof(CharImage));
	di->columns = p->columns;
	di->rows = p->rows;
	di->number = p->number;
	di->pixels = (unsigned char*)calloc(di->number,sizeof(unsigned char));
	for( i = 0; i < p->number; i++)
		di->pixels[i] = p->pixels[i];
	return di;
}


CharImage*CharImageNew(uint32 columns, uint32 rows)
{
	CharImage*di;

	di = (CharImage*)malloc(sizeof(CharImage));
	di->columns = columns;
	di->rows = rows;
	di->number = columns*rows;
	di->pixels = (unsigned char*)calloc(di->number,sizeof(unsigned char));

	return di;
}

CharImage*CharImageCreateRaw(char*name, uint32 columns, uint32 rows, int bps)
{
	CharImage*di;
	FILE*fp;
	di = CharImageNew(columns, rows);
	fp = fopen(name, "rb");
	fread(di->pixels, sizeof(di->pixels), di->number, fp);
	fclose(fp);
	return di;
}

CharImage*CharImageComposeMax(CharImage*di,CharImage*di_j)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
	for ( i = 0; i < di_j->number; i++)
		if( di->pixels[i] <= di_j->pixels[i] )
			di->pixels[i] = di_j->pixels[i];
	return di;
}

CharImage*CharImageInvertWithMask(CharImage*di,CharImage*mask)
{
	uint32 i;
/*
fprintf(stdout,"\n %u %u %u %u",mask->rows,mask->columns,di->rows,di->columns);
*/
	if ( di->number != mask->number )
		g_error("CharImageInvertWithMask dimensions wrong");

	for ( i = 0; i < di->number; i++)
		if( mask->pixels[i] == FilledPixel ) {
/*fprintf(stdout,"\n %u %f",i,mask->pixels[i]);*/
			di->pixels[i] = FilledPixel - di->pixels[i];
		}
	return di;
}

CharImage*CharImageCrossMask(CharImage*di,CharImage*mask)
{
	uint32 i;
/*
fprintf(stdout,"\n %u %u %u %u",mask->rows,mask->columns,di->rows,di->columns);
*/
	if ( di->number != mask->number )
		g_error("CharImageInvertWithMask dimensions wrong");

	for ( i = 0; i < di->number; i++)
		if( mask->pixels[i] == FilledPixel ) {
/*fprintf(stdout,"\n %u %f",i,mask->pixels[i]);*/
			if( !Visible(di,i) )
				di->pixels[i] = FilledPixel;
			else
				di->pixels[i] = EmptyPixel;
		}
	return di;
}

CharImage*CharImageInvert(CharImage*di)
{
	uint32 i;
	for ( i = 0; i < di->number; i++)
		di->pixels[i] = FilledPixel - di->pixels[i];
	return di;
}


CharImage*CharImageInvertNWithMask(CharImage*di,CharImage*mask)
{
	uint32 i;
	register unsigned char MaxIntensity;

	if ( di->number != mask->number )
		g_error("CharImageInvertWithMask dimensions wrong");
	MaxIntensity = EmptyPixel;
	for ( i = 0; i < di->number; i++)
		if ( mask->pixels[i] == FilledPixel && MaxIntensity < di->pixels[i] )
			MaxIntensity = di->pixels[i];
	for ( i = 0; i < di->number; i++)
		if( mask->pixels[i] == FilledPixel ) {
			di->pixels[i] = MaxIntensity - di->pixels[i];
		}
	return di;
}

CharImage*CharImageInvertN(CharImage*di)
{
	uint32 i;
	register unsigned char MaxIntensity;

	MaxIntensity = EmptyPixel;
	for ( i = 0; i < di->number; i++)
		if ( MaxIntensity < di->pixels[i] )
			MaxIntensity = di->pixels[i];
	for ( i = 0; i < di->number; i++)
		di->pixels[i] = MaxIntensity - di->pixels[i];
	return di;
}


CharImage*CharImageComposePlus(CharImage*di,CharImage*di_j)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
	for ( i = 0; i < di_j->number; i++) {
		int d = di->pixels[i] + di_j->pixels[i];
		if ( d > FilledPixel ) {
			di->pixels[i] = FilledPixel;
		} else {
			di->pixels[i] = d;
		}
	}
	return di;
}


CharImage*CharImageAdd(CharImage*di,CharImage*di_j)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
	for ( i = 0; i < di_j->number; i++) {
		register int p = di->pixels[i] + di_j->pixels[i];
		di->pixels[i] = ( p < FilledPixel ) ? p : FilledPixel;
	}
	return di;
}

CharImage*CharImageComposeMinusAbs(CharImage*di,CharImage*w)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,w)
	for ( i = 0; i < w->number; i++) {
		int h = di->pixels[i] - w->pixels[i];
		if ( h < EmptyPixel ) {
			di->pixels[i] = -h;
		} else {
			di->pixels[i] = h;
		}
	}
	return di;
}

void CharImageComposeMinus(CharImage*di,CharImage*w,CharImage*vm,CharImage*vp)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,w,vm,vp)
	for ( i = 0; i < w->number; i++) {
		int h = di->pixels[i] - w->pixels[i];
		if ( h < EmptyPixel ) {
			vm->pixels[i] = -h;
		} else {
			vp->pixels[i] = h;
		}
	}
}


CharImage*CharImageMul(CharImage*di,CharImage*di_j)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
	for ( i = 0; i < di_j->number; i++) {
		di->pixels[i] = (unsigned char)floor( (double)di->pixels[i] * (double)di_j->pixels[i] / (double)FilledPixel + 0.5);
	}
	return di;
}

SuperImage*SuperImageMul(SuperImage*di,SuperImage*di_j)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
	for ( i = 0; i < di_j->number; i++) {
		di->pixels[i] = floor( di->pixels[i] * di_j->pixels[i] / (double)(di_j->filledPixel) + 0.5);
	}
	return di;
}

CharImage*CharImageContrastStretch(CharImage*di, double a, double b)
{
	uint32 i;
	CharImage*dj;
	double c, d;
	double maxLevel;
	double minLevel;
	dj = CharImageClone(di);
	maxLevel = (double)CharImageMaxLevel(di) / (double)FilledPixel;
	minLevel = (double)CharImageMinLevel(di) / (double)FilledPixel;
	c = ( b - a ) / ( maxLevel - minLevel );
	d = ( maxLevel * a - minLevel * b ) / ( maxLevel - minLevel );
#pragma omp parallel for schedule(static) default(none) shared(di,c,d)
	for ( i = 0; i < di->number; i++) {
		register double x  = (double)di->pixels[i] / (double)FilledPixel;
		di->pixels[i] = (unsigned char)( ( c * x + d ) * (double)FilledPixel );
	}
	return di;
}


CharImage*CharImageContrastStretchRelay(CharImage*di, double a, double b, double paof, double pbof)
{
	uint32 i;
	CharImage*dj;
	double c, d;
	double aof, bof;
	double maxLevel;
	double minLevel;
	dj = CharImageClone(di);
	maxLevel = (double)CharImageMaxLevel(di) / (double)FilledPixel;
	minLevel = (double)CharImageMinLevel(di) / (double)FilledPixel;
	aof = paof * ( maxLevel - minLevel );
	bof = pbof * ( maxLevel - minLevel );
	maxLevel -= bof;
	minLevel += aof;
	c = ( b - a ) / ( maxLevel - minLevel );
	d = ( maxLevel * a - minLevel * b ) / ( maxLevel - minLevel );
#pragma omp parallel for schedule(static) default(none) shared(di,c,d,minLevel,maxLevel)
	for ( i = 0; i < di->number; i++) {
		register double x  = (double)di->pixels[i] / (double)FilledPixel;
		if ( x < minLevel ) {
			di->pixels[i] = EmptyPixel;
		} else if ( minLevel <= x && x <= maxLevel ) {
			di->pixels[i] = (unsigned char)( ( c * x + d ) * (double)FilledPixel );
		} else if ( x > maxLevel ) {
			di->pixels[i] = FilledPixel;
		}
	}
	return di;
}

double myExp(double x)
{
	return exp(x);
}

double myLog(double x)
{
	return log(x+1);
}

double myInvLog(double x)
{
	return 1.0 / log(x+1);
}

double mySqr(double x)
{
	return x * x;
}

double mySqrt(double x)
{
	return sqrt(x);
}

double myTanh(double x)
{
	return 0.5 * tanh( x ) + 0.5;
}

CharImage*CharImageContrastTransferRelay(CharImage*di, double a, double b, double paof, double pbof, char *func, double ax, double bx)
{
	uint32 i;
	CharImage*dj;
	double c, d;
	double aof, bof;
	double maxLevel;
	double minLevel;
	double tmaxLevel;
	double tminLevel;
	double (*transfer)(double);
	if (!strcmp(func,"exp"))
		transfer = myExp;
	else if (!strcmp(func,"log"))
		transfer = myLog;
	else if (!strcmp(func,"invlog"))
		transfer = myInvLog;
	else if (!strcmp(func,"sqr"))
		transfer = mySqr;
	else if (!strcmp(func,"sqrt"))
		transfer = mySqrt;
	else if (!strcmp(func,"tanh"))
		transfer = myTanh;
	else
		g_error("CharImageTransferRelay: %s is not implemented!", func);
	dj = CharImageClone(di);
	maxLevel = (double)CharImageMaxLevel(di) / (double)FilledPixel;
	minLevel = (double)CharImageMinLevel(di) / (double)FilledPixel;
	aof = paof * ( maxLevel - minLevel );
	bof = pbof * ( maxLevel - minLevel );
	maxLevel -= bof;
	minLevel += aof;
	tmaxLevel = transfer(ax * maxLevel + bx);
	tminLevel = transfer(ax * minLevel + bx);
	c = ( b - a ) / ( tmaxLevel - tminLevel );
	d = ( tmaxLevel * a - tminLevel * b ) / ( tmaxLevel - tminLevel );
#pragma omp parallel for schedule(static) default(none) shared(di,c,d,ax,bx,minLevel,maxLevel,transfer)
	for ( i = 0; i < di->number; i++) {
		register double x  = (double)di->pixels[i] / (double)FilledPixel;
		if ( x < minLevel )
			di->pixels[i] = EmptyPixel;
		else if ( minLevel <= x && x <= maxLevel )
			di->pixels[i] = (unsigned char)( ( c * transfer( ax * x + bx ) + d ) * (double)FilledPixel );
		else if ( x > maxLevel )
			di->pixels[i] = FilledPixel;
	}
	return di;
}

CharImage*dummy()
{
	CharImage*ci;
	int i;
	ci = CharImageNew(16, 16);
	for ( i = 0; i < FilledPixel + 1; i++) {
		ci->pixels[i] = (unsigned char) i;
	}
	return ci;
}


void plot(FILE*fp, CharImage*ci)
{
	int i;
	for ( i = 0; i < FilledPixel + 1; i++) {
		fprintf(fp," %d %u\n", i, ci->pixels[i]);
	}
}

void plot_sp(FILE*fp, CharImage*ci, int ns, char*flag)
{
	int i, j, tr;
	double dot;
	tr = 0;
	for ( i = 0; i < ci->rows; i+=ns) {
		if ( tr == 0 )
			for ( j = 0; j < ci->columns; j++) {
				if ( !strcmp(flag, "log") )
					dot = log((double)ci->pixels[i * ci->columns + j]);
				else if ( !strcmp(flag, "eqn") )
					dot = (double)ci->pixels[i * ci->columns + j];
				fprintf(fp," %f %f %f\n", (double)i, (double)j, dot );
			}
		else
			for ( j = ci->columns - 1; j >= 0 ; j--) {
				if ( !strcmp(flag, "log") )
					dot = log((double)ci->pixels[i * ci->columns + j]);
				else if ( !strcmp(flag, "eqn") )
					dot = (double)ci->pixels[i * ci->columns + j];
				fprintf(fp," %f %f %f\n", (double)i, (double)j, dot );
			}
		tr++;
		tr %= 2;
	}
}

CharImage*CharImageScaleIntensity(CharImage*di, double scale)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,scale)
	for ( i = 0; i < di->number; i++) {
		register int dot;
		dot = (int)floor( scale * (double)di->pixels[i]);
		di->pixels[i] = ( dot > FilledPixel ) ? FilledPixel : (unsigned char)dot;
	}
	return di;
}


void CharImageThreshold(CharImage*dp, double threshold)
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(dp,threshold)
	for ( i = 0; i < dp->number; i++ ) {
		if ( dp->pixels[i] < (unsigned char)threshold )
			dp->pixels[i] = EmptyPixel;
		else
			dp->pixels[i] = FilledPixel;
	}
}

CharImage*CharImageBorder(CharImage*dp,uint32 ColumnsToAdd,uint32 RowsToAdd,uint32 ColumnsBorderWidth,uint32 RowsBorderWidth)
{
	CharImage*di;
	uint32 c, r;
	di = CharImageNew(dp->columns + ColumnsToAdd, dp->rows + RowsToAdd);
	for ( c = 0; c < dp->columns; c++ ) {
		for ( r =  0; r < dp->rows ; r++ ) {
			di->pixels[ ( r + ColumnsBorderWidth ) * di->columns + ( c + RowsBorderWidth )] = dp->pixels[ r * dp->columns + c ];
		}
	}
	CharImageDelete(dp);
	return di;
}

CharImage*CharImageCrop(CharImage*dp,uint32 ColumnsToDelete,uint32 RowsToDelete,uint32 ColumnsBorderWidth,uint32 RowsBorderWidth)
{
	CharImage*di;
	uint32 c, r;
	di = CharImageNew(dp->columns - ColumnsToDelete, dp->rows - RowsToDelete);
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ ( r + ColumnsBorderWidth ) * dp->columns + ( c + RowsBorderWidth ) ];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageTranspose(CharImage*dp)
{
	CharImage*di;
	uint32 c, r;
	di = CharImageNew(dp->rows, dp->columns);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(di,dp)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ ( dp->rows - c - 1 ) * dp->columns + r ];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageReverseColumn(CharImage*dp)
{
	CharImage*di;
	uint32 c, r;
	di = CharImageNew(dp->columns, dp->rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(di,dp)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ r * dp->columns + ( dp->columns - c - 1 ) ];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageReverseRow(CharImage*dp)
{
	CharImage*di;
	uint32 c, r;
	di = CharImageNew(dp->columns, dp->rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(di,dp)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ ( dp->rows - r - 1 ) * dp->columns + c ];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageRotate1(CharImage*di, double theta)
{
	uint32 ColumnsToAdd, RowsToAdd;
	uint32 ColumnsToDelete, RowsToDelete;
	uint32 Columns, Rows;
	uint32 ColumnsBorderWidth, RowsBorderWidth;
	double ColumnShear, RowShear;
	Columns = di->columns;
	Rows = di->rows;
	RowShear = sin( theta );
	ColumnShear = -tan( 0.5 * theta );
/*	RowsBorderWidth = (uint32)fabs( (double)di->rows * ColumnShear + 0.5 );
	ColumnsToAdd = 2*RowsBorderWidth;
	ColumnsBorderWidth = (uint32)fabs((double)( di->columns + ColumnsToAdd ) * RowShear + 0.5);
	RowsToAdd = 2*ColumnsBorderWidth;
*/
fprintf(stdout,"\n col_sh = %f  r_sh = %f", ColumnShear, RowShear);
fprintf(stdout,"\n %u %u roffset = %u coffset = %u", ColumnsToAdd, RowsToAdd, ColumnsBorderWidth, RowsBorderWidth);

/*	di = CharImageBorder(di,ColumnsToAdd,RowsToAdd,ColumnsBorderWidth,RowsBorderWidth);*/
/*	CharImageColumnShear(di, ColumnShear, RowsBorderWidth, ColumnsBorderWidth);*/
	CharImageColumnShear(di, ColumnShear, 0, 0);
	CharImageWrite(di, "sher.tif");
	CharImageRowShear(di, RowShear, 0, 0);
	CharImageWrite(di, "sher2.tif");
	CharImageColumnShear(di, ColumnShear, 0, 0);
/*	RowsBorderWidth = (uint32)fabs(0.5*(double)Rows * RowShear + 0.5);
	ColumnsToDelete = ColumnsToAdd - RowsBorderWidth;
	ColumnsBorderWidth = (uint32)fabs(0.5*(double)Columns * RowShear + 0.5);
	RowsToDelete = RowsToAdd - ColumnsBorderWidth;
	RowsBorderWidth = (uint32)ceil(0.5*(double)ColumnsToDelete);
	ColumnsBorderWidth = (uint32)ceil(0.5*(double)RowsToDelete);
	di = CharImageCrop(di,ColumnsToDelete,RowsToDelete,ColumnsBorderWidth,RowsBorderWidth);*/
	return di;
}

void CharImageColumnShear(CharImage*di, double shear, uint32 coffset, uint32 roffset)
{
	uint32 column, row;
	int  displacement;
	int  displacement2;
	double weight;
	double dot, rmid;
	unsigned char *pixels;
	dot = (double)( di->rows - 1);
	rmid = 0.5 * dot;
	pixels = (unsigned char*)calloc(di->columns, sizeof(unsigned char));
	for ( row = roffset; row < di->rows - roffset; row++ ) {
		dot = (double)( row );
		dot = shear * (dot - rmid);
		if ( dot > 0 ) {
			displacement = ( int )floor(dot);
			weight = dot - (double)displacement;
			displacement2 = displacement + 1;
		} else {
			displacement = ( int )(-1.0 * floor(-dot));
			weight = -dot + (double)displacement;
			displacement2 = displacement - 1;
		}
fprintf(stdout,"\n row: %u displacement: %d weight = %f", row, displacement, weight);
fflush(stdout);
		for ( column = coffset; column < di->columns - coffset; column++ ) {
			pixels[column] = di->pixels[row*di->columns+column];
			di->pixels[row*di->columns+column] = EmptyPixel;
		}
		for ( column = 0; column < di->columns; column++ ) {
			double p = (double)EmptyPixel;
			double p2 = (double)EmptyPixel;
			int d = -displacement + (int)column;
			int d2 = -displacement2 + (int)column;
			double w = weight;
			if ( 0 <= d && d < di->columns )
				p = (double)pixels[d];
			else
				w = 1.0;
			if ( 0 <= d2 && d2 < di->columns )
				p2 = (double)pixels[d2];
			else
				w = 0.0;
			di->pixels[row*di->columns + column ] = (unsigned char)floor(w * p2 + (1.0 - w) * p);
		}
	}
	free(pixels);
}

void CharImageRowShear(CharImage*di, double shear, uint32 coffset, uint32 roffset)
{
	uint32 column, row;
	int  displacement;
	int  displacement2;
	double weight;
	double dot, cmid;
	unsigned char *pixels;
	pixels = (unsigned char*)calloc(di->rows, sizeof(unsigned char));
	dot = (double)( di->columns - 1);
	cmid = 0.5 * dot;
	for ( column = coffset; column < di->columns - coffset; column++ ) {
		dot = (double)( column );
		dot = shear * (dot - cmid);
		if ( dot > 0 ) {
			displacement = ( int )floor(dot);
			weight = dot - (double)displacement;
			displacement2 = displacement + 1;
		} else {
			displacement = ( int )(-1.0 * floor(-dot));
			weight = -dot + (double)displacement;
			displacement2 = displacement - 1;
		}
		for ( row = roffset; row < di->rows - roffset; row++ ) {
			pixels[row] = di->pixels[row*di->columns+column];
			di->pixels[row*di->columns+column] = EmptyPixel;
		}
		for ( row = 0; row < di->rows; row++ ) {
			double p = (double)EmptyPixel;
			double p2 = (double)EmptyPixel;
			int d = -displacement + (int)row;
			int d2 = -displacement2 + (int)row;
			double w = weight;
			if ( 0 <= d && d < di->rows )
				p = (double)pixels[d];
			else
				w = 1.0;
			if ( 0 <= d2 && d2 < di->rows )
				p2 = (double)pixels[d2];
			else
				w = 0.0;
			di->pixels[ row * di->columns + column] =  (unsigned char)floor(w * p2 + (1.0 - w) * p);
		}
	}
	free(pixels);
}

void StructElemGetNElem(FILE*fp, int *nelem)
{
	fscanf(fp,"%d",nelem);
}

void StructElemGetMedian(FILE*fp, int *median)
{
	fscanf(fp,"%d",median);
}

void StructElemGetElem(FILE*fp, int nelem, int *elem)
{
	int row;
	for ( row = 0; row < nelem; row++)
		fscanf(fp,"%d ", &elem[row]);
}

CharImage*CharImageStructElem(int magnification, char *file)
{
	CharImage*dp;
	int row, column, *elem, nelem, max, median, n;
	uint32 dim;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	elem = (int*)calloc(nelem, sizeof(int));
	StructElemGetElem(fp, nelem, elem);
	fclose(fp);
	max = 0;
	for ( n = 0; n < nelem; n += 2)
		if ( max < elem[n] )
			max = elem[n];
	dim = (uint32)(2 * ( max + 1 ) + 4);
	dp = CharImageNew(dim, dim);
	for ( n = 0; n < nelem; n += 2) {
		column = max + 2 + elem[ n ];
		row = max + 2 + elem[ n + 1 ];
		dp->pixels[ row * (int)dp->columns + column ] = FilledPixel;
	}
	if (magnification) dp = CharImageScale(dp, magnification, magnification);
	return dp;
}

void StructElem(char*fout, int vsize, int hsize, StructElemType type)
{
	FILE*out;
	int row, column, *elem, nelem, halfr, halfc, median, co;
	double a, b, x, c, r;

	if ( vsize < 3 || hsize < 3 || vsize % 2 == 0 || hsize % 2 == 0 )
		g_error("parus: StructElem error in sizes: should be > 3 and odd");
	halfr = (int)( (double)vsize / 2.0 );
	halfc = (int)( (double)hsize / 2.0 );
	out = fopen(fout,"w");
	if ( !out )
		g_error("parus: StructElem can't open %s", fout);
	switch ( type ) {
		case square:
			nelem = 2 * vsize * hsize;
			elem = (int*)calloc(nelem, sizeof(int));
			for ( row = 0; row < vsize; row++)
				for ( column = 0, co = 0; column < 2 * hsize; column += 2, co++) {
					elem[row * 2 * hsize + column] = co - halfc;
					elem[row * 2 * hsize + column + 1] = row - halfr;
				}
		break;
		case disk:
			nelem = 0;
			elem = (int*)NULL;
			a = (double)vsize / 2.0;
			b = (double)hsize / 2.0;
			for ( row = 0; row < vsize; row++) {
				r = (double)( row - halfr ) / a;
				for ( column = 0; column < hsize; column++) {
					c = (double)( column - halfc ) / b;
					x = sqrt( r * r + c * c );
					if ( x <= 1.0 ) {
						elem = (int*)realloc(elem, (nelem + 2) * sizeof(int));
						elem[nelem] = column - halfc;
						elem[nelem + 1] = row - halfr;
						nelem += 2;
					}
				}
			}
		break;
		default:
			g_error("parus: StructElem type is not supported");
	}
	median = (int)floor( 0.5 * ( (double)nelem / 2.0 ) );
	fprintf(out,"%d\n", nelem);
	fprintf(out,"%d\n", median);
	for ( row = 0; row < nelem; row++)
		fprintf(out,"%d ", elem[row]);
	fclose(out);
	free(elem);
}

char*pr_get_name_temp()
{
	char *file;
	int filedescriptor;
	int ret;
#ifndef G_OS_WIN32
	mode_t mode, old_mode;
/* S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH*/
	mode = S_IXOTH;
	old_mode = umask(mode);
#endif
	file = g_strdup("stelXXXXXX");
	filedescriptor = g_mkstemp(file);
	if ( filedescriptor == -1 )
		g_error("pr_get_name_temp: error creating temporary file");
	ret = close(filedescriptor);
	if ( ret == -1 )
		g_error("pr_get_name_temp: error closing file descriptor");
	return file;
}


CharImage*CharImageMedianSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageMedianWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageMedianWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int med;
	int *pattern;
	CharImage*dpm;
	unsigned char *p;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	p = (unsigned char*)calloc(npattern,sizeof(unsigned char));
	dpm = CharImageClone(dp);
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			int offset = 0;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					p[np] = dp->pixels[indexr*dp->columns+indexc];
				} else {
					p[np] = EmptyPixel;
					offset++;
				}
			}
			qsort((void*)p,npattern,sizeof(unsigned char),com1);
			med = offset + (int)ceil( 0.5 * ((double)npattern - (double)offset )) - 1;
			if ( med < npattern )
				dpm->pixels[row*dp->columns+column] = p[med];
			else
				dpm->pixels[row*dp->columns+column] = EmptyPixel;
		}
	}
	CharImageDelete(dp);
	free(p);
	return dpm;
}

CharImage*CharImageFMedianWithSE(CharImage*dp, int npattern, int*pattern, int rank)
{
	uint32 column, row;
	int np, np2;
	int median;
	int index = 0;
	double factor;
	CharImage*dpm;
	dpm = CharImageClone(dp);
	factor = 1.0 / ((double)rank);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,pattern,dpm,npattern,rank,factor) private(np,index,np2,median)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			int offset = 0;
			int hist[256];
			for ( np = 0; np < 256; np++ )
				hist[np] = 0;
			for ( np = 0, np2 = 0; np < npattern; np++, np2 += 2 ) {
				register int indexc = column + pattern[np2];
				register int indexr = row + pattern[np2 + 1];
				if ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					index = (int)(dp->pixels[ indexr * dp->columns + indexc ]);
					hist[index]++;
				} else {
					offset++;
				}
			}
			median = 0;
			index = 0;
			np2 = (int)(factor * (npattern - offset));
			if ( (npattern - offset) % rank )
				np2++;
			for ( np = 0; np < 256; np++ ) {
				median += hist[np];
				if ( median >= np2 ) {
					index = np;
					break;
				}
			}
			dpm->pixels[row*dp->columns+column] = (unsigned char)index;
		}
	}
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageMedian(CharImage*dp, int MedianFlag, int Hflag)
{
	uint32 column, row;
	uint32 np;

	int npattern;
	int median;
	int *pattern;

	CharImage*dpm;

/* 3x3 median
1 1 1
1 1 1
1 1 1
*/
int npattern3 = 9;
int median3 = 4;
int pattern3[18]={-1,-1,-1,0, -1,1,0,-1, 0,0, 0,1, 1,-1, 1,0, 1,1 };

/* 5x5 square median
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
*/
int npattern5 = 25;
int median5 = 12;
int pattern5[50]={0, 2, 1, 2, -1, 2, 0, -2, 1, -2, -1, -2, 2, 1, 2, 0, 2, -1, -2, 1, -2, 0, -2, -1, -1, -1, -1, 0, -1, 1, 0, -1, 0, 0, 0, 1, 1, -1, 1, 0, 1, 1, 2, 2, -2, -2, 2, -2, -2, 2 };

/* 5x5 octagonal median
0 1 1 1 0
1 1 1 1 1
1 1 1 1 1
1 1 1 1 1
0 1 1 1 0
*/
int npattern5o = 21;
int median5o = 10;
int pattern5o[42]={0, 2, 1, 2, -1, 2, 0, -2, 1, -2, -1, -2, 2, 1, 2, 0, 2, -1, -2, 1, -2, 0, -2, -1, -1, -1, -1, 0, -1, 1, 0, -1, 0, 0, 0, 1, 1, -1, 1, 0, 1, 1 };

/* 7x7 octagonal median */
int npattern7o = 37;
int median7o = 18;
int pattern7o[74]={-1,-1, -1,0, -1,1, 0,-1, 0,0, 0,1, 1,-1, 1,0, 1,1, -2,0, 2,0, -3,0, 3,0, -2,1, 2,1, -2,-1, 2,-1, -1,2, -1,-2, 0,2, 0,-2, 1,2, 1,-2, -3,1, -3,-1, 3,1, 3,-1, -2,2, -2,-2, 2,2, 2,-2, -1,3, 1,3, -1,-3, 1,-3, 0,3, 0,-3};

/* elipse 3x5 median */
int npattern3e = 11;
int median3e = 5;
int pattern3e[22]={-1,-1,-1,0, -1,1,0,-1, 0,0, 0,1, 1,-1, 1,0, 1,1,-2,0,2,0};

/* elipse 5x7 median */
int npattern5e = 17;
int median5e = 8;
int pattern5e[34]={-1,-1,-1,0, -1,1,0,-1, 0,0, 0,1, 1,-1, 1,0, 1,1,-2,0,2,0,-3,0,3,0,-2,1,2,1,-2,-1,2,-1};


/* elipse 5x7 median */
int npattern7e = 23;
int median7e = 11;
int pattern7e[46]={-1,-1,-1,0, -1,1,0,-1, 0,0, 0,1, 1,-1, 1,0, 1,1,-2,0,2,0,-3,0,3,0,-2,1,2,1,-2,-1,2,-1,-1,2,-1,-2,0,2,0,-2,1,2,1,-2};


	unsigned char *p;

	if ( MedianFlag == 3 ) {
		npattern = npattern3;
		median = median3;
		pattern = pattern3;
	} else if ( MedianFlag == 5 ) {
		npattern = npattern5;
		median = median5;
		pattern = pattern5;
	} else if ( MedianFlag == 4 ) {
		npattern = npattern3e;
		median = median3e;
		pattern = pattern3e;
	} else if ( MedianFlag == 6 ) {
		npattern = npattern5e;
		median = median5e;
		pattern = pattern5e;
	} else if ( MedianFlag == 7 ) {
		npattern = npattern7e;
		median = median7e;
		pattern = pattern7e;
	} else if ( MedianFlag == 8 ) {
		npattern = npattern7o;
		median = median7o;
		pattern = pattern7o;
	} else if ( MedianFlag == 9 ) {
		npattern = npattern5o;
		median = median5o;
		pattern = pattern5o;
	} else {
		g_error("Median Filter flag unknown %d",MedianFlag);
	}

	p = (unsigned char*)calloc(npattern,sizeof(unsigned char));

	dpm = CharImageClone(dp);

	if ( Hflag == 0 ) {
		for ( column=0; column<dp->columns; column++ ) {
			for ( row=0; row<dp->rows; row++ ) {
				for ( np=0; np<npattern; np++ ) {
					register int indexc = column + pattern[2*np];
					register int indexr = row + pattern[2*np+1];
					p[np] = ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) ? dp->pixels[indexr*dp->columns+indexc] : EmptyPixel ;
				}
				qsort((void*)p,npattern,sizeof(unsigned char),com1);
				dpm->pixels[row*dp->columns+column] = p[median];
			}
		}
	} else if ( Hflag == 1 ) {
		Hist *hist;
		unsigned char mlevel;
		hist = (Hist*)calloc(256, sizeof(Hist));
		for ( column=0; column<dp->columns; column++ ) {
			for ( row=0; row<dp->rows; row++ ) {
				for ( np = 0; np < 256; np++ ) {
					hist[np].npi = 0;
					hist[np].level = (unsigned char)np;
				}
				for ( np=0; np<npattern; np++ ) {
					register int indexc = column + pattern[2*np];
					register int indexr = row + pattern[2*np+1];
					p[np] = ( -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) ? dp->pixels[indexr*dp->columns+indexc] : EmptyPixel ;
					hist[p[np]].npi++;
					hist[p[np]].level = (unsigned char)p[np];
				}
				mlevel = CharImageHistogramMedianLevel(hist);
				dpm->pixels[row*dp->columns+column] = mlevel;
			}
		}
		free(hist);
	}

	CharImageDelete(dp);
	free(p);

	return dpm;
}

int com1(const void*a,const void*s)
{
	unsigned char *z;
	unsigned char *y;
	z = ( unsigned char*)a;
	y = ( unsigned char*)s;
	if ( *z < *y ) {
		return -1;
	}
	if ( *z > *y ) {
		return 1;
	}
	return 0;
}

unsigned char CharImageHistogramMedianLevel(Hist*h)
{
	int l;
	qsort((void*)h, 256, sizeof(Hist), com2);
	l = 0;
	while ( h[l].npi > 0 ) l++;
	return ( (unsigned char)( h[ 1 + (int)((double)l / 2.0 ) ].level ) );
}

int com2(const void*a,const void*s)
{
	Hist *z;
	Hist *y;
	z = ( Hist*)a;
	y = ( Hist*)s;
	if ( z->npi > y->npi ) return -1;
	if ( z->npi < y->npi ) return 1;
	return 0;
}

CharImage*CharImageEdge(CharImage*dp)
{
	uint32 column, row;
	CharImage*dpm;
	dpm = CharImageClone(dp);
	for( row = 1; row < dp->rows - 1; row++) {
		for( column = 1; column < dp->columns - 1; column++) {
			register int vdotc;
			register int vdotr;
			vdotc = -dp->pixels[( row - 1 )*dp->columns + column - 1 ];
			vdotc += dp->pixels[( row - 1 )*dp->columns + column + 1 ];
			vdotc -= dp->pixels[ row *dp->columns + column - 1 ];
			vdotc += dp->pixels[ row *dp->columns + column + 1 ];
			vdotc -= dp->pixels[( row + 1 )*dp->columns + column - 1 ];
			vdotc += dp->pixels[( row + 1 )*dp->columns + column + 1 ];
			vdotr = -dp->pixels[( row - 1 ) * dp->columns + column - 1 ];
			vdotr += dp->pixels[( row + 1 ) * dp->columns + column - 1 ];
			vdotr -= dp->pixels[( row - 1 ) * dp->columns + column ];
			vdotr += dp->pixels[( row + 1 ) * dp->columns + column ];
			vdotr -= dp->pixels[( row - 1 ) * dp->columns + column + 1 ];
			vdotr += dp->pixels[( row + 1 ) * dp->columns + column + 1 ];
			dpm->pixels[row * dpm->columns + column ] = (unsigned char) floor( sqrt( vdotc*vdotc + vdotr*vdotr) / 3.0 );
		}
	}
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageErosion(CharImage*dp)
{
	uint32 column, row;
	CharImage*dpm;

	dpm = CharImageClone(dp);
	for ( column=1; column<dp->columns-1; column++ ) {
		for ( row=1; row<dp->rows-1; row++ ) {
			dpm->pixels[row*dp->columns+column] = dp->pixels[row*dp->columns+column];
			if ( dp->pixels[(row-1)*dp->columns+column] == EmptyPixel ||
				dp->pixels[(row-1)*dp->columns+(column-1)] == EmptyPixel ||
				dp->pixels[(row-1)*dp->columns+(column+1)] == EmptyPixel ||
				dp->pixels[row*dp->columns+(column-1)] == EmptyPixel ||
				dp->pixels[row*dp->columns+(column+1)] == EmptyPixel ||
				dp->pixels[(row+1)*dp->columns+(column-1)] == EmptyPixel ||
				dp->pixels[(row+1)*dp->columns+column] == EmptyPixel ||
				dp->pixels[(row+1)*dp->columns+(column+1)] == EmptyPixel )
				dpm->pixels[row*dp->columns+column] = EmptyPixel;
		}
	}

	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageDilation(CharImage*dp)
{
	uint32 column, row;
	CharImage*dpm;
	dpm = CharImageClone(dp);
	for ( column=1; column<dp->columns-1; column++ ) {
		for ( row=1; row<dp->rows-1; row++ ) {
			dpm->pixels[row*dp->columns+column] = dp->pixels[row*dp->columns+column];
			if ( dp->pixels[(row-1)*dp->columns+column] == FilledPixel ||
				dp->pixels[(row-1)*dp->columns+(column-1)] == FilledPixel ||
				dp->pixels[(row-1)*dp->columns+(column+1)] == FilledPixel ||
				dp->pixels[row*dp->columns+(column-1)] == FilledPixel ||
				dp->pixels[row*dp->columns+(column+1)] == FilledPixel ||
				dp->pixels[(row+1)*dp->columns+(column-1)] == FilledPixel ||
				dp->pixels[(row+1)*dp->columns+column] == FilledPixel ||
				dp->pixels[(row+1)*dp->columns+(column+1)] == FilledPixel )
				dpm->pixels[row*dp->columns+column] = FilledPixel;
		}
	}
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageDryDilation(CharImage*dp)
{
	uint32 column, row;
	CharImage*dpm;
	dpm = CharImageClone(dp);
	for ( column=1; column<dp->columns-1; column++ ) {
		for ( row=1; row<dp->rows-1; row++ ) {
			dpm->pixels[row*dp->columns+column] = dp->pixels[row*dp->columns+column];
			if ( ( dp->pixels[(row-1)*dp->columns+column] == FilledPixel &&
				dp->pixels[(row-1)*dp->columns+(column-1)] == FilledPixel &&
				dp->pixels[(row-1)*dp->columns+(column+1)] == FilledPixel &&
				dp->pixels[(row+1)*dp->columns+(column-1)] == FilledPixel &&
				dp->pixels[(row+1)*dp->columns+(column+1)] == FilledPixel &&
				dp->pixels[(row+1)*dp->columns+column] == FilledPixel ) ||
				( dp->pixels[row*dp->columns+(column-1)] == FilledPixel &&
				dp->pixels[(row-1)*dp->columns+(column-1)] == FilledPixel &&
				dp->pixels[(row+1)*dp->columns+(column-1)] == FilledPixel &&
				dp->pixels[row*dp->columns+(column+1)] == FilledPixel &&
				dp->pixels[(row-1)*dp->columns+(column+1)] == FilledPixel &&
				dp->pixels[(row+1)*dp->columns+(column + 1)] == FilledPixel ) )
				dpm->pixels[row*dp->columns+column] = FilledPixel;
		}
	}
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageFill(CharImage*dp)
{
	uint32 column, row;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;

	uint32 cmid;
	uint32 rmid;

	cmin	= dp->columns;
	cmax	= 0;
	rmin	= dp->rows;
	rmax	= 0;

	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			if( dp->pixels[row*dp->columns+column] == FilledPixel ) {
				if ( row > rmax ) rmax = row;
				if ( row < rmin ) rmin = row;
				if ( column > cmax ) cmax = column;
				if ( column < cmin ) cmin = column;
			}
		}
	}

	cmid = (uint32)( 0.5 * ( cmax - cmin ));
	rmid = (uint32)( 0.5 * ( rmax - rmin ));
/*fprintf(stdout,"\n %u %u %u %u %u %u", cmax, cmin, cmid, rmax, rmin, rmid);*/
	for ( row = rmin; row < rmid; row++ ) {
/*
fprintf(stdout,"\n 1 %u", row);
*/
		column=0;
		while (  column <= cmid ) {
			while ( column < dp->columns && dp->pixels[row*dp->columns+column] == EmptyPixel ) column++;
			while ( column < dp->columns && dp->pixels[row*dp->columns+column] == FilledPixel ) column++;
			if ( column <= cmid ) {
				register uint32 c_column = column;
				register uint32 mid_column, mid_row;
				while ( column <= cmax && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
					column++;
				}
				mid_column = (uint32)( 0.5 * ( column - c_column )) + c_column;
				mid_row = row;
				while ( mid_row >= rmin && dp->pixels[mid_row*dp->columns+mid_column] == EmptyPixel ) mid_row--;
				if ( column <= cmax && mid_row >= rmin ) {
/*fprintf(stdout,"\n 2 fill %u %u", row, column - c_column );*/
					column = c_column;
					while ( column < dp->columns && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
						dp->pixels[row*dp->columns+column] = FilledPixel;
						column++;
					}
				}/* else {
					fprintf(stdout,"\n %u %u",mid_column, mid_row);
				}*/
			}
		}

		column = dp->columns - 1;
		while (  column >= cmid ) {
			while ( column > 0 && dp->pixels[row*dp->columns+column] == EmptyPixel ) column--;
			while ( column > 0 && dp->pixels[row*dp->columns+column] == FilledPixel ) column--;
			if ( column >= cmid ) {
				register uint32 c_column = column;
				register uint32 mid_column, mid_row;
				while ( column >= cmin && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
					column--;
				}
				mid_column = (uint32)( 0.5 * ( c_column - column )) + column;
				mid_row = row;
				while ( mid_row >= rmin && dp->pixels[mid_row*dp->columns+mid_column] == EmptyPixel ) mid_row--;
				if ( column >= cmin  && mid_row >= rmin ) {
/*fprintf(stdout,"\n 3 fill %u %u", row, c_column - column );*/
					column = c_column;
					while ( column > 0 && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
						dp->pixels[row*dp->columns+column] = FilledPixel;
						column--;
					}
				}/* else {
					fprintf(stdout,"\n %u %u",mid_column, mid_row);
				}*/
			}
		}


	}


	for ( row = rmid; row < rmax; row++ ) {
/*fprintf(stdout,"\n 4 %u", row);*/
		column=0;
		while (  column <= cmid ) {
			while ( column < dp->columns && dp->pixels[row*dp->columns+column] == EmptyPixel ) column++;
			while ( column < dp->columns && dp->pixels[row*dp->columns+column] == FilledPixel ) column++;
			if ( column <= cmid ) {
				register uint32 c_column = column;
				register uint32 mid_column, mid_row;
				while ( column <= cmax && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
					column++;
				}
				mid_column = (uint32)( 0.5 * ( column - c_column )) + c_column;
				mid_row = row;
				while ( mid_row <= rmax && dp->pixels[mid_row*dp->columns+mid_column] == EmptyPixel ) mid_row++;
				if ( column <= cmax && mid_row <= rmax ) {
					column = c_column;
					while ( column < dp->columns && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
						dp->pixels[row*dp->columns+column] = FilledPixel;
						column++;
					}
				}/* else {
					fprintf(stdout,"\n %u %u",mid_column, mid_row);
				}*/
			}
		}

		column = dp->columns - 1;
		while (  column >= cmid ) {
			while ( column > 0 && dp->pixels[row*dp->columns+column] == EmptyPixel ) column--;
			while ( column > 0 && dp->pixels[row*dp->columns+column] == FilledPixel ) column--;
			if ( column >= cmid ) {
				register uint32 c_column = column;
				register uint32 mid_column, mid_row;
				while ( column >= cmin && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
					column--;
				}
				mid_column = (uint32)( 0.5 * ( c_column - column )) + column;
				mid_row = row;
				while ( mid_row <= rmax && dp->pixels[mid_row*dp->columns+mid_column] == EmptyPixel ) mid_row++;
				if ( column >= cmin  && mid_row <= rmax ) {
					column = c_column;
					while ( column > 0 && dp->pixels[row*dp->columns+column] == EmptyPixel ) {
						dp->pixels[row*dp->columns+column] = FilledPixel;
						column--;
					}
				}/* else {
					fprintf(stdout,"\n %u %u",mid_column, mid_row);
				} */
			}
		}


	}


	return dp;
}


ImageMoments*CharImageFindMoments(CharImage*dp)
{
	uint32 column, row;
	double Sr;
	double Sc;
	double Srr;
	double Scc;
	double Src;
	double Area;
	double arg;
	double theta;
	ImageMoments*Mom;
	Area	= 0.0;
	Sr	= 0.0;
	Sc	= 0.0;
	Srr	= 0.0;
	Scc	= 0.0;
	Src	= 0.0;
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			if( dp->pixels[row*dp->columns+column] == FilledPixel ) {
				Area	+= 1.0;
				Sr	+= (double)row;
				Sc	+= (double)column;
				Srr	+= (double)( row * row );
				Scc	+= (double)( column * column );
				Src	+= (double)( row * column );
/*fprintf(stdout,"\n %u %f %f %f %f %f %f",column*dp->rows+row, Area, Sr , Sc , Scc, Srr, Src);*/
			}
		}
	}
/*
fprintf(stdout,"\n %f %f %f %f %f %f", Area, Sr , Sc , Scc, Srr, Src);
*/
	Mom		= ( ImageMoments* )malloc(sizeof(ImageMoments));
	Mom->Mr		= Srr - Sr * Sr / Area;
	Mom->Mc		= Scc - Sc * Sc / Area;
	Mom->Mrc	= Src - Sr * Sc / Area;
	arg		= 0.5 *  ( Mom->Mr - Mom->Mc + sqrt( ( Mom->Mr - Mom->Mc ) * ( Mom->Mr - Mom->Mc ) + 4 * Mom->Mrc * Mom->Mrc ) ) / Mom->Mrc ;
	theta		= atan(arg);
	Mom->Theta	= theta;
	return Mom;
}

Attributes*CharAnalyze(unsigned char*data, uint32 dim)
{
  uint32 i;

  double n;

  Attributes*attributes;

  register double mean = 0;
  register double p2 = 0;
  register double point;

  attributes = (Attributes*)malloc(sizeof(Attributes));

  for ( i=0; i<dim; i++ ) {
      point = (double)data[i];
      mean += point;
      p2 += point*point;
   }

  if ( VERBOSE > 0 ) {
    fprintf(stdout,"\nAnalyze: mean = %f p2 = %f n = %u", mean,p2,dim);
  }

  n = (double)dim;

  attributes->mean = mean/n;
  attributes->StDev = sqrt( p2/(n-1.0) - mean*mean/(n*(n-1.0)) );

  if ( VERBOSE > 0 ) {
    fprintf(stdout,"\n mean = %f stdev = %f", attributes->mean,attributes->StDev);
  }

  return attributes;
}

CharImage*CharImageHistogramEquilize(CharImage*dp, uint32 RowStart, uint32 ColumnStart, uint32 RowStop, uint32 ColumnStop)
{
	uint32 i;
	uint32 np;
	double T, S;
	CharImage*dpm;
	int index;
	Hist*histogram;

	histogram = CharImageHistogram(dp,RowStart,ColumnStart,dp->rows,dp->columns);
	dpm = CharImageClone(dp);
	np = (  RowStop - RowStart ) * ( ColumnStop - ColumnStart );
	T = (double)np;

	S = 0;
	for ( index = 0; index < 256; index++) {
		S += (double)histogram[index].npi;
		for ( i = 0; i < histogram[index].npi; i++ ) {
			dpm->pixels[histogram[index].pi[i]] = (unsigned char)floor( FilledPixel * S / T );
		}
	}

	CharImageDelete(dp);
	HistogramFree(histogram);
	return dpm;
}

CharImage*CharImageHistogramNormalize(CharImage*dp)
{
	uint32 i;
	double T, S;
	CharImage*dpm;
	unsigned char max_intensity, min_intensity;
	max_intensity = EmptyPixel;
	min_intensity = FilledPixel;
	dpm = CharImageClone(dp);
	for ( i = 0; i < dp->number; i++ ) {
		if ( max_intensity < dp->pixels[i] ) {
			max_intensity = dp->pixels[i];
		}
		if ( min_intensity > dp->pixels[i] ) {
			min_intensity = dp->pixels[i];
		}
	}
	T = (double)FilledPixel / (double)(max_intensity - min_intensity);
	for ( i = 0; i < dp->number; i++ ) {
		S = T * ( (double)dp->pixels[i] - (double)min_intensity );
		dpm->pixels[i] = (unsigned char)( S + 0.5 );
	}
	CharImageDelete(dp);
	return dpm;
}

CharImage*CharImageHistogramEquilizeWithMask(CharImage*dp, CharImage*mask)
{
	uint32 i;
	int np;
	double T, S;
	CharImage*dpm;
	int index;
	Hist*histogram;
	histogram = CharImageHistogramWithMask(dp, mask);
	dpm = CharImageCopy(dp);
//	dpm = CharImageClone(dp);
	np = 0;
	for ( index = 0; index < mask->number; index++) {
		if ( mask->pixels[index] == FilledPixel )
			np++;
	}
	T = (double)np;
	S = 0;
	for ( index = 0; index < 256; index++) {
		S += (double)histogram[index].npi;
		for ( i = 0; i < histogram[index].npi; i++ ) {
			dpm->pixels[histogram[index].pi[i]] = (unsigned char)floor( FilledPixel * S / T );
		}
	}
	CharImageDelete(dp);
	HistogramFree(histogram);
	return dpm;
}


Hist*CharImageHistogramWithMask(CharImage*dp, CharImage*mask)
{
	uint32 column, row;
	int index;
	Hist*histogram;
	histogram = (Hist*)calloc(256,sizeof(Hist));
	for ( index = 0; index < 256; index++) {
		histogram[index].npi = 0;
		histogram[index].pi = NULL;
		histogram[index].level = (unsigned char)index;
	}
	for ( column = 0; column <  dp->columns; column++ ) {
		for ( row =  0; row < dp->rows; row++ ) {
			if ( mask->pixels[ row * dp->columns + column ] == FilledPixel ) {
				index = (int)dp->pixels[ row * dp->columns + column ];
				histogram[index].pi = (uint32 *)realloc(histogram[index].pi, (histogram[index].npi+1)*sizeof(uint32));
				histogram[index].pi[histogram[index].npi] = row * dp->columns + column;
				histogram[index].npi++;
/*fprintf(stdout,"\n %d %u %u",index,dp->pixels[ row * dp->columns + column ],histogram[index].npi);*/
			}
		}
	}
	return histogram;
}


Hist*CharImageHistogram(CharImage*dp, uint32 RowStart, uint32 ColumnStart, uint32 RowStop, uint32 ColumnStop)
{
	uint32 column, row;
	int index;
	Hist*histogram;

	histogram = (Hist*)calloc(256,sizeof(Hist));
	for ( index = 0; index < 256; index++) {
		histogram[index].npi = 0;
		histogram[index].pi = NULL;
		histogram[index].level = (unsigned char)index;
	}

	for ( column =  ColumnStart; column <  ColumnStop; column++ ) {
		for ( row =  RowStart; row <  RowStop; row++ ) {
			index = (int)dp->pixels[ row * dp->columns + column ];
			histogram[index].pi = (uint32 *)realloc(histogram[index].pi, (histogram[index].npi+1)*sizeof(uint32));
			histogram[index].pi[histogram[index].npi] = row * dp->columns + column;
			histogram[index].npi++;
/*fprintf(stdout,"\n %d %u %u",index,dp->pixels[ row * dp->columns + column ],histogram[index].npi);*/
		}
	}

	return histogram;
}

void HistogramFree(Hist*histogram)
{
	int index;
	for ( index = 0; index < 256; index++)
		free(histogram[index].pi);
	free(histogram);
}

CharImage*CharImageDistance(CharImage*dp)
{
	register uint32 column, row;
	register double q3, q4;
	double *buffer;
	buffer = (double*)calloc( dp->number, sizeof(double));
	for( row = 0; row < dp->number; row++) {
		buffer[row] = (double)dp->pixels[row];
	}
	q3 = 1.0;
/*	q4 =sqrt( 2.0);*/
	q4 =1.5;
/* Forward  */
	for( row = 1; row < dp->rows - 1; row++) {
		for( column = 1; column < dp->columns - 1; column++) {
			register double dist;
			register double d;
			dist = buffer[( row - 1 )*dp->columns + column - 1 ] + q4;
			d = buffer[( row - 1 )*dp->columns + column ] + q3;
			if( d < dist )
				dist = d;
			d = buffer[( row - 1 )*dp->columns + column + 1 ] + q4;
			if( d < dist )
				dist = d;
			d = buffer[row * dp->columns + column - 1 ] + q3;
			if( d < dist )
				dist = d;
			d = buffer[row * dp->columns + column ];
			if( d < dist )
				dist = d;
/*			dp->pixels[row * dp->columns + column ] = ( dist < FilledPixel ) ? (unsigned char)dist : (unsigned char)FilledPixel;*/
			buffer[row * dp->columns + column ] = dist;
		}
	}
/* Backward */
	for( row = dp->rows - 2; row > 0; row--) {
		for( column = dp->columns - 2; column > 0; column--) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			d = buffer[row * dp->columns + column + 1] + q3;
			if( d < dist )
				dist = d;
			d = buffer[(row + 1) * dp->columns + column - 1] + q4;
			if( d < dist )
				dist = d;
			d = buffer[(row + 1) * dp->columns + column ] + q3;
			if( d < dist )
				dist = d;
			d = buffer[(row + 1) * dp->columns + column + 1 ] + q4;
			if( d < dist )
				dist = d;
/*			dp->pixels[row * dp->columns + column ] = ( dist < FilledPixel ) ? (unsigned char)dist : (unsigned char)FilledPixel;*/
			buffer[row * dp->columns + column ] = dist;
		}
	}
	for( row = 0; row < dp->number; row++) {
/*		( dist < FilledPixel ) ? (unsigned char)dist : (unsigned char)FilledPixel;*/
		dp->pixels[row] = (unsigned char)floor(buffer[row]);
	}
	free(buffer);
	return dp;
}

CharImage*CharImageChampherDistance(CharImage*dp, int flag)
{
	int column, row;
	int c, r;
	double *filter;
	int *mask;
	int npoints;
	int nmask;
	int npoints_7 = 16;
	int nmask_7 = 32;
	int mask_7[32] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1,
					-3, -1,
					-1, -3,
					1, -3,
					3, -1,
					-3, -2,
					-2, -3,
					2, -3,
					3, -2 };
	int npoints_5 = 8;
	int nmask_5 = 16;
	int mask_5[16] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1 };
	int npoints_3 = 4;
	int nmask_3 = 8;
	int mask_3[8] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1};
	double *buffer;
	int i, j;
	buffer = (double*)calloc( dp->number, sizeof(double));
	for( row = 0; row < dp->number; row++) {
		buffer[row] = (double)dp->pixels[row];
	}
	if ( flag == 3 ) {
		npoints = npoints_3;
		nmask = nmask_3;
		mask = mask_3;
	} else if ( flag == 5 ) {
		npoints = npoints_5;
		nmask = nmask_5;
		mask = mask_5;
	} else if ( flag == 7 ) {
		npoints = npoints_7;
		nmask = nmask_7;
		mask = mask_7;
	} else
		g_error("Champher Distance: option %d not supported!", flag);
	filter = (double*)calloc( npoints, sizeof(double) );
	for ( i = 0; i < npoints; i++) {
		filter[i] = sqrt( mask[2*i] * mask[2*i] + mask[2*i + 1] * mask[2*i + 1] );
	}
/* Forward  */
/*#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,buffer,npoints,mask,filter) private(i,j,c,r)*/
	for( row = 0; row < dp->rows ; row++) {
		for( column = 0; column < dp->columns ; column++) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column + mask[j];
				r = row + mask[j + 1];
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
/* Backward */
/*#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,buffer,npoints,mask,filter) private(i,j,c,r)*/
	for( row = dp->rows - 1; row >= 0; row--) {
		for( column = dp->columns - 1; column >= 0; column--) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column - mask[j];
				r = row - mask[j + 1];
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
#pragma omp parallel for schedule(static) default(none) shared(dp,buffer)
	for( row = 0; row < dp->number; row++) {
		dp->pixels[row] = (unsigned char)floor(buffer[row]);
	}
	free(buffer);
	free(filter);
	return dp;
}


CharImage*CharImageChampherXDistance(CharImage*dp, int flag)
{
	int column, row;
	int c, r;
	double *filter;
	int *mask;
	int npoints;
	int nmask;
	int npoints_7 = 16;
	int nmask_7 = 32;
	int mask_7[32] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1,
					-3, -1,
					-1, -3,
					1, -3,
					3, -1,
					-3, -2,
					-2, -3,
					2, -3,
					3, -2 };
	int npoints_5 = 8;
	int nmask_5 = 16;
	int mask_5[16] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1 };
	int npoints_3 = 4;
	int nmask_3 = 8;
	int mask_3[8] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1};
	double *buffer;
	int i, j;
	buffer = (double*)calloc( dp->number, sizeof(double));
	for( row = 0; row < dp->number; row++) {
		buffer[row] = (double)dp->pixels[row];
	}
	if ( flag == 3 ) {
		npoints = npoints_3;
		nmask = nmask_3;
		mask = mask_3;
	} else if ( flag == 5 ) {
		npoints = npoints_5;
		nmask = nmask_5;
		mask = mask_5;
	} else if ( flag == 7 ) {
		npoints = npoints_7;
		nmask = nmask_7;
		mask = mask_7;
	} else
		g_error("Champher Distance: option %d not supported!", flag);
	filter = (double*)calloc( npoints, sizeof(double) );
	for ( i = 0; i < npoints; i++) {
		filter[i] = sqrt( mask[2*i] * mask[2*i] + mask[2*i + 1] * mask[2*i + 1] );
	}

/* Forward  */
	for( row = 0; row < dp->rows ; row++) {
		for( column = 0; column < dp->columns ; column++) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column;
				r = row + mask[j + 1];
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
/* Backward */
	for( row = dp->rows - 2; row > 0; row--) {
		for( column = dp->columns - 2; column > 0; column--) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column;
				r = row - mask[j + 1];
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
	for( row = 0; row < dp->number; row++) {
		dp->pixels[row] = (unsigned char)floor(buffer[row]);
	}
	free(buffer);
	free(filter);
	return dp;
}


CharImage*CharImageChampherYDistance(CharImage*dp, int flag)
{
	int column, row;
	int c, r;
	double *filter;
	int *mask;
	int npoints;
	int nmask;
	int npoints_7 = 16;
	int nmask_7 = 32;
	int mask_7[32] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1,
					-3, -1,
					-1, -3,
					1, -3,
					3, -1,
					-3, -2,
					-2, -3,
					2, -3,
					3, -2 };
	int npoints_5 = 8;
	int nmask_5 = 16;
	int mask_5[16] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1,
					-2, -1,
					-1, -2,
					1, -2,
					2, -1 };
	int npoints_3 = 4;
	int nmask_3 = 8;
	int mask_3[8] = { -1, 0,
					0, -1,
					-1, -1,
					1, -1};
	double *buffer;
	int i, j;
	buffer = (double*)calloc( dp->number, sizeof(double));
	for( row = 0; row < dp->number; row++) {
		buffer[row] = (double)dp->pixels[row];
	}
	if ( flag == 3 ) {
		npoints = npoints_3;
		nmask = nmask_3;
		mask = mask_3;
	} else if ( flag == 5 ) {
		npoints = npoints_5;
		nmask = nmask_5;
		mask = mask_5;
	} else if ( flag == 7 ) {
		npoints = npoints_7;
		nmask = nmask_7;
		mask = mask_7;
	} else
		g_error("Champher Distance: option %d not supported!", flag);
	filter = (double*)calloc( npoints, sizeof(double) );
	for ( i = 0; i < npoints; i++) {
		filter[i] = sqrt( mask[2*i] * mask[2*i] + mask[2*i + 1] * mask[2*i + 1] );
	}

/* Forward  */
	for( row = 0; row < dp->rows ; row++) {
		for( column = 0; column < dp->columns ; column++) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column + mask[j];
				r = row;
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
/* Backward */
	for( row = dp->rows - 2; row > 0; row--) {
		for( column = dp->columns - 2; column > 0; column--) {
			register double dist;
			register double d;
			dist = buffer[row * dp->columns + column];
			for ( i = 0, j = 0; i < npoints; i++, j+=2 ) {
				c = column - mask[j];
				r = row;
				if ( 0 <= c && c < dp->columns && 0<= r && r < dp->rows ) {
					d = buffer[ r * dp->columns + c] + filter[i];
					if ( d < dist )
						dist = d;
				}
			}
			buffer[row * dp->columns + column ] = dist;
		}
	}
	for( row = 0; row < dp->number; row++) {
		dp->pixels[row] = (unsigned char)floor(buffer[row]);
	}
	free(buffer);
	free(filter);
	return dp;
}

void CharImageDelete(CharImage*di)
{
	free(di->pixels);
	free(di);
}


CharImage*CharImageComposeMaxN(char*ID, char*extension)
{
	FILE*listfp;
	int list;
	int done;
	struct dirent*direntptr;
	DIR*dirptr;
	char *infile;
	int counter;
	CharImage*di, *di_j;
	list = 0;
	dirptr = opendir(ID);
	if (!dirptr) {
		listfp = fopen(ID,"r");
		if (!listfp)
			g_error("CharImageComposeMaxN Can't open %s neither as directory nor as file!",ID);
		else
			list = 1;
	}
	done = 0;
	if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
		done = 1;
	counter = 0;
/* loop over list */
	while(!counter && !done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di = CharImageCreate(infile);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if ( !counter )
		g_error("Max empty");
	while(!done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di_j = CharImageCreate(infile);
			di = CharImageComposeMax(di,di_j);
			CharImageDelete(di_j);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if (list) {
		fclose(listfp);
	} else {
		closedir(dirptr);
	}
	return di;
}

CharImage*char_image_max(char**files, int n_files)
{
	int counter;
	CharImage*di, *di_j;
	di = CharImageCreate(files[0]);
	for ( counter = 1; counter < n_files; counter++ ) {
		di_j = CharImageCreate(files[counter]);
		di = CharImageComposeMax(di, di_j);
		CharImageDelete(di_j);
	}
	return di;
}

CharImage*CharImageComposePlusN(char*ID, char*extension)
{
	FILE*listfp;
	int list;
	int done;
	struct dirent*direntptr;
	DIR*dirptr;
	int counter;
	CharImage*di, *di_j;
	char *infile;
	list = 0;
	dirptr = opendir(ID);
	if (!dirptr) {
		listfp = fopen(ID,"r");
		if (!listfp)
			g_error("CharImageComposePlusN Can't open %s neither as directory nor as file!",ID);
		else
			list = 1;
	}
	done = 0;
	if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
		done = 1;
	counter = 0;
/* loop over list */
	while(!counter) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di = CharImageCreate(infile);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	while(!done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di_j = CharImageCreate(infile);
			di = CharImageComposePlus(di,di_j);
			CharImageDelete(di_j);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if (list) {
		fclose(listfp);
	} else {
		closedir(dirptr);
	}
	return di;
}

char*iterate_compose(FILE**listfp, DIR**dirptr, char*ID)
{
	char*record;
	char*base;
	int length;
	struct dirent*direntptr;
	char*infile = NULL;
	record = (char*)calloc(MAX_RECORD,sizeof(char));
	base = record;
	if ( !(*dirptr) ) {
		if( ( record = fgets(record, MAX_RECORD, (*listfp) ) ) ) {
			length = strlen(record);
			record[length - 1] = '\0';
/*			if ( g_file_test(record, G_FILE_TEST_EXISTS) )*/
				infile = g_strdup(record);
		}
	} else {
		direntptr = readdir( (*dirptr) );
		if ( ( direntptr = readdir((*dirptr)) ) ) {
			infile = g_build_filename(ID, direntptr->d_name, NULL);
		}
	}
	free(base);
	return infile;
}

CharImage*CharImageAverage(char*ID, char*extension)
{
	FILE*listfp;
	int list;
	int done;
	DIR*dirptr;
	char*record;
	int counter;
	CharImage*di;
	CharImage*di_j;
	char *infile;
	double *AverageImage;
	int j;
	list = 0;
	dirptr = opendir(ID);
	if (!dirptr) {
		listfp = fopen(ID,"r");
		if (!listfp)
			g_error("CharImageAverage: Can't open %s neither as directory nor as file!",ID);
		else
			list = 1;
	}
	done = 0;
	if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
		done = 1;
/* loop over list */
	counter = 0;
	while( !counter && !done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di = CharImageCreate(infile);
			AverageImage = (double*)calloc(di->number, sizeof(double));
			for ( j = 0; j < di->number; j++ ) {
				AverageImage[j] = (double)di->pixels[j];
			}
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if ( !counter )
		g_error("Average empty");
	while(!done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di_j = CharImageCreate(infile);
			for ( j = 0; j < di->number; j++ )
				AverageImage[j] += (double)di_j->pixels[j];
			CharImageDelete(di_j);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if (list) {
		fclose(listfp);
	} else {
		closedir(dirptr);
	}
	if ( counter > 1 ) {
		for ( j = 0; j < di->number; j++ ) {
			di->pixels[j] = (unsigned char)( AverageImage[j] / (double)counter);
		}
		free(AverageImage);
	}
	return di;
}

CharImage*char_image_average(char**files, int size)
{
	int counter;
	CharImage*di;
	CharImage*di_j;
	double *AverageImage;
	int j;
	di = CharImageCreate(files[0]);
	AverageImage = (double*)calloc(di->number, sizeof(double));
#pragma omp parallel for schedule(static) default(none) shared(di,AverageImage)
	for ( j = 0; j < di->number; j++ ) {
		AverageImage[j] = (double)di->pixels[j];
	}
	for ( counter = 1; counter < size; counter++ ) {
		di_j = CharImageCreate(files[counter]);
#pragma omp parallel for schedule(static) default(none) shared(di_j,AverageImage)
		for ( j = 0; j < di_j->number; j++ ) {
			AverageImage[j] += (double)di_j->pixels[j];
		}
		CharImageDelete(di_j);
	}
#pragma omp parallel for schedule(static) default(none) shared(di,AverageImage,size)
	for ( j = 0; j < di->number; j++ ) {
		di->pixels[j] = (unsigned char)( AverageImage[j] / (double)size);
	}
	free(AverageImage);
	return di;
}

HalfImage*CharImageFindHalfImage(CharImage*dp)
{
	HalfImage*hi;
	uint32 column, row, M;
	double L;
	double R;

	hi = (HalfImage*)malloc(sizeof(HalfImage));
	L = 0.0;
	M = 0;
	for ( column=0; column<(uint32)( (double)dp->columns / 2 ); column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			L += (double)dp->pixels[row*dp->columns+column];
			M++;
		}
	}
	L /= (double)M;
	hi->L = L;
	R	= 0.0;
	R = 0;
	for ( column = (uint32)( (double)dp->columns / 2 ); column < dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			R += (double)dp->pixels[row*dp->columns+column];
			M++;
		}
	}
	R /= (double)M;
	hi->R = R;
	return hi;
}

ImageFeatureHalfAxis*CharImageFindFeatureHalfAxis(CharImage*dp)
{
	ImageFeatureHalfAxis*difha;
	uint32 column, row;

	double Cr;
	double Cc;
	double cmin;
	double cmax;
	double rmin;
	double rmax;
	double Area;
	difha = (ImageFeatureHalfAxis*)malloc(sizeof(ImageFeatureHalfAxis));
	Area	= 0.0;
	Cr	= 0.0;
	Cc	= 0.0;
	cmin	= (double)dp->columns;
	cmax	= 0.0;
	rmin	= (double)dp->rows;
	rmax	= 0.0;
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			if( dp->pixels[row*dp->columns+column] == FilledPixel ) {
				Area	+= 1.0;
				Cr	+= (double)row;
				Cc	+= (double)column;
				if ( (double)row > rmax ) rmax = (double)row;
				if ( (double)row < rmin ) rmin = (double)row;
				if ( (double)column > cmax ) cmax = (double)column;
				if ( (double)column < cmin ) cmin = (double)column;
/*fprintf(stdout,"\n %u %f %f %f %f %f %f",column*dp->rows+row, Area, Sr , Sc , Scc, Srr, Src);*/
			}
		}
	}
	Cc /= Area;
	Cr /= Area;
	difha->Cr		= Cr;
	difha->Cc		= Cc;
	difha->Cplus		= cmax - Cc;
	difha->Rplus		= rmax - Cr;
	difha->Cminus		= Cc - cmin;
	difha->Rminus		= Cr - rmin;
	return difha;
}


CharImage*CharImageCropFeature(CharImage*dp, uint32*UpperOffset, uint32*LowerOffset, uint32*LeftOffset, uint32*RightOffset)
{
	uint32 column, row;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	CharImage*di;
	cmin = dp->columns;
	cmax = 0;
	rmin = dp->rows;
	rmax = 0;
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			if ( dp->pixels[ row * dp->columns + column] == FilledPixel ) {
				if ( row > rmax ) rmax = row;
				if ( row < rmin ) rmin = row;
				if ( column > cmax ) cmax = column;
				if ( column < cmin ) cmin = column;
			}
		}
	}
	Rows = (uint32)( rmax - rmin + 1 );
	Columns = (uint32)( cmax - cmin + 1 );
	di = CharImageNew(Columns,Rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di,rmin,cmin)
	for ( column = 0; column < di->columns; column++ ) {
		for ( row = 0; row < di->rows; row++ ) {
			di->pixels[ row * di->columns + column ] = dp->pixels[ ( row + rmin ) * dp->columns + column + cmin];
		}
	}
	*UpperOffset = rmin;
	*LowerOffset = dp->rows -1 - rmax;
	*LeftOffset = cmin;
	*RightOffset = dp->columns -1 - cmax;
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageCropGeometry(CharImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	CharImage*di;
	rmax = dp->rows -1 - LowerOffset;
	cmax = dp->columns -1 - RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax - rmin + 1 );
	Columns = (uint32)( cmax - cmin + 1 );
	di = CharImageNew(Columns,Rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di,rmin,cmin)
	for ( column=0; column<di->columns; column++ ) {
		for ( row=0; row<di->rows; row++ ) {
			di->pixels[row*di->columns+column] = dp->pixels[( row + rmin )*dp->columns+column + cmin];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImagePadGeometry(CharImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	CharImage*di;
	rmax = dp->rows + LowerOffset;
	cmax = dp->columns + RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax + rmin );
	Columns = (uint32)( cmax + cmin );
	di = CharImageNew(Columns,Rows);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di,rmin,cmin)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			di->pixels[( row + rmin ) * di->columns + column + cmin ] = dp->pixels[ row * dp->columns + column];
		}
	}
	CharImageDelete(dp);
	return di;
}


CharImage*CharImageErase(CharImage*dp, double RowsPercent, int RowSign, double ColumnsPercent, int ColumnSign)
{
	uint32 column, row;
	uint32 Columns, Rows;
	double a, b;
	Rows = (uint32)( RowsPercent * dp->rows / 100.0 );
	Columns = (uint32)( ColumnsPercent * dp->columns / 100.0 );
	a = (double)Rows / (double)Columns;
	b = (double)Rows;
	if ( ColumnSign > 0 ) {
		for ( column=Columns; column<dp->columns; column++ ) {
			if ( RowSign < 0 ) {
				for ( row=0; row<(uint32)(a*column-b); row++ ) {
					dp->pixels[row*dp->columns+column] = EmptyPixel;
				}
			} else {
				for ( row=(uint32)(a*column-b); row<dp->rows; row++ ) {
					dp->pixels[row*dp->columns+column] = EmptyPixel;
				}
			}
		}
	} else {
		for ( column=0; column<Columns; column++ ) {
			if ( RowSign < 0 ) {
				for ( row=0; row< (uint32)(-a*column+b); row++ ) {
					dp->pixels[row*dp->columns+column] = EmptyPixel;
				}
			} else {
				for ( row=(uint32)(-a*column+b); row<dp->rows; row++ ) {
					dp->pixels[row*dp->columns+column] = EmptyPixel;
				}
			}
		}
	}
	return dp;
}

double CharImageThresholdHist(CharImage*dp, double a, double b)
{
	uint32 umax;
	int index, max;
	double threshold;
	Hist*histogram;
	histogram = CharImageHistogram(dp, 0, 0, dp->rows, dp->columns);
	umax = histogram[0].npi;
	max = 0;
	for ( index = 1; index < 256; index++) {
		if ( histogram[index].npi > umax ) {
			umax = histogram[index].npi;
			max = index;
		}
	}
	threshold = floor( (double)max * a + b );
	return threshold;
}

double CharImageThresholdHist1(CharImage*dp)
{
	int index, k;
	double *p;
	double omega;
	double mu0, mu1;
	double criterion;
	double dummy;
	double denom;
	double threshold;
	uint32 MaxNumber;
	int histogram[256];
	MaxNumber = dp->number;
	for ( index = 0; index < 256; index++) {
		histogram[index] = 0;
	}
	for ( index = 0; index < MaxNumber; index++) {
		histogram[dp->pixels[index]]++;
	}
	p = (double*)calloc(256,sizeof(double));
	for ( index = 0; index < 256; index++) {
		p[index] = (double)histogram[index] / (double)MaxNumber;
	}
	threshold = computeOtsu(p, 256);
	free(p);
	return threshold;
}

double computeOtsu(double *p, int n)
{
	int index, k;
	double omega;
	double mu0, mu1;
	double criterion;
	double dummy;
	double denom;
	unsigned char threshold;
	k = 1;
	omega = p[k] + p[k-1];
	mu0 = 2 * p[k] + p[k-1];
	mu1 = 0;
	for ( index = k + 1; index < n; index++) {
		mu1 += ( index + 1 ) * p[index];
	}
	dummy = omega * mu1  - ( 1 - omega ) * mu0 ;
	denom = omega * ( 1 - omega );
	criterion = dummy * dummy / denom;
	threshold = (unsigned char)k;
/*fprintf(stdout,"\n %d %u", k,threshold);*/
	for ( k = 2; k < n; k++) {
		register double crit;
		omega += p[k];
		mu0 += ( k + 1 ) * p[k];
		mu1 -= ( k + 1 ) * p[k];
		dummy = omega * mu1  - ( 1 - omega ) * mu0 ;
		denom = omega * ( 1 - omega );
/*fprintf(stdout," %d %f %f %f %f", k,omega * mu1 , ( 1 - omega ) * mu0,denom,omega);*/
		crit = dummy * dummy / denom;
		if ( crit >= criterion ) {
			criterion = crit;
			threshold = (unsigned char)k;
		} else {
			break;
		}
/*fprintf(stdout," %u %f\n", threshold,criterion);*/
	}
	return (double)threshold;
}

void ComputeISEF(double*isef, double b, double*orig, uint32 columns, uint32 rows)
{
	int column, row, number;
	double c, c2;
	double *y1;
	double *y2;
	number = (int)( columns * rows);
	y1 = (double*)calloc(number, sizeof(double));
	y2 = (double*)calloc(number, sizeof(double));
	c = (1-b)/(1+b);
	c2 = b * c;
	for( column = 0; column < (int)columns; column++) {
		y1[column] = c * orig[column];
		y2[ ((int)rows - 1) * (int)columns + column ] = c2 * orig[ ((int)rows - 1) * (int)columns + column ];
	}
	for( row = 1; row < (int)rows ; row++) {
		for( column = 0; column < (int)columns; column++) {
			y1[row * (int)columns + column ] = c * (double)orig[row * (int)columns + column ] + b * y1[ ( row - 1 ) * (int)columns + column ];
		}
	}
	for( row = (int)rows - 2; row >= 0; row--) {
		for( column = 0; column < (int)columns; column++) {
			y2[row * (int)columns + column ] = c2 * (double)orig[row * (int)columns + column ] + b * y2[ ( row + 1 ) * (int)columns + column ];
		}
	}
	for( column = 0; column < (int)columns ; column++) {
		isef[ ( (int)rows-1 ) * (int)columns + column ] = y1[ ( (int)rows-1 ) * (int)columns + column ];
	}
	for( row = 0; row < (int)rows - 1 ; row++) {
		for( column = 0; column < (int)columns ; column++) {
			isef[ row * (int)columns + column ] = y1[ row * (int)columns + column ] + y2[ ( row + 1 ) * (int)columns + column ];
		}
	}
	for( row = 0; row < (int)rows ; row++) {
		y1[ row * (int)columns ] = c * isef[ row * (int)columns ];
		y2[ row * (int)columns + (int)columns - 1] = c2 * isef[ row * (int)columns + (int)columns - 1];
	}
	for( column = 1; column < (int)columns ; column++) {
		for( row = 0; row < (int)rows ; row++) {
			y1[row * (int)columns + column] = c * isef[row * (int)columns + column] + b * y1[row * (int)columns + column - 1];
		}
	}
	for( column = (int)columns - 2; column >= 0 ; column--) {
		for( row = 0; row < (int)rows ; row++) {
			y2[row * (int)columns + column] = c2 * isef[row * (int)columns + column] + b * y2[row * (int)columns + column + 1];
		}
	}
	for( row = 0; row < (int)rows ; row++) {
		isef[row * (int)columns + (int)columns - 1] = y1[row * (int)columns + (int)columns - 1];
	}
	for( row = 0; row < (int)rows; row++) {
		for( column = 0; column < (int)columns - 1; column++) {
			isef[row * (int)columns + column] = y1[row * (int)columns + column] + y2[row * (int)columns + column + 1];
		}
	}

	free(y1);
	free(y2);
}

void ComputeBLI(double*isef1, double*isef2, int*bli, uint32 columns, uint32 rows)
{
	uint32 column, row;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(bli,isef1,isef2,columns,rows)
	for( row = 0; row < rows; row++) {
		for( column = 0; column < columns ; column++) {
			bli[row * columns + column] = ( isef2[row * columns + column] > isef1[row * columns + column] );
		}
	}
}

void ComputeADGRAD(double *isef, int *bli, uint32 rows, uint32 columns, double *adgrad, uint32 window)
{
	uint32 column, row;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(bli,isef,adgrad,columns,rows,window)
	for( row = 1; row < rows - 1; row++) {
		for( column = 1; column < columns - 1 ; column++) {
			adgrad[row * columns + column] = ( ZeroCrossing(isef, bli, row, column, rows, columns) ) ? AdGrad(isef, bli, row, column, rows, columns, window) : 0.0;
		}
	}
}

int ZeroCrossing(double *isef, int *bli, uint32 row, uint32 column, uint32 rows, uint32 columns)
{
	if (bli[row * columns + column] == 1 && bli[ ( row + 1 ) * columns + column] == 0) /* positive z-c */ {
/*		if (isef[ (row+1 ) * columns + column] - isef[ ( row - 1 ) * columns + column] > 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if (bli[row * columns + column] == 1 && bli[row * columns + column+1] == 0 ) /* positive z-c */ {
/*		if (isef[ row * columns + column+1] - isef[ row * columns + column - 1] > 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if ( bli[row * columns + column] == 1 && bli[ ( row-1 ) * columns + column ] == 0) /*negative z-c */ {
/*		if (isef[ ( row + 1 ) * columns + column] - isef[( row - 1 ) * columns + column] < 0 )*/
			return 1;
/*		else
			return 0;*/
	} else if (bli[row * columns + column] == 1 && bli[row * columns + column-1] == 0 ) /*negative z-c */ {
/*		if (isef[ row * columns + column + 1 ] - isef[ row * columns + column - 1] < 0 )*/
			return 1;
/*		else
			return 0;*/
	} /*else if (bli[row * columns + column] == 0 && bli[ ( row + 1 ) * columns + column] == 1) {
		if (isef[ (row+1 ) * columns + column] - isef[ ( row - 1 ) * columns + column] > 0 )
			return 1;
		else
			return 0;
	} else if (bli[row * columns + column] == 0 && bli[row * columns + column+1] == 1 ) {
		if (isef[ row * columns + column+1] - isef[ row * columns + column - 1] > 0 )
			return 1;
		else
			return 0;
	} else if ( bli[row * columns + column] == 0 && bli[ ( row-1 ) * columns + column ] == 1) {
		if (isef[ ( row + 1 ) * columns + column] - isef[( row - 1 ) * columns + column] < 0 )
			return 1;
		else
			return 0;
	} else if (bli[row * columns + column] == 0 && bli[row * columns + column-1] == 1 ) {
		if (isef[ row * columns + column + 1 ] - isef[ row * columns + column - 1] < 0 )
			return 1;
		else
			return 0;
	}*/ else /* not a z-c */
		return 0;
}

double AdGrad(double *isef, int *bli, uint32 row, uint32 column, uint32 rows, uint32 columns, uint32 window)
{
	double adgrad;
	uint32 co, ro;
	register double S1, S2;
	register int N1, N2;
	uint32 halfwin;
	uint32 Rstart, Rfinish, Cstart, Cfinish;
	halfwin = (uint32)( (double)window / 2.0 );
	Rstart = ( (int)row - (int)halfwin >= 0 ) ? row - halfwin : 0;
	Cstart = ((int)column - (int)halfwin >= 0) ? column - halfwin : 0;
	Rfinish = ((int)row + (int)halfwin < (int)rows - 1 ) ? row + halfwin : rows - 1;
	Cfinish = ((int)column + (int)halfwin < (int)columns - 1 ) ? column + halfwin : columns - 1;
	S1 = 0.0;
	S2 = 0.0;
	N1 = 0;
	N2 = 0;
	for ( ro = Rstart; ro <= Rfinish; ro++) {
		for ( co = Cstart; co <= Cfinish; co++) {
			if ( bli[ ro * columns + co ] ) {
				S1 += isef[ro * columns + co];
				N1++;
			} else {
				S2 += isef[ro * columns + co];
				N2++;
			}
		}
	}
	if ( N1 )
		S1 /= (double)N1;
	if ( N2 )
		S2 /= (double)N2;
	adgrad = S2 - S1;
	if ( adgrad < 0 ) adgrad = -adgrad;
	return adgrad;
}

void HysteresisThreshold(CharImage*dpm, double *adgrad, double low, double high, uint32 segment, int connectivity)
{
	uint32 index, max_segment;
	guint8 *mask;
	mask = g_new0(guint8, dpm->number);
	max_segment = dpm->number / 2;
#pragma omp parallel for schedule(static) default(none) shared(dpm)
	for( index = 0; index < dpm->number; index++) {
		dpm->pixels[index] = EmptyPixel;
	}
#pragma omp parallel for schedule(static) default(none) shared(dpm,mask,adgrad,low,high,connectivity,segment,max_segment)
	for( index = 0; index < dpm->number; index++) {
		if ( mask[index] != 1 && adgrad[index] > high ) {
			PList*seg;
			int length;
			seg = Segment(mask, adgrad, index, dpm->rows, dpm->columns, low, connectivity, &length, max_segment);
/*				length = Length(seg);*/
/*
fprintf(stdout,"\n l = %d", length);
fflush(stdout);
*/
			if ( length > segment ) {
				MarkPlist(seg, dpm, FilledPixel);
			} /*else {
					MarkPlist(seg, dpm, EmptyPixel);
				}*/
/*				PListFree(seg);*/
			deletePList(seg);
		}
	}
	g_free(mask);
}

int Length(PList*p)
{
	int length = 0;
	PList*cur;
	cur = p;
	while ( cur ) {
		length++;
		cur = cur->next;
	}
	return length;
}

void MarkPlist(PList*p, CharImage*dpm, unsigned char intensity)
{
	PList*cur;
	cur = p;
	while ( cur ) {
		dpm->pixels[cur->index] = intensity;
		cur = cur->next;
	}
}

void PListFree(PList*p)
{
	if ( p ) {
		PListFree(p->next);
		free(p);
	}
}


void deletePList(PList*p)
{
	PList*cur, *prev;
	while ( p ) {
		prev = p;
		p = p->next;
		free(prev);
	}
}


PList*Segment(guint8 *mask, double *adgrad, uint32 index, uint32 rows, uint32 columns, double low, int connectivity, int *length, uint32 max_segment)
{
	PList*list;
/* non-recursion */
	PList *cur;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	int ccolumn, crow, c, r;
	if ( connectivity != 8 && connectivity != 4 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	} else if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	}
/* */
	list = (PList*)malloc(sizeof(PList));
	list->index = index;
	list->next = (PList*)NULL;
	mask[index] = 1;
/*	PListGo(dp, list, row + 1, column, rows, columns, low, adgrad, connectivity);
	PListGo(dp, list, row - 1, column, rows, columns, low, adgrad, connectivity);
	PListGo(dp, list, row, column + 1, rows, columns, low, adgrad, connectivity);
	PListGo(dp, list, row, column - 1, rows, columns, low, adgrad, connectivity);
	if ( connectivity == 8 ) {
		PListGo(dp, list, row + 1, column + 1, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row + 1, column - 1, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row - 1, column - 1, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row - 1, column + 1, rows, columns, low, adgrad, connectivity);
	}*/
/* */
	(*length) = 1;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n += 2 ) {
			ccolumn = (int) ( cur->index % columns );
			crow = (int) ( (double)( cur->index - ccolumn ) / (double)columns );
			r = ext[n] + crow;
			c = ext[n + 1] + ccolumn;
			index = columns * r + c;
			if ( r >= 0 && r < rows && c >= 0 && c < columns && mask[index] != 1 && adgrad[ index ] > low ) {
				PList*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList*)malloc(sizeof(PList)) ) )
					g_error("PList3dGo can't allocate element");
				(*length)++;
				s->next->index = index;
				s->next->next = (PList*)NULL;
				mask[index] = 1;
			}
			if ( (*length) > max_segment ) {
				return list;
			}
		}
	}
/* */
	return list;
}

void PListGo(CharImage*dp, PList*list, int row, int column, uint32 rows, uint32 columns, double low, double *adgrad, int connectivity)
{
	PList*p;
	PList*s;
	if ( row >= 0 && row < rows && column >= 0 && column < columns && dp->pixels[row * columns + column] != 1 && adgrad[ row * columns + column ] > low ) {
		p = list;
		s = list;
		while ( p ) {
			s = p;
			p = p->next;
		}
		s->next = (PList*)malloc(sizeof(PList));
		s->next->index = (uint32)row * columns + (uint32)column;
		s->next->next = (PList*)NULL;
		dp->pixels[s->next->index] = 1;
		PListGo(dp, list, row + 1, column, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row - 1, column, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row, column + 1, rows, columns, low, adgrad, connectivity);
		PListGo(dp, list, row, column - 1, rows, columns, low, adgrad, connectivity);
		if ( connectivity == 8 ) {
			PListGo(dp, list, row + 1, column + 1, rows, columns, low, adgrad, connectivity);
			PListGo(dp, list, row + 1, column - 1, rows, columns, low, adgrad, connectivity);
			PListGo(dp, list, row - 1, column - 1, rows, columns, low, adgrad, connectivity);
			PListGo(dp, list, row - 1, column + 1, rows, columns, low, adgrad, connectivity);
		}
	}
}

CharImage*CharImageEdgeShenCastan92(CharImage*dp, double a1, double a2, double low, double high, uint32 window, uint32 segment, int connectivity)
{
	CharImage*dpm;
	uint32 n;
	double*orig;
	double*isef1, *isef2;
	int*bli;
	double*adgrad;
	orig = (double*)calloc(dp->number, sizeof(double));
	for ( n = 0; n< dp->number; n++ ) {
		orig[n] = (double)dp->pixels[n];
	}
	isef1 = (double*)calloc(dp->number, sizeof(double));
	ComputeISEF(isef1, a1, orig, dp->columns, dp->rows);
	isef2 = (double*)calloc(dp->number, sizeof(double));
	ComputeISEF(isef2, a2, isef1, dp->columns, dp->rows);
	bli = (int*)calloc(dp->number, sizeof(int));
	ComputeBLI(isef1, isef2, bli, dp->columns, dp->rows);
	dpm = CharImageClone(dp);
#ifdef BLI_IMAGE
for ( n = 0; n< dp->number; n++ ) {
	dpm->pixels[n] = (unsigned char)((double)bli[n] * (double)FilledPixel);
}
CharImageWrite(dpm,"bli.tif");
#endif
	adgrad = (double*)calloc(dp->number, sizeof(double));
	ComputeADGRAD(isef1, bli, dp->rows, dp->columns, adgrad, window);
	free(orig);
	free(bli);
	free(isef1);
	free(isef2);
	HysteresisThreshold(dpm, adgrad, low, high, segment, connectivity);
	free(adgrad);
	return dpm;
}

CharImage*CharImageMask(CharImage*di,CharImage*mask)
{
	uint32 i;
	if ( di->number != mask->number )
		g_error("CharImageInvertWithMask dimensions wrong");
	for ( i = 0; i < di->number; i++)
		if( mask->pixels[i] == EmptyPixel ) {
			di->pixels[i] = EmptyPixel;
		}
	return di;
}

CharImage*CharImageMaskOnFilled(CharImage*di,CharImage*mask)
{
	uint32 i;
	if ( di->number != mask->number )
		g_error("CharImageInvertWithMask dimensions wrong");
#pragma omp parallel for schedule(static) default(none) shared(di,mask)
	for ( i = 0; i < di->number; i++)
		if( mask->pixels[i] == EmptyPixel ) {
			di->pixels[i] = FilledPixel;
		}
	return di;
}

unsigned char CharImageMaxLevel(CharImage*p)
{
	unsigned char Level;
	uint32 i;

	Level = 0;
	for ( i = 0; i< p->number; i++)
		if ( Level < p->pixels[i] )
			Level = p->pixels[i];

	return Level;
}


unsigned char CharImageMinLevel(CharImage*p)
{
	unsigned char Level;
	uint32 i;

	Level = 255;
	for ( i = 0; i< p->number; i++)
		if ( Level > p->pixels[i] )
			Level = p->pixels[i];

	return Level;
}


int NoConnection8(CharImage*d,uint32 index)
{
	uint32 i[9], j;
	unsigned char flag, l, p[9];
	int n;

	n = 0;

	i[0] = index - 1 - d->columns;
	i[1] = index - d->columns;
	i[2] = index + 1 - d->columns;
	i[3] = index + 1;
	i[4] = index + 1 + d->columns;
	i[5] = index + d->columns;
	i[6] = index - 1 + d->columns;
	i[7] = index - 1;
	i[8] = index - 1 - d->columns;

	p[0] = ( d->pixels[i[0]] > EmptyPixel || d->pixels[i[1]] > EmptyPixel || d->pixels[i[8]] > EmptyPixel  ) ? 1:0;
	p[1] = ( d->pixels[i[1]] > EmptyPixel ) ? 1:0;
	p[2] = ( d->pixels[i[2]] > EmptyPixel || d->pixels[i[1]] > EmptyPixel || d->pixels[i[3]] > EmptyPixel ) ? 1:0;
	p[3] = ( d->pixels[i[3]] > EmptyPixel ) ? 1:0;
	p[4] = ( d->pixels[i[4]] > EmptyPixel || d->pixels[i[5]] > EmptyPixel || d->pixels[i[3]] > EmptyPixel ) ? 1:0;
	p[5] = ( d->pixels[i[5]] > EmptyPixel ) ? 1:0;
	p[6] = ( d->pixels[i[6]] > EmptyPixel || d->pixels[i[7]] > EmptyPixel || d->pixels[i[5]] > EmptyPixel ) ? 1:0;
	p[7] = ( d->pixels[i[7]] > EmptyPixel ) ? 1:0;
	p[8] = ( d->pixels[i[8]] > EmptyPixel || d->pixels[i[7]] > EmptyPixel || d->pixels[i[0]] > EmptyPixel ) ? 1:0;

	flag = p[0];
	n = 0;
	l = flag;

	for ( j = 1; j < 9; j++ ) {
		register int fc = p[j];
		if ( flag != fc ) {
			n++;
			flag = fc;
		}
	}

	if ( l == 1 && n < 2 ) return 1;
	if ( l == 1 && n == 2 && flag == l ) return 1;
	if ( l == 0 && n < 3 ) return 1;

	return 0;
}


int NoConnection4(CharImage*d,uint32 index)
{
	uint32 i[5];
	int n;

	n = 0;

	i[0] = index - d->columns;
	i[1] = index + 1;
	i[2] = index + d->columns;
	i[3] = index - 1;
	i[4] = index - d->columns;

	if ( d->pixels[i[0]] > EmptyPixel && d->pixels[i[2]] > EmptyPixel ) return 0;
	if ( d->pixels[i[1]] > EmptyPixel && d->pixels[i[3]] > EmptyPixel ) return 0;

/*

	p[0] = ( d->pixels[i[0]] > EmptyPixel || d->pixels[i[1]] > EmptyPixel || d->pixels[i[4]] > EmptyPixel ) ? 1:0;
	p[1] = ( d->pixels[i[1]] > EmptyPixel || d->pixels[i[0]] > EmptyPixel || d->pixels[i[2]] > EmptyPixel ) ? 1:0;
	p[2] = ( d->pixels[i[2]] > EmptyPixel || d->pixels[i[1]] > EmptyPixel || d->pixels[i[3]] > EmptyPixel ) ? 1:0;
	p[3] = ( d->pixels[i[3]] > EmptyPixel || d->pixels[i[2]] > EmptyPixel || d->pixels[i[4]] > EmptyPixel ) ? 1:0;
	p[4] = ( d->pixels[i[4]] > EmptyPixel || d->pixels[i[0]] > EmptyPixel || d->pixels[i[3]] > EmptyPixel ) ? 1:0;

	flag = p[0];
	n = 0;
	l = flag;

	for ( j = 1; j < 5; j++ ) {
		register int fc = p[j];
		if ( flag != fc ) {
			n++;
			flag = fc;
		}
	}

	if ( l == 1 && n < 2 ) return 1;
	if ( l == 1 && n == 2 && flag == l ) return 1;
	if ( l == 0 && n < 3 ) return 1;
*/

	return 1;
}


int NoConnectionEDM(CharImage*d,uint32 index, CharImage*edm,unsigned char level)
{
	uint32 i[9], j;
	unsigned char flag, l, p[9];
	int n;

	n = 0;

	i[0] = index - 1 - d->columns;
	i[1] = index - d->columns;
	i[2] = index + 1 - d->columns;
	i[3] = index + 1;
	i[4] = index + 1 + d->columns;
	i[5] = index + d->columns;
	i[6] = index - 1 + d->columns;
	i[7] = index - 1;
	i[8] = index - 1 - d->columns;

	p[0] = ( ( d->pixels[i[0]] > EmptyPixel && edm->pixels[i[0]] != level ) || ( d->pixels[i[1]] > EmptyPixel && edm->pixels[i[1]] != level ) || ( d->pixels[i[8]] > EmptyPixel && edm->pixels[i[8]] != level ) ) ? 1:0;
	p[1] = ( ( d->pixels[i[1]] > EmptyPixel && edm->pixels[i[1]] != level ) ) ? 1:0;
	p[2] = ( ( d->pixels[i[2]] > EmptyPixel && edm->pixels[i[2]] != level ) || ( d->pixels[i[1]] > EmptyPixel && edm->pixels[i[1]] != level ) || ( d->pixels[i[3]] > EmptyPixel && edm->pixels[i[3]] != level ) ) ? 1:0;
	p[3] = ( ( d->pixels[i[3]] > EmptyPixel && edm->pixels[i[3]] != level )) ? 1:0;
	p[4] = ( ( d->pixels[i[4]] > EmptyPixel && edm->pixels[i[4]] != level ) || ( d->pixels[i[5]] > EmptyPixel && edm->pixels[i[5]] != level ) || ( d->pixels[i[3]] > EmptyPixel && edm->pixels[i[3]] != level ) ) ? 1:0;
	p[5] = ( ( d->pixels[i[5]] > EmptyPixel && edm->pixels[i[5]] != level ) ) ? 1:0;
	p[6] = ( ( d->pixels[i[6]] > EmptyPixel && edm->pixels[i[6]] != level ) || ( d->pixels[i[7]] > EmptyPixel && edm->pixels[i[7]] != level ) || ( d->pixels[i[5]] > EmptyPixel && edm->pixels[i[5]] != level ) ) ? 1:0;
	p[7] = ( ( d->pixels[i[7]] > EmptyPixel && edm->pixels[i[7]] != level ) ) ? 1:0;
	p[8] = ( ( d->pixels[i[8]] > EmptyPixel && edm->pixels[i[8]] != level ) || ( d->pixels[i[7]] > EmptyPixel && edm->pixels[i[7]] != level ) || ( d->pixels[i[0]] > EmptyPixel && edm->pixels[i[0]] != level ) ) ? 1:0;

	flag = p[0];
	n = 0;
	l = flag;

	for ( j = 1; j < 9; j++ ) {
		register int fc = p[j];
		if ( flag != fc ) {
			n++;
			flag = fc;
		}
	}

	if ( l == 1 && n < 2 ) return 1;
	if ( l == 1 && n == 2 && flag == l ) return 1;
	if ( l == 0 && n < 3 ) return 1;

	return 0;
}


int ConnectionColumn(CharImage*cim,uint32 index,int sign)
{
	int flags = 0;

	if ( cim->pixels[index - cim->columns+sign] > 0 ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+sign] > 0  ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+cim->columns+sign] > 0 ) { /* == FilledPixel ) {*/
		flags++;
	}
/*fprintf(stdout,"\n column %u %d %d",index,sign,flags);*/
	return flags;
}


int ConnectionRow(CharImage*cim,uint32 index,int sign)
{
	int flags = 0;

	if ( cim->pixels[index+1+sign*cim->columns] > 0  ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+sign*cim->columns] > 0  ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index-1+sign*cim->columns] > 0  ) { /* == FilledPixel ) {*/
		flags++;
	}

/*fprintf(stdout,"\n row %u %d %d",index,sign,flags);*/
	return flags;
}



int ConnectionColumnEdm(CharImage*cim,uint32 index,int sign,CharImage*edm,unsigned char level)
{
	int flags = 0;

	if ( cim->pixels[index - cim->columns+sign] > 0 && edm->pixels[index - cim->columns+sign] != level ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+sign] > 0  && edm->pixels[index+sign] != level ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+cim->columns+sign] > 0  && edm->pixels[index + cim->columns+sign] != level ) { /* == FilledPixel ) {*/
		flags++;
	}
/*fprintf(stdout,"\n column %u %d %d",index,sign,flags);*/
	return flags;
}


int ConnectionRowEdm(CharImage*cim,uint32 index,int sign,CharImage*edm,unsigned char level)
{
	int flags = 0;

	if ( cim->pixels[index+1+sign*cim->columns] > 0  && edm->pixels[index+1+sign*cim->columns] != level ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index+sign*cim->columns] > 0 && edm->pixels[index+sign*cim->columns] != level ) { /* == FilledPixel ) {*/
		flags++;
	}

	if ( cim->pixels[index-1+sign*cim->columns] > 0  && edm->pixels[index-1+sign*cim->columns] != level) { /* == FilledPixel ) {*/
		flags++;
	}

/*fprintf(stdout,"\n row %u %d %d",index,sign,flags);*/
	return flags;
}


/*
				if ( di->pixels[PixelIndices[i]] == EmptyPixel ) {
					fprintf(stdout,"\n Empty %u", PixelIndices[i]);
				}
				if ( di->pixels[PixelIndices[i]] != EmptyPixel ) {
					int flags = 0;
					int nflag = 0;
					int sflag = 0;
					int wflag = 0;
					int eflag = 0;
					int nwflag = 0;
					int swflag = 0;
					int neflag = 0;
					int seflag = 0;

					if ( cim->pixels[PixelIndices[i] - cim->columns] == FilledPixel ) {
						nflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]-cim->columns-1] == FilledPixel ) {
						nwflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]-cim->columns+1] == FilledPixel ) {
						neflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]-1] == FilledPixel ) {
						wflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]+1] == FilledPixel ) {
						eflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]+cim->columns-1] == FilledPixel ) {
						seflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]+cim->columns] == FilledPixel ) {
						sflag = 1;
						flags++;
					}

					if ( cim->pixels[PixelIndices[i]+cim->columns+1] == FilledPixel ) {
						swflag = 1;
						flags++;
					}

					if ( flags == 1 ) {
						dpm->pixels[PixelIndices[i]] = FilledPixel;
					} else if ( ( nflag && sflag ) || ( nflag && eflag ) || ( nflag && wflag ) || ( sflag && eflag ) || ( sflag && wflag) || ( eflag && wflag ) ) {
						dpm->pixels[PixelIndices[i]] = FilledPixel;
					}
				}
*/

/*
			for ( column=1; column<di->columns-1; column++ ) {
				for ( row=1; row<di->rows-1; row++ ) {
					dpm->pixels[row*dpm->columns+column] = cim->pixels[row*cim->columns+column];
					if ( di->pixels[row*dpm->columns+column] != EmptyPixel ) {
						int flags = 0;
						int nflag = 0;
						int sflag = 0;
						int wflag = 0;
						int eflag = 0;
						int nwflag = 0;
						int swflag = 0;
						int neflag = 0;
						int seflag = 0;

						if ( cim->pixels[(row-1)*cim->columns+column] == FilledPixel ) {
							nflag = 1;
							flags++;
						}

						if ( cim->pixels[(row-1)*cim->columns+(column-1)] == FilledPixel ) {
							nwflag = 1;
							flags++;
						}

						if ( cim->pixels[(row-1)*cim->columns+(column+1)] == FilledPixel ) {
							neflag = 1;
							flags++;
						}

						if ( cim->pixels[row*cim->columns+(column-1)] == FilledPixel ) {
							wflag = 1;
							flags++;
						}

						if ( cim->pixels[row*cim->columns+(column+1)] == FilledPixel ) {
							eflag = 1;
							flags++;
						}

						if ( cim->pixels[(row+1)*cim->columns+(column-1)] == FilledPixel ) {
							seflag = 1;
							flags++;
						}

						if ( cim->pixels[(row+1)*cim->columns+column] == FilledPixel ) {
							sflag = 1;
							flags++;
						}

						if ( cim->pixels[(row+1)*cim->columns+(column+1)] == FilledPixel ) {
							swflag = 1;
							flags++;
						}

						if ( flags == 1 ) {
							dpm->pixels[row*dpm->columns+column] = FilledPixel;
						} else if ( ( nflag && sflag ) || ( nflag && eflag ) || ( nflag && wflag ) || ( sflag && eflag ) || ( sflag && wflag) || ( eflag && wflag ) ) {
							dpm->pixels[row*dpm->columns+column] = FilledPixel;
						}
					}
				}
			}
*/

int Visible(CharImage*di,uint32 index)
{
	register int sum = di->pixels[index];
/*	register int dot = sum;

	sum += di->pixels[index-di->columns];
	sum += di->pixels[index+di->columns];

	sum += di->pixels[index-di->columns-1];
	sum += di->pixels[index+di->columns-1];

	sum += di->pixels[index-di->columns+1];
	sum += di->pixels[index+di->columns+1];

	sum += di->pixels[index-1];
	sum += di->pixels[index+1];

	return ( 9*dot > sum ) ? 1:0;
*/
/*
	if ( sum > di->pixels[index-di->columns] )
	if ( sum > di->pixels[index+di->columns] )

	if ( sum > di->pixels[index-di->columns-1] )
	if ( sum > di->pixels[index+di->columns-1] )

	if ( sum > di->pixels[index-di->columns+1] )
	if ( sum > di->pixels[index+di->columns+1] )

	if ( sum > di->pixels[index-1] )
	if ( sum > di->pixels[index+1] ) return 1;
*/
	if ( sum > 0 ) return 1;
	return 0;
}

/*

int com_hist(const void*a,const void*s)
{
  int *z;
  int *y;

  z = ( int*)a;
  y = ( int*)s;

 if ( histogram[*z].npi < histogram[*y].npi ) return -1;
 if ( histogram[*z].npi == histogram[*y].npi ) return 0;
 if ( histogram[*z].npi > histogram[*y].npi ) return 1;
}


CharImage*Thinning(CharImage*d,uint32 NMAX)
{
	uint32 row, column;

	int NoChangeMade = 0;
	uint32 kounter = 0;

	kounter = 0;
	while ( kounter < NMAX && !NoChangeMade ) {
		NoChangeMade = 1;

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}


		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}
			}
		}


		kounter++;
	}


	kounter = 0;
	while ( kounter < NMAX && !NoChangeMade ) {
		NoChangeMade = 1;

		for (row = 1; row < I->rows-1; row++) {
			for (column = 1; column < I->columns - 1; column++) {
				int nmatches;
				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 6;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[row*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

				nmatches = 7;

				if ( I->pixels[row*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column+1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row-1)*I->columns+column-1] == FilledPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column+1] == EmptyPixel ) nmatches--;
				if ( I->pixels[(row+1)*I->columns+column-1] == EmptyPixel ) nmatches--;

				if ( !nmatches ) {
					I->pixels[row*I->columns+column] = EmptyPixel;
					NoChangeMade = 0;
				}

			}
		}

		kounter++;
	}


	return I;
}

CharImage*Prunning(CharImage*I,uint32 NMAX)
{

}


*/

CharImage*CharImageScale(CharImage*ci, double rfactor, double cfactor)
{
	CharImage*resized;
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)(cfactor * (double)ci->columns), (uint32)(rfactor * (double)ci->rows) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
/* rfactor plays in vertical direction */
	if ( rfactor > cfactor ) {
		CharImage*resizedvert;
		resizedvert = CharImageNew( ci->columns, (uint32)(rfactor * (double)ci->rows) );
		CharImageScaleVertical( ci, rfactor, resized );
		CharImageScaleHorizontal( resizedvert, cfactor, resized );
		CharImageDelete(resizedvert);
	} else {
		CharImage*resizedhor;
		resizedhor = CharImageNew( (uint32)(cfactor * (double)ci->columns), ci->rows );
		CharImageScaleHorizontal( ci, cfactor, resizedhor);
		CharImageScaleVertical( resizedhor, rfactor, resized);
		CharImageDelete(resizedhor);
	}
	return resized;
}


void CharImageScaleVertical(CharImage*ci, double factor, CharImage*resized)
{
# define support 2
	uint32 row;
	uint32 column;
	double scale;
	int n;
	scale = ( 1.0 < factor ) ? 1.0 : factor;
	for ( row = 0; row < resized->rows; row++ ) {
		uint32 rows[2*support];
		double contrib[2*support];
		double center;
		double start;
		double stop;
		register double normalize;

		center = ( (double)row + 0.5 ) / factor;
		start = ( center - support + 0.5 > 0.0 ) ? center - support + 0.5 : 0.0;
		stop = ( center + support + 0.5 < (double)ci->rows ) ? center + support + 0.5 : (double)ci->rows;

		normalize = 0;
		for ( n = 0; n < (int)(stop - start); n++ ) {
			rows[n] = (uint32)start + (uint32)n;
			contrib[n] = Cubic( scale * ( start + n - center + 0.5 ));
			normalize += contrib[n];
		}
		if ( normalize != 0.0 && normalize != 1.0 ) {
			for ( n = 0; n < (int)(stop - start); n++ ) {
				contrib[n] /= normalize;
			}
		}
		for ( column = 0; column < resized->columns; column++ ) {
			for ( n = 0; n < (int)(stop - start); n++ ) {
				resized->pixels[row*resized->columns+column] += contrib[n] * ci->pixels[rows[n]*ci->columns+column];
			}
		}
	}
}


void CharImageScaleHorizontal(CharImage*ci, double factor, CharImage*resized)
{
# define support 2
	uint32 row;
	uint32 column;
	double scale;

	int n;

	scale = ( 1.0 < factor ) ? 1.0 : factor;

	for ( column = 0; column < resized->columns; column++ ) {
		uint32 columns[2*support];
		double contrib[2*support];
		double center;
		double start;
		double stop;
		register double normalize;

		center = ( (double)column + 0.5 ) / factor;
		start = ( center - support + 0.5 > 0.0 ) ? center - support + 0.5 : 0.0;
		stop = ( center + support + 0.5 < (double)ci->columns ) ? center + support + 0.5 : (double)ci->columns;

		normalize = 0;
		for ( n = 0; n < (int)(stop - start); n++ ) {
			columns[n] = (uint32)start + (uint32)n;
			contrib[n] = Cubic( scale * ( start + n - center + 0.5 ));
			normalize += contrib[n];
		}
		if ( normalize != 0.0 && normalize != 1.0 ) {
			for ( n = 0; n < (int)(stop - start); n++ ) {
				contrib[n] /= normalize;
			}
		}
		for ( row = 0; row < resized->rows; row++ ) {
			for ( n = 0; n < (int)(stop - start); n++ ) {
				resized->pixels[row*resized->columns+column] += contrib[n] * ci->pixels[row*ci->columns+columns[n]];
			}
		}
	}
}

double Cubic(double x)
{
  if (x < -2.0)
    return(0.0);
  if (x < -1.0)
    return((2.0+x)*(2.0+x)*(2.0+x)/6.0);
  if (x < 0.0)
    return((4.0+x*x*(-6.0-3.0*x))/6.0);
  if (x < 1.0)
    return((4.0+x*x*(-6.0+3.0*x))/6.0);
  if (x < 2.0)
    return((2.0-x)*(2.0-x)*(2.0-x)/6.0);
  return(0.0);
}

ShapeDescriptor*ShapeDescriptorNew(Blob*b, uint32 rows, uint32 columns)
{
	ShapeDescriptor*s;
	uint32*p;
	uint32 np;
	double m00;
	double m01;
	double m10;
	double xmean;
	double ymean;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double mu00;
	double mu02;
	double mu20;
	double mu11;
	double mu12;
	double mu21;
	double mu30;
	double mu03;
	double eta02;
	double eta20;
	double eta11;
	double eta12;
	double eta21;
	double eta30;
	double eta03;
	double I_1;
	double I_2;
	double I_3;
	double I_4;
	double I_5;
	double I_6;
	double I_7;
	uint32 i;
	double *row, *column, a;

	p = b->nuclear_pixels;
	np = b->Nnuclear_pixels;
	row = (double*)calloc(np, sizeof(double));
	column = (double*)calloc(np, sizeof(double));
	m00 = 0.0;
	m01 = 0.0;
	m10 = 0.0;
	xmin = DBL_MAX;
	xmax = 0;
	ymin = DBL_MAX;
	ymax = 0;
	for ( i = 0; i < np; i++ ) {
		column[i] = (double) ( p[i] % columns );
		row[i] = (double) ( (double)( (double)p[i] - column[i] ) / (double)columns );
		m00 += 1.0;
		m01 += row[i];
		m10 += column[i];
		if ( xmin > column[i] )
			xmin = column[i];
		if ( xmax < column[i] )
			xmax = column[i];
		if ( ymin > row[i] )
			ymin = row[i];
		if ( ymax < row[i] )
			ymax = row[i];
	}
	if ( m00 == 0 )
		g_error("ShapeDesciptorNew: empty blob");
	a = m00;
	xmean = m10 / a;
	ymean = m01 / a;
	mu00 = m00;
	mu02 = 0.0;
	mu20 = 0.0;
	mu11 = 0.0;
	mu12 = 0.0;
	mu21 = 0.0;
	mu30 = 0.0;
	mu03 = 0.0;
	for ( i = 0; i < np; i++ ) {
		register double r = row[i] - ymean;
		register double c = column[i] - xmean;
		mu02 += r * r;
		mu20 += c * c;
		mu11 += c * r;
		mu12 += c * r * r;
		mu21 += c * c * r;
		mu30 += c * c * c;
		mu03 += r * r * r;
	}
	a *= mu00;
	eta02 = mu02 / a;
	eta20 = mu20 / a;
	eta11 = mu11 / a;
	a *= sqrt(mu00);
	eta12 = mu12 / a;
	eta21 = mu21 / a;
	eta30 = mu30 / a;
	eta03 = mu03 / a;
	I_1 = eta20 + eta02;
	I_2 = (eta20 - eta02) * (eta20 - eta02) + 4.0 * eta11 * eta11;
	I_3 = (eta30 - 3.0 * eta12) * (eta30 - 3.0 * eta12) + (3.0 * eta21 - eta03) * (3.0 * eta21 - eta03);
	I_4 = (eta30 + eta12) * (eta30 + eta12) + (eta21 + eta03) * (eta21 + eta03);
	I_5 = (eta30 - 3.0 * eta12) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03) ) + (3.0 * eta21 - eta03) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03));
	I_6 = (eta20 - eta02) * ( (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03) + 4.0 * eta11 * (eta30 + eta12) * (eta21 + eta03) );
	I_7 = ( 3.0 * eta21 - eta03 ) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03)) + (eta30 - 3.0 * eta12) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) );
	free(row);
	free(column);
	s = (ShapeDescriptor*)malloc(sizeof(ShapeDescriptor));
	s->m00 = m00;
	s->m01 = m01;
	s->m10 = m10;
	s->xmean = xmean;
	s->ymean = ymean;
	s->xmin = xmin;
	s->ymin = ymin;
	s->xmax = xmax;
	s->ymax = ymax;
	s->mu00 = mu00;
	s->mu02 = mu02;
	s->mu20 = mu20;
	s->mu11 = mu11;
	s->mu12 = mu12;
	s->mu21 = mu21;
	s->mu30 = mu30;
	s->mu03 = mu03;
	s->eta02 = eta02;
	s->eta20 = eta20;
	s->eta11 = eta11;
	s->eta12 = eta12;
	s->eta21 = eta21;
	s->eta30 = eta30;
	s->eta03 = eta03;
	s->I_1 = I_1;
	s->I_2 = I_2;
	s->I_3 = I_3;
	s->I_4 = I_4;
	s->I_5 = I_5;
	s->I_6 = I_6;
	s->I_7 = I_7;
	s->rotation_angle = 0.5 * atan( 2 * mu11 / ( mu02 - mu20 ) );
	s->mu20_prime = cos(s->rotation_angle) * cos(s->rotation_angle) * mu20 + sin(s->rotation_angle) * sin(s->rotation_angle) * mu02 - sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu02_prime = sin(s->rotation_angle) * sin(s->rotation_angle) * mu20 + cos(s->rotation_angle) * cos(s->rotation_angle) * mu02 + sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu11_prime = 0.5 * sin( 2.0 * s->rotation_angle ) * ( mu20 - mu02 ) + mu11 * cos( 2.0 * s->rotation_angle );
	a = s->mu02_prime;
	a = a*a*a;
	a = 4 * sqrt( a / s->mu20_prime ) / (double)MY_PI_CONST;
	a = sqrt( sqrt ( a ) );
	s->ellipse_axis_b = a;
	s->ellipse_axis_a = 4 * s->mu02_prime / ( (double)MY_PI_CONST * a * a * a );
	return s;
}

void ShapeDescriptorPrint(GKeyFile*gkf, ShapeDescriptor*s, char*grp)
{
	char *buf;
	buf = g_strdup_printf("%13.6f", s->m00);
	g_key_file_set_value(gkf, grp, "m00", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->m01);
	g_key_file_set_value(gkf, grp, "m01", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->m10);
	g_key_file_set_value(gkf, grp, "m10", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->xmean);
	g_key_file_set_value(gkf, grp, "xmean", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ymean);
	g_key_file_set_value(gkf, grp, "ymean", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->xmin);
	g_key_file_set_value(gkf, grp, "xmin", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->xmax);
	g_key_file_set_value(gkf, grp, "xmax", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ymin);
	g_key_file_set_value(gkf, grp, "ymin", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ymax);
	g_key_file_set_value(gkf, grp, "ymax", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu00);
	g_key_file_set_value(gkf, grp, "mu00", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu02);
	g_key_file_set_value(gkf, grp, "mu02", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu20);
	g_key_file_set_value(gkf, grp, "mu20", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu11);
	g_key_file_set_value(gkf, grp, "mu11", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu12);
	g_key_file_set_value(gkf, grp, "mu12", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu21);
	g_key_file_set_value(gkf, grp, "mu21", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu30);
	g_key_file_set_value(gkf, grp, "mu30", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu03);
	g_key_file_set_value(gkf, grp, "mu03", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta02);
	g_key_file_set_value(gkf, grp, "eta02", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta20);
	g_key_file_set_value(gkf, grp, "eta20", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta11);
	g_key_file_set_value(gkf, grp, "eta11", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta12);
	g_key_file_set_value(gkf, grp, "eta12", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta21);
	g_key_file_set_value(gkf, grp, "eta21", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta30);
	g_key_file_set_value(gkf, grp, "eta30", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->eta03);
	g_key_file_set_value(gkf, grp, "eta03", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_1);
	g_key_file_set_value(gkf, grp, "I_1", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_2);
	g_key_file_set_value(gkf, grp, "I_2", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_3);
	g_key_file_set_value(gkf, grp, "I_3", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_4);
	g_key_file_set_value(gkf, grp, "I_4", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_5);
	g_key_file_set_value(gkf, grp, "I_5", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_6);
	g_key_file_set_value(gkf, grp, "I_6", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->I_7);
	g_key_file_set_value(gkf, grp, "I_7", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->rotation_angle);
	g_key_file_set_value(gkf, grp, "rotation_angle", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu20_prime);
	g_key_file_set_value(gkf, grp, "mu20_prime", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu02_prime);
	g_key_file_set_value(gkf, grp, "mu02_prime", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->mu11_prime);
	g_key_file_set_value(gkf, grp, "mu11_prime", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ellipse_axis_b);
	g_key_file_set_value(gkf, grp, "ellipse_axis_b", buf);
	g_free(buf);
	buf = g_strdup_printf("%13.6f", s->ellipse_axis_a);
	g_key_file_set_value(gkf, grp, "ellipse_axis_a", buf);
	g_free(buf);
}
/*
ShapeDescriptor*ShapeDescriptorGrayNew(CharImage*ci, uint32 rows, uint32 columns)
{
	ShapeDescriptor*s;
	uint32*p;
	uint32 np;
	double m00;
	double m01;
	double m10;
	double xmean;
	double ymean;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double mu00;
	double mu02;
	double mu20;
	double mu11;
	double mu12;
	double mu21;
	double mu30;
	double mu03;
	double eta02;
	double eta20;
	double eta11;
	double eta12;
	double eta21;
	double eta30;
	double eta03;
	double I_1;
	double I_2;
	double I_3;
	double I_4;
	double I_5;
	double I_6;
	double I_7;
	uint32 i;
	double *row, *column, a;

	p = b->nuclear_pixels;
	np = b->Nnuclear_pixels;
	row = (double*)calloc(np, sizeof(double));
	column = (double*)calloc(np, sizeof(double));
	m00 = 0.0;
	m01 = 0.0;
	m10 = 0.0;
	xmin = DBL_MAX;
	xmax = 0;
	ymin = DBL_MAX;
	ymax = 0;
	for ( i = 0; i < np; i++ ) {
		column[i] = (double) ( p[i] % columns );
		row[i] = (double) ( (double)( (double)p[i] - column[i] ) / (double)columns );
		m00 += 1.0;
		m01 += row[i];
		m10 += column[i];
		if ( xmin > column[i] )
			xmin = column[i];
		if ( xmax < column[i] )
			xmax = column[i];
		if ( ymin > row[i] )
			ymin = row[i];
		if ( ymax < row[i] )
			ymax = row[i];
	}
	if ( m00 == 0 )
		g_error("ShapeDesciptorNew: empty blob");
	a = m00;
	xmean = m10 / a;
	ymean = m01 / a;
	mu00 = m00;
	mu02 = 0.0;
	mu20 = 0.0;
	mu11 = 0.0;
	mu12 = 0.0;
	mu21 = 0.0;
	mu30 = 0.0;
	mu03 = 0.0;
	for ( i = 0; i < np; i++ ) {
		register double r = row[i] - ymean;
		register double c = column[i] - xmean;
		mu02 += r * r;
		mu20 += c * c;
		mu11 += c * r;
		mu12 += c * r * r;
		mu21 += c * c * r;
		mu30 += c * c * c;
		mu03 += r * r * r;
	}
	a *= mu00;
	eta02 = mu02 / a;
	eta20 = mu20 / a;
	eta11 = mu11 / a;
	a *= sqrt(mu00);
	eta12 = mu12 / a;
	eta21 = mu21 / a;
	eta30 = mu30 / a;
	eta03 = mu03 / a;
	I_1 = eta20 + eta02;
	I_2 = (eta20 - eta02) * (eta20 - eta02) + 4.0 * eta11 * eta11;
	I_3 = (eta30 - 3.0 * eta12) * (eta30 - 3.0 * eta12) + (3.0 * eta21 - eta03) * (3.0 * eta21 - eta03);
	I_4 = (eta30 + eta12) * (eta30 + eta12) + (eta21 + eta03) * (eta21 + eta03);
	I_5 = (eta30 - 3.0 * eta12) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03) ) + (3.0 * eta21 - eta03) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03));
	I_6 = (eta20 - eta02) * ( (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03) + 4.0 * eta11 * (eta30 + eta12) * (eta21 + eta03) );
	I_7 = ( 3.0 * eta21 - eta03 ) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03)) + (eta30 - 3.0 * eta12) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) );
	free(row);
	free(column);
	s = (ShapeDescriptor*)malloc(sizeof(ShapeDescriptor));
	s->m00 = m00;
	s->m01 = m01;
	s->m10 = m10;
	s->xmean = xmean;
	s->ymean = ymean;
	s->xmin = xmin;
	s->ymin = ymin;
	s->xmax = xmax;
	s->ymax = ymax;
	s->mu00 = mu00;
	s->mu02 = mu02;
	s->mu20 = mu20;
	s->mu11 = mu11;
	s->mu12 = mu12;
	s->mu21 = mu21;
	s->mu30 = mu30;
	s->mu03 = mu03;
	s->eta02 = eta02;
	s->eta20 = eta20;
	s->eta11 = eta11;
	s->eta12 = eta12;
	s->eta21 = eta21;
	s->eta30 = eta30;
	s->eta03 = eta03;
	s->I_1 = I_1;
	s->I_2 = I_2;
	s->I_3 = I_3;
	s->I_4 = I_4;
	s->I_5 = I_5;
	s->I_6 = I_6;
	s->I_7 = I_7;
	s->rotation_angle = 0.5 * atan( 2 * mu11 / ( mu02 - mu20 ) );
	s->mu20_prime = cos(s->rotation_angle) * cos(s->rotation_angle) * mu20 + sin(s->rotation_angle) * sin(s->rotation_angle) * mu02 - sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu02_prime = sin(s->rotation_angle) * sin(s->rotation_angle) * mu20 + cos(s->rotation_angle) * cos(s->rotation_angle) * mu02 + sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu11_prime = 0.5 * sin( 2.0 * s->rotation_angle ) * ( mu20 - mu02 ) + mu11 * cos( 2.0 * s->rotation_angle );
	a = s->mu02_prime;
	a = a*a*a;
	a = 4 * sqrt( a / s->mu20_prime ) / (double)MY_PI_CONST;
	a = sqrt( sqrt ( a ) );
	s->ellipse_axis_b = a;
	s->ellipse_axis_a = 4 * s->mu02_prime / ( (double)MY_PI_CONST * a * a * a );
	return s;
}
*/
char*measurements_print(ShapeDescriptor*s, unsigned int flags)
{
	char*res, *buf;
	res = (char*)calloc(MAX_RECORD, sizeof(char));
	buf =  (char*)calloc(MAX_RECORD, sizeof(char));
	if ( flags & 1 ) {
		sprintf(buf, "%13.6f ", s->m00);
		res = strcat(res, buf);
	}
	if ( flags & 2 ) {
		sprintf(buf, "%13.6f ", s->m01);
		res = strcat(res, buf);
	}
	if ( flags & 4 ) {
		sprintf(buf, "%13.6f ", s->m10);
		res = strcat(res, buf);
	}
	if ( flags & 8 ) {
		sprintf(buf, "%13.6f ", s->xmean);
		res = strcat(res, buf);
	}
	if ( flags & 16 ) {
		sprintf(buf, "%13.6f ", s->ymean);
		res = strcat(res, buf);
	}
	if ( flags & 32 ) {
		sprintf(buf, "%13.6f ", s->xmin);
		res = strcat(res, buf);
	}
	if ( flags & 64 ) {
		sprintf(buf, "%13.6f ", s->xmax);
		res = strcat(res, buf);
	}
	if ( flags & 128 ) {
		sprintf(buf, "%13.6f ", s->ymin);
		res = strcat(res, buf);
	}
	if ( flags & 256 ) {
		sprintf(buf, "%13.6f ", s->ymax);
		res = strcat(res, buf);
	}
	if ( flags & 512 ) {
		sprintf(buf, "%13.6f ", s->mu00);
		res = strcat(res, buf);
	}
	if ( flags & 1024 ) {
		sprintf(buf, "%13.6f ", s->mu02);
		res = strcat(res, buf);
	}
	if ( flags & 2048 ) {
		sprintf(buf, "%13.6f ", s->mu20);
		res = strcat(res, buf);
	}
	if ( flags & 4096 ) {
		sprintf(buf, "%13.6f ", s->mu11);
		res = strcat(res, buf);
	}
/*	if ( flags & 14 ) {
		sprintf(buf, "%13.6f ", s->mu12);
		res = strcat(res, buf);
	}
				if ( flags & 16 ) {
		sprintf(buf, "%13.6f ", s->mu21);
		res = strcat(res, buf);
	}
					if ( flags & 17 ) {
		sprintf(buf, "%13.6f ", s->mu30);
		res = strcat(res, buf);
	}
				if ( flags & 18 ) {
		sprintf(buf, "%13.6f ", s->mu03);
		res = strcat(res, buf);
	}

	if ( flags & 19 ) {
		sprintf(buf, "%13.6f ", s->eta02);
		res = strcat(res, buf);
	}
				if ( flags & 20 ) {
		sprintf(buf, "%13.6f ", s->eta20);
		res = strcat(res, buf);
	}
				if ( flags & 21 ) {
		sprintf(buf, "%13.6f ", s->eta11);
		res = strcat(res, buf);
	}
				if ( flags & 22 ) {
		sprintf(buf, "%13.6f ", s->eta12);
		res = strcat(res, buf);
	}
				if ( flags & 23 ) {
		sprintf(buf, "%13.6f ", s->eta21);
		res = strcat(res, buf);
	}
				if ( flags & 24 ) {
		sprintf(buf, "%13.6f ", s->eta30);
		res = strcat(res, buf);
	}
				if ( flags & 25 ) {
		sprintf(buf, "%13.6f ", s->eta03);
		res = strcat(res, buf);
	}
				if ( flags & 26 ) {
		sprintf(buf, "%13.6f ", s->I_1);
		res = strcat(res, buf);
	}
				if ( flags &27 ) {
		sprintf(buf, "%13.6f ", s->I_2);
		res = strcat(res, buf);
	}
				if ( flags & 28 ) {
		sprintf(buf, "%13.6f ", s->I_3);
		res = strcat(res, buf);
	}
				if ( flags & 29 ) {
		sprintf(buf, "%13.6f ", s->I_4);
		res = strcat(res, buf);
	}
				if ( flags & 30 ) {
		sprintf(buf, "%13.6f ", s->I_5);
		res = strcat(res, buf);
	}
				if ( flags & 31 ) {
		sprintf(buf, "%13.6f ", s->I_6);
		res = strcat(res, buf);
	}
				if ( flags & 32 ) {
		sprintf(buf, "%13.6f ", s->I_7);
		res = strcat(res, buf);
	}
				if ( flags & 33 ) {
		sprintf(buf, "%13.6f ", s->rotation_angle);
		res = strcat(res, buf);
	}
				if ( flags & 34 ) {
		sprintf(buf, "%13.6f ", s->mu20_prime);
		res = strcat(res, buf);
	}
				if ( flags & 35 ) {
		sprintf(buf, "%13.6f ", s->mu02_prime);
		res = strcat(res, buf);
	}
				if ( flags & 36 ) {
		sprintf(buf, "%13.6f ", s->mu11_prime);
		res = strcat(res, buf);
	}
				if ( flags & 37 ) {
		sprintf(buf, "%13.6f ", s->ellipse_axis_b);
		res = strcat(res, buf);
	}
				if ( flags & 38 ) {
		sprintf(buf, "%13.6f ", s->ellipse_axis_a);
		res = strcat(res, buf);
	}*/
				return res;
}


double ShapeDescriptorSimilarity(ShapeDescriptor*s1, ShapeDescriptor*s2)
{
	return ( s1->I_1 - s2->I_1 ) * ( s1->I_1 - s2->I_1 ) + ( s1->I_2 - s2->I_2 ) * ( s1->I_2 - s2->I_2 ) + ( s1->I_3 - s2->I_3 ) * ( s1->I_3 - s2->I_3 ) + ( s1->I_4 - s2->I_4 ) * ( s1->I_4 - s2->I_4 ) + ( s1->I_5 - s2->I_5 ) * ( s1->I_5 - s2->I_5 ) + ( s1->I_6 - s2->I_6 ) * ( s1->I_6 - s2->I_6 ) + ( s1->I_7 - s2->I_7 ) * ( s1->I_7 - s2->I_7 );
}

ShapeDescriptor*CharImage2ShapeDescriptor(CharImage*ci)
{
	Blob *b;
	uint32 n;
	ShapeDescriptor*sd;
	b = (Blob*)malloc(sizeof(Blob));
	b->Nnuclear_pixels = 0;
	b->Nenergid_pixels = 0;
	b->cm = NULL;
	b->nuclear_pixels = (uint32*)NULL;
	for ( n = 0; n < ci->number; n++ ) {
		if ( ci->pixels[n] == FilledPixel ) {
			b->nuclear_pixels = (uint32*)realloc(b->nuclear_pixels, ( b->Nnuclear_pixels + 1 ) * sizeof(uint32));
			b->nuclear_pixels[b->Nnuclear_pixels] = n;
			b->Nnuclear_pixels++;
		}
	}
	sd = ShapeDescriptorNew(b , ci->rows, ci->columns);
	BlobDelete(b);
	return sd;
}

void BlobDelete(Blob*b)
{
	if ( b->Nnuclear_pixels ) free(b->nuclear_pixels);
	if ( b->Nenergid_pixels ) free(b->energid_pixels);
	if ( b->cm ) free(b->cm);
	free(b);
}

void CharImageCheapWaterShed(CharImage*di, int connectivity)
{
	int **labels;
	unsigned char **distances;
	int IntensityLevel;
	int current_dist;
	int index1, index2;
	unsigned int i, j, size;
	int current_label;
	unsigned char HMIN, HMAX;
	unsigned int *indices, index;
	int pix;
	CheapFifo *queue;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	if ( connectivity != 4 && connectivity != 8 )
		g_error("NewCharVoxelStructure connectivity %d not supported", connectivity);
	if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	} else if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	}
/* Instead of cvs */
	size = di->number;
	indices = (unsigned int*)calloc(size, sizeof(unsigned int));
	labels = (int**)calloc(di->columns, sizeof(int*));
	distances = (unsigned char**)calloc(di->columns, sizeof(unsigned char*));
	for ( i = 0; i < di->columns; i++) {
		labels[i] = (int*)calloc(di->rows, sizeof(int));
		distances[i] = (unsigned char*)calloc(di->rows, sizeof(unsigned char));
		for ( j = 0; j < di->rows; j++) {
			indices[ j * di->columns + i ] = j * di->columns + i;
			labels[i][j] = INIT;
			distances[i][j] = 0;
		}
	}
/**/
	sv = di;
	qsort((void *)indices, (size_t)size,(size_t)sizeof(unsigned int), compare_indices_plane);
	sv = (CharImage*)NULL;
	current_label = 0;
	current_dist = 0;
	index1 = 0;
	queue = CheapFifoNew();
	HMIN = di->pixels[indices[0]];
	HMAX = di->pixels[indices[size-1]];
	index2 = 0;
	for ( IntensityLevel = (int)HMIN; IntensityLevel <= (int)HMAX; IntensityLevel++ ) {
/*fprintf(stdout,"\n level: %d label: %d", (int)IntensityLevel, current_label);
fflush(stdout);*/
		for ( index = index1; index < size; index++) {
			if ( (int)di->pixels[indices[index]] != IntensityLevel ) {
				index1 = index;
				break;
			}
			i = (unsigned int)( indices[index] % di->columns );
			j = (unsigned int)( (double)( indices[index] - i ) / (double)di->columns );
			labels[i][j] = MASK;
			for ( n = 0; n < next; n += 2) {
				int c, r;
				c = ext[n] + i;
				r = ext[n+1] + j;
				if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) ) {
					if ( labels[c][r] >= WSHED ) {
						distances[i][j] = 1;
						CheapFifoAdd(queue, indices[index]);
					}
				}
			}
		}
/* end for */
		current_dist = 1;
		CheapFifoAddFictitious(queue);
/*fprintf(stdout," index1: %d", (int)index1);
fflush(stdout);*/
		while (1) {
			int flag  = 0;
			pix = CheapFifoRemove(queue);
/*fprintf(stdout,"\n level: %d dist: %d pix: %d", IntensityLevel, (int)current_dist, pix);
fflush(stdout);*/
			if ( pix == FICTITIOUS ) {
				if ( CheapFifoEmpty(queue) ) {
					break;
				} else {
					CheapFifoAddFictitious(queue);
					current_dist++;
					pix = CheapFifoRemove(queue);
				}
			}
			i = (unsigned int)( pix % di->columns );
			j = (unsigned int)( (double)( pix - i ) / (double)di->columns );
			for ( n = 0; n < next; n += 2) {
				int c, r;
				c = ext[n] + i;
				r = ext[n+1] + j;
				if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) ) {
/* in Vincent: if ( distances[c][r] < current_dist && labels[c][r] >= WSHED ) { */
					if ( distances[c][r] <= current_dist && labels[c][r] >= WSHED ) {
						if ( labels[c][r] > WSHED ) {
							if ( labels[i][j] == MASK || labels[i][j] == WSHED ) {
								if ( !flag ) labels[i][j] = labels[c][r];
							} else if ( labels[i][j] != labels[c][r] ) {
								labels[i][j] = WSHED;
								flag = 1;
							}
						} else if ( labels[i][j] == MASK ) {
							labels[i][j] = WSHED;
						}
					} else if ( labels[c][r] == MASK && distances[c][r] == 0 ) {
						distances[c][r] = current_dist + 1;
						CheapFifoAdd(queue, r * di->columns + c);
					}
				}
			}
		}/* end while */
/*fprintf(stdout,"level: %d dist: %d\n", IntensityLevel, current_dist);
fflush(stdout);
fprintf(stdout,"\n end while level: %d", IntensityLevel);
fflush(stdout);*/
		for ( index = index2; index < size; index++) {
			if ( (int)di->pixels[indices[index]] != IntensityLevel ) {
				index2 = index;
				break;
			}
			i = (unsigned int)( indices[index] % di->columns );
			j = (unsigned int)( (double)( indices[index] - i ) / (double)di->columns );
			distances[i][j] = 0;
/*if ( IntensityLevel == 255 ) fprintf(stdout," %d %d %d\n", i, j, labels[i][j]);*/
			if ( labels[i][j] == MASK ) {
				current_label++;
/*if ( IntensityLevel == 255 ) fprintf(stdout," %d", current_label);*/
				labels[i][j] = current_label;
				CheapFifoAdd(queue, indices[index]);
				while ( !CheapFifoEmpty(queue) ) {
					pix = CheapFifoRemove(queue);
					i = (unsigned int)( pix % di->columns );
					j = (unsigned int)( (double)( pix - i ) / (double)di->columns );
					for ( n = 0; n < next; n += 2) {
						int c, r;
						c = ext[n] + i;
						r = ext[n+1] + j;
						if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) ) {
							if ( labels[c][r] == MASK ) {
								CheapFifoAdd(queue, r * di->columns + c);
								labels[c][r] = current_label;
							}
						}
					}
				}
			}
		}
	}/* end main for */
/*fprintf(stdout,"\n end for level: %d", IntensityLevel);
getchar();*/
	for ( index = 0; index < size; index++) {
		int AllNeighWSHED = 1;
		i = (unsigned int)( index % di->columns );
		j = (unsigned int)( (double)( index - i ) / (double)di->columns );
		for ( n = 0; n < next; n += 2) {
			int c, r;
			c = ext[n] + i;
			r = ext[n+1] + j;
			if ( ( 0 <= c ) && ( c < di->columns) && ( 0 <= r ) && ( r < di->rows ) ) {
				if ( labels[c][r] != WSHED ) {
					AllNeighWSHED = 0;
					break;
				}
			}
		}
		di->pixels[index] = ( i == 0 || j == 0 || i == di->columns - 1 || j == di->rows - 1 || ( labels[i][j] == WSHED && !AllNeighWSHED ) ) ? EmptyPixel:FilledPixel;
	}
	for ( i = 0; i < di->columns; i++) {
		free(labels[i]);
		free(distances[i]);
	}
	free(labels);
	free(distances);
	free(indices);
}

void CharImageLift(CharImage*di)
{
	int i;
	for ( i = 0; i < di->columns; i++) {
		if ( di->pixels[i] == EmptyPixel ) di->pixels[i] = 1;
	}
}


int compare_indices_plane(const void *a, const void *b)
{
	unsigned int*x;
	unsigned int*y;
	unsigned char x1, y1;
	x = (unsigned int*)a;
	y = (unsigned int*)b;
	x1 = sv->pixels[*x];
	y1 = sv->pixels[*y];
	if ( x1 > y1 ) return 1;
	if ( x1 < y1 ) return -1;
	return 0;
}

void CheapFifoDelete(CheapFifo*queue)
{
	if(queue->tail)CheapListDelete(queue->tail);
	free(queue);
}

CheapFifo*CheapFifoNew()
{
	CheapFifo*q;
	q = (CheapFifo*)malloc(sizeof(CheapFifo));
	q->head = (CheapList*)NULL;
	q->tail = (CheapList*)NULL;
	q->size = 0;
	return q;
}

void CheapFifoAdd(CheapFifo*queue, unsigned int cv)
{
	CheapList*p;
	p = (CheapList*)malloc(sizeof(CheapList));
	p->pixel = (int)cv;
	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (struct CheapList*)NULL;
	}
	p->prev = (struct CheapList*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
/*
fprintf(stdout,"\nfifo_add: c: %u ; r: %u ; size: %d ; label: %d ",cp->row,cp->column,queue->size, cp->label);
*/
}

int CheapFifoRemove(CheapFifo*queue)
{
	int cp;
	CheapList*t;
	t = queue->tail;
	cp = t->pixel;
	queue->tail = queue->tail->prev;
	if (queue->tail) queue->tail->next = (CheapList*)NULL;
	free(t);
	queue->size--;
	if ( queue->size == 0 )
		queue->head = (CheapList*)NULL;
	return cp;
}

void CheapFifoAddFictitious(CheapFifo*queue)
{
	CheapList*p;
	p = (CheapList*)malloc(sizeof(CheapList));
	p->pixel = FICTITIOUS;
	if ( queue->head ) {
		p->next = queue->head;
		p->next->prev = p;
	} else {
		p->next = (CheapList*)NULL;
	}
	p->prev = (CheapList*)NULL;
	queue->head = p;
	queue->size++;
	if (!queue->tail) queue->tail = p;
}

int CheapFifoEmpty(CheapFifo*queue)
{
	if (queue->size == 0) return 1;
	return 0;
}

void CheapListDelete(CheapList*tail)
{
	if ( tail->prev )
		CheapListDelete(tail->prev);
	free(tail);
}

CharImage*CharImageShapeSelect(CharImage*cv, ShapeDescriptor*sd, double eps, CheckShapeType cstype, int connectivity, int MaxSegment, int MinSegment)
{
	CharImage*ci;
	int column, row;
	switch ( cstype ) {
		case shape_accept:
			ci = CharImageClone(cv);
		break;
		case shape_reject:
			ci = CharImageCopy(cv);
		break;
		default:
			g_error("Shape: Unknown %d", (int)cstype);
	}
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,connectivity,MaxSegment,MinSegment,eps,cstype,sd)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			if ( cv->pixels[row * cv->columns + column] == FilledPixel ) {
				PList*list;
				ShapeDescriptor*csd;
				double dist;
				int SegmentLength;
				list = IntensitySegment(cv, FilledPixel, row, column, connectivity, cv->number, &SegmentLength, MinSegment);
				MarkPlist(list, cv, EmptyPixel);
				if ( MinSegment < SegmentLength && SegmentLength < MaxSegment ) {
					csd = PList2ShapeDescriptor(list, cv->columns, cv->rows);
					dist = ShapeDescriptorSimilarity(sd, csd);
					if ( dist < eps ) {
/*fprintf(stdout,"\n %f %f", csd->m000, dist);
fprintf(stdout," nash");
fflush(stdout);*/
						switch ( cstype ) {
							case shape_accept:
								MarkPlist(list, ci, FilledPixel);
							break;
							case shape_reject:
								MarkPlist(list, ci, EmptyPixel);
							break;
							default:
								g_error("Shape: Unknown %d", (int)cstype);
						}
					}
					free(csd);
				}
				deletePList(list);
			}
		}
	}
	CharImageDelete(cv);
	return ci;
}

ShapeDescriptor*PList2ShapeDescriptor(PList*list, int columns, int rows)
{
	ShapeDescriptor*s;
	uint32 np;
	double m00;
	double m01;
	double m10;
	double xmean;
	double ymean;
	double mu00;
	double mu02;
	double mu20;
	double mu11;
	double mu12;
	double mu21;
	double mu30;
	double mu03;
	double eta02;
	double eta20;
	double eta11;
	double eta12;
	double eta21;
	double eta30;
	double eta03;
	double I_1;
	double I_2;
	double I_3;
	double I_4;
	double I_5;
	double I_6;
	double I_7;
	uint32 i;
	double *row, *column, a;
	PList*cur;
	np = 0;
	row = (double*)NULL;
	column = (double*)NULL;
	m00 = 0.0;
	m01 = 0.0;
	m10 = 0.0;
	for ( cur = list; cur; cur = cur->next ) {
		row = (double*)realloc(row, (np + 1) * sizeof(double));
		column = (double*)realloc(column , (np + 1) * sizeof(double));
		column[np] = (double) ( cur->index % columns );
		row[np] = (double) ( (double)( cur->index - column[np] ) / (double)columns );
		m00 += 1.0;
		m01 += row[np];
		m10 += column[np];
		np++;
	}
	if ( m00 == 0 )
		g_error("ShapeDesciptorNew: empty blob");
	a = m00;
	xmean = m10 / a;
	ymean = m01 / a;
	mu00 = m00;
	mu02 = 0.0;
	mu20 = 0.0;
	mu11 = 0.0;
	mu12 = 0.0;
	mu21 = 0.0;
	mu30 = 0.0;
	mu03 = 0.0;
	for ( i = 0; i < np; i++ ) {
		register double r = row[i] - ymean;
		register double c = column[i] - xmean;
		mu02 += r * r;
		mu20 += c * c;
		mu11 += c * r;
		mu12 += c * r * r;
		mu21 += c * c * r;
		mu30 += c * c * c;
		mu03 += r * r * r;
	}
	a *= mu00;
	eta02 = mu02 / a;
	eta20 = mu20 / a;
	eta11 = mu11 / a;
	a *= sqrt(mu00);
	eta12 = mu12 / a;
	eta21 = mu21 / a;
	eta30 = mu30 / a;
	eta03 = mu03 / a;
	I_1 = eta20 + eta02;
	I_2 = (eta20 - eta02) * (eta20 - eta02) + 4.0 * eta11 * eta11;
	I_3 = (eta30 - 3.0 * eta12) * (eta30 - 3.0 * eta12) + (3.0 * eta21 - eta03) * (3.0 * eta21 - eta03);
	I_4 = (eta30 + eta12) * (eta30 + eta12) + (eta21 + eta03) * (eta21 + eta03);
	I_5 = (eta30 - 3.0 * eta12) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03) ) + (3.0 * eta21 - eta03) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03));
	I_6 = (eta20 - eta02) * ( (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) * (eta21 + eta03) + 4.0 * eta11 * (eta30 + eta12) * (eta21 + eta03) );
	I_7 = ( 3.0 * eta21 - eta03 ) * (eta30 + eta12) * ( (eta30 + eta12) * (eta30 + eta12) - 3.0 * (eta21 + eta03) * (eta21 + eta03)) + (eta30 - 3.0 * eta12) * (eta21 + eta03) * ( 3.0 * (eta30 + eta12) * (eta30 + eta12) - (eta21 + eta03) );
	free(row);
	free(column);
	s = (ShapeDescriptor*)malloc(sizeof(ShapeDescriptor));
	s->m00 = m00;
	s->m01 = m01;
	s->m10 = m10;
	s->xmean = xmean;
	s->ymean = ymean;
	s->mu00 = mu00;
	s->mu02 = mu02;
	s->mu20 = mu20;
	s->mu11 = mu11;
	s->mu12 = mu12;
	s->mu21 = mu21;
	s->mu30 = mu30;
	s->mu03 = mu03;
	s->eta02 = eta02;
	s->eta20 = eta20;
	s->eta11 = eta11;
	s->eta12 = eta12;
	s->eta21 = eta21;
	s->eta30 = eta30;
	s->eta03 = eta03;
	s->I_1 = I_1;
	s->I_2 = I_2;
	s->I_3 = I_3;
	s->I_4 = I_4;
	s->I_5 = I_5;
	s->I_6 = I_6;
	s->I_7 = I_7;
	s->rotation_angle = 0.5 * atan( 2 * mu11 / ( mu02 - mu20 ) );
	s->mu20_prime = cos(s->rotation_angle) * cos(s->rotation_angle) * mu20 + sin(s->rotation_angle) * sin(s->rotation_angle) * mu02 - sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu02_prime = sin(s->rotation_angle) * sin(s->rotation_angle) * mu20 + cos(s->rotation_angle) * cos(s->rotation_angle) * mu02 + sin( 2.0 * s->rotation_angle ) * mu11;
	s->mu11_prime = 0.5 * sin( 2.0 * s->rotation_angle ) * ( mu20 - mu02 ) + mu11 * cos( 2.0 * s->rotation_angle );
	a = s->mu02_prime;
	a = a*a*a;
	a = 4 * sqrt( a / s->mu20_prime ) / (double)MY_PI_CONST;
	a = sqrt( sqrt ( a ) );
	s->ellipse_axis_b = a;
	s->ellipse_axis_a = 4 * s->mu02_prime / ( (double)MY_PI_CONST * a * a * a );
	return s;
}

PList*IntensitySegment(CharImage*cv, unsigned char Intensity, uint32 row, uint32 column, int connectivity, int MaxSegment, int*SegmentLength, int MinSegment)
{
	PList*list, *cur;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	if ( connectivity != 8 && connectivity != 4 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	} else if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	}
	list = (PList*)malloc(sizeof(PList));
	(*SegmentLength) = 1;
	list->index = row * cv->columns + column;
	list->next = (PList*)NULL;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=2 ) {
			int column = (int) ( cur->index % cv->columns );
			int row = (int) ( (double)( cur->index - column ) / (double)cv->columns );
			int r = ext[n] + row;
			int c = ext[n + 1] + column;
			uint32 index = cv->columns * r + c;
			if ( r >= 0 && r < cv->rows && c >= 0 && c < cv->columns && cv->pixels[index] == Intensity && !IsLabeled(list, index) ) {
				PList*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList*)malloc(sizeof(PList)) ) )
					g_error("PList3dGo can't allocate element");
				(*SegmentLength)++;
/*fprintf(stdout, " %d ;", (*SegmentLength));
fflush(stdout);*/
				s->next->index = index;
				s->next->next = (PList*)NULL;
			}
/* these lines save us from infinite loop */
			if ( (*SegmentLength) > MaxSegment )
				return list;
		}
	}
	return list;
}

int IsLabeled(PList*p, uint32 index)
{
	PList*cur;
	cur = p;
	while ( cur ) {
		if ( cur->index == index )
			return 1;
		cur = cur->next;
	}
	return 0;
}

CharImage*CharImageRegionalMin(CharImage*cv, int connectivity, int MaxSegment, int MinSegment, int MaxNumSeg)
{
	CharImage*ci;
	int column, row;
	int NumSeg;
	unsigned char Intensity;
	NumSeg = 0;
	ci = CharImageClone(cv);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,connectivity,MaxSegment,MinSegment) private(Intensity)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			Intensity = cv->pixels[row * cv->columns + column];
			if ( Intensity != FilledPixel ) {
				PList*list;
				int SegmentLength;
				list = IntensitySegment(cv, Intensity, row, column, connectivity, MaxSegment, &SegmentLength, MinSegment);
				MarkPlist(list, cv, FilledPixel);
				if ( MinSegment < SegmentLength && SegmentLength < MaxSegment ) {
					PList*edge;
					edge = IntensitySegmentEdge(list, cv, connectivity);
					if ( PListMin( edge, cv ) > Intensity ) {
/*fprintf(stdout,"\n %u", PListMin( edge, cv ));
fflush(stdout);*/
						MarkPlist(list, ci, FilledPixel);
					}
					deletePList(edge);
				}
				deletePList(list);
			}
		}
	}
	CharImageDelete(cv);
	return ci;
}

CharImage*CharImageRegionalMax(CharImage*cv, int connectivity, int MaxSegment, int MinSegment, int MaxNumSeg)
{
	CharImage*ci;
	int column, row;
	int NumSeg;
	unsigned char Intensity;
	NumSeg = 0;
	ci = CharImageClone(cv);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(cv,ci,connectivity,MaxSegment,MinSegment) private(Intensity)
	for ( column = 0; column < cv->columns; column++ ) {
		for ( row = 0; row < cv->rows; row++ ) {
			Intensity = cv->pixels[row * cv->columns + column];
			if ( Intensity != EmptyPixel ) {
				PList*list;
				int SegmentLength;
				list = IntensitySegment(cv, Intensity, row, column, connectivity, MaxSegment, &SegmentLength, MinSegment);
				MarkPlist(list, cv, EmptyPixel);
				if ( MinSegment < SegmentLength && SegmentLength < MaxSegment ) {
					PList*edge;
					edge = IntensitySegmentEdge(list, cv, connectivity);
					if ( PListMax( edge, cv ) < Intensity ) {
/*fprintf(stdout,"\n %u", PListMax( edge, cv ));
fflush(stdout);*/
						MarkPlist(list, ci, FilledPixel);
					}
					deletePList(edge);
				}
				deletePList(list);
			}
		}
	}
	CharImageDelete(cv);
	return ci;
}


PList*IntensitySegmentEdge(PList*list, CharImage*cv, int connectivity)
{
	PList*edge, *cur;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	if ( connectivity != 8 && connectivity != 4 )
		g_error("SegmentEdge connectivity %d not supported", connectivity);
	if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	} else if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	}
	edge = (PList*)NULL;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=2 ) {
			int column = (int) ( cur->index % cv->columns );
			int row = (int) ( (double)( cur->index - column ) / (double)cv->columns );
			int r = ext[n] + row;
			int c = ext[n + 1] + column;
			uint32 index = cv->columns * r + c;
			if ( r >= 0 && r < cv->rows && c >= 0 && c < cv->columns && !IsLabeled(list, index) && !IsLabeled(edge, index) ) {
				PList *s;
				s = edge;
				if ( ! (edge = (PList*)malloc(sizeof(PList)) ) )
					g_error("SegmentEdge can't allocate element");
				edge->index = index;
				edge->next = s;
			}
		}
	}
	return edge;
}

unsigned char PListMin(PList* edge, CharImage*cv)
{
	PList*cur;
	unsigned char min;
	cur = edge;
	min = FilledPixel;
	while ( cur ) {
		unsigned char Intensity = cv->pixels[cur->index];
		if ( Intensity < min )
			min = Intensity;
		cur = cur->next;
	}
	return min;
}


unsigned char PListMax(PList* edge, CharImage*cv)
{
	PList*cur;
	unsigned char max;
	cur = edge;
	max = EmptyPixel;
	while ( cur ) {
		unsigned char Intensity = cv->pixels[cur->index];
		if ( Intensity > max )
			max = Intensity;
		cur = cur->next;
	}
	return max;
}

CharImage*CharImageGErosionSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageGErosionWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageGErosionWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,pattern,dpm,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			unsigned char newval;
			newval = FilledPixel;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) {
					unsigned char Intensity = dp->pixels[indexr*dp->columns+indexc];
					if ( Intensity < newval )
						newval = Intensity;
				}
			}
			dpm->pixels[row*dp->columns+column] = newval;
		}
	}
	free(pattern);
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageGDilationSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageGDilationWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageGDilationWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,pattern,dpm,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			unsigned char newval;
			newval = EmptyPixel;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) {
					unsigned char Intensity = dp->pixels[indexr*dp->columns+indexc];
					if ( Intensity > newval )
						newval = Intensity;
				}
			}
			dpm->pixels[row*dp->columns+column] = newval;
		}
	}
	free(pattern);
	CharImageDelete(dp);
	return dpm;
}

CharImage*CharImageConditionalGDilationWithSE(CharImage*dp, char *file, CharImage*ci, int *cond)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	int lcond = 0;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageCopy(dp);
/*printf("lcond before %d\n", lcond);*/
/*#pragma omp parallel default(none) shared(dp,ci,pattern,dpm,npattern) private(column,row,np) reduction(||:lcond)
#pragma omp for collapse(2)*/
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,ci,pattern,dpm,npattern) reduction(||:lcond) private(np) //private(column,row,np)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
/*printf("Нить %d выполнила итерацию %d  %d\n", omp_get_thread_num(), column, row);*/
			if ( ci->pixels[row*ci->columns+column] != EmptyPixel ) {
				unsigned char newval;
				newval = EmptyPixel;
				for ( np=0; np<npattern; np++ ) {
					register int indexc = column + pattern[2*np];
					register int indexr = row + pattern[2*np+1];
					if (  -1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) {
						unsigned char Intensity = dp->pixels[indexr*dp->columns+indexc];
						if ( Intensity > newval )
							newval = Intensity;
					}
				}
				if ( dpm->pixels[row*dp->columns+column] != newval ) {
					dpm->pixels[row*dp->columns+column] = newval;
					lcond = 1;
				}
			}
		}
	}
/*printf("lcond after %d\n", lcond);*/
	free(pattern);
	CharImageDelete(dp);
	*cond = lcond;
	return dpm;
}

CharImage*CharImageReconstruct(CharImage*mask, CharImage*marker, int connectivity)
{
	int index, i, j;
	int **labels;
	CheapFifo *queue;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext4_min[4] = {0, -1,
				-1, 0};
	int next4_min = 4;
	int ext4_plus[4] = {0, 1,
				1, 0};
	int next4_plus = 4;
	int ext8[16] = {0, 1,
				0, -1,
				1, 0,
				-1, 0,
				1, 1,
				1, -1,
				-1, 1,
				-1, -1};
	int next8 = 16;
	int ext8_min[6] = {0, -1,
				-1, 0,
				-1, -1};
	int next8_min = 6;
	int ext8_plus[6] = {0, 1,
				1, 0,
				1, 1};
	int next8_plus = 6;
	int *ext;
	int next;
	int n;
	int *ext_min;
	int next_min;
	int *ext_plus;
	int next_plus;
	int k;
	if ( connectivity != 4 &&  connectivity != 8)
		g_error("Reconstruct connectivity %d not supported", connectivity);
	if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
		next_min = next4_min;
		ext_min = ext4_min;
		next_plus = next4_plus;
		ext_plus = ext4_plus;
	} else if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
		next_min = next4_min;
		ext_min = ext4_min;
		next_plus = next4_plus;
		ext_plus = ext4_plus;
	}
	for ( i = 0; i < marker->columns; i++ ) {
		for ( j = 0; j < marker->rows; j++ ) {
			unsigned char newval = marker->pixels[j * marker->columns + i];
			for ( n = 0; n < next_plus; n += 2) {
				int c, r, index1;
				unsigned char curval;
				c = ext_plus[n] + i;
				r = ext_plus[n+1] + j;
				if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) ) {
					index1 = r * marker->columns + c;
					curval = marker->pixels[index1];
					if ( newval < curval ) {
						newval = curval;
					}
				}
			}
			marker->pixels[j * marker->columns + i] = ( newval < mask->pixels[j * marker->columns + i] ) ? newval : mask->pixels[j * marker->columns + i];
		}
	}
	labels = (int**)calloc(marker->columns, sizeof(int*));
	for ( i = 0; i < marker->columns; i++) {
		labels[i] = (int*)calloc(marker->rows, sizeof(int));
		for ( j = 0; j < marker->rows; j++) {
			labels[i][j] = 0;
		}
	}
	queue = CheapFifoNew();
	for ( i = marker->columns - 1; i >= 0; i-- ) {
		for ( j = marker->rows - 1; j >= 0; j-- ) {
			unsigned char newval;
			index = j * marker->columns + i;
			newval = marker->pixels[j * marker->columns + i];
			for ( n = 0; n < next_min; n += 2) {
				int c, r, index1;
				unsigned char curval;
				c = ext_min[n] + i;
				r = ext_min[n+1] + j;
				if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) ) {
					index1 = r * marker->columns + c;
					curval = marker->pixels[index1];
					if ( newval < curval ) {
						newval = curval;
					}
				}
			}
			newval = ( newval < mask->pixels[j * marker->columns + i] ) ? newval : mask->pixels[j * marker->columns + i];
/*			newval = ( newval < mask->pixels[j * marker->columns + i] ) ? mask->pixels[j * marker->columns + i] : newval;*/
			marker->pixels[j * marker->columns + i] = newval;
			for ( n = 0; n < next_min; n += 2) {
				int c, r, index1;
				unsigned char curval;
				c = ext_min[n] + i;
				r = ext_min[n+1] + j;
				if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) ) {
					index1 = r * marker->columns + c;
					curval = marker->pixels[index1];
					if ( /*&& labels[c][r] == 0*/ curval < newval && curval < mask->pixels[index1] ) {
						labels[i][j] = 1;
						CheapFifoAdd(queue, index);
						break;
					}
				}
			}
		}
	}
/*	return marker;
	CheapFifoAddFictitious(queue);*/
	k = 0;
	while ( !CheapFifoEmpty(queue) ) {
/*fprintf(stdout,"\n %d size %d", k, queue->size);
fflush(stdout);
k++;*/
		index = CheapFifoRemove(queue);
		i = (int)( index % marker->columns );
		j = (int)( (double)( index - i ) / (double)marker->columns );
/*fprintf(stdout," index %d ", index);*/
		labels[i][j] = 0;
		for ( n = 0; n < next; n += 2) {
			int c, r, index1;
			c = ext[n] + i;
			r = ext[n+1] + j;
			index1 = r * marker->columns + c;
			if ( ( 0 <= c ) && ( c < marker->columns) && ( 0 <= r ) && ( r < marker->rows ) && labels[c][r] == 0 && marker->pixels[index1] < marker->pixels[index] && mask->pixels[index1] != marker->pixels[index1] ) {
/*fprintf(stdout,"\n changed %d from %u", index1, marker->pixels[index1]);*/
				marker->pixels[index1] = ( marker->pixels[index] < mask->pixels[index1] ) ? marker->pixels[index] : mask->pixels[index1];
				CheapFifoAdd(queue, index1);
/*fprintf(stdout,"to %u", marker->pixels[index1]);*/
				labels[c][r] = 1;
			}
		}
	}
	for ( i = 0; i < marker->columns; i++) {
		free(labels[i]);
	}
	free(labels);
	CheapFifoDelete(queue);
	return marker;
}

CharImage*CharImageLHeqSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageLHeqWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageLHeqWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,pattern,dpm,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			int base;
			register double S;
			int histogram[256];
			for ( np = 0; np < 256; np++ )
				histogram[np] = 0;
			base = 0;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					histogram[dp->pixels[indexr*dp->columns+indexc]]++;
					base++;
				}
			}
			S = 0;
			for ( np = 0; np < dp->pixels[row*dp->columns+column] + 1; np++) {
				S += (double)histogram[np];
			}
			dpm->pixels[row*dp->columns+column] = (unsigned char)floor( (double)FilledPixel * S / (double)base);
		}
	}
	CharImageDelete(dp);
	return dpm;
}

CharImage*CharImageLHBGWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,pattern,dpm,npattern) private(np)
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			int histogram[256], base, pixval;
			for ( np = 0; np < 256; np++ )
				histogram[np] = 0;
			base = 0;
			for ( np=0; np < npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					pixval = dp->pixels[indexr*dp->columns+indexc];
					histogram[pixval]++;
					base = histogram[pixval] > histogram[base] ? pixval : base;
				}
			}
			dpm->pixels[row*dp->columns+column] = (unsigned char)base;
		}
	}
	CharImageDelete(dp);
	return dpm;
}

CharImage*CharImageLOtsuSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageLOtsuWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageLOtsuSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageLOtsuWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	const int SupPixel = (int)FilledPixel + 1;
	double *histogram;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageLOtsu can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
	histogram = (double*)calloc(SupPixel, sizeof(double));
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			register double base;
			unsigned char pix;
			for ( np = 0; np < 256; np++ ) histogram[np] = 0;
			base = 0;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					histogram[dp->pixels[indexr*dp->columns+indexc]] += 1.0;
					base += 1.0;
				}
			}
			for ( np = 0; np < 256; np++ ) histogram[np] /= base;
			pix = dp->pixels[row*dp->columns+column];
/*fprintf(stdout," %u %f %u\n", pix, computeOtsu(histogram, SupPixel), (unsigned char)computeOtsu(histogram, SupPixel));*/
			if ( pix < (unsigned char)computeOtsu(histogram, SupPixel) )
				dpm->pixels[row*dp->columns+column] = EmptyPixel;
			else
				dpm->pixels[row*dp->columns+column] = FilledPixel;
		}
	}
	free(histogram);
	CharImageDelete(dp);
	return dpm;
}


CharImage*CharImageCHolSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageCHolWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageCHolWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	CharImage*dpm;
	unsigned char Intensity;
	int cond;
	dpm = CharImageCopy(dp);
	dp = CharImageInvert(dp);
	row = 0;
#pragma omp parallel for schedule(static) collapse(1) default(none) shared(dp,row) private(Intensity)
	for ( column = 0; column < dp->columns; column++ ) {
		Intensity = dp->pixels[row * dp->columns + column];
		if ( Intensity != EmptyPixel ) {
			PList*list;
			int SegmentLength;
			list = IntensitySegmentFresh(dp, Intensity, row, column, 4, (int)dp->number, &SegmentLength, 0, EmptyPixel);
			deletePList(list);
		}
	}
	row = dp->rows - 1;
#pragma omp parallel for schedule(static) collapse(1) default(none) shared(dp,row) private(Intensity)
	for ( column = 0; column < dp->columns; column++ ) {
		Intensity = dp->pixels[row * dp->columns + column];
		if ( Intensity != EmptyPixel ) {
			PList*list;
			int SegmentLength;
			list = IntensitySegmentFresh(dp, Intensity, row, column, 4, (int)dp->number, &SegmentLength, 0, EmptyPixel);
			deletePList(list);
		}
	}
	column = 0;
#pragma omp parallel for schedule(static) collapse(1) default(none) shared(dp,column) private(Intensity)
	for ( row = 0; row < dp->rows; row++ ) {
		Intensity = dp->pixels[row * dp->columns + column];
		if ( Intensity != EmptyPixel ) {
			PList*list;
			int SegmentLength;
			list = IntensitySegmentFresh(dp, Intensity, row, column, 4, (int)dp->number, &SegmentLength, 0, EmptyPixel);
			deletePList(list);
		}
	}
	column = dp->columns - 1;
#pragma omp parallel for schedule(static) collapse(1) default(none) shared(dp,column) private(Intensity)
	for ( row = 0; row < dp->rows; row++ ) {
		Intensity = dp->pixels[row * dp->columns + column];
		if ( Intensity != EmptyPixel ) {
			PList*list;
			int SegmentLength;
			list = IntensitySegmentFresh(dp, Intensity, row, column, 4, (int)dp->number, &SegmentLength, 0, EmptyPixel);
			deletePList(list);
		}
	}
	cond = 1;
	while ( cond ) {
		dp = CharImageConditionalGDilationWithSE(dp, file, dpm, &cond);
	}
	CharImageDelete(dpm);
	return dp;
}


int IntensitySegmentTouchBorder(PList*list, CharImage*cv, int connectivity)
{
	PList*cur;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	int touch;
	touch = 0;
	if ( connectivity != 8 && connectivity != 4 )
		g_error("SegmentEdge connectivity %d not supported", connectivity);
	if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	} else if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	}
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=2 ) {
			int column = (int) ( cur->index % cv->columns );
			int row = (int) ( (double)( cur->index - column ) / (double)cv->columns );
			int r = ext[n] + row;
			int c = ext[n + 1] + column;
			uint32 index = cv->columns * r + c;
			if ( !IsLabeled(list, index) && ( r < 0 || r >= cv->rows || c < 0 || c >= cv->columns ) ) {
				return 1;
			}
		}
	}
	return touch;
}


PList*IntensitySegmentFresh(CharImage*cv, unsigned char Intensity, uint32 row, uint32 column, int connectivity, int MaxSegment, int*SegmentLength, int MinSegment, unsigned char toPut)
{
	PList*list, *cur;
	int ext4[8] = {0, 1,
				0, -1,
				1, 0,
				-1, 0};
	int next4 = 8;
	int ext8[16] = { 0, 1,
				0, -1,
				1, 1,
				1, 0,
				1, -1,
				-1, 0,
				-1, -1,
				-1, 1};
	int next8 = 16;
	int *ext;
	int next;
	int n;
	uint32 index;
	if ( connectivity != 8 && connectivity != 4 )
		g_error("Segment connectivity %d not supported", connectivity);
	if ( connectivity == 8 ) {
		next = next8;
		ext = ext8;
	} else if ( connectivity == 4 ) {
		next = next4;
		ext = ext4;
	}
	list = (PList*)malloc(sizeof(PList));
	(*SegmentLength) = 1;
	index = row * cv->columns + column;
	list->index = index;
	list->next = (PList*)NULL;
	cv->pixels[index] = toPut;
	for ( cur =  list; cur; cur = cur->next ) {
		for ( n = 0; n < next; n+=2 ) {
			int column = (int) ( cur->index % cv->columns );
			int row = (int) ( (double)( cur->index - column ) / (double)cv->columns );
			int r = ext[n] + row;
			int c = ext[n + 1] + column;
			index = cv->columns * r + c;
			if ( r >= 0 && r < cv->rows && c >= 0 && c < cv->columns && cv->pixels[index] == Intensity) {

				PList*prev, *s;
				prev = cur;
				s = list;
				while ( prev ) {
					s = prev;
					prev = prev->next;
				}
				if ( ! (s->next = (PList*)malloc(sizeof(PList)) ) )
					g_error("PList3dGo can't allocate element");
				(*SegmentLength)++;
/*fprintf(stdout, " %d ;", (*SegmentLength));
fflush(stdout);*/
				s->next->index = index;
				s->next->next = (PList*)NULL;
				cv->pixels[index] = toPut;
			}
/* these lines save us from infinite loop */
			if ( (*SegmentLength) > MaxSegment )
				return list;
		}
	}
	return list;
}



CharImage*CharImageOLLHeqSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageOLLHeqWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMedianSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageOLLHeqWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int *pattern;
	int i;
	CharImage**dpm;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = (CharImage**)calloc(npattern, sizeof(CharImage*));
	for ( np = 0; np < npattern; np++ ) {
		dpm[np] = CharImageClone(dp);
	}
	for ( column=0; column<dp->columns; column++ ) {
		for ( row=0; row<dp->rows; row++ ) {
			int histogram[256];
			int base;
			register double S;
			for ( np = 0; np < 256; np++ ) histogram[np] = 0;
			base = 0;
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					histogram[dp->pixels[indexr*dp->columns+indexc]]++;
					base++;
				}
			}
			for ( np=0; np<npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					S = 0;
					for ( np = 0; np < dp->pixels[indexr*dp->columns+indexc] + 1; np++) {
						S += (double)histogram[np];
					}
					dpm[np]->pixels[row*dp->columns+column] = (unsigned char)floor( (double)FilledPixel * S / (double)base);
				}
			}
		}
	}
	CharImageDelete(dp);
	dp = dpm[median];
	for ( np = 0; np < npattern; np++ ) {
		if ( np != median )
			CharImageDelete(dpm[np]);
	}
	free(dpm);
	return dp;
}

CharImage*CharImageIExpand(CharImage*ci, int rfactor, int cfactor)
{
	CharImage*resized;
	if ( rfactor < 1 || cfactor < 1)
		g_error("CharImageExpand: can't expand to a value less than 1! ");
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)((double)cfactor * (double)ci->columns), (uint32)((double)rfactor * (double)ci->rows) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
/* rfactor plays in vertical direction */
	if ( rfactor > cfactor ) {
		CharImage*resizedvert;
		resizedvert = CharImageNew( ci->columns, (uint32)((double)rfactor * (double)ci->rows) );
		CharImageIExpandVertical( ci, rfactor, resized );
		CharImageIExpandHorizontal( resizedvert, cfactor, resized );
		CharImageDelete(resizedvert);
	} else {
		CharImage*resizedhor;
		resizedhor = CharImageNew( (uint32)((double)cfactor * (double)ci->columns), ci->rows );
		CharImageIExpandHorizontal( ci, cfactor, resizedhor);
		CharImageIExpandVertical( resizedhor, rfactor, resized);
		CharImageDelete(resizedhor);
	}
	return resized;
}


void CharImageIExpandVertical(CharImage*ci, int factor, CharImage*resized)
{
	uint32 row, rrow;
	uint32 column;
	int n;
	for ( row = 0, rrow = 0; row < ci->rows; row++, rrow += factor) {
		for ( column = 0; column < resized->columns; column++ ) {
			for ( n = 0; n < factor; n++ ) {
				resized->pixels[ ( rrow + n ) * resized->columns+column] = ci->pixels[ row * ci->columns+column];
			}
		}
	}
}


void CharImageIExpandHorizontal(CharImage*ci, int factor, CharImage*resized)
{
	uint32 row;
	uint32 column, rcolumn;
	int n;
	for ( column = 0, rcolumn = 0; column < ci->columns; column++, rcolumn += factor ) {
		for ( row = 0; row < resized->rows; row++ ) {
			for ( n = 0; n < factor; n++ ) {
				resized->pixels[ row * resized->columns + rcolumn + n ] = ci->pixels[ row * ci->columns+column];
			}
		}
	}
}

/*T. Crimmins The Geometric Filter for Speckle Reduction, Applied Optics, Vol. 24, No. 10, 15 May 1985.*/
void CharImageCrimminsDespekle(CharImage*ci, int iterations)
{
	uint32 row;
	uint32 column;
	int n;
	int i;
	int step = 1;
	int ext8[16] = { 0, -1,
				1, -1,
				1, 0,
				1, 1,
				0, 1,
				-1, 1,
				-1, 0,
				-1, -1};
	int next8 = 16;
	int conn = 8;
	int **buffer, *buff, *buffer1, *buffer2;
	buffer = (int**)calloc(2, sizeof(int*));
	buffer1 = (int*)calloc(ci->number, sizeof(int));
	buffer2 = (int*)calloc(ci->number, sizeof(int));
	for ( column = 0; column < ci->number ; column++ ) {
		buffer1[column] = (int)ci->pixels[column];
		buffer2[column] = (int)ci->pixels[column];
	}
	buffer[0] = buffer1;
	buffer[1] = buffer2;
	for ( n = 0; n < iterations; n++) {
/* dark pixel adjustment */
		for ( i = 0; i < conn; i += 2) {
			int ac = ext8[ i ];
			int ar = ext8[ i + 1];
			int cc = ext8[ i + conn ];
			int cr = ext8[ i + conn + 1];
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( a >= b + 2 )
						b += step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( a > b && b <= c )
						b += step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( c > b && b <= a )
						b += step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( c >= b + 2 )
						b += step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
/*		}
/* light pixel adjustment
		for ( i = 0; i < conn; i += 2) {
			int ac = ext8[ i ];
			int ar = ext8[ i + 1];
			int cc = ext8[ i + conn ];
			int cr = ext8[ i + conn + 1];*/
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( a <= b - 2 )
						b -= step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( a < b && b >= c )
						b -= step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( c < b && b >= a )
						b -= step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
			for ( column = 1; column < ci->columns - 1; column++ ) {
				for ( row = 1; row < ci->rows - 1; row++ ) {
					int a = buffer[0][ ( row + ar ) * ci->columns + column + ac ];
					int b = buffer[0][ row * ci->columns + column ];
					int c = buffer[0][ ( row + cr ) * ci->columns + column + cc ];
					if ( c <= b - 2 )
						b -= step;
					buffer[1][ row * ci->columns + column ] = b;
				}
			}
			buff = buffer[0];
			buffer[0] = buffer[1];
			buffer[1] = buff;
		}
	}
	for ( column = 0; column < ci->number ; column++ ) {
		int b = buffer[0][column];
		if ( b < (int)EmptyPixel )
			ci->pixels[column] = (unsigned char)EmptyPixel;
		else if ( b <= (int)FilledPixel )
			ci->pixels[column] = (unsigned char)b;
		else
			ci->pixels[column] = (unsigned char)FilledPixel;
	}
	free(buffer);
	free(buffer1);
	free(buffer2);
}

CharImage*CharImageResample(CharImage*ci, double rfactor, double cfactor)
{
	CharImage*resized;
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)(cfactor * (double)ci->columns + 0.5), (uint32)(rfactor * (double)ci->rows + 0.5) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
/* rfactor plays in vertical direction */
	if ( rfactor > cfactor ) {
		CharImage*resizedvert;
		resizedvert = CharImageNew( ci->columns, (uint32)(rfactor * (double)ci->rows + 0.5 ) );
		CharImageResampleVertical( ci, rfactor, resized );
		CharImageResampleHorizontal( resizedvert, cfactor, resized );
		CharImageDelete(resizedvert);
	} else {
		CharImage*resizedhor;
		resizedhor = CharImageNew( (uint32)(cfactor * (double)ci->columns + 0.5 ), ci->rows );
		CharImageResampleHorizontal( ci, cfactor, resizedhor);
		CharImageResampleVertical( resizedhor, rfactor, resized);
		CharImageDelete(resizedhor);
	}
	return resized;
}


CharImage*CharImageSample(CharImage*ci, double rfactor, double cfactor)
{
	CharImage*resized;
	uint32 row;
	uint32 column;
	uint32 rcenter;
	uint32 ccenter;
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)(cfactor * (double)ci->columns), (uint32)(rfactor * (double)ci->rows) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
/* rfactor plays in vertical direction */
	for ( row = 0; row < resized->rows; row++ ) {
		rcenter = (uint32)( (double)row - 0.5 ) / rfactor;
		for ( column = 0; column < resized->columns; column++ ) {
			ccenter = (uint32)( (double)column - 0.5 ) / cfactor;
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns+ccenter];
		}
	}
	return resized;
}


void CharImageResampleVertical(CharImage*ci, double factor, CharImage*resized)
{
	uint32 row;
	uint32 column;
	uint32 center;
	for ( row = 0; row < resized->rows; row++ ) {
		center = (uint32)( (double)row + 0.5 ) / factor;
		for ( column = 0; column < resized->columns; column++ ) {
			resized->pixels[row*resized->columns+column] = ci->pixels[ center * ci->columns+column];
		}
	}
}


void CharImageResampleHorizontal(CharImage*ci, double factor, CharImage*resized)
{
	uint32 row;
	uint32 column;
	uint32 center;
	for ( column = 0; column < resized->columns; column++ ) {
		center = (uint32)( (double)column + 0.5 ) / factor;
		for ( row = 0; row < resized->rows; row++ ) {
			resized->pixels[row*resized->columns+column] = ci->pixels[ row * ci->columns + center];
		}
	}
}


CharImage*CharImageIntensityPolyKern(CharImage*di, Kernel *kern)
{
	uint32 i;
	for ( i = 0; i < di->number; i++) {
		register int o;
		register double dot;
		register double po;
		register double p;
		dot = 0;
		p = (double)di->pixels[i];
		po = 1.0;
		for ( o = 0; o < kern->order; o++) {
			dot += kern->array[o] * po;
			po *= p;
		}
		dot = floor(dot);
		if ( dot <= (double)EmptyPixel )
			di->pixels[i] = (unsigned char)EmptyPixel;
		else if ( dot <= (double)FilledPixel )
			di->pixels[i] = (unsigned char)dot;
		else
			di->pixels[i] = (unsigned char)FilledPixel;
	}
	return di;
}

Kernel *KernelFromString(char*string)
{
	Kernel *kern;
	char*skip;
	char*format;
	int o;
	kern = (Kernel*)malloc(sizeof(Kernel));
	sscanf(string,"%d", &kern->order);
	kern->array = (double*)calloc( kern->order, sizeof(double));
	skip = (char*)calloc(MAX_RECORD, sizeof(char));
	format = (char*)calloc(MAX_RECORD, sizeof(char));
	skip = strcpy(skip, "%*d,");
	format = strcpy(format, skip);
	format = strcat(format, "%lf");
	for( o = 0; o < kern->order; o++ ) {
		sscanf(string, format, &kern->array[o]);
		skip = strcat(skip, "%*lf,");
		format = strcpy(format, skip);
		format = strcat(format, "%lf");
	}
	free(skip);
	free(format);
	return kern;
}

CharImage*CharImageShrink(CharImage*ci, double rfactor, double cfactor)
{
	uint32 row;
	uint32 column;
	uint32 row2;
	uint32 column2;
	uint32 rcenter;
	uint32 ccenter;
	CharImage*resized;
	double rfactor1, rcenterd;
	double cfactor1, ccenterd;
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)(cfactor * (double)ci->columns + 0.5), (uint32)(rfactor * (double)ci->rows + 0.5) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
	row2 = (uint32)( (double)resized->rows / 2.0 + 0.5);
	column2 = (uint32)( (double)resized->columns / 2.0  + 0.5);
/* rfactor plays in vertical direction */
	rfactor1 = 1.0 / rfactor;
	cfactor1 = 1.0 / cfactor;
	rcenterd = 0;
	for ( row = 0; row < row2; row++ ) {
/*		rcenter = (uint32)( (double)row / rfactor );*/
		rcenter = (uint32)( rcenterd );
		ccenterd = 0;
		for ( column = 0; column < column2; column++ ) {
/*			ccenter = (uint32)( (double)column / cfactor );*/
			ccenter = (uint32)( ccenterd );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
			ccenterd += cfactor1;
		}
		for ( column = column2; column < resized->columns; column++ ) {
/*			ccenter = (uint32)( (double)column / cfactor  + 0.5 );*/
			ccenter = (uint32)( ccenterd + 0.5 );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
			ccenterd += cfactor1;
		}
		rcenterd += rfactor1;
	}
	for ( row = row2; row < resized->rows; row++ ) {
/*		rcenter = (uint32)( (double)row / rfactor + 0.5);*/
		rcenter = (uint32)( rcenterd + 0.5 );
		ccenterd = 0;
		for ( column = 0; column < column2; column++ ) {
/*			ccenter = (uint32)( (double)column / cfactor );*/
			ccenter = (uint32)( ccenterd );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
			ccenterd += cfactor1;
		}
		for ( column = column2; column < resized->columns; column++ ) {
/*			ccenter = (uint32)( (double)column / cfactor  + 0.5 );*/
			ccenter = (uint32)( ccenterd + 0.5 );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
			ccenterd += cfactor1;
		}
		rcenterd += rfactor1;
	}
	return resized;
}

CharImage*CharImageExpand(CharImage*ci, double rfactor, double cfactor)
{
	uint32 row;
	uint32 column;
	uint32 row2;
	uint32 column2;
	uint32 rcenter;
	uint32 ccenter;
	CharImage*resized;
	fprintf(stdout,"\n old size:\n columns %u rows %u", ci->columns, ci->rows);
	resized = CharImageNew( (uint32)(cfactor * (double)ci->columns), (uint32)(rfactor * (double)ci->rows) );
	fprintf(stdout,"\nnew size:\n columns %u rows %u", resized->columns, resized->rows);
	row2 = (uint32)( (double)resized->rows / 2.0 + 0.5);
	column2 = (uint32)( (double)resized->columns / 2.0  + 0.5);
/* rfactor plays in vertical direction */
	for ( row = 0; row < row2; row++ ) {
		rcenter = (uint32)( (double)row / rfactor );
		for ( column = 0; column < column2; column++ ) {
			ccenter = (uint32)( (double)column / cfactor );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
		}
		for ( column = column2; column < resized->columns; column++ ) {
			ccenter = (uint32)( (double)column / cfactor  + 0.5 );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
		}
	}
	for ( row = row2; row < resized->rows; row++ ) {
		rcenter = (uint32)( (double)row / rfactor + 0.5);
		for ( column = 0; column < column2; column++ ) {
			ccenter = (uint32)( (double)column / cfactor );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
		}
		for ( column = column2; column < resized->columns; column++ ) {
			ccenter = (uint32)( (double)column / cfactor  + 0.5 );
			resized->pixels[row*resized->columns+column] = ci->pixels[ rcenter * ci->columns + ccenter];
		}
	}
	return resized;
}

CharImage*CharImageRotate(CharImage*di, double theta)
{
	CharImage*ci;
	uint32 column, row;
	uint32 column1, row1;
	uint32 column2, row2;
	double a00, a01, a10, a11;
	double z00, z01, z10, z11;
	double rdot, rmid;
	double cdot, cmid;
	unsigned char *pixels;
	ci = CharImageClone(di);
	a00 = cos( -theta );
	a01 = -sin( -theta );
	a10 = sin( -theta );
	a11 = cos( -theta );
	rdot = (double)( di->rows - 1);
	rmid = 0.5 * rdot;
	cdot = (double)( di->columns - 1);
	cmid = 0.5 * cdot;
	for ( row = 0; row < di->rows; row++ ) {
		rdot = (double)( row - rmid );
		for ( column = 0; column < di->columns; column++ ) {
			double c;
			double r;
			double x;
			double y;
			double c1;
			double r1;
			double c2;
			double r2;
			double pix;
			cdot = (double)( column - cmid );
			c = a00 * cdot + a01 * rdot;
			r = a10 * cdot + a11 * rdot;
			if ( c > 0 ) {
				c1 = floor(c + cmid);
				x = c + cmid - c1;
				c2 = c1 + 1.0;
			} else {
				c1 = ceil(c + cmid);
				x = c1 - c - cmid;
				c2 = c1 - 1.0;
			}
			if ( r > 0 ) {
				r1 = floor(r + rmid);
				y = r + rmid - r1;
				r2 = r1 + 1.0;
			} else {
				r1 = ceil(r + rmid);
				y = r1 - r - rmid;
				r2 = r1 - 1.0;
			}
			z00 = (double)EmptyPixel;
			z10 = (double)EmptyPixel;
			z01 = (double)EmptyPixel;
			z11 = (double)EmptyPixel;
			column1 = (uint32)c1;
			row1 = (uint32)r1;
			column2 = (uint32)c2;
			row2 = (uint32)r2;
			if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
				z00 = (double)di->pixels[row1*di->columns+column1];
			if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
				z10 = (double)di->pixels[row2*di->columns+column1];
			if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
				z01 = (double)di->pixels[row1*di->columns+column2];
			if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
				z11 = (double)di->pixels[row2*di->columns+column2];
			pix = z00 * ( 1 - x - y + x * y);
			pix += z01 * ( x - x * y);
			pix += z10 * ( y - x * y);
			pix += z11 * x * y;
			if ( z00 != (double)EmptyPixel && z00 == z01 && z00 == z11 && z00 == z10 )
				ci->pixels[row*di->columns+column] = di->pixels[row1*di->columns+column1];
			else
				ci->pixels[row*di->columns+column] = (unsigned char)floor(pix);
		}
	}
	return ci;
}

void CharImageParseColorString(char*color, unsigned char*Red, unsigned char*Green, unsigned char*Blue)
{
	char c;
	c = *color;
	if ( isdigit( (int)c ) ) {
		if ( 3 != sscanf(color,"%u,%u,%u", Red, Green, Blue) )
			g_error("CharImageParseColorString can't parse %s", color);
	} else if ( isalpha( (int)c ) ) {
		if ( !strcmp(color,"red") ) {
			*Red = 255;
			*Green = 0;
			*Blue = 0;
		} else if ( !strcmp(color,"green") ) {
			*Red = 0;
			*Green = 255;
			*Blue = 0;
		} else if ( !strcmp(color,"blue") ) {
			*Red = 0;
			*Green = 0;
			*Blue = 255;
		} else if ( !strcmp(color,"white") ) {
			*Red = 255;
			*Green = 255;
			*Blue = 255;
		} else if ( !strcmp(color,"black") ) {
			*Red = 0;
			*Green = 0;
			*Blue = 0;
		} else if ( !strcmp(color,"yellow") ) {
			*Red = 255;
			*Green = 255;
			*Blue = 0;
		} else if ( !strcmp(color,"magenta") ) {
			*Red = 255;
			*Green = 0;
			*Blue = 255;
		} else if ( !strcmp(color,"cyan") ) {
			*Red = 0;
			*Green = 255;
			*Blue = 255;
		} else if ( !strcmp(color,"pink") ) {
			*Red = 250;
			*Green = 175;
			*Blue = 190;
		} else if ( !strcmp(color,"brown") ) {
			*Red = 128;
			*Green = 0;
			*Blue = 128;
		} else {
			g_error("CharImageParseColorString unknown color %s", color);
		}
	} else
		g_error("CharImageParseColorString can't parse %s", color);
}

void CharImageAddColor(CharImage*rci, CharImage*gci, CharImage*bci, CharImage*di, char*color)
{
	uint32 n;
	double r;
	double g;
	double b;
	unsigned char Red, Green, Blue;
	CharImageParseColorString(color, &Red, &Green, &Blue);
	for( n = 0; n < rci->number; n++) {
		r = (double)rci->pixels[n] + (double)Red * (double)di->pixels[n] / (double)FilledPixel;
		g = (double)gci->pixels[n] + (double)Green * (double)di->pixels[n] / (double)FilledPixel;
		b = (double)bci->pixels[n] + (double)Blue * (double)di->pixels[n] / (double)FilledPixel;
		if ( r <= 0 )
			rci->pixels[n] = (unsigned char)EmptyPixel;
		else if (  0 < r && r < (double)FilledPixel )
			rci->pixels[n] = (unsigned char)( r + 0.5);
		else
			rci->pixels[n] = (unsigned char)FilledPixel;
		if ( g <= 0 )
			gci->pixels[n] = (unsigned char)EmptyPixel;
		else if (  0 < g && g < (double)FilledPixel )
			gci->pixels[n] = (unsigned char)( g + 0.5);
		else
			gci->pixels[n] = (unsigned char)FilledPixel;
		if ( b <= 0 )
			bci->pixels[n] = (unsigned char)EmptyPixel;
		else if (  0 < b && b < (double)FilledPixel )
			bci->pixels[n] = (unsigned char)( b + 0.5);
		else
			bci->pixels[n] = (unsigned char)FilledPixel;
	}
}

void CharImageCompMasks(CharImage*cand,CharImage*mask, char*fn)
{
	unsigned char **rgb;
	unsigned char *r_cand;
	unsigned char *g_both;
	unsigned char *b_mask;
	uint32 columns, column, cc, cm;
	uint32 rows, row, rc, rm;
	uint32 number;
	double x;
	double y;
	columns = ( mask->columns > cand->columns ) ? mask->columns : cand->columns;
	rows = ( mask->rows > cand->rows ) ? mask->rows : cand->rows;
	number = columns * rows;
	r_cand = (unsigned char*)calloc(number, sizeof(unsigned char));
	g_both = (unsigned char*)calloc(number, sizeof(unsigned char));
	b_mask = (unsigned char*)calloc(number, sizeof(unsigned char));
	rgb = (unsigned char**)calloc(3, sizeof(unsigned char *) );
	rgb[0] = r_cand;
	rgb[1] = g_both;
	rgb[2] = b_mask;
	for ( column = 0; column < columns; column++) {
		x = (double)column / (double)columns;
		cc = (uint32)( (double)cand->columns * x + 0.5);
		cm = (uint32)( (double)mask->columns * x + 0.5);
/*fprintf(stdout,"\n %u %u", column, cc);
	fflush(stdout);*/
		for ( row = 0; row < rows; row++) {
			y = (double)row / (double)rows;
			r_cand[row * columns + column] = EmptyPixel;
			g_both[row * columns + column] = EmptyPixel;
			b_mask[row * columns + column] = EmptyPixel;
			rc = (uint32)( (double)cand->rows * y + 0.5);
			rm = (uint32)( (double)mask->rows * y + 0.5);
			if ( mask->pixels[rm * mask->columns + cm ] == FilledPixel || cand->pixels[rc * cand->columns + cc ] == FilledPixel ) {
				if( mask->pixels[rm * mask->columns + cm ] == FilledPixel && cand->pixels[rc * cand->columns + cc ] == FilledPixel ) {
					g_both[row * columns + column] = FilledPixel;
				} else if( mask->pixels[rm * mask->columns + cm ] == FilledPixel ) {
					b_mask[row * columns + column] = FilledPixel;
				} else {
					r_cand[row * columns + column] = FilledPixel;
				}
			}
		}
	}
	grf_write_char_rgb(fn, rows, columns, rgb, 3);
	free(r_cand);
	free(g_both);
	free(b_mask);
	free(rgb);
}


void CharImageCreateRGB(char*infile, CharImage**r, CharImage**g, CharImage**b)
{
	unsigned char*pixels;
	uint32 rows, columns;
	uint32 row, column;
	grf_read_char_rgb(infile, &rows, &columns, &pixels);
	*r = CharImageNew(columns, rows);
	*g = CharImageNew(columns, rows);
	*b = CharImageNew(columns, rows);
	for ( row = 0; row < rows; row++ ) {
		for ( column = 0; column < columns; column++ ) {
			(*r)->pixels[row * (*r)->columns + column] = pixels[row * ( (*r)->columns * 3 ) + 3 * column];
			(*g)->pixels[row * (*g)->columns + column] = pixels[row * ( (*g)->columns * 3 ) + 3 * column + 1];
			(*b)->pixels[row * (*b)->columns + column] = pixels[row * ( (*b)->columns * 3 ) + 3 * column + 2];
		}
	}
	free(pixels);
}

CharImage*CharImageMM(CharImage*di,CharImage*dj)
{
	uint32 i;
	double r1, r2, r3;
	CharImage*ci;
	ci = CharImageClone(di);
	for ( i = 0; i < di->number; i++) {
		r1 = (double)di->pixels[i] / (double)FilledPixel;
		r2 = (double)dj->pixels[i] / (double)FilledPixel;
		r3 = sqrt( 0.5 * ( r1 * r1 + r2 * r2 ) ) * (double)FilledPixel ;
		ci->pixels[i] = (unsigned char)r3;
	}
	return ci;
}

CharImage*CharImageHueSV(CharImage*r, CharImage*g, CharImage*b)
{
	uint32 i;
	double r1, r2, r3, m, M, h, *H, s;
	CharImage*ci;
	ci = CharImageClone(r);
	H = (double*)calloc(ci->number, sizeof(double));
	for ( i = 0; i < ci->number; i++) {
		r1 = (double)r->pixels[i];
		r2 = (double)g->pixels[i];
		r3 = (double)b->pixels[i];
		m = r1;
		M = r1;
		if ( M < r2 ) M = r2;
		if ( M < r3 ) M = r3;
		if ( m > r2 ) m = r2;
		if ( m > r3 ) m = r3;
		h = (double)EmptyPixel;
		if ( m != M ) {
			if ( M == r1 && r2 >= r3 ) {
				h = 60.0 * ( r2 - r3 ) / ( M - m ) ;
			} else if ( M == r1 && r2 < r3 ) {
				h = 60.0 * ( r2 - r3 ) / ( M - m ) + 360.0;
			} else if ( M == r2 ) {
				h = 60.0 * ( r3 - r1 ) / ( M - m ) + 120.0;
			} else if ( M == r3 ) {
				h = 60.0 * ( r1 - r2 ) / ( M - m ) + 240.0;
			}
			s = 1.0 - m / M;
			h *= s;
		} else {
			h = 60.0 * r1 / 255.0;
			s = 1.0 - 1.0 / 255.0 ;
			h *= s;
		}
		H[i] = h;
	}
	M = H[0];
	for ( i = 1; i < ci->number; i++) {
		if ( M < H[i] ) M = H[i];
	}
fprintf(stdout," M = %f\n", M);
	for ( i = 0; i < ci->number; i++) {
		ci->pixels[i] = (unsigned char)floor( 0.5 + (double)FilledPixel * H[i] / M);
	}
	free(H);
	return ci;
}


CharImage*CharImageHueSL(CharImage*r, CharImage*g, CharImage*b)
{
	uint32 i;
	double r1, r2, r3, m, M, h, *H, s, L;
	CharImage*ci;
	ci = CharImageClone(r);
	H = (double*)calloc(ci->number, sizeof(double));
	for ( i = 0; i < ci->number; i++) {
		r1 = (double)r->pixels[i];
		r2 = (double)g->pixels[i];
		r3 = (double)b->pixels[i];
		m = r1;
		M = r1;
		if ( M < r2 ) M = r2;
		if ( M < r3 ) M = r3;
		if ( m > r2 ) m = r2;
		if ( m > r3 ) m = r3;
		h = (double)EmptyPixel;
		if ( m != M ) {
			if ( M == r1 && r2 >= r3 ) {
				h = floor( 0.5 + 60.0 * ( r2 - r3 ) / ( M - m ) );
			} else if ( M == r1 && r2 < r3 ) {
				h = floor( 0.5 + 60.0 * ( r2 - r3 ) / ( M - m ) + 360.0);
			} else if ( M == r2 ) {
				h = floor( 0.5 + 60.0 * ( r3 - r1 ) / ( M - m ) + 120.0 );
			} else if ( M == r3 ) {
				h = floor( 0.5 + 60.0 * ( r1 - r2 ) / ( M - m ) + 240.0 );
			}
			L = 0.5 * ( m + M );
			if ( L <= 0.5 * 255.0 )
				s = 0.5 * ( M - m ) / L;
			else
				s = 0.5 * ( M - m ) / ( 255.0 - L );
			h *= s;
		} else {
			h = floor( 0.5 + 60.0 * r1 / 255.0 );
			if ( m <= 0.5 * 255.0 )
				s = 0.5 * 255.0 / m;
			else
				s = 0.5 * 255.0 / ( 255.0 - L );
			h *= s;
		}
		H[i] = h;
	}
	M = H[0];
	for ( i = 1; i < ci->number; i++) {
		if ( M < H[i] ) M = H[i];
	}
fprintf(stdout," M = %f\n", M);
	for ( i = 0; i < ci->number; i++) {
		ci->pixels[i] = (unsigned char)( (double)FilledPixel * H[i] / M);
	}
	free(H);
	return ci;
}

/* anisotropic diffusion by Perona*/
CharImage*CharImageAnisoDiff(CharImage*ci, int iterations, double lambda, double threshold, char*func)
{
	CharImage*c0, *c1, **cc, *c, *a;
	uint32 row;
	uint32 column;
	int n, i, h[256], size;
	double cn, dn, cs, ds, ce, de, cw, dw, K, dr, dc, sum;
	int step = 0;
	double (*perona)(double x, double K);
	if ( !strcmp(func,"exp") ) {
		perona = exp_perona;
	} else if ( !strcmp(func,"frac") ) {
		perona = frac_perona;
	} else {
		g_error("CharImageAnisoDiff unknown func");
	}
	for ( i = 0; i < 256; i++ ) h[i] = 0;
	size = (int)ci->number;
	for ( row = 1; row < ci->rows - 1; row++ ) {
		for ( column = 1; column < ci->columns - 1; column++ ) {
			dr = (double)ci->pixels[ ( row + 1 ) * ci->columns + column ] - (double)ci->pixels[ ( row - 1 ) * ci->columns + column ];
			dc = (double)ci->pixels[ row * ci->columns + column + 1 ] - (double)ci->pixels[ row * ci->columns + column - 1 ];
			i = (int)( 0.5 + 0.5 * sqrt( dr * dr + dc * dc ) );
			h[i]++;
		}
	}
	c0 = CharImageCopy(ci);
	c1 = CharImageCopy(c0);
	cc = (CharImage**)calloc(2, sizeof(CharImage*));
	cc[0] = c0;
	cc[1] = c1;
	c = cc[0];
	a = cc[1];
	for ( n = 0; n < iterations; n++ ) {
		a = cc[step];
		step++;
		step %= 2;
		c = cc[step];
		sum = 0;
		K = 0;
		for ( i = 0; i < 256; i++ ) {
			if ( sum >= threshold * size ) {
				K = (double)i;
				break;
			}
			sum += h[i];
		}
/*
fprintf(stdout,"K = %f\n", K);
fflush(stdout);*/
		if ( K == 0 ) K = 255.0;
		for ( i = 0; i < 256; i++ ) h[i] = 0;
		for ( row = 1; row < ci->rows - 1; row++ ) {
			for ( column = 1; column < ci->columns - 1; column++ ) {
				dn = (double)(a->pixels[ ( row - 1 ) * a->columns + column ]) - (double)(a->pixels[ row * a->columns + column ]);
				ds = (double)(a->pixels[ ( row + 1 ) * a->columns + column ]) - (double)(a->pixels[ row * a->columns + column ]);
				de = (double)(a->pixels[row * a->columns + column + 1 ]) - (double)(a->pixels[ row  * a->columns + column ]);
				dw = (double)(a->pixels[row * a->columns + column - 1 ]) - (double)(a->pixels[ row * a->columns + column ]);
				cn = perona(dn, K);
				cs = perona(ds, K);
				ce = perona(de, K);
				cw = perona(dw, K);
/*				c->pixels[row * c->rows + column ] = (unsigned char)( 0.5 + (double)a->pixels[row * a->rows + column ] +0.25 * lambda * (cn * dn + cs * ds + ce * de + cw * dw) );*/
				c->pixels[row * c->columns + column ] = (unsigned char)( (double)a->pixels[row * a->columns + column ] +0.25 * lambda * (cn * dn  + cs * ds + ce * de + cw * dw));
				dr = ds - dn;
				dc = de - dw;
				i = (int)( 0.5 + 0.5 * sqrt( dr * dr + dc * dc ) );
/*
if ( a->pixels[row * a->rows + column ] == c->pixels[row * a->rows + column ]) {
	fprintf(stdout,"row = %u, column = %u i = %d L = %f :: %f,%f,%f,%f %f,%f,%f,%f\n",  row, column, i, 0.25 * lambda * (cn * dn + cs * ds + ce * de + cw * dw ),  cn,cs,ce,cw, dn,ds,de,dw);
	fflush(stdout);
}
*/
				h[i]++;
			}
		}
	}
	free(a);
	free(cc);
	return c;
}

double exp_perona(double x, double K)
{
	return exp( -x*x/(K*K));
}

double frac_perona(double x, double K)
{
	return 1/ ( 1 + x*x/(K*K));
}

void plot_round_polar(FILE*fp, CharImage*ci, CharImage*di, char*flag, double xmean, double ymean)
{
	int i, j;
	double dot, x, y;
	double radius, theta;
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns; j++) {
			if ( ci->pixels[i * ci->columns + j] == (unsigned char)FilledPixel ) {
				x = (double)j - xmean;
				y = ymean - (double)i;
				radius = sqrt( y * y + x * x );
				if ( x == 0 )
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST;
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST;
					else
						theta = 0;
				else if ( x > 0 )
					theta = atan( y / x );
				else
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
					else
						theta = -1.0 * MY_PI_CONST;
				if ( !strcmp(flag, "log") )
					dot = log((double)di->pixels[i * ci->columns + j]);
				else if ( !strcmp(flag, "eqn") )
					dot = (double)di->pixels[i * ci->columns + j];
/*				dot = (theta * 180 / MY_PI_CONST ) * 255.0 / 360.0;*/
				fprintf(fp," %f %f %f\n", radius, theta * 180 / MY_PI_CONST , dot );
			}
		}
	}
}

CharImage*CharImageRoPolScr(CharImage*ci, CharImage*di, double rmin, double rmax, double tmin, double tmax, double xmean, double ymean)
{
	CharImage*dj;
	int i, j;
	double dot, x, y;
	double radius, theta;
	dj = CharImageClone(ci);
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns; j++) {
			dj->pixels[i * ci->columns + j] = (unsigned char)EmptyPixel;
			if ( ci->pixels[i * ci->columns + j] == (unsigned char)FilledPixel ) {
				x = (double)j - xmean;
				y = ymean - (double)i;
				radius = sqrt( y * y + x * x );
				if ( x == 0 )
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST;
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST;
					else
						theta = 0;
				else if ( x > 0 )
					theta = atan( y / x );
				else
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
					else
						theta = -1.0 * MY_PI_CONST;
				if ( rmin < radius && radius < rmax && tmin < theta && theta < tmax ) {
					dj->pixels[i * ci->columns + j] = di->pixels[i * ci->columns + j];
				}
			}
		}
	}
	return dj;
}


CharImage*CharImageGrid(CharImage*ci, int ns)
{
	int i, j;
	double dot;
	CharImage*di;
	di = CharImageClone(ci);
	for ( i = 0; i < ci->rows; i+=ns) {
		for ( j = 0; j < ci->columns ; j++) {
			di->pixels[i * ci->columns + j] = (unsigned char)FilledPixel;
		}
	}
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns ; j+=ns) {
			di->pixels[i * ci->columns + j] = (unsigned char)FilledPixel;
		}
	}
	di = CharImageInvert(di);
	return di;
}

CharImage*CharImageRoPolGrid(CharImage*ci, int tbin, double theta_eps, double xmean, double ymean)
{
	CharImage*dj;
	int i, j, k;
	double tstep, x, y;
	double radius, theta, theta_curr;
	dj = CharImageCopy(ci);
	tstep = 2.0 * MY_PI_CONST / (double)tbin;
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns; j++) {
			if ( ci->pixels[i * ci->columns + j] == (unsigned char)FilledPixel ) {
				x = (double)j - xmean;
				y = ymean - (double)i;
				radius = sqrt( y * y + x * x );
				if ( x == 0 )
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST;
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST;
					else
						theta = 0;
				else if ( x > 0 )
					theta = atan( y / x );
				else
					if ( y > 0 )
						theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
					else if ( y < 0 )
						theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
					else
						theta = -1.0 * MY_PI_CONST;
				theta_curr = -1.0 * MY_PI_CONST;
				for ( k = 0; k < tbin; k++ ) {
/*fprintf(stdout,"theta_curr = %f, theta = %f\n", theta_curr, theta);
fflush(stdout);*/
					if ( fabs( theta_curr - theta ) < theta_eps ) {
/*fprintf(stdout,"< %f\n", theta_eps);
fflush(stdout);*/
						dj->pixels[i * ci->columns + j] = (unsigned char)EmptyPixel;
						break;
					}
					theta_curr += tstep;
				}
			}
		}
	}
	return dj;
}

CharImage*CharImageLev(CharImage*ci, unsigned char lev)
{
	CharImage*dj;
	int i, j;
	double dot, x, y;
	double radius, theta;
	dj = CharImageClone(ci);
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns; j++) {
			dj->pixels[i * ci->columns + j] = (unsigned char)EmptyPixel;
			if ( ci->pixels[i * ci->columns + j] == lev ) {
				dj->pixels[i * ci->columns + j] = (unsigned char)FilledPixel;
			}
		}
	}
	return dj;
}

void cart2pol(double x, double y, double*theta, double*radius)
{
	*radius = sqrt( y * y + x * x );
	if ( x == 0 )
		if ( y > 0 )
			*theta = 0.5 * MY_PI_CONST;
		else if ( y < 0 )
			*theta = -0.5 * MY_PI_CONST;
		else
			*theta = 0;
	else if ( x > 0 )
		*theta = atan( y / x );
	else
		if ( y > 0 )
			*theta = 0.5 * MY_PI_CONST + 0.5 * MY_PI_CONST - atan( y / -x );
		else if ( y < 0 )
			*theta = -0.5 * MY_PI_CONST - 0.5 * MY_PI_CONST + atan( -y / -x );
		else
			*theta = -1.0 * MY_PI_CONST;
}

void BlobImageRoPolPrint(FILE*fp, BlobImage*bi, double xmean, double ymean)
{
	uint32 i, j, columns;
	double tmax, tmin, t, rmax, rmin, r;
	BlobList*cur;
	columns = (uint32)bi->columns;
	fprintf(stdout, "%d\n", bi->NBlobs);
	for ( i = 0, cur = bi->list; i < bi->NBlobs; i++, cur = cur->next ) {
		tmax = 0;
		rmax = 0;
		tmin = (double)bi->columns * (double)bi->rows;
		rmin = (double)bi->columns * (double)bi->rows;
		for ( j = 0; j < cur->blob->Nnuclear_pixels; j++ ) {
			double column = (double)(cur->blob->nuclear_pixels[j] % columns) - xmean;
			double row = ymean - ( (double)cur->blob->nuclear_pixels[j] - column ) / (double)columns;
			cart2pol(column, row, &t, &r);
/*fprintf(stdout," %f %f\n", t, r);*/
			if ( r > rmax ) {
				rmax = r;
				tmax = t;
			}
			if ( r < rmin ) {
				rmin = r;
				tmin = t;
			}
		}
		fprintf(fp, "%d %13.6f %13.6f %13.6f %13.6f %13.6f %13.6f\n", i, cur->blob->cm->x, cur->blob->cm->y, tmax, rmax, tmin, rmin);
	}
}

CharImage*CharImageBelt(CharImage*ci, char*beltname, double xmean, double ymean)
{
	CharImage*dj;
	int i, j, belt_n, n, k;
	double x, y, *belt_ra, *belt_ta, *belt_ri, *belt_ti;
	double radius, theta, a, b, rmin, rmax;
	FILE*fp;
	fp = fopen(beltname, "r");
	if ( !fp )
		g_error("parus: belt can't open %s", beltname);
	fscanf(fp, "%d\n", &belt_n);
	belt_ra = (double*)calloc(belt_n, sizeof(double));
	belt_ta = (double*)calloc(belt_n, sizeof(double));
	belt_ri = (double*)calloc(belt_n, sizeof(double));
	belt_ti = (double*)calloc(belt_n, sizeof(double));
	for ( k = 0; k < belt_n; k++ ) {
		fscanf(fp, "%*d%*lf%*lf%lf%lf%lf%lf\n", &belt_ta[k], &belt_ra[k], &belt_ti[k], &belt_ri[k]);
	}
	fclose(fp);
	dj = CharImageClone(ci);
	for ( i = 0; i < ci->rows; i++) {
		for ( j = 0; j < ci->columns; j++) {
			dj->pixels[i * ci->columns + j] = (unsigned char)EmptyPixel;
			if ( ci->pixels[i * ci->columns + j] == (unsigned char)FilledPixel ) {
				x = (double)j - xmean;
				y = ymean - (double)i;
				cart2pol(x, y, &theta, &radius);
				n = -1;
				for ( k = 1; k < belt_n; k++ ) {
					if ( belt_ti[k - 1] < theta && theta <= belt_ti[k] ) {
						n = k;
						break;
					}
				}
				if ( n == -1 ) {
					double x1, y1, x2, y2;
					x1 = belt_ti[belt_n - 1];
					x2 = 2.0 * MY_PI_CONST + belt_ti[0];
					y1 = belt_ri[belt_n - 1];
					y2 = belt_ri[0];
					if ( theta > 0 )
						theta = theta;
					else
						theta = 2.0 * MY_PI_CONST + theta;
					a = (y2 - y1) / (x2 - x1);
					b = (y1 * x2 - y2 * x1) / (x2 - x1);
				} else {
					a = (belt_ri[n - 1] - belt_ri[n]) / (belt_ti[n - 1] - belt_ti[n]);
					b = (belt_ri[n] * belt_ti[n - 1] - belt_ri[n - 1] * belt_ti[n]) / (belt_ti[n - 1] - belt_ti[n]);
				}
				rmin = a * theta + b;
				n = -1;
				for ( k = 1; k < belt_n; k++ ) {
					if ( belt_ta[k - 1] < theta && theta <= belt_ta[k] ) {
						n = k;
						break;
					}
				}
				if ( n == -1 ) {
					double x1, y1, x2, y2;
					x1 = belt_ta[belt_n - 1];
					x2 = 2.0 * MY_PI_CONST + belt_ta[0];
					y1 = belt_ra[belt_n - 1];
					y2 = belt_ra[0];
					if ( theta > 0 )
						theta = theta;
					else
						theta = 2.0 * MY_PI_CONST + theta;
					a = (y2 - y1) / (x2 - x1);
					b = (y1 * x2 - y2 * x1) / (x2 - x1);
				} else {
					a = (belt_ra[n - 1] - belt_ra[n]) / (belt_ta[n - 1] - belt_ta[n]);
					b = (belt_ra[n] * belt_ta[n - 1] - belt_ra[n - 1] * belt_ta[n]) / (belt_ta[n - 1] - belt_ta[n]);
				}
				rmax = a * theta + b;
				if ( rmin <= radius && radius <= rmax )
					dj->pixels[i * ci->columns + j] = (unsigned char)FilledPixel;
			}
		}
	}
	free(belt_ra);
	free(belt_ta);
	free(belt_ri);
	free(belt_ti);
	return dj;
}

SuperImage*SuperImageCreate(char*infile)
{
	SuperImage*di;
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->filledPixel = grf_read_super(infile,&di->rows,&di->columns,&di->pixels);
	di->number = di->columns * di->rows;
	di->emptyPixel = 0;
	switch (di->filledPixel) {
		case 255:
			di->bits_per_pixel = 8;
		break;
		case 4095:
			di->bits_per_pixel = 12;
		break;
		case 65535:
			di->bits_per_pixel = 16;
		break;
	}
	return di;
}

SuperImage*SuperImageNew(uint32 columns, uint32 rows, int bits_per_pixel)
{
	SuperImage*di;
	uint32 i;
	int filled_pixel;
	switch (bits_per_pixel) {
		case 8:
			filled_pixel = 255;
		break;
		case 12:
			filled_pixel = 4095;
		break;
		case 16:
			filled_pixel = 65535;
		break;
		default:
			g_error("SuperImageNew: %d bits per pixel is not valid", bits_per_pixel);
	}
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->bits_per_pixel = bits_per_pixel;
	di->filledPixel = filled_pixel;
	di->columns = columns;
	di->rows = rows;
	di->number = di->columns * di->rows;
	di->emptyPixel = 0;
	di->pixels = (double*)calloc(di->number, sizeof(double));
#pragma omp parallel for schedule(static) default(none) shared(di)
	for ( i = 0; i < di->number; i++ )
		di->pixels[i] = di->emptyPixel;
	return di;
}

SuperImage*SuperImageNewLabel(uint32 columns, uint32 rows, int bits_per_pixel, double label)
{
	SuperImage*di;
	uint32 i;
	int filled_pixel;
	switch (bits_per_pixel) {
		case 8:
			filled_pixel = 255;
		break;
		case 12:
			filled_pixel = 4095;
		break;
		case 16:
			filled_pixel = 65535;
		break;
		default:
			g_error("SuperImageNew: %d bits per pixel is not valid", bits_per_pixel);
	}
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->bits_per_pixel = bits_per_pixel;
	di->filledPixel = filled_pixel;
	di->columns = columns;
	di->rows = rows;
	di->number = di->columns * di->rows;
	di->emptyPixel = 0;
	di->pixels = (double*)calloc(di->number, sizeof(double));
#pragma omp parallel for schedule(static) default(none) shared(di, label)
	for ( i = 0; i < di->number; i++ )
		di->pixels[i] = label;
	return di;
}

SuperImage*SuperImageCreateEmpty(char*infile)
{
	SuperImage*di;
	int i;
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->filledPixel = grf_read_dim_super(infile,&di->rows,&di->columns);
	di->number = di->columns * di->rows;
	di->emptyPixel = 0;
	di->pixels = (double*)calloc(di->number, sizeof(double));
#pragma omp parallel for schedule(static) default(none) shared(di)
	for ( i = 0; i < di->number; i++ )
		di->pixels[i] = di->emptyPixel;
	return di;
}

SuperImage*SuperImageClone(SuperImage*si)
{
	SuperImage*di;
	int i;
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->columns = si->columns;
	di->rows = si->rows;
	di->number = si->number;
	di->pixels = (double*)calloc(si->number,sizeof(double));
	di->emptyPixel = 0;
#pragma omp parallel for schedule(static) default(none) shared(di)
	for ( i = 0; i < di->number; i++ )
		di->pixels[i] = di->emptyPixel;
	di->filledPixel = si->filledPixel;
	di->bits_per_pixel = si->bits_per_pixel;
	return di;
}

SuperImage*SuperImageCopy(SuperImage*si)
{
	SuperImage*di;
	int i;
	di = (SuperImage*)malloc(sizeof(SuperImage));
	di->columns = si->columns;
	di->rows = si->rows;
	di->number = si->number;
	di->emptyPixel = si->emptyPixel;
	di->pixels = (double*)calloc(si->number,sizeof(double));
#pragma omp parallel for schedule(static) default(none) shared(di,si)
	for ( i = 0; i < di->number; i++ )
		di->pixels[i] = si->pixels[i];
	di->filledPixel = si->filledPixel;
	di->bits_per_pixel = si->bits_per_pixel;
	return di;
}

void SuperImageWrite(SuperImage*si, char*outfile)
{
	grf_write_super(outfile, si->rows, si->columns, si->pixels, si->filledPixel);
}

SuperImage*SuperImageCompose(SuperImage*di,SuperImage*di_j, char*oper)
{
	uint32 i;
	if ( !strcmp(oper, "avg" ) || !strcmp(oper, "add" ) ) {
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
		for ( i = 0; i < di_j->number; i++)
				di->pixels[i] += di_j->pixels[i];
	} else if ( !strcmp(oper, "max" ) ) {
#pragma omp parallel for schedule(static) default(none) shared(di,di_j)
		for ( i = 0; i < di_j->number; i++) {
			if( di->pixels[i] < di_j->pixels[i] ) {
				di->pixels[i] = di_j->pixels[i];
			}
		}
	}
	return di;
}

void SuperImageDMul(SuperImage*di, double r )
{
	uint32 i;
#pragma omp parallel for schedule(static) default(none) shared(di,r)
	for ( i = 0; i < di->number; i++) {
		di->pixels[i] *= r;
	}
}

SuperImage*SuperImageComposeN(char*ID, char*extension, char*oper)
{
	FILE*listfp;
	int list;
	int done;
	struct dirent*direntptr;
	DIR*dirptr;
	char*record;
	char *infile;
	int scanf_flag,counter;
	SuperImage*di, *di_j;
	list = 0;
	dirptr = opendir(ID);
	if (!dirptr) {
		listfp = fopen(ID,"r");
		if (!listfp)
			g_error("SuperImageComposeMaxN Can't open %s neither as directory nor as file!",ID);
		else
			list = 1;
	}
	done = 0;
	if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
		done = 1;
	counter = 0;
/* loop over list */
	while(!counter && !done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di = SuperImageCreate(infile);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if ( !counter )
		g_error("Average","empty");
	while(!done) {
		if ( !strcmp(&infile[strlen(infile) - strlen(extension)],extension) ) {
			di_j = SuperImageCreate(infile);
			di = SuperImageCompose(di, di_j, oper);
			SuperImageDelete(di_j);
			counter++;
		}
		if ( infile )
			g_free(infile);
		if ( !( infile = iterate_compose(&listfp, &dirptr, ID) ) )
			done = 1;
	}
	if (list) {
		fclose(listfp);
	} else {
		closedir(dirptr);
	}
	if ( !strcmp(oper, "avg") ) {
		SuperImageDMul(di, 1.0/(double)counter );
	}
	return di;
}

SuperImage*super_image_compose(char**files, int n_files, char*oper)
{
	int scanf_flag, counter;
	SuperImage*di, *di_j;
	di = SuperImageCreate(files[0]);
	for ( counter = 1; counter < n_files; counter++ ) {
		di_j = SuperImageCreate(files[counter]);
		di = SuperImageCompose(di, di_j, oper);
		SuperImageDelete(di_j);
	}
	if ( !strcmp(oper, "avg") ) {
		SuperImageDMul(di, 1.0/(double)n_files );
	}
	return di;
}

void SuperImageDelete(SuperImage*di)
{
	free(di->pixels);
	free(di);
}


CharImage*processCharImageByBlobs(CharImage*image, BlobImage*bi)
{
	CharImage*cim;
	int index, label, i;
	BlobList*cur;
	double p[FilledPixel + 1], threshold;
	cim = CharImageClone(image);
	for ( index = 0; index < cim->number; index++)
		cim->pixels[index] = EmptyPixel;
	for ( label = 0, cur = bi->list; label < bi->NBlobs; label++, cur = cur->next ) {
		for ( i = 0; i < FilledPixel + 1; i++ )
			p[i] = 0.0;
		for ( index = 0; index < cur->blob->Nenergid_pixels; index++)
			p[image->pixels[cur->blob->energid_pixels[index]]] += 1.0;
		for ( i = 0; i < FilledPixel + 1; i++ )
			p[i] /= cur->blob->Nenergid_pixels;
/*		for ( index = 0; index < cur->blob->Nnuclear_pixels; index++)
			p[image->pixels[cur->blob->nuclear_pixels[index]]] += 1.0;
		for ( i = 0; i < FilledPixel + 1; i++ )
			p[i] /= cur->blob->Nnuclear_pixels;*/
		threshold = computeOtsu(p, (int)(FilledPixel + 1));
/*fprintf(stdout,"\n threshold = %f", threshold);*/
		for ( index = 0; index < cur->blob->Nenergid_pixels; index++)
			if ( image->pixels[cur->blob->energid_pixels[index]] < (unsigned char)threshold )
				cim->pixels[cur->blob->energid_pixels[index]] = EmptyPixel;
			else
				cim->pixels[cur->blob->energid_pixels[index]] = FilledPixel;
	}
	return cim;
}

CharImage*CharImageGDistance(CharImage*p, int maxfringe, char*arg)
{
	CharImage*di;
	int q, i, j, fringe_i;
	double saturation_bright, fringe_weight, fringe_bright, max_bright, curr_pix;
	double (*fbright)(CharImage*p, int q, int i, int j);
	if ( !strcmp(arg, "max"))
		fbright = fringe_bright_max;
	else if ( !strcmp(arg, "sum"))
		fbright = fringe_bright_sum;
	else
		g_error("CharImageGDistance: wrong %s", arg);
	max_bright = (double)FilledPixel;
	di = CharImageClone(p);
	for ( i = 0; i < p->rows; i++) {
		for ( j = 0; j < p->columns; j++) {
			curr_pix = 0.0;
			saturation_bright = (double)p->pixels[ i * p->columns + j ];
			for ( q = 1; q < maxfringe; q++ ) {
				int row, column;
				fringe_weight = 1 - saturation_bright / max_bright;
				curr_pix += fringe_weight;
				fringe_bright = fbright(p, q, i, j);
				saturation_bright += fringe_bright;
/*fprintf(stdout, "%d %d saturation_bright = %9.5f\n", i, j, saturation_bright);
fflush(stdout);*/
				if ( saturation_bright >= max_bright )
					break;
			}
/*fprintf(stdout, "%d %d %9.5f\n", i, j, curr_pix);
fflush(stdout);*/
			di->pixels[ i * p->columns + j ] = (unsigned char)( curr_pix + 0.5 );
		}
	}
	return di;
}

double fringe_bright_max(CharImage*p, int q, int i, int j)
{
	double fringe_bright, pix;
	int fringe_i, row, column;
	fringe_bright = 0;
	for ( fringe_i = 1 - q; fringe_i < q ; fringe_i++ ) {
		row = i + fringe_i;
		column = j - q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i + fringe_i;
		column = j + q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i - fringe_i;
		column = j - q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i - fringe_i;
		column = j + q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i + q;
		column = j - fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i + q;
		column = j + fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i - q;
		column = j - fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
		row = i - q;
		column = j + fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
			pix = (double)p->pixels[ row * p->columns + column ];
			if ( fringe_bright < pix )
				fringe_bright = pix;
		}
	}
	row = i - q;
	column = j + q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
		pix = (double)p->pixels[ row * p->columns + column ];
		if ( fringe_bright < pix )
			fringe_bright = pix;
	}
	row = i + q;
	column = j - q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
		pix = (double)p->pixels[ row * p->columns + column ];
		if ( fringe_bright < pix )
			fringe_bright = pix;
	}
	row = i - q;
	column = j - q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
		pix = (double)p->pixels[ row * p->columns + column ];
		if ( fringe_bright < pix )
			fringe_bright = pix;
	}
	row = i + q;
	column = j + q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns ) {
		pix = (double)p->pixels[ row * p->columns + column ];
		if ( fringe_bright < pix )
			fringe_bright = pix;
	}
	return fringe_bright;
}

double fringe_bright_sum(CharImage*p, int q, int i, int j)
{
	double fringe_bright;
	int fringe_i, row, column;
	fringe_bright = 0;
	for ( fringe_i = 1 - q; fringe_i < q ; fringe_i++ ) {
		row = i + fringe_i;
		column = j - q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i + fringe_i;
		column = j + q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i - fringe_i;
		column = j - q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i - fringe_i;
		column = j + q;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i + q;
		column = j - fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i + q;
		column = j + fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i - q;
		column = j - fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
		row = i - q;
		column = j + fringe_i;
		if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
			fringe_bright += (double)p->pixels[ row * p->columns + column ];
	}
	row = i - q;
	column = j + q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
		fringe_bright += (double)p->pixels[ row * p->columns + column ];
	row = i + q;
	column = j - q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
		fringe_bright += (double)p->pixels[ row * p->columns + column ];
	row = i - q;
	column = j - q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
		fringe_bright += (double)p->pixels[ row * p->columns + column ];
	row = i + q;
	column = j + q;
	if ( 0 <= row && row < p->rows && 0 <= column && column <= p->columns )
		fringe_bright += (double)p->pixels[ row * p->columns + column ];
	return fringe_bright;
}


CharImage*CharImageLStretchWithSE(CharImage*dp, char *file, double a, double b)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int *pattern;
	CharImage*dpm;
	FILE*fp;
	double c, d;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	dpm = CharImageClone(dp);
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			register double maxLevel = (double)EmptyPixel;
			register double minLevel = (double)FilledPixel;
			register double x  = (double)dp->pixels[row * dp->columns + column] / (double)FilledPixel;
			unsigned char p;
			for ( np = 0; np < npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows) {
					p = dp->pixels[indexr*dp->columns+indexc];
					if ( p > maxLevel )
						maxLevel = (double)p;
					if ( p < minLevel )
						minLevel = (double)p;
				}
			}
			maxLevel /= (double)FilledPixel;
			minLevel /= (double)FilledPixel;
			c = ( b - a ) / ( maxLevel - minLevel );
			d = ( maxLevel * a - minLevel * b ) / ( maxLevel - minLevel );
			dpm->pixels[row*dp->columns+column] = (unsigned char)( ( c * x + d ) * (double)FilledPixel );
		}
	}
	CharImageDelete(dp);
	return dpm;
}

CharImage*CharImageLStretchSE(CharImage*dp, int vsize, int hsize, StructElemType type, double a, double b)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageLStretchWithSE(dp, file, a, b);
	if ( remove(file) )
		g_warning("CharImageLStretchSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}


CharImage*CharImageMatchSE(CharImage*dp, int vsize, int hsize, StructElemType type)
{
	char *file;
	file = pr_get_name_temp();
	StructElem( file, vsize, hsize, type);
	dp = CharImageMatchWithSE(dp, file);
	if ( remove(file) )
		g_warning("CharImageMatchSE: temp file %s could not be deleted", file);
	free(file);
	return dp;
}

CharImage*CharImageMatchWithSE(CharImage*dp, char *file)
{
	uint32 column, row;
	uint32 np;
	int npattern, nelem;
	int median;
	int med;
	int *pattern;
	CharImage*dpm;
	unsigned char *p;
	FILE*fp;
	fp = fopen(file,"r");
	if ( !fp )
		g_error("parus: CharImageStructElem can't open %s", file);
	StructElemGetNElem(fp, &nelem);
	StructElemGetMedian(fp, &median);
	pattern = (int*)calloc(nelem, sizeof(int));
	npattern = (int)( (double)nelem / 2.0 );
	StructElemGetElem(fp, nelem, pattern);
	fclose(fp);
	p = (unsigned char*)calloc(npattern,sizeof(unsigned char));
	dpm = CharImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,dpm,pattern,npattern) private(np)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			int offset = 0;
			for ( np = 0; np < npattern; np++ ) {
				register int indexc = column + pattern[2*np];
				register int indexr = row + pattern[2*np+1];
				if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows && dp->pixels[indexr*dp->columns+indexc] != FilledPixel) {
					offset++;
					break;
				}
			}
			if ( !offset ) {
				for ( np = 0; np < npattern; np++ ) {
					register int indexc = column + pattern[2*np];
					register int indexr = row + pattern[2*np+1];
					if (-1 < indexc && indexc < dp->columns && -1 < indexr && indexr < dp->rows ) {
						dp->pixels[indexr*dp->columns+indexc] = EmptyPixel;
					}
				}
				dpm->pixels[row*dp->columns+column] = FilledPixel;
			}
		}
	}

	CharImageDelete(dp);
	free(p);
	return dpm;
}


SuperImage*SuperImagePadGeometry(SuperImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	SuperImage*di;
	rmax = dp->rows + LowerOffset;
	cmax = dp->columns + RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax + rmin );
	Columns = (uint32)( cmax + cmin );
	di = SuperImageNew(Columns,Rows, dp->bits_per_pixel);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di,rmin,cmin)
	for ( column = 0; column < dp->columns; column++ ) {
		for ( row = 0; row < dp->rows; row++ ) {
			di->pixels[( row + rmin ) * di->columns + column + cmin ] = dp->pixels[ row * dp->columns + column];
		}
	}
	SuperImageDelete(dp);
	return di;
}


SuperImage*SuperImageCropGeometry(SuperImage*dp, uint32 UpperOffset, uint32 LowerOffset, uint32 LeftOffset, uint32 RightOffset)
{
	uint32 column, row;
	uint32 Columns, Rows;
	uint32 cmin;
	uint32 cmax;
	uint32 rmin;
	uint32 rmax;
	SuperImage*di;
	rmax = dp->rows -1 - LowerOffset;
	cmax = dp->columns -1 - RightOffset;
	rmin = UpperOffset;
	cmin = LeftOffset;
	Rows = (uint32)( rmax - rmin + 1 );
	Columns = (uint32)( cmax - cmin + 1 );
	di = SuperImageNew(Columns,Rows, dp->bits_per_pixel);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di,rmin,cmin)
	for ( column=0; column<di->columns; column++ ) {
		for ( row=0; row<di->rows; row++ ) {
			di->pixels[row*di->columns+column] = dp->pixels[( row + rmin )*dp->columns+column + cmin];
		}
	}
	SuperImageDelete(dp);
	return di;
}

SuperImage*SuperImageRotate(SuperImage*di, double theta)
{
	SuperImage*ci;
	uint32 column, row;
	uint32 column1, row1;
	uint32 column2, row2;
	double a00, a01, a10, a11;
	double z00, z01, z10, z11;
	double rdot, rmid;
	double cdot, cmid;
	unsigned char *pixels;
	ci = SuperImageClone(di);
	a00 = cos( -theta );
	a01 = -sin( -theta );
	a10 = sin( -theta );
	a11 = cos( -theta );
	rdot = (double)( di->rows - 1);
	rmid = 0.5 * rdot;
	cdot = (double)( di->columns - 1);
	cmid = 0.5 * cdot;
	for ( row = 0; row < di->rows; row++ ) {
		rdot = (double)( row - rmid );
		for ( column = 0; column < di->columns; column++ ) {
			double c;
			double r;
			double x;
			double y;
			double c1;
			double r1;
			double c2;
			double r2;
			double pix;
			cdot = (double)( column - cmid );
			c = a00 * cdot + a01 * rdot;
			r = a10 * cdot + a11 * rdot;
			if ( c > 0 ) {
				c1 = floor(c + cmid);
				x = c + cmid - c1;
				c2 = c1 + 1.0;
			} else {
				c1 = ceil(c + cmid);
				x = c1 - c - cmid;
				c2 = c1 - 1.0;
			}
			if ( r > 0 ) {
				r1 = floor(r + rmid);
				y = r + rmid - r1;
				r2 = r1 + 1.0;
			} else {
				r1 = ceil(r + rmid);
				y = r1 - r - rmid;
				r2 = r1 - 1.0;
			}
			z00 = (double)di->emptyPixel;
			z10 = (double)di->emptyPixel;
			z01 = (double)di->emptyPixel;
			z11 = (double)di->emptyPixel;
			column1 = (uint32)c1;
			row1 = (uint32)r1;
			column2 = (uint32)c2;
			row2 = (uint32)r2;
			if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
				z00 = (double)di->pixels[row1*di->columns+column1];
			if ( 0 <= c1 && c1 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
				z10 = (double)di->pixels[row2*di->columns+column1];
			if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r1 && r1 < (double)di->rows )
				z01 = (double)di->pixels[row1*di->columns+column2];
			if ( 0 <= c2 && c2 < (double)di->columns && 0 <= r2 && r2 < (double)di->rows )
				z11 = (double)di->pixels[row2*di->columns+column2];
			pix = z00 * ( 1 - x - y + x * y);
			pix += z01 * ( x - x * y);
			pix += z10 * ( y - x * y);
			pix += z11 * x * y;
			if ( z00 != (double)di->emptyPixel && z00 == z01 && z00 == z11 && z00 == z10 )
				ci->pixels[row*di->columns+column] = di->pixels[row1*di->columns+column1];
			else
				ci->pixels[row*di->columns+column] = floor(pix);
		}
	}
	return ci;
}

SuperImage*SuperImageReverseColumn(SuperImage*dp)
{
	SuperImage*di;
	uint32 c, r;
	di = SuperImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ r * dp->columns + ( dp->columns - c - 1 ) ];
		}
	}
	SuperImageDelete(dp);
	return di;
}


SuperImage*SuperImageReverseRow(SuperImage*dp)
{
	SuperImage*di;
	uint32 c, r;
	di = SuperImageClone(dp);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ ( dp->rows - r - 1 ) * dp->columns + c ];
		}
	}
	SuperImageDelete(dp);
	return di;
}

SuperImage*SuperImageTranspose(SuperImage*dp)
{
	SuperImage*di;
	uint32 c, r;
	di = SuperImageNew(dp->rows, dp->columns, dp->bits_per_pixel);
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(dp,di)
	for ( c = 0; c < di->columns; c++ ) {
		for ( r = 0; r < di->rows; r++ ) {
			di->pixels[ r * di->columns + c] = dp->pixels[ ( dp->rows - c - 1 ) * dp->columns + r ];
		}
	}
	SuperImageDelete(dp);
	return di;
}

Element*elemenet_new(uint32 c, uint32 r, uint32 p)
{
	Element*elem;
	elem = (Element*)malloc(sizeof(Element));
	elem->column = c;
	elem->row = r;
	elem->plane = p;
}

void element_free(Element*arg)
{
	free(arg);
}

void mls_reg_func_common(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, double alpha, double*p_ast_1, double*p_ast_2, double*q_ast_1, double*q_ast_2)
{
	double weight;
	double sum_weights, sum_p_1, sum_p_2, sum_q_1, sum_q_2, r, c;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	sum_weights = 0;
	sum_p_1 = 0;
	sum_q_1 = 0;
	sum_p_2 = 0;
	sum_q_2 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = ( (p[i] - c) * (p[i] - c) + (p[i + 1] - r) * (p[i + 1] - r) );
		if ( -1e-12 < weight && weight < 1e-12 ) {
			weight = 1.0;
		}
		weight = 1.0 / weight;
		sum_weights += weight;
		sum_p_1 += weight * p[i];
		sum_p_2 += weight * p[i + 1];
		sum_q_1 += weight * q[i];
		sum_q_2 += weight * q[i + 1];
		weights[j] = weight;
	}
	*p_ast_1 = sum_p_1 / sum_weights;
	*p_ast_2 = sum_p_2 / sum_weights;
	*q_ast_1 = sum_q_1 / sum_weights;
	*q_ast_2 = sum_q_2 / sum_weights;
	for ( i = 0; i < nlm; i += 2 ) {
		p_hat[i] = p[i] - (*p_ast_1);
		p_hat[i + 1] = p[i + 1] - (*p_ast_2);
		q_hat[i] = q[i] - (*q_ast_1);
		q_hat[i + 1] = q[i + 1] - (*q_ast_2);
	}
}

unsigned char mls_reg_func_get(CharImage*data, double c, double r, double p_ast_1, double p_ast_2, double q_ast_1, double q_ast_2, double mn_11, double mn_12, double mn_21, double mn_22)
{
	unsigned char intensity;
	double dr, dc;
	int i, j;
	dc = ( c - p_ast_1 ) * mn_11 + ( r - p_ast_2 ) * mn_21 + q_ast_1;
	dr = ( c - p_ast_1 ) * mn_12 + ( r - p_ast_2 ) * mn_22 + q_ast_2;
	i = (int)( dc + 0.5 );
	j = (int)( dr + 0.5 );
	if ( 0 <= i && i < data->columns && 0 <= j && j < data->rows ) {
		intensity = data->pixels[ j * data->columns + i ];
	} else {
		intensity = EmptyPixel;
	}
	return intensity;
}

void mls_reg_func_get_coords(int*i, int*j, double c, double r, double p_ast_1, double p_ast_2, double q_ast_1, double q_ast_2, double mn_11, double mn_12, double mn_21, double mn_22)
{
	unsigned char intensity;
	double dr, dc;
	dc = ( c - p_ast_1 ) * mn_11 + ( r - p_ast_2 ) * mn_21 + q_ast_1;
	dr = ( c - p_ast_1 ) * mn_12 + ( r - p_ast_2 ) * mn_22 + q_ast_2;
	*i = (int)( dc + 0.5 );
	*j = (int)( dr + 0.5 );
}

void mls_reg_func_super_affine(int*co, int*ro, int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, SuperImage*data, double alpha)
{
	double weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, m_11, m_12, m_22, d, n_11, n_12, n_21, n_22, mn_11, mn_12, mn_21, mn_22;
	double r, c, dr, dc;
	int i, j;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	m_11 = 0;
	m_12 = 0;
	m_22 = 0;
	n_11 = 0;
	n_12 = 0;
	n_21 = 0;
	n_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		m_11 += weight * p_hat[i] * p_hat[i];
		m_12 += weight * p_hat[i] * p_hat[i + 1];
		m_22 += weight * p_hat[i + 1] * p_hat[i + 1];
		n_11 += weight * p_hat[i] * q_hat[i];
		n_12 += weight * p_hat[i] * q_hat[i + 1];
		n_21 += weight * q_hat[i] * p_hat[i + 1];
		n_22 += weight * p_hat[i + 1] * q_hat[i + 1];
	}
	d = m_12 * m_12 - m_11 * m_22;
	weight = m_11;
	m_11 = -m_22 / d;
	m_22 = -weight / d;
	m_12 /= d;
	mn_11 = m_11 * n_11 + m_12 * n_21;
	mn_12 = m_11 * n_12 + m_12 * n_22;
	mn_21 = m_12 * n_11 + m_22 * n_21;
	mn_22 = m_12 * n_12 + m_22 * n_22;
	mls_reg_func_get_coords(co, ro, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
}

void mls_reg_func_super_similar(int*co, int*ro, int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, SuperImage*data, double alpha)
{
	double mu, weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, mn_11, mn_12, mn_21, mn_22;
	double r, c;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	mu = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		mu += weight * ( p_hat[i] * p_hat[i] +  p_hat[i + 1] * p_hat[i + 1] );
	}
	mn_11 = 0;
	mn_12 = 0;
	mn_21 = 0;
	mn_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j] / mu;
		mn_11 += weight * ( p_hat[i] * q_hat[i] + p_hat[i + 1] * q_hat[i + 1] );
		mn_12 += weight * ( p_hat[i] * q_hat[i + 1] - p_hat[i + 1] * q_hat[i] );
		mn_21 += weight * ( p_hat[i + 1] * q_hat[i] - p_hat[i] * q_hat[i + 1] );
		mn_22 += weight * ( p_hat[i + 1] * q_hat[i + 1] + p_hat[i] * q_hat[i] );
	}
	mls_reg_func_get_coords(co, ro, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
}

void mls_reg_func_super_rigid(int*co, int*ro, int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, SuperImage*data, double alpha)
{
	double mu, mu_1, mu_2, weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, mn_11, mn_12, mn_21, mn_22;
	double r, c;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	mu_1 = 0;
	mu_2 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		mu_1 += weight * ( q_hat[i] * p_hat[i] +  q_hat[i + 1] * p_hat[i + 1] );
		mu_2 += weight * ( -q_hat[i] * p_hat[i + 1] + q_hat[i + 1] * p_hat[i] );
	}
	mu = sqrt ( mu_1 * mu_1 + mu_2 * mu_2 );
	mn_11 = 0;
	mn_12 = 0;
	mn_21 = 0;
	mn_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j] / mu;
		mn_11 += weight * ( p_hat[i] * q_hat[i] + p_hat[i + 1] * q_hat[i + 1] );
		mn_12 += weight * ( p_hat[i] * q_hat[i + 1] - p_hat[i + 1] * q_hat[i] );
		mn_21 += weight * ( p_hat[i + 1] * q_hat[i] - p_hat[i] * q_hat[i + 1] );
		mn_22 += weight * ( p_hat[i + 1] * q_hat[i + 1] + p_hat[i] * q_hat[i] );
	}
	mls_reg_func_get_coords(co, ro, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
}

unsigned char mls_reg_func_affine(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha)
{
	double weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, m_11, m_12, m_22, d, n_11, n_12, n_21, n_22, mn_11, mn_12, mn_21, mn_22;
	double r, c, dr, dc;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	m_11 = 0;
	m_12 = 0;
	m_22 = 0;
	n_11 = 0;
	n_12 = 0;
	n_21 = 0;
	n_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		m_11 += weight * p_hat[i] * p_hat[i];
		m_12 += weight * p_hat[i] * p_hat[i + 1];
		m_22 += weight * p_hat[i + 1] * p_hat[i + 1];
		n_11 += weight * p_hat[i] * q_hat[i];
		n_12 += weight * p_hat[i] * q_hat[i + 1];
		n_21 += weight * q_hat[i] * p_hat[i + 1];
		n_22 += weight * p_hat[i + 1] * q_hat[i + 1];
	}
	d = m_12 * m_12 - m_11 * m_22;
	weight = m_11;
	m_11 = -m_22 / d;
	m_22 = -weight / d;
	m_12 /= d;
	mn_11 = m_11 * n_11 + m_12 * n_21;
	mn_12 = m_11 * n_12 + m_12 * n_22;
	mn_21 = m_12 * n_11 + m_22 * n_21;
	mn_22 = m_12 * n_12 + m_22 * n_22;
	intensity = mls_reg_func_get(data, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
	return intensity;
}

unsigned char mls_reg_func_similar(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha)
{
	double mu, weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, mn_11, mn_12, mn_21, mn_22;
	double r, c;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	mu = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		mu += weight * ( p_hat[i] * p_hat[i] +  p_hat[i + 1] * p_hat[i + 1] );
	}
	mn_11 = 0;
	mn_12 = 0;
	mn_21 = 0;
	mn_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j] / mu;
		mn_11 += weight * ( p_hat[i] * q_hat[i] + p_hat[i + 1] * q_hat[i + 1] );
		mn_12 += weight * ( p_hat[i] * q_hat[i + 1] - p_hat[i + 1] * q_hat[i] );
		mn_21 += weight * ( p_hat[i + 1] * q_hat[i] - p_hat[i] * q_hat[i + 1] );
		mn_22 += weight * ( p_hat[i + 1] * q_hat[i + 1] + p_hat[i] * q_hat[i] );
	}
	intensity = mls_reg_func_get(data, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
	return intensity;
}

unsigned char mls_reg_func_rigid(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha)
{
	double mu, mu_1, mu_2, weight, p_ast_1, q_ast_1, p_ast_2, q_ast_2, mn_11, mn_12, mn_21, mn_22;
	double r, c;
	int i, j;
	unsigned char intensity;
	r = (double)row;
	c = (double)column;
	mls_reg_func_common(column, row, p, q, p_hat, q_hat, weights, nlm, alpha, &p_ast_1, &p_ast_2, &q_ast_1, &q_ast_2);
	mu_1 = 0;
	mu_2 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j];
		mu_1 += weight * ( q_hat[i] * p_hat[i] +  q_hat[i + 1] * p_hat[i + 1] );
		mu_2 += weight * ( -q_hat[i] * p_hat[i + 1] + q_hat[i + 1] * p_hat[i] );
	}
	mu = sqrt ( mu_1 * mu_1 + mu_2 * mu_2 );
	mn_11 = 0;
	mn_12 = 0;
	mn_21 = 0;
	mn_22 = 0;
	for ( i = 0, j = 0; i < nlm; i += 2, j++ ) {
		weight = weights[j] / mu;
		mn_11 += weight * ( p_hat[i] * q_hat[i] + p_hat[i + 1] * q_hat[i + 1] );
		mn_12 += weight * ( p_hat[i] * q_hat[i + 1] - p_hat[i + 1] * q_hat[i] );
		mn_21 += weight * ( p_hat[i + 1] * q_hat[i] - p_hat[i] * q_hat[i + 1] );
		mn_22 += weight * ( p_hat[i + 1] * q_hat[i + 1] + p_hat[i] * q_hat[i] );
	}
	intensity = mls_reg_func_get(data, c, r, p_ast_1, p_ast_2, q_ast_1, q_ast_2, mn_11, mn_12, mn_21, mn_22);
	return intensity;
}

double*char_image_get_landmarks(CharImage*landmarks, int*i)
{
	double*p;
	int row, column;
	p = NULL;
	*i = 0;
	for ( row = 0 ; row < landmarks->rows; row++ ) {
		for ( column = 0 ; column < landmarks->columns; column++ ) {
			if ( landmarks->pixels[ row * landmarks->columns + column] == FilledPixel ) {
				p = (double*)realloc(p, ( (*i) + 2 ) * sizeof(double) );
				p[ (*i) ] = column;
				(*i)++;
				p[ (*i) ] = row;
				(*i)++;
			}
		}
	}
	return p;
}

CharImage*char_image_mlsreg(CharImage*landmarks_fixed, CharImage*landmarks, CharImage*data, char*type, double alpha)
{
	CharImage*registered;
	int column, row, rows, columns;
	int i, j, nlm;
	double *p, *q;
	double *p_hat, *q_hat;
	double *weights;
	unsigned char (*mls_reg_func)(int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, CharImage*data, double alpha);
	if ( !strcmp(type, "affine") ) {
		mls_reg_func = mls_reg_func_affine;
	} else if ( !strcmp(type, "similar") ) {
		mls_reg_func = mls_reg_func_similar;
	} else if ( !strcmp(type, "rigid") ) {
		mls_reg_func = mls_reg_func_rigid;
	} else {
		return NULL;
	}
	rows = landmarks_fixed->rows;
	columns = landmarks_fixed->columns;
	registered = CharImageClone(landmarks_fixed);
	p = NULL;
	q = NULL;
	p = char_image_get_landmarks(landmarks_fixed, &i);
	q = char_image_get_landmarks(landmarks, &j);
	if ( i != j || !p || !q ) {
		if ( p )
			free(p);
		if ( q )
			free(q);
		return NULL;
	}
	nlm = j;
	p_hat = (double*)calloc(nlm + nlm, sizeof(double) );
	q_hat = (double*)calloc(nlm + nlm, sizeof(double) );
	weights = (double*)calloc(nlm, sizeof(double) );
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(registered,rows,columns,p,q,p_hat,q_hat,weights,nlm,data,alpha,mls_reg_func)
	for ( row = 0 ; row < rows; row++ ) {
		for ( column = 0; column < columns; column++ ) {
			registered->pixels[row * columns + column] = mls_reg_func(column, row, p, q, p_hat, q_hat, weights, nlm, data, alpha);
		}
	}
	free(p);
	free(q);
	return registered;
}

SuperImage*super_image_mlsreg(CharImage*landmarks_fixed, CharImage*landmarks, SuperImage*data, char*type, double alpha)
{
	SuperImage*registered;
	int column, row, rows, columns, c, r;
	int i, j, nlm;
	double *p, *q;
	double *p_hat, *q_hat;
	double *weights;
	void (*mls_reg_func_super)(int*c, int*r, int column, int row, double*p, double*q, double*p_hat, double*q_hat, double*weights, int nlm, SuperImage*data, double alpha);
	if ( !strcmp(type, "affine") ) {
		mls_reg_func_super = mls_reg_func_super_affine;
	} else if ( !strcmp(type, "similar") ) {
		mls_reg_func_super = mls_reg_func_super_similar;
	} else if ( !strcmp(type, "rigid") ) {
		mls_reg_func_super = mls_reg_func_super_rigid;
	} else {
		return NULL;
	}
	rows = landmarks_fixed->rows;
	columns = landmarks_fixed->columns;
	registered = SuperImageClone(data);
	p = NULL;
	q = NULL;
	p = char_image_get_landmarks(landmarks_fixed, &i);
	q = char_image_get_landmarks(landmarks, &j);
	if ( i != j || !p || !q ) {
		if ( p )
			free(p);
		if ( q )
			free(q);
		return NULL;
	}
	nlm = j;
	p_hat = (double*)calloc(nlm + nlm, sizeof(double) );
	q_hat = (double*)calloc(nlm + nlm, sizeof(double) );
	weights = (double*)calloc(nlm, sizeof(double) );
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(registered,rows,columns,p,q,p_hat,q_hat,weights,nlm,data,alpha,mls_reg_func_super) private(r,c)
	for ( row = 0 ; row < rows; row++ ) {
		for ( column = 0; column < columns; column++ ) {
			mls_reg_func_super(&c, &r, column, row, p, q, p_hat, q_hat, weights, nlm, data, alpha);
			if ( 0 <= c && c < data->columns && 0 <= r && r < data->rows ) {
				registered->pixels[row * columns + column] = data->pixels[r * columns + c];
			}
		}
	}
	free(p);
	free(q);
	return registered;
}
