/***************************************************************************
 *            prostak_vfuncs.h
 *
 *  Tue Nov  3 14:35:13 2009
 *  Copyright  2009  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef PROSTAK_VFUNCS_H
#define PROSTAK_VFUNCS_H

#include <glib.h>
#include <grf.h>

G_BEGIN_DECLS

int grf_callback_vmax(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data);
int grf_callback_vavg(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data);
int grf_callback_vmabs(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data);
int grf_callback_vrgb(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*in_pixels_3, double*out_pixels_1, void*user_data);
int grf_callback_vaff(uint32 plane, uint32 row, uint32 rows, uint32 columns, uint16 in_bps_1, double*in_pixels_1, double*in_pixels_2, double*out_pixels_1, void*user_data);
int prostak_vmax(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vrgb(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vavg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vmabs(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vrmbg(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);
int prostak_vaff(int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, double param, int repetitions, char*ostring, int n_input_pointers, char**input_pointers, int n_output_pointers, char**output_pointers);

G_END_DECLS

#endif /* PROSTAK_VFUNCS_H */
