/***************************************************************************
                          prostak.c  -  description
                             -------------------
    begin                : ðÔÎ ñÎ× 21 2005
    copyright            : (C) 2005 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fenv.h>
#include <math.h>

#include <prostak.h>

static int VERBOSE;
static int PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 = 0;
static char*oper = NULL;
static int repetitions = 1;
static double param = 1.0;
static char*ostring;
static char*mask;
static int GeometryIsSet = 0;
static int HasMask = 0;

static const char help[]     =

"Usage: prostak [options] <operation> <input file> <output file>\n\n"

"Argument:\n"
"  <image>          input data file\n\n"

"Options:\n"
"  -h                  prints this help message\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"  -v                  print version and compilation date\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static char version[MAX_RECORD];                 /* version gets set below */
/*
static const char verstring[] =

"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";
*/
static const char usage[]    =
"Usage: prostak   [-h]\n"
"  -p <val>            parameter\n"
"  -r <n>              repetitions\n"
"  -s <x,x1,y,y1>      set geometry\n"
"                 [-v]\n";

static int rounding_mode_old, rounding_mode_new, rounding_mode_res;

int ParseCommandLine(int argc, char**argv)
{
	int         c;                     /* used to parse command line options */
/* external declarations for command line option parsing (unistd.h) */
	extern char *optarg;                     /* command line option argument */
	extern int  optind;                /* pointer to current element of argv */
	extern int  optopt;               /* contain option character upon error */
/* set the version string */
	optarg = NULL;
	while( (c = getopt(argc, argv, OPTS)) != -1 ) {
		switch(c) {
			case 'a':
				PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 = 1;
			break;
			case 'b':
				PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16 = 1;
			break;
			case 'h':                                            /* -h help option */
				g_error(help);
			break;
			case 'm':
				mask = strcpy(mask,optarg);
				HasMask = 1;
			break;
			case 'o':
				oper = g_strdup(optarg);
			break;
			case 'p':
				param = atof(optarg);
			break;
			case 'r':
				repetitions = atoi(optarg);
			break;
			case 's':
				ostring = g_strdup(optarg);
				GeometryIsSet = 1;
			break;
			case 'v':                                  /* -v prints version message */
				exit(0);
			break;
			case ':':
				g_error("prostak: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				g_error("prostak: unrecognized option -%c", optopt);
		}
	}

/* error checking here */

	if ( ((argc - (optind - 1)) < 1) )
		g_error(usage);
	return optind;
}

int main(int argc,char **argv)
{
	int optind; /* returned by getopt from ParseCommandLine: index of the first non-option argument */
	FILE*fp;
	char *logfnam;
	char*inputs_list = NULL, *outputs_list = NULL;
	char**input_pointers = NULL, **output_pointers = NULL;
	int n_input_pointers = 0, n_output_pointers = 0;
	GError*gerror = NULL;
/*	rounding_mode_new = FE_TONEAREST;
	rounding_mode_new = FE_UPWARD;
	rounding_mode_new = FE_DOWNWARD;*/
	rounding_mode_new = FE_TOWARDZERO;
	rounding_mode_old = fegetround();
	rounding_mode_res = fesetround(rounding_mode_new);
/*	fprintf(stderr, "Rounding mode was changed from %d to %d with %d (0 - success)\n", rounding_mode_old, rounding_mode_new, rounding_mode_res);*/
	mask = (char *)calloc(MAX_RECORD, sizeof(char));
	optind = ParseCommandLine(argc,argv);
	if ( !oper && optind < argc ) {
		oper = g_strdup(argv[optind]);
		optind++;
	}
	if ( optind < argc ) {
		inputs_list = g_strdup(argv[optind]);
		input_pointers = g_strsplit(inputs_list, ",", MAX_RECORD);
		if ( input_pointers )
			n_input_pointers = g_strv_length(input_pointers);
		optind++;
	}
	if ( optind < argc ) {
		outputs_list = g_strdup(argv[optind]);
		output_pointers = g_strsplit(outputs_list, ",", MAX_RECORD);
		if ( output_pointers )
			n_output_pointers = g_strv_length(output_pointers);
	}
/*	g_type_init();*/
	if ( !strcmp(oper,"info")) {
		prostak_info(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"intense")) {
		prostak_intense(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"affine")) {
		prostak_affine(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"max")) {
		prostak_max(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"threshold3d")) {
		prostak_threshold3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"max3d")) {
		prostak_max3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"avg")) {
		prostak_avg(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"plus")) {
		prostak_plus(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"minusabs")) {
		prostak_minusabs(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"minus")) {
		prostak_minus(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"drydilation")) {
		prostak_drydilation(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"threshb")) {
		prostak_threshold_blobs(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"threshold")) {
		prostak_threshold(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"hystthresh")) {
		prostak_hysteresis_threshold(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"hystthresh3d")) {
		prostak_hysteresis_threshold3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"rotate")) {
		prostak_rotate(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"turn")) {
		prostak_turn(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"turn3d")) {
		prostak_turn3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"align")) {
		prostak_align(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"chemar")) {
		prostak_chemar(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"revrow")) {
		prostak_reverserow(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"revrow3d")) {
		prostak_reverserow3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"revcol")) {
		prostak_reversecolumn(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"revcol3d")) {
		prostak_reversecolumn3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"transpose")) {
		prostak_transpose(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"transpose3d")) {
		prostak_transpose3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"crop")) {
		prostak_crop(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"halfsizes")) {
		prostak_halfsizes(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"geometry")) {
		prostak_geometry(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"geometry3d")) {
		prostak_geometry3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"raw")) {
		prostak_raw(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"pad")) {
		prostak_pad(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"pad3d")) {
		prostak_pad3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"erase")) {
		prostak_erase(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"distance")) {
		prostak_distance(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gdistance")) {
		prostak_distance_g(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"xdistance")) {
		prostak_distance_x(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"ydistance")) {
		prostak_distance_y(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gmag")) {
		prostak_gmag(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"edge")) {
		prostak_edgesc(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"fill")) {
		prostak_fill(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"invert")) {
		prostak_invert(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"invert3d")) {
		prostak_invert3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"grid")) {
		prostak_grid(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"grid3d")) {
		prostak_grid3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"rogri")) {
		prostak_ropol_grid(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"invertn")) {
		prostak_invert_n(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"cross")) {
		prostak_cross(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mask")) {
		prostak_mask(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"cnm")) {
		prostak_cnm(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"heq")) {
		prostak_heq(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"hno")) {
		prostak_hno(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lheq")) {
		prostak_lheq(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"chole")) {
		prostak_chole(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lotsu")) {
		prostak_lotsu(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"hist")) {
		prostak_hist(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"median")) {
		prostak_median(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"match")) {
		prostak_match(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gerosion")) {
		prostak_gerosion(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gopen")) {
		prostak_gopen(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gclose")) {
		prostak_gclose(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gclose3d")) {
		prostak_gclose3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gopen3d")) {
		prostak_gopen3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gdilation")) {
		prostak_gdilation(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lstretch")) {
		prostak_lstretch(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"ppix")) {
		prostak_ppix(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mppix")) {
		prostak_mppix(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"andif")) {
		prostak_anisodiff(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"despekle") ) {
		prostak_despeckle(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"strel")) {
		prostak_strel(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vstrel")) {
		prostak_vstrel(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"scale")) {
		prostak_scale(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shrink")) {
		prostak_shrink(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shrink3d")) {
		prostak_shrink3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"expand")) {
		prostak_expand(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"expand3d")) {
		prostak_expand3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"noop")) {
		prostak_noop(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mul")) {
		prostak_mul(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mul3d")) {
		prostak_mul3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mumu")) {
		prostak_mumu(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"hues")) {
		prostak_hues(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"huel")) {
		prostak_huel(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vvarbc3d")) {
		prostak_vvarbc3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vvarbc")) {
		prostak_vvarbc(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vrmbg")) {
		prostak_vrmbg(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"blo2pol")) {
		prostak_blo2pol(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"blob")) {
		prostak_blob(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"solo")) {
		prostak_solo(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"bolb")) {
		prostak_bolb(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"ybolb")) {
		prostak_bolb_y(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"bolin")) {
		prostak_bolind(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shape")) {
		prostak_shape(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shape3d")) {
		prostak_shape3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"ropol")) {
		prostak_round_polar(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"ropol_scr")) {
		prostak_ropol_scr(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"ropri")) {
		prostak_ropol_print(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"robel")) {
		prostak_belt(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"lev")) {
		prostak_lev(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shape_select")) {
		prostak_shape_select(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"sselect")) {
		prostak_shape_select2(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"sselect3d")) {
		prostak_shape_select3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"movl")) {
		prostak_movl(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"splitrgb")) {
		prostak_splitrgb(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"splitlsm")) {
		prostak_splitlsm(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"maxlsm")) {
		prostak_maxlsm(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"diglsm")) {
		prostak_diglsm(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"insert")) {
		prostak_insert3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"volume_row")) {
		prostak_volume_row(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"volume_column")) {
		prostak_volume_column(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"cwtsd3d")) {
		prostak_cheap_watershed3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
#ifndef G_OS_WIN32
	} else if  ( !strcmp(oper,"surf3d")) {
		prostak_surf3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"surf3dfull")) {
		prostak_surf3dfull(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"surf2vol")) {
		prostak_surf2vol(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
#endif
	} else if  ( !strcmp(oper,"pump3d")) {
		prostak_pump3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mslice3d")) {
		prostak_mslice3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mslicegcp3d")) {
		prostak_mslicegcp3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"mlsreg")) {
		prostak_mlsreg(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"rv3d")) {
		prostak_rv3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lv3d")) {
		prostak_lv3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"tv3d")) {
		prostak_tv3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"cunbent3d")) {
		prostak_cunbent3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"cwtsd")) {
		prostak_cwtsd(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"reconstruct")) {
		prostak_reconstruct(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lift")) {
		prostak_lift(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"dummy")) {
		prostak_dummy(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"plot")) {
		prostak_plot(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"plot_sp")) {
		prostak_plot_sp(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"regmin")) {
		prostak_regionalmin(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"regmax")) {
		prostak_regionalmax(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"median3d")) {
		prostak_median3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"unbent3d")) {
		prostak_unbent3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"chole3d")) {
		prostak_chole3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lheq3d")) {
		prostak_lheq3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gerosion3d")) {
		prostak_gerosion3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"gdilation3d")) {
		prostak_gdilation3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"reconstruct3d")) {
		prostak_reconstruct3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"rec3dbp")) {
		prostak_rec3dbp(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"setp")) {
		prostak_setp(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"punite")) {
		prostak_punite(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"getp")) {
		prostak_getp(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"pbreak")) {
		prostak_pbreak(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"strel3d")) {
		prostak_strel3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"strel3dw")) {
		prostak_strel3dw(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vstrel3d")) {
		prostak_vstrel3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"distance3d") ) {
		prostak_distance3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"restack3d") ) {
		prostak_restack3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"edge3d") ) {
		prostak_edge3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"despekle3d") ) {
		prostak_despeckle3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"shape_select3d")) {
		prostak_shape_select3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"quremask")) {
		prostak_quremask(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"quresort")) {
		prostak_quresort(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"quthicken")) {
		prostak_quthicken(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"querode")) {
		prostak_querode(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qu3d")) {
		prostak_qu3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"quvu")) {
		prostak_vu(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qumark3d")) {
		prostak_qumark3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qumap3d")) {
		prostak_qumap3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qurelabel")) {
		prostak_qurelabel(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qu3dtrans")) {
		prostak_qu3dtrans(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qu3dinit")) {
		prostak_qu3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"qu3d2csv")) {
		prostak_qu3d2csv(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"zscale3d")) {
		prostak_zscale3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"vtxt")) {
		prostak_vtxt3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"contrast")) {
		prostak_contrast(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"pardec")) {
		prostak_pardec(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"decsup")) {
		prostak_decsup(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"decbsup")) {
		prostak_decbsup(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"decwiener")) {
		prostak_decwiener(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"dectm")) {
		prostak_dectm(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"decinv")) {
		prostak_decinv(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vmax")) {
		prostak_vmax(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vavg")) {
		prostak_vavg(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vmabs")) {
		prostak_vmabs(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vaff")) {
		prostak_vaff(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if ( !strcmp(oper,"vrgb")) {
		prostak_vrgb(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"apee3d")) {
		prostak_apee3d(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"apee")) {
		prostak_apee(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"apsc")) {
		prostak_apsc(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"ar_minus")) {
		prostak_ar_minus(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"ar_x")) {
		prostak_ar_x(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"m_ar_plus")) {
		prostak_m_ar_plus(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"m_ar_x")) {
		prostak_m_ar_x(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else if  ( !strcmp(oper,"lhbg")) {
		prostak_lhbg(PROCESS_AT_ORIGINAL_RESOLUTION_12_OR_16, param, repetitions, ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else {
		g_error("prostak unknown operation %s",oper);
	}
	free(oper);
	free(ostring);
	free(mask);
	rounding_mode_res = fesetround(rounding_mode_old);
/*	fprintf(stderr, "Rounding mode was changed from %d to %d with %d (0 - success)\n", rounding_mode_new, rounding_mode_old, rounding_mode_res);*/
	return 1;
}
