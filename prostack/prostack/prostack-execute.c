/***************************************************************************
 *            prostack-execute.c
 *
 *  Ср Март 11 09:51:05 2015
 *  Copyright  2015  kkozlov
 *  <user@host>
 ****************************************************************************/

#include <config.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <gio/gio.h>
#include <prostack-execute.h>
#include <Workspace.h>
#include <Node.h>
#include <Kimono.h>

#include "prostack-dbus-generated.h"

static gboolean nodbus = FALSE;
static gchar*default_name;

static ProStackInterface *pro_stack_interface_proxy;

#define KIMONO_EXAMPLES_DIR "/prostack/examples"
#define LOCALE_DIR "locale"

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

static Workspace*myWS;
static char *filename = NULL;
static char *dirname = NULL;
static char *basefilename = NULL;
static GtkWidget *mw;
static int filename_flag = 0;
static char*title, *subtitle;
static GThread*threadWS;
static GtkWidget *runButton;
static GtkWidget *stopButton;
static GtkWidget *optab;
static GtkWidget *pbar;
static GtkWidget *uid_entry;
static GtkWidget *filechooserbutton;
static short isSetLogger = 1;
static int modified = 1;
static GtkWidget *headerbar1;

gboolean toolbar_set_sensitive(gpointer data)
{
	Workspace *ws = (Workspace *)data;
	WorkspaceStatus wstatus = workspace_get_status(ws);
	if (wstatus == WS_RUNNING) {
		gtk_widget_set_sensitive(stopButton, TRUE);
		gtk_widget_set_sensitive(runButton, FALSE);
	} else if (wstatus == WS_FAILED || wstatus == WS_TERMINATED || wstatus == WS_COMPLETED) {
		gtk_widget_set_sensitive(stopButton, FALSE);
		gtk_widget_set_sensitive(runButton, TRUE);
	} else if (wstatus == WS_WAITING) {
		gtk_widget_set_sensitive(stopButton, FALSE);
		gtk_widget_set_sensitive(runButton, TRUE);
	}
	return FALSE;
}

gpointer threadRunWS(gpointer *arg)
{
	Workspace *ws;
	WorkspaceStatus wstatus;
	ListOfNodes *curr;
	ws = (Workspace*)arg;
	wstatus = WS_FAILED;
	workspace_set_status(myWS, WS_RUNNING);
	workspaceRun(ws);
	wstatus = workspace_get_status(myWS);
	g_warning("Status = %d", wstatus);
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)ws);
	g_thread_exit (NULL);
}

gboolean toolbar1_cancel_event( GtkWidget *widget, gpointer data )
{
	ListOfNodes *curr;
	WorkspaceStatus wstatus;
	wstatus = workspace_get_status(myWS);
	if ( wstatus == WS_RUNNING ) {
		g_mutex_lock(&(myWS->stopMutex));
		myWS->clean = 1;
		g_mutex_unlock(&(myWS->stopMutex));
		g_thread_join(threadWS);
		notify_error(_("Workspace stopped!"));
	}
	workspaceRmTemp(myWS);
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		g_mutex_lock (&(curr->node->statusMutex));
		if ( curr->node->status == completed )
			nodeCleanOutPut(curr->node);
		curr->node->status = waiting;
		g_mutex_unlock (&(curr->node->statusMutex));
		trace_progress(curr->node);
	}
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR(pbar), 0);
	threadWS = (GThread*)NULL;
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
	return FALSE;
}

gboolean toolbar1_save_event( GtkWidget *widget, gpointer data )
{
	int error_occur = 0;
	char*jobid = (char*)gtk_entry_get_text(GTK_ENTRY (uid_entry));
	char*mountpoint = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (filechooserbutton));
	ws_set_ins_ous_jobid(myWS, mountpoint, jobid);
	if ( filename_flag == 1 ) {
		error_occur = workspace_to_file(myWS, filename);
		if ( error_occur == 1 ) {
			GtkWidget* dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_WARNING,
				GTK_BUTTONS_CLOSE,
				_("This file is read only. Use Save As"));
/* Destroy the dialog when the user responds to it (e.g. clicks a button) */
			g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
			gtk_widget_show (dialog);
			return FALSE;
		}
		bambu_set_title(1);
	} else {
		fileSelectSave();
	}
	return FALSE;
}

gboolean toolbar1_save_as_event( GtkWidget *widget, gpointer data )
{
	char*jobid = (char*)gtk_entry_get_text(GTK_ENTRY (uid_entry));
	char*mountpoint = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (filechooserbutton));
	ws_set_ins_ous_jobid(myWS, mountpoint, jobid);
	fileSelectSave();
	return FALSE;
}

gboolean toolbar1_run_event( GtkWidget *widget, gpointer data )
{
	GError*gerror = NULL;
	WorkspaceStatus wstatus;
	char*jobid, *mountpoint;
	wstatus = workspace_get_status(myWS);
	if ( wstatus != WS_RUNNING ) {
		jobid = (char*)gtk_entry_get_text(GTK_ENTRY (uid_entry));
		mountpoint = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (filechooserbutton));
		if ( !mountpoint || !jobid ) {
			GtkWidget*d;
			d = gtk_message_dialog_new ((GtkWindow*)mw,
                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_ERROR,
                                  GTK_BUTTONS_CLOSE,
                                  _("Job ID and ouput directory are required"));
			gtk_dialog_run (GTK_DIALOG (d));
			gtk_widget_destroy (d);
			return TRUE;
		}
		gtk_widget_set_sensitive(runButton, FALSE);
		g_mutex_lock (&(myWS->stopMutex));
		myWS->clean = 0;
		g_mutex_unlock (&(myWS->stopMutex));
		workspace_set_status(myWS, WS_RUNNING);
		ws_set_ins_ous_jobid(myWS, mountpoint, jobid);
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR(pbar), 0);
		if ( mountpoint ) g_free(mountpoint);
		if ( ( threadWS = g_thread_try_new( "Run", (GThreadFunc)threadRunWS, (gpointer)myWS, &gerror ) ) == NULL ) {
			if ( gerror ) {
				notify_error(gerror->message);
				g_error_free(gerror);
			} else {
				notify_error(_("Can't run!"));
			}
			workspace_set_status(myWS, WS_FAILED);
		}
	} else {
		notify_error(_("Workspace is running!"));/*^^*/
	}
	gdk_threads_add_idle (toolbar_set_sensitive, (gpointer)myWS);
	return FALSE;
}

void undoDownSrtream(Node*ne)
{
	ListOfNodes*curr;
	WorkspaceStatus wstatus;
	gtk_widget_set_sensitive(stopButton, FALSE);
	wstatus = workspace_get_status(myWS);
	if ( wstatus == WS_RUNNING ) {
		g_mutex_lock(&(myWS->stopMutex));
		myWS->clean = 1;
		g_mutex_unlock(&(myWS->stopMutex));
		g_thread_join(threadWS);
	}
	listOfNodesPropagateStatus(myWS->listOfNodes, ne, waiting);
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		g_mutex_lock (&(curr->node->statusMutex));
		if ( curr->node->status == running ) {
			curr->node->status = waiting;
			g_mutex_unlock (&(curr->node->statusMutex));
		} else {
			g_mutex_unlock (&(curr->node->statusMutex));
		}
	}
	gtk_widget_set_sensitive(runButton, TRUE);
}

void uiIns(Node*ne, GtkWidget*button)
{
	GtkWidget *eLabel, *eEntry, *eHbox, *dbox;
	GtkDialog *dialog;
	char*fn;
	int flag;
	fn = (char*)NULL;
	flag = 0;
	dialog = (GtkDialog *)gtk_file_chooser_dialog_new (_("Select file"), (GtkWindow*)mw, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
	dbox = gtk_dialog_get_content_area (dialog);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( ne->file && strlen(ne->file) > 0 ) {
		char*scheme = g_uri_parse_scheme(ne->file);
		if ( !scheme ) {
#ifdef G_OS_WIN32
			fn = g_strdup_printf("file:///%s", ne->file);
#else
			fn = g_strdup_printf("file://%s", ne->file);
#endif
		} else {
			fn = g_strdup(ne->file);
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), fn);
		g_free(fn);
	}
	eLabel = gtk_label_new(_("Label"));
	eEntry = gtk_entry_new();
	gtk_widget_show(eLabel);
	gtk_widget_show(eEntry);
	eHbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
	gtk_widget_show(eHbox);
	gtk_box_pack_start(GTK_BOX(eHbox), eLabel, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(eHbox), eEntry, FALSE, TRUE, 1);
	gtk_box_pack_start(GTK_BOX(dbox), eHbox, FALSE, TRUE, 1);
	gtk_entry_set_text( GTK_ENTRY (eEntry), ne->label);
	gtk_editable_set_editable(GTK_EDITABLE (eEntry), FALSE );
	if (gtk_dialog_run (dialog) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		char *newfolder;
		newfilename = gtk_file_chooser_get_uri((GtkFileChooser *) dialog);
		newfolder = gtk_file_chooser_get_current_folder_uri(GTK_FILE_CHOOSER (dialog));
		if ( newfolder ) {
			g_free(newfolder);
		}
		g_mutex_lock(&(ne->statusMutex));
		if ( ne->status != waiting )
			flag = 1;
		if ( newfilename ) {
			if ( ne->file )
				g_free(ne->file);
			ne->file = g_strdup(newfilename);
			g_free(newfilename);
		}
		g_mutex_unlock(&(ne->statusMutex));
		if (ne->buf)
			g_free(ne->buf);
		ne->buf = g_path_get_basename((const gchar*)ne->file);
		gtk_button_set_label (button, ne->buf);
		bambu_set_title(0);
	}
	gtk_widget_destroy ((GtkWidget *)dialog);
	if ( flag ) undoDownSrtream(ne);
}

GtkWidget *create_option_dialog(char*name)
{
	GtkWidget *dialog = gtk_dialog_new_with_buttons (NULL, GTK_WINDOW (mw),
                                                  (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                  GTK_DIALOG_USE_HEADER_BAR |
                                                  GTK_DIALOG_DESTROY_WITH_PARENT),
                                                  "_OK", GTK_RESPONSE_ACCEPT,
                                                  NULL);
	GtkWidget *hbar = gtk_dialog_get_header_bar(GTK_DIALOG(dialog));
	gtk_header_bar_set_has_subtitle (hbar, TRUE);
	gtk_header_bar_set_subtitle (hbar, name);
	gtk_header_bar_set_title (hbar, _("Options"));
	gtk_header_bar_set_show_close_button (hbar, FALSE);
	GValue a = G_VALUE_INIT;
	g_value_init (&a, G_TYPE_INT);
	g_value_set_int (&a, GTK_PACK_START);
	GtkWidget *child = gtk_dialog_add_button (dialog, "_Cancel", GTK_RESPONSE_REJECT);
	gtk_container_child_set_property (GTK_CONTAINER(hbar), child, "pack-type", &a);
	gtk_widget_set_size_request (dialog, 400, 350);
	gtk_window_set_resizable(GTK_WINDOW (dialog), FALSE);
	gtk_dialog_set_default_response((GtkDialog *)dialog, GTK_RESPONSE_ACCEPT);
	return dialog;
}

void uiPam(Node*ne, GtkWidget*button)
{
	int i, j, need_undo;
	int length, static_length;
	UiDef *ud;
	GtkWidget**labels;
	GtkWidget**eLabels;
	GtkWidget*Caption;
	GtkWidget*Timeout;
	GtkWidget*eTimeout;
	GtkWidget*Lab;
	GtkWidget*eLab;
	GtkWidget*dialog;
	GtkWidget *table;
	GtkWidget *dialog_vbox3;
	GtkWidget *scrolledwindow2;
	GtkWidget *viewport2;
	GtkWidget *use_meta_label, *use_meta_toggle;
	char*pattern;
	char*sample;
	dialog = create_option_dialog (ne->pam->name);
	dialog_vbox3 = gtk_dialog_get_content_area (GTK_DIALOG(dialog));
	gtk_widget_show (dialog_vbox3);
	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy  ( ( GtkScrolledWindow *)scrolledwindow2,
                                             GTK_POLICY_AUTOMATIC,
                                             GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolledwindow2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), scrolledwindow2, TRUE, TRUE, 0);
	viewport2 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport2);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), viewport2);
	ud = getUiDef(ne);
	static_length = 2;
	length = static_length + 1;
	if ( ud ) {
		length += ud->length;
	}
	table = gtk_grid_new ();
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (viewport2), table);
	Caption = gtk_label_new(ne->label);//label
	gtk_widget_show (Caption);
	gtk_grid_attach (GTK_GRID (table), Caption, 0, 0, 2, 1);
	Timeout = gtk_label_new(_("Timeout"));
	gtk_widget_show (Timeout);
	gtk_grid_attach (GTK_GRID (table), Timeout, 0, 1, 1, 1);
	eTimeout = gtk_entry_new();
	sample = (char*)calloc(MAX_RECORD, sizeof(char));
	sprintf(sample, "%d", ne->timeDelay);
	gtk_entry_set_text( GTK_ENTRY (eTimeout), sample);
	free(sample);
	gtk_editable_set_editable(GTK_EDITABLE (eTimeout), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (eTimeout), TRUE);
	gtk_widget_show (eTimeout);
	gtk_grid_attach (GTK_GRID (table), eTimeout, 1, 1, 1, 1);
	if ( ud ) {
		labels = (GtkWidget**)calloc(ud->length, sizeof(GtkWidget*));
		eLabels = (GtkWidget**)calloc(ud->length, sizeof(GtkWidget*));
		pattern = (char*)calloc(MAX_RECORD, sizeof(char));
		for( i = 0; i < ud->length; i++ ) {
			sprintf(pattern, "$%d", i + 1);
			sample = outstrsubst(ne->id, ud->format, pattern);
			labels[i] = gtk_label_new(ud->opts[i].label);
			gtk_widget_show (labels[i]);
			gtk_grid_attach (GTK_GRID (table), labels[i], 0, static_length + i, 1, 1);
			if ( !strcmp(ud->opts[i].type, "bool") ) {
				eLabels[i] = gtk_check_button_new();
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
				if ( sample && !strcmp(sample, ud->opts[i].value[0]) ) {
					gtk_toggle_button_set_active((GtkToggleButton *)eLabels[i], TRUE);
				} else {
					gtk_toggle_button_set_active((GtkToggleButton *)eLabels[i], FALSE);
				}
			} else if  ( !strcmp(ud->opts[i].type, "choice") ) {
				eLabels[i] = gtk_combo_box_text_new();
				for ( j = 0; j < ud->opts[i].lval; j++ ) {
					gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT(eLabels[i]), ud->opts[i].value[j]);
					if ( sample && g_strcmp0 (sample, ud->opts[i].value[j]) == 0) {
						gtk_combo_box_set_active (GTK_COMBO_BOX(eLabels[i]) ,j);
					}
				}
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
			} else {
				eLabels[i] = gtk_entry_new();
				if ( !sample )
					gtk_entry_set_text( GTK_ENTRY (eLabels[i]), (const gchar *)ud->opts[i].value[0] );
				else
					gtk_entry_set_text( GTK_ENTRY (eLabels[i]), (const gchar *)sample );
				gtk_editable_set_editable(GTK_EDITABLE (eLabels[i]), TRUE );
				gtk_entry_set_activates_default(GTK_ENTRY (eLabels[i]), TRUE);
				gtk_widget_show (eLabels[i]);
				gtk_grid_attach (GTK_GRID (table), eLabels[i], 1, static_length + i, 1, 1);
			}
			if ( sample )
				free(sample);
		}
		free(pattern);
	}
	use_meta_label = gtk_label_new(_("Autoadjust"));
	gtk_widget_show (use_meta_label);
	gtk_grid_attach (GTK_GRID (table), use_meta_label, 0, length - 1, 1, 1);
	use_meta_toggle = gtk_check_button_new();
	gtk_widget_show (use_meta_toggle);
	gtk_grid_attach (GTK_GRID (table), use_meta_toggle, 1, length - 1, 1, 1);
	if ( ne->use_metaname == 1 ) {
		gtk_toggle_button_set_active((GtkToggleButton *)use_meta_toggle, TRUE);
	} else {
		gtk_toggle_button_set_active((GtkToggleButton *)use_meta_toggle, FALSE);
	}
	need_undo = 0;
	if ( gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT ) {
		g_mutex_lock(&(ne->statusMutex));
		if ( ne->status != waiting ) {
			need_undo = 1;
		}
		if ( gtk_toggle_button_get_active((GtkToggleButton *)use_meta_toggle) ) {
			ne->use_metaname = 1;
		} else {
			ne->use_metaname = 0;
		}
		ne->timeDelay = atoi((char*)gtk_entry_get_text(GTK_ENTRY ( eTimeout )));
		if ( ud ) {
			if ( ne->id )
				free( ne->id );
			ne->id = (char*)calloc(MAX_RECORD, sizeof(char));
			ne->id = strcpy(ne->id, ud->format);
			ne->id = (char*)realloc(ne->id, MAX_RECORD * sizeof(char));
			pattern = (char*)calloc(MAX_RECORD, sizeof(char));
			for( i = 0; i < ud->length; i++ ) {
				sprintf(pattern, "$%d", i + 1);
				if ( !strcmp(ud->opts[i].type, "bool") ) {
					if ( gtk_toggle_button_get_active((GtkToggleButton *)eLabels[i]) )
						sample = ud->opts[i].value[0];
					else
						sample = ud->opts[i].value[1];
				} else if  ( !strcmp(ud->opts[i].type, "choice") ) {
					sample = (char*)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(eLabels[i]));
				} else {
					sample = (char*)gtk_entry_get_text(GTK_ENTRY (eLabels[i]));
				}
				g_strstrip(sample);
				instrsubst(ne->id, pattern, sample);
			}
			g_mutex_unlock(&(ne->statusMutex));
			free(pattern);
			deleteUiDef(ud);
			free(eLabels);
			free(labels);
		} else {
			g_mutex_unlock(&(ne->statusMutex));
		}
		bambu_set_title(0);
	} else {
		if ( ud ) {
			deleteUiDef(ud);
			free(eLabels);
			free(labels);
		}
	}
	gtk_widget_destroy(dialog);
	if ( need_undo ) undoDownSrtream(ne);
}

gboolean uiNode(Node*ne, GtkWidget*button)
{
	if ( ne->type == pam && strcmp(ne->pam->name, "save") ) {
		uiPam(ne, button);
	} else if ( ne->type == ins ) {
		uiIns(ne, button);
	}
	return FALSE;
}

GtkWidget*create_window1 (Workspace*ws, char*name)
{
	GtkWidget *window1;
	GtkWidget *table1;
	GtkWidget *scrolledwindow1;
	GtkWidget *viewport1, *hs;
	GtkWidget *tButton;
	GtkWidget *label_1;
	GtkWidget *view;
	GtkTextBuffer *buffer;
	int etagh = 0;
/* */
	myWS = ws;
	filename = g_strdup(name);
	dirname = g_path_get_dirname (filename);
	basefilename = g_path_get_basename(filename);
	filename_flag = 1;
	window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (window1, 400, 600);
	headerbar1 = gtk_header_bar_new();
	gtk_header_bar_set_show_close_button (headerbar1, TRUE);
	gtk_window_set_titlebar (window1, headerbar1);
	gtk_widget_show (headerbar1);
	title = g_new0( char*, MAX_RECORD);
	subtitle = g_new0( char*, MAX_RECORD);
	bambu_set_title(1);
	g_signal_connect(window1, "delete_event", G_CALLBACK(mwdelete), NULL); /* dirty */
	table1 = gtk_grid_new ();
	gtk_widget_show (table1);
	gtk_container_add (GTK_CONTAINER (window1), table1);
/* save as */
	tButton = gtk_button_new_from_icon_name ("document-save-as-symbolic", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_save_as_event), NULL);
	gtk_header_bar_pack_start (headerbar1, tButton);
	gtk_widget_set_tooltip_text (GTK_WIDGET(tButton), _("Save as"));
/* save */
	tButton = gtk_button_new_from_icon_name ("document-save-symbolic", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (tButton);
	g_signal_connect(tButton, "clicked", G_CALLBACK(toolbar1_save_event), NULL);
	gtk_header_bar_pack_start (headerbar1, tButton);
	gtk_widget_set_tooltip_text (GTK_WIDGET(tButton), _("Save"));
/* cancel */
	stopButton = gtk_button_new_from_icon_name ("process-stop-symbolic", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (stopButton);
	g_signal_connect(stopButton, "clicked", G_CALLBACK(toolbar1_cancel_event), NULL);
	gtk_header_bar_pack_start (headerbar1, stopButton);
	gtk_widget_set_tooltip_text (GTK_WIDGET(stopButton), _("Stop"));
/* run */
	runButton = gtk_button_new_with_label (_("Run"));
	gtk_widget_show (runButton);
	g_signal_connect(runButton, "clicked", G_CALLBACK(toolbar1_run_event), NULL);
	gtk_header_bar_pack_end (headerbar1, runButton);
/* jobid */
	label_1 = gtk_label_new(_("Job ID"));
	gtk_widget_show (label_1);
	gtk_grid_attach (GTK_GRID (table1), label_1, 0, etagh, 1, 1);
	uid_entry = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY (uid_entry), "jobid" );
	gtk_editable_set_editable(GTK_EDITABLE (uid_entry), TRUE );
	gtk_entry_set_activates_default(GTK_ENTRY (uid_entry), TRUE);
	gtk_widget_show (uid_entry);
	gtk_grid_attach (GTK_GRID (table1), uid_entry, 1, etagh++, 1, 1);
/* outdir */
	label_1 = gtk_label_new(_("Output directory"));
	gtk_widget_show (label_1);
	gtk_grid_attach (GTK_GRID (table1), label_1, 0, etagh, 1, 1);
	filechooserbutton = gtk_file_chooser_button_new (_("Select output folder"), GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
	gtk_widget_show (filechooserbutton);
	gtk_grid_attach (GTK_GRID (table1), filechooserbutton, 1, etagh++, 1, 1);
/* options */
	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow1);
	gtk_grid_attach (GTK_GRID (table1), scrolledwindow1, 0, etagh++, 2, 1);
	gtk_scrolled_window_set_min_content_height(scrolledwindow1, 300);
	gtk_scrolled_window_set_min_content_width(scrolledwindow1, 500);
/* viewport for options */
	viewport1 = gtk_viewport_new (NULL, NULL);
	gtk_widget_show (viewport1);
	gtk_container_add (GTK_CONTAINER (scrolledwindow1), viewport1);
	optab = gtk_grid_new ();
	gtk_widget_show (optab);
	gtk_widget_set_hexpand (optab, TRUE);
	gtk_container_add (GTK_CONTAINER (viewport1), optab);
/* Comments */
	hs = gtk_hseparator_new ();
	gtk_widget_show (hs);
	gtk_grid_attach (GTK_GRID (table1), hs, 0, etagh++, 2, 1);
	label_1 = gtk_label_new(_("Comments"));
	gtk_widget_show (label_1);
	gtk_grid_attach (GTK_GRID (table1), label_1, 0, etagh++, 2, 1);
	if (myWS->comment != NULL) {
		view = gtk_text_view_new ();
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
		gtk_text_buffer_set_text (buffer, myWS->comment, -1);
		gtk_widget_show (view);
		gtk_grid_attach (GTK_GRID (table1), view, 0, etagh++, 2, 1);
		gtk_widget_set_hexpand (view, TRUE);
		gtk_widget_set_vexpand (view, TRUE);
		gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(view), GTK_WRAP_WORD);
		gtk_text_view_set_editable (GTK_TEXT_VIEW(view), FALSE);
	}
/* Comments */
	hs = gtk_hseparator_new ();
	gtk_widget_show (hs);
	gtk_grid_attach (GTK_GRID (table1), hs, 0, etagh++, 2, 1);
/* progres bar */
	pbar = gtk_progress_bar_new ();
	gtk_widget_show (pbar);
	gtk_grid_attach (GTK_GRID (table1), pbar, 0, etagh++, 2, 1);
	gtk_progress_bar_set_show_text (GTK_PROGRESS_BAR(pbar), TRUE);
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR(pbar), NULL);
	return window1;
}

void make_optab()
{
	ListOfNodes *curr;
	Node*ne;
	int i;
	GtkWidget*label, *button, *hs;
	gtk_grid_set_row_spacing (GTK_GRID (optab), 2);
	bambu_set_title(1);
	i = 0;
	hs = gtk_hseparator_new ();
	gtk_widget_show (hs);
	gtk_grid_attach (GTK_GRID (optab), hs, 0, i, 2, 1);
	i++;
	label = gtk_label_new(_("Input files"));
	gtk_widget_show (label);
	gtk_grid_attach (GTK_GRID (optab), label, 0, i, 2, 1);
	i++;
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		ne = curr->node;
		if ( ne->type == ins ) {
			ne->buf = g_path_get_basename((const gchar*)ne->file);
		} else if ( ne->type == ous ) {
			ne->buf = g_path_get_basename((const gchar*)ne->file);
		}
		if ( ne->type == ins ) {
			label = gtk_label_new(ne->label);
			gtk_widget_show (label);
			gtk_grid_attach (GTK_GRID (optab), label, 0, i, 1, 1);
			gtk_widget_set_hexpand (label, TRUE);
			button = gtk_button_new_with_label(ne->buf);
			gtk_widget_show (button);
			gtk_grid_attach (GTK_GRID (optab), button, 1, i, 1, 1);
			g_signal_connect_swapped(button, "clicked", G_CALLBACK(uiNode), ne);
			i++;
		}
	}
	hs = gtk_hseparator_new ();
	gtk_widget_show (hs);
	gtk_grid_attach (GTK_GRID (optab), hs, 0, i, 2, 1);
	i++;
	label = gtk_label_new(_("Algorithmic parameters"));
	gtk_widget_show (label);
	gtk_grid_attach (GTK_GRID (optab), label, 0, i, 2, 1);
	i++;
	for (curr = myWS->listOfNodes; curr; curr = curr->next) {
		ne = curr->node;
		if ( ne->vip == 1 ) {
			label = gtk_label_new(ne->label);
			gtk_widget_show (label);
			gtk_grid_attach (GTK_GRID (optab), label, 0, i, 1, 1);
			gtk_widget_set_hexpand (label, TRUE);
			button = gtk_button_new_with_label(_("Options"));
			gtk_widget_show (button);
			gtk_grid_attach (GTK_GRID (optab), button, 1, i, 1, 1);
			g_signal_connect_swapped(button, "clicked", G_CALLBACK(uiNode), ne);
			i++;
		}
	}
}

void bambu_set_title(int saved)
{
/* Base Name */
	if (basefilename != NULL) {
		if ( saved == 1 ) {
			modified = 0;
			if (myWS->name)
				sprintf(title, "%s (%s)", myWS->name, basefilename);
			else
				sprintf(title, "%s (%s)", _("Untitled"), basefilename);
		} else {
			modified = 1;
			if (myWS->name)
				sprintf(title, "*%s (%s)", myWS->name, _("Unsaved"));
			else
				sprintf(title, "*%s (%s)", _("Untitled"), _("Unsaved"));
		}
	} else {
		if ( saved == 1 ) {
			modified = 0;
			if (myWS->name)
				sprintf(title, "%s (%s)", myWS->name, _("Unsaved"));
			else
				sprintf(title, "%s (%s)", _("Untitled"), _("Unsaved"));
		} else {
			modified = 1;
			if (myWS->name)
				sprintf(title, "*%s (%s)", myWS->name, _("Unsaved"));
			else
				sprintf(title, "*%s (%s)", _("Untitled"), _("Unsaved"));
		}
	}
/* Dir Name */
	if (dirname != NULL) {
		sprintf(subtitle, "%s", dirname);
	} else {
		sprintf(subtitle, "%s", _("Unsaved"));
	}
	gtk_header_bar_set_title (headerbar1, title);
	gtk_header_bar_set_subtitle (headerbar1, subtitle);
}

void fileSelectSave()
{
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new (_("Save Workflow"),
				(GtkWindow*)mw,
				GTK_FILE_CHOOSER_ACTION_SAVE,
				"_Cancel", GTK_RESPONSE_CANCEL,
				"_Save", GTK_RESPONSE_ACCEPT,
				NULL);
	gtk_file_chooser_set_local_only ((GtkFileChooser *)dialog, FALSE);
	if ( filename_flag == 1 && filename && strlen(filename) > 0 ) {
		char*scheme = g_uri_parse_scheme(filename);
		if ( !scheme ) {
			filename = g_strdup_printf("file://%s", filename);
		} else {
			g_free(scheme);
		}
		gtk_file_chooser_set_uri (GTK_FILE_CHOOSER (dialog), filename);
	}
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *newfilename;
		newfilename = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (dialog));
		if ( filename ) g_free(filename);
		if ( dirname ) g_free(dirname);
		if ( basefilename ) g_free(basefilename);
		filename = g_strdup(newfilename);
		dirname = g_path_get_dirname (filename);
		basefilename = g_path_get_basename(filename);
		filename_flag = 1;
		workspace_to_file(myWS, filename);
		bambu_set_title(1);
		g_free (newfilename);
	}
	gtk_widget_destroy (dialog);
}

gboolean mwdelete( GtkWidget *widget, gpointer data )
{
	GtkWidget *dialog;
	gint result;
	if (modified == 1) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
			(GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_YES_NO,
			_("Save workflow?"));
		gtk_dialog_add_button(GTK_DIALOG (dialog), "_Cancel", GTK_RESPONSE_CANCEL);
		result = gtk_dialog_run (GTK_DIALOG (dialog));
		if (result == GTK_RESPONSE_YES) {
			fileSelectSave();
		} else if ( result == GTK_RESPONSE_CANCEL ) {
			gtk_widget_destroy (dialog);
			return TRUE;
		}
	}
	gtk_main_quit();
	if ( myWS ) {
		workspaceRmTemp(myWS);
	}
	return FALSE;
}

static void close_bambu_signal_handler (GObject *object, GParamSpec *pspec, gpointer user_data);

int init_gdbus_connection()
{
	GError *gerror = NULL;
	pro_stack_interface_proxy = pro_stack_interface_proxy_new_for_bus_sync (
	           G_BUS_TYPE_SESSION,
	           G_DBUS_PROXY_FLAGS_NONE,
				"ru.spbstu.sysbio.ProStack", /* name*/
				"/ru/spbstu/sysbio/ProStack", /* object_path */
				NULL,
				&gerror);
	if ( !pro_stack_interface_proxy ) {
		if ( gerror ) {
			g_warning("Unable to connect to dbus: %s", gerror->message);
			g_error_free(gerror);
		}
		return 0;
	}
	g_signal_connect (pro_stack_interface_proxy, "close-bambu-signal", G_CALLBACK (close_bambu_signal_handler), NULL);
	return 1;
}

static void close_bambu_signal_handler (GObject *object, GParamSpec *pspec, gpointer user_data)
{
	g_print("Received signal and it says\n");
	g_idle_add (mwdelete, NULL);
}

gboolean trace_node(gpointer data)
{
/* update progress here */
	double frac;
	frac = gtk_progress_bar_get_fraction (GTK_PROGRESS_BAR(pbar));
	frac = frac + 1.0 / (myWS->numberOfNodes);
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR(pbar), frac);
	g_print("frac = %f, incr = %f\n", frac, 1.0 / (myWS->numberOfNodes));
	return FALSE;
}

void trace_progress(Node*ne)
{
	NodeStatus statusOfNode;
	g_mutex_lock (&(ne->statusMutex));
	statusOfNode = ne->status;
	g_mutex_unlock (&(ne->statusMutex));
	if ( statusOfNode == completed ) {
		g_idle_add (trace_node, NULL);
	}
}

gboolean bambu_error(gpointer data)
{
	char*msg = (char*)data;
	GtkWidget*dialog;
	int we_are_stopping;
	dialog = gtk_message_dialog_new (GTK_WINDOW (mw),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_WARNING,
		GTK_BUTTONS_CLOSE,
		msg,
		NULL);
 /* Destroy the dialog when the user responds to it (e.g. clicks a button) */
	g_signal_connect_swapped (dialog, "response",
		G_CALLBACK (gtk_widget_destroy),
		dialog);
	gtk_widget_show (dialog);
	return FALSE;
}

void notify_error(char*msg)
{
	char*dmsg = g_strdup(msg);
	g_idle_add (bambu_error, dmsg);
}

static GOptionEntry entries[] =
{
	{ "project-name", 'p', 0, G_OPTION_ARG_STRING, &default_name, N_("Project"), N_("Project") },
	{ "nodbus", 'n', 0, G_OPTION_ARG_NONE, &nodbus, "Do not connect to DBus", NULL },
	{ NULL }
};

int main (int argc, char *argv[])
{
	char *wsfilename = (char*)NULL;
	char *scheme = (char*)NULL;
	Workspace*ws;
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	setlocale (LC_ALL, "");
	g_set_application_name (_("PKWorker"));
	g_message(g_get_application_name ());
	gtk_init (&argc, &argv);
	setError(g_print);
	setErrorNode(g_print);
	setErrorOlapServer(g_print);
	setErrorLon(g_print);
	setTracer(trace_progress);
	setNotifyNode(notifyNode);
	if ( isSetLogger ) {
		setLoggerNode(g_print);
		setLoggerOlapServer(g_print);
	}
	kimono_init(&argc, &argv, FALSE);
	GError *error = NULL;
	GOptionContext *context;
	context = g_option_context_new (" - execute scenario");
	g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
	g_option_context_add_group (context, gtk_get_option_group (TRUE));
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_error ("option parsing failed: %s\n", error->message);
	}
	if (!nodbus && !init_gdbus_connection()) {
		g_message("prostack-execute: no prostack");
	}
	gtk_window_set_default_icon_from_file (subst_data_dir("/icons/gnome/48x48/mimetypes/gnome-mime-application-x-prostack.png"), NULL);
	if (default_name != NULL) {
		char*gcd = g_build_filename(subst_data_dir(KIMONO_EXAMPLES_DIR), default_name, NULL);
		g_chdir (gcd);
		char*str = g_strdup_printf("%s.ap", default_name);
		wsfilename = g_build_filename(gcd, str, NULL);
		g_free(gcd);
		g_free(str);
	} else {
		if ( argc < 2 ) {
			g_error("prostack-execute: no input file");
		}
		scheme = g_uri_parse_scheme((const char*)argv[argc - 1]);
		if ( !scheme && !g_path_is_absolute((const char*)argv[argc - 1]) ) {
			char*gcd = g_get_current_dir();
			wsfilename = g_build_filename(gcd, argv[argc - 1], NULL);
			g_free(gcd);
		} else {
			wsfilename = g_strdup(argv[argc - 1]);
		}
	}
	g_message(wsfilename);
	ws = workspace_from_file((const char*)wsfilename);
	if ( !ws ) {
		g_error("prostack-execute: failed to load workspace from no input file %s", wsfilename);
	}
	mw = create_window1 (ws, wsfilename);
	gtk_widget_show (mw);
	setError(notify_error);
	setErrorNode(notify_error);
	setErrorOlapServer(notify_error);
	setErrorLon(notify_error);
	make_optab();
	gtk_main ();
	kimono_shutdown();
	return 0;
}
