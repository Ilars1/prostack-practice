This operator expands the dimensions of the input image. 
It adds the pixels to the each side of the image. The number 
of pixels is defined in the second input that can be generated 
by the crop operator.


INPUT
1 image:.tif
2 text:.txt
OUTPUT
1 image:.tif
