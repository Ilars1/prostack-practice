This operator prints the values of pixels along the line to the text file. 
The line is defined as two parametric functions: 
$r  = a_r * t + b_r$ for row and 
$c = a_c * t + b_c$ for column, 
where parameter $t$ spans the interval $[t_0, t_N]$ . 
The second output shows the image with the line superimposed on it.


INPUT
1 image:.tif
OUTPUT
1 text:.txt
2 image:.tif
PARAMETERS
ac - slope of parametric function for column,
bc - offset of parametric function for column,
ar - slope of parametric function for row,
br - offset of parametric function for row,
t0 - lower limit for parameter,
tN - upper limit for parameter.
