This operator filters the input table of numerical values. The table can consist of arbitrary 
number of rows greater than one. The number of columns should be greater than two. The output consists of two columns. 
Parameters  X and F define the numbers of columns in input table that are printed in the output.
The rows are selected by the following rule: $Y min < Y < Y max$, where Y represents the value 
of the current row in the third column of the input table. The rows of the output are sorted 
with respect to the first column. Consequently, the output can be directly fed to the gnuplot.


INPUT
1 text:.txt
OUTPUT
1 text:.txt
PARAMETERS
X -- number of the column for the argument,
F -- number of the column for the function value,
Y min -- lower bound for y,
Y max -- upper bound for y.
