This operator computes a two dimensional median filter of a structural element over the given image. The structural element comes from the second input.


INPUT
1 image:.tif
2 text:.txt
OUTPUT
1 image:.tif
PARAMETERS
Repetitions- number of sweeps

