NAME

hues - Combine color channels into one image.

DESCRIPTION

Produce the grayscale image of Hue times Saturation using Red, Green and Blue
color channels.

INPUT
1 image:.tif
2 image:.tif
3 image:.tif

OUTPUT
1 image:.tif
