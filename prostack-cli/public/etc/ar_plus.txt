This operator sums up the image dimensions that are
given by a string of four numbers which are
distances measured in pixels from the image center to its borders.
The dimensions can be calculated by the halfsizes
operator.


INPUT
1 text:.txt
2 text:.txt
OUTPUT
1 text:.txt
