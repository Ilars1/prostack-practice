This operator rotates image to a given angle (second input) that can be calculated by the rotate operator.


INPUT
1 image:.tif
2 text:.txt
OUTPUT
1 image:.tif
