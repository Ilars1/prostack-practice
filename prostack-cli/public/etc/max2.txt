This operator computes pixel by pixel maximum of two input images.


INPUT
1 image:.tif
2 image:.tif
3 image:.tif
OUTPUT
1 image:.tif
