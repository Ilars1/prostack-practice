include VERSION

SUBDIRS=prostak quastack glaz door prostack iapee cmove prutik mod_prostack examples rascon frameproc

SPECS=specs/prostack.spec nsis/ProStack.nsis nsis/mingwbuild

HAVE_AUTOTOOLS=prostak quastack glaz door prostack iapee cmove prutik mod_prostack examples rascon frameproc

HAVE_CONFIGURE=$(HAVE_AUTOTOOLS)

PRJ_PREFIX=prostack

RPMBUILD ?= $(PWD)/rpmbuild
TARGET ?= master

# After updating the version in VERSION you should run the version-update
# target.

ifeq ($(ProStack_VERSION_IS_GIT_SNAPSHOT),"yes")
GIT_VERSION=$(shell git show --pretty=format:"%h" --stat HEAD 2>/dev/null|head -1)
ifneq ($(GIT_VERSION),)
ProStack_VERSION=$(ProStack_VERSION_MAJOR).$(ProStack_VERSION_MINOR).$(ProStack_VERSION_RELEASE)GIT$(GIT_VERSION)
endif # in a git tree and git returned a version
endif # git

ifndef ProStack_VERSION
ifdef ProStack_VERSION_PRE_RELEASE
ProStack_VERSION=$(ProStack_VERSION_MAJOR).$(ProStack_VERSION_MINOR).$(ProStack_VERSION_RELEASE).pre$(ProStack_VERSION_PRE_RELEASE)
else
ifdef ProStack_VERSION_RC_RELEASE
ProStack_VERSION=$(ProStack_VERSION_MAJOR).$(ProStack_VERSION_MINOR).$(ProStack_VERSION_RELEASE).rc$(ProStack_VERSION_RC_RELEASE)
else
ProStack_VERSION=$(ProStack_VERSION_MAJOR).$(ProStack_VERSION_MINOR).$(ProStack_VERSION_RELEASE)
endif # rc
endif # pre
endif # ipa_version

PROSTACK_TARBALL_PREFIX=$(PRJ_PREFIX)-$(ProStack_VERSION)
PROSTACK_TARBALL=$(PROSTACK_TARBALL_PREFIX).tgz

ProStack_RPM_RELEASE=$(shell cat RELEASE)

LIBDIR ?= /usr/lib

all: bootstrap-autogen
	@for subdir in $(SUBDIRS); do \
		(cd $$subdir && $(MAKE) $@) || exit 1; \
	done

bootstrap-autogen: version-update
	@echo "Building ProStack $(ProStack_VERSION)"
	@for subdir in $(HAVE_AUTOTOOLS); do \
		echo "$$subdir"; \
		(cd $$subdir && if [ ! -e Makefile ]; then autoreconf -fi; fi) || exit 1; \
	done
	@for subdir in $(HAVE_CONFIGURE); do \
		echo "$$subdir"; \
		(cd $$subdir && if [ ! -e Makefile ]; then ./configure --prefix=$$HOME; fi) || exit 1; \
	done

autogen: version-update
	@echo "Building ProStack $(ProStack_VERSION)"
	@for subdir in $(HAVE_AUTOTOOLS); do \
		echo "$$subdir"; \
		(cd $$subdir && if [ ! -e Makefile ]; then autoreconf -fi;intltoolize --force; fi) || exit 1; \
	done
	@for subdir in $(HAVE_CONFIGURE); do \
		echo "$$subdir"; \
		(cd $$subdir && if [ ! -e Makefile ]; then ./configure --prefix=/usr; fi) || exit 1; \
	done

install: all
	@for subdir in $(SUBDIRS); do \
		(cd $$subdir && $(MAKE) $@) || exit 1; \
	done

test:
	@for subdir in $(SUBDIRS); do \
		(cd $$subdir && $(MAKE) $@) || exit 1; \
	done

release-update:
	if [ ! -e RELEASE ]; then echo 0 > RELEASE; fi

version-update: release-update
	@for f in $(SPECS); do \
		sed -e s/__VERSION__/$(ProStack_VERSION)/ -e s/__RELEASE__/$(ProStack_RPM_RELEASE)/ $$f.in > $$f; \
	done
	cp specs/prostack.spec .
	@for subdir in $(HAVE_AUTOTOOLS); do \
		(cd $$subdir && if [ -e version.m4.in ]; then sed -e s/__VERSION__/$(ProStack_VERSION)/ version.m4.in > version.m4; fi) || exit 1; \
	done
	@for subdir in $(HAVE_CONFIGURE); do \
		(cd $$subdir && if [ -e Makefile.in.in ]; then sed -e s/__VERSION__/$(ProStack_VERSION)/ Makefile.in.in > Makefile.in; fi) || exit 1; \
	done

archive:
	-mkdir -p dist
	git archive --format=tar --prefix=prostack/ $(TARGET) | (cd dist && tar xf -)

local-archive:
	-mkdir -p dist/prostack
	@for subdir in $(SUBDIRS); do \
		cp -pr $$subdir dist/prostack/.; \
	done

archive-cleanup:
	rm -fr dist/prostack

tarballs:
	-mkdir -p dist/sources

        # all
	mkdir -p dist/$(PROSTACK_TARBALL_PREFIX)
	@for subdir in $(SUBDIRS); do \
		cp -pr $$subdir dist/$(PROSTACK_TARBALL_PREFIX); \
	done
	rm -f dist/sources/$(PROSTACK_TARBALL)
	@for subdir in $(SUBDIRS); do \
		(cd dist/$(PROSTACK_TARBALL_PREFIX)/$$subdir && if [ -e Makefile ]; then make distclean; fi && rm -fr html html_sect *.pdf *.log *.lof *.aux *.dvi *.out *.ps *.toc); \
	done
	cd dist; tar cfz sources/$(PROSTACK_TARBALL) $(PROSTACK_TARBALL_PREFIX)
	rm -fr dist/$(PROSTACK_TARBALL_PREFIX)

rpmroot:
	mkdir -p $(RPMBUILD)/BUILD
	mkdir -p $(RPMBUILD)/RPMS
	mkdir -p $(RPMBUILD)/SOURCES
	mkdir -p $(RPMBUILD)/SPECS
	mkdir -p $(RPMBUILD)/SRPMS

rpmdistdir:
	mkdir -p dist/rpms
	mkdir -p dist/srpms

rpm-prostack:
	cp dist/sources/$(PROSTACK_TARBALL) $(RPMBUILD)/SOURCES/.
	rpmbuild --define "_topdir $(RPMBUILD)" -ba specs/prostack.spec
	cp rpmbuild/RPMS/*/$(PRJ_PREFIX)-$(ProStack_VERSION)-*.rpm dist/rpms/
	cp rpmbuild/SRPMS/$(PRJ_PREFIX)-$(ProStack_VERSION)-*.src.rpm dist/srpms/

rpms: rpmroot rpmdistdir rpm-prostack

win32:
	mkdir -p mingw32-build/build
	cp nsis/ProStack.nsis mingw32-build
	cp nsis/mingwbuild mingw32-build
	cp nsis/EnvVarUpdate.nsh mingw32-build
	cp nsis/exetype.pl mingw32-build
	cp dist/sources/$(PROSTACK_TARBALL) mingw32-build
	cd mingw32-build; ./mingwbuild `pwd`/$(PROSTACK_TARBALL)

repodata:
	-createrepo -p dist

dist: version-update archive tarballs archive-cleanup rpms repodata

local-dist: autogen clean local-archive tarballs archive-cleanup rpms

clean: version-update
	@for subdir in $(SUBDIRS); do \
		(cd $$subdir && $(MAKE) $@) || exit 1; \
	done
	rm -f *~

distclean: version-update
	@for subdir in $(SUBDIRS); do \
		(cd $$subdir && $(MAKE) $@) || exit 1; \
	done
	rm -fr rpmbuild dist

maintainer-clean: clean
	rm -fr rpmbuild dist

sources: version-update tarballs
	cp dist/sources/$(PROSTACK_TARBALL) .
	cp specs/prostack.spec .
