/***************************************************************************
 *            Workspace.h
 *
 *  Thu Feb  9 16:03:22 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _WORKSPACE_H
#define _WORKSPACE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ListOfNodes.h>


typedef enum WorkspaceStatus {
	WS_WAITING,
	WS_RUNNING,
	WS_FAILED,
	WS_COMPLETED,
	WS_TERMINATED
} WorkspaceStatus;


typedef struct Workspace {
	char *author;
	char *name;
	char *comment;
	char *id;
	int ID;
	int cur_ID;
	int numberOfNodes;
	int cur_macro_ID;
	int numberOfMacro;
	ListOfNodes *listOfNodes;
	GMutex stopMutex;
	int clean;
	GMutex statusMutex;
	WorkspaceStatus status;
	GMutex integrMutex;
	char*comp_inputs;
	char*comp_outputs;
	char*comp_ostring;
	char*comp_uidescr;
} Workspace;

void workspaceDelete(Workspace*ws);
void workspaceRun(Workspace*ws);
void *threadRunNode(void *arg);
Workspace*workspaceCreate();
void setError(void (*ptr)(char*msg));
void setTracer(void (*ptr)(Node*ne));
void notifyNode(Node*ne, NodeStatus status);
void traceText(Node*ne);
void workspaceRmTemp(Workspace*ws);
void workspaceAddNode(Workspace*ws, Node*ne);
void deleteNodeFromWS(Workspace*ws, Node*node);
Workspace*workspaceEmpty();
void workspaceCompile2Script(Workspace*ws, char*name, int*nodeIDs, int nNodeIDs, char *path);
void writeWSScript(FILE*fp, int*nodeIDs, int nNodeIDs, char*name, UiDef**uiDefs);
void writeWSScript1(FILE*fp, char*name);
char*mFormat(char*format);
void writeWSin(FILE*fp, Workspace*ws);
void writeWSout(FILE*fp, Workspace*ws);
void writeWSui2(FILE*fp, int*nodeIDs, int nNodeIDs, char*name, UiDef**uiDefs, ListOfNodes*lon);
void workspaceRestoreNode(Workspace*ws, Node*ne);
void listOfNodesPropagateStatus(ListOfNodes *lon, Node*ne, NodeStatus status);
void *threadPropNode(void *arg);
void clean_workspace_threads(void*arg);
void clean_node_thread(void *arg);
void ws_set_server_root(char*str);
void workspaceAddMacro(Workspace*ws, Node*macro_node);
void workspaceDeleteMacro(Workspace*ws, Node*macro_node);
void workspaceRebuildMacro(Workspace*ws, Node*macro_node);
void node_macro_set_connections(ListOfNodes*lon, Node*macro_node);
int node_macro_inclcudes_node(ListOfNodes*lon, Node*node, Node*macro_node);
void mark_selected_as_macro(ListOfNodes*lon, int macro_number);
void node_macro_set_ports_string(ListOfNodes*lon, Node*macro_node);
Node* ws_get_ins_node(Workspace*ws);
void ws_set_ins_ous(Workspace*ws, char *inses, char*mountpoint, char*server, int portnum);
void ws_set_host_port(Workspace*ws, char*server, int portnum);
int ws_get_num_failed_nodes(Workspace*ws);
void workspacePaste(Workspace*wksp, Workspace*addon, int x, int y);
Workspace*workspaceParse(GKeyFile*gkf);
void workspacePrint(Workspace*wksp, GKeyFile*gkf);
void workspaceDeleteStruct(Workspace*ws);
void workspacePrintNNodes(Workspace*wksp, GKeyFile*gkf, int n);
int workspace_to_file(Workspace*ws, char*fn);
Workspace*workspace_from_file(const char*filename);
Workspace*workspace_from_string(gchar*contents, int length, GError**gerror);
void workspaceCleanOutputs(Workspace*ws);
void workspace_set_status(Workspace*ws, WorkspaceStatus status);
WorkspaceStatus workspace_get_status(Workspace*ws);
void setBBNode(Node*node);
void getBbWorkspace(Workspace*ws, int *wsWidth, int *wsHeight, int Pegas);
void workspaceTrimCoordinates(Workspace*ws);
void ws_set_thread_pool_size(gint size);
void traceDb(Node*ne);
Node*ws_get_selected_node_by_xy(Workspace*ws, int x, int y, int *inPort, int *outPort, int *Conn, int MACRO_NUMBER);
void ws_clear_node_selections(Workspace*ws, int *inPort, int *outPort, int *Conn, int MACRO_NUMBER);
Node*ws_find_one_selected(Workspace*ws);
int workspace_set_inputs(Workspace*ws, char*wsinputs, char*inputs);
int workspace_set_outputs(Workspace*ws, char*wsoutputs, char*outputs);

WorkspaceStatus workspace_run_from_lib(char*mname, char*user, char*inputs, char*outputs, char*wksplibtag);
char*workspace_to_string(Workspace*ws, int*length);
int workspace_to_wksplib(Workspace*ws, Node*sys_node, char*inputs, char*outputs, char*wksplibtag, char*spath, char*message);
void workspaceNormalizeIDs(Workspace*wksp);
void workspaceCorrectMacroOnPaste(Workspace*wksp, int MACRO_NUMBER);

void ws_set_ins_realtive(Workspace*ws, gchar*type_relative_ins, int relative_ins_paths);
int workspace_get_compilation_infos(GKeyFile*gkf, gchar**uidescr, gchar**inputs, gchar**outputs, gchar**ostring, GError**gerror);
int workspace_get_compilation_infos_from_file(const char*str, gchar**uidescr, gchar**inputs, gchar**outputs, gchar**ostring, GError**gerror);
void workspace_to_module(Workspace*ws);
void ws_set_file_save(Workspace*ws, char**input_pointers, char **output_pointers, int n_input_pointers, int n_output_pointers);
int workspace_to_methods(gchar*Name, gchar*metaname, int metaweight, gchar*Executable, int type, gchar*uidescr, gchar*message, gchar*inputs, gchar*outputs, gchar*ostring, char*spath, char*str, OlapServer*os, GError**gerror);
int workspace_get_compilation_infos_from_file(const char*str, gchar**uidescr, gchar**inputs, gchar**outputs, gchar**ostring, GError**gerror);

void ws_set_ins_ous_jobid(Workspace*ws, char*mountpoint, char *jobid);

#ifdef __cplusplus
}
#endif

#endif /* _WORKSPACE_H */
