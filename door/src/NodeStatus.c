/***************************************************************************
 *            NodeStatus.c
 *
 *  Mon Feb 27 08:53:47 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <NodeStatus.h>

static int npool = 0;
static NodeStatusType **pool = (NodeStatusType**)NULL;

void initNodeStatus(NodeStatusType*ns)
{
/*	pthread_mutex_lock(&poolMutex);
	pool = (NodeStatusType**)realloc(pool, ( npool + 1 ) * sizeof(NodeStatusType*));
	ns->ID = npool;
	ns->status = waiting;
	pool[npool] = ns;
	npool++;
	pthread_mutex_unlock(&poolMutex);*/
}

void setNodeStatus(NodeStatusType*ns, NodeStatus st)
{
/*	pthread_mutex_lock(&poolMutex);
	ns->status = st;
	pool[ns->ID]->status = st;
	pthread_mutex_unlock(&poolMutex);*/
}

NodeStatus getNodeStatus(NodeStatusType*ns)
{
	NodeStatus st;
/*	pthread_mutex_lock(&poolMutex);
	st = pool[ns->ID]->status;
	pthread_mutex_unlock(&poolMutex);*/
	return st;
}

