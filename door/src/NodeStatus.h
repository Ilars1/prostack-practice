/***************************************************************************
 *            NodeStatus.h
 *
 *  Mon Feb 27 08:54:05 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _NODESTATUS_H
#define _NODESTATUS_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum NodeStatus {
	waiting,
	running,
	failed,
	completed,
	terminated
} NodeStatus;

typedef struct NodeStatusType {
	int ID;
	NodeStatus status;
} NodeStatusType;

void initNodeStatus(NodeStatusType*ns);
void setNodeStatus(NodeStatusType*ns, NodeStatus st);
NodeStatus getNodeStatus(NodeStatusType*ns);
/*void deleteNodeStatus(NodeStatusType*ns);*/

#ifdef __cplusplus
}
#endif

#endif /* _NODESTATUS_H */
