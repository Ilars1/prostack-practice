/***************************************************************************
 *            Node.h
 *
 *  Tue Feb 14 09:49:04 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _NODE_H
#define _NODE_H

#ifdef __cplusplus
extern "C"
{
#endif
#ifdef G_OS_WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <libsoup/soup.h>
#include <Port.h>
#include <Connection.h>
#include <OlapPAM.h>
#include <OlapServer.h>
#include <VInfo.h>

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

#ifndef MAXTOKENS
#define MAXTOKENS 256
#endif

typedef enum NodeStatus {
	waiting,
	running,
	failed,
	completed,
	terminated
} NodeStatus;

typedef enum NodeType {
	pam,
	file,
	ins,
	ous,
	macro,
	sys,
	display
} NodeType;

typedef enum NodeMode {
	dialog,
	echo,
	silent
} NodeMode;

typedef struct Node {
	VInfo *info;
	NodeStatus status;
	NodeMode mode;
	NodeType type;
	int ID;
	short terminal;
	int use_metaname;
	char *id;
	char *file;
	char *label;
	char *uid;
	char *buf;
	OlapServer *oServer;
	int timeDelay;
	OlapPAM *pam;
	int nInConns;
	Port *inConn;
	int nOutConns;
	Port *outConn;
	GMutex statusMutex;
	GMutex condMutex;
	GCond statusCond;
	GMutex *parent_mutex;
	int *parent_signal;
	char*parent_name;
	char*version;
	GPid pid;
	double microseconds;
	int vip;
} Node;

short isReady(Node *ne, int *flag_wait);
void runNodePam(Node *ne);
void runNodeFile(Node *ne);
Node *newNode();
void deleteNode(Node*ne);
char *requestNode(Node*ne);
void respondNode(Node*ne, char *respond);
void setNodePorts(Node*op);
char *requestFILE(char*path, char*file);
void runNodeDisplay(Node *ne);
void setErrorNode(void (*ptr)(char*msg));
void setLoggerNode(void (*ptr)(char*msg));
void setNotifyNode(void (*ptr)(Node*ne, NodeStatus status));
char *requestEXEC(Node*ne, char*cmd);
void nodeCleanOutPut(Node*ne);
Node *dupNode(Node *node);
void nodeSave(Node*ne, FILE*f);
void nodeSaveOutputConnections(Node*ne, FILE*f);


typedef struct OptionString {
	char*label;
	char*type;
	char*op;
	char**value;
	int lval;
} OptionString;

typedef struct UiDef {
	int length;
	OptionString*opts;
	char*format;
} UiDef;

UiDef*getUiDef(Node*ne);
UiDef*parseUiDef(char*respond);
void deleteUiDef(UiDef*ud);
char*str2ud(char *respond, int start, int end, int*len);
char*str2ud2(char *respond, int start, int end, int*len);
void instrsubst(char*string, char*pattern, char*sample);
void setOuter(void (*ptr)(Node*ne));
char*outstrsubst(char*string, char*format, char*pattern);

void nodeCheckTerminal(Node*ne);

void node_macro_set_in_ports(Node*n);
void node_macro_set_out_ports(Node*n);

void node_macro_get_inport(Node*node, int port, int*nodeID, int*portID);
void node_macro_get_outport(Node*node, int port, int*nodeID, int*portID);

int node_macro_get_outport_number(Node*node, int nodeID, int portID);
char *requestSYS(char*sysname, char*sysparms);

void nodePrint(Node*ne, GKeyFile*gkf);
Node*nodeParse(GKeyFile*gkf, char*grp);
void outPortPrint(Node*ne, int nport, int nconn, GKeyFile*gkf);
Connection**outPortParse(GKeyFile*gkf, char*key, int *n);
void disp(Node*ne);
int copy_big_files(char*from, char*to);

int door_call(char*method, Node*node);
int door_get(Node*node);
int door_put(Node*node);
int door_update(Node*node);
int door_delete(Node*node);
void nodeRmDisp(Node*node);
void gnc_gpid_kill(GPid pid);
void gsrc_pam_func(GPid pid, gint status, gpointer data);
int door_get_pam(Node*node);
int door_get_ins(Node*node);
int door_get_ous(Node*node);
int door_get_file(Node*node);
int door_get_display(Node*node);
int door_get_sys(Node*node);
int door_put_sys(Node*node);
int door_delete_pam(Node*node);
int door_delete_ins(Node*node);
int door_delete_display(Node*node);
int door_delete_sys(Node*node);
char*strdup_replace(char*str, char*needle, char*replacement);
Node*proxy_sys_node(char*mode);
void proxy_sys_node_del(Node*sproxy);
void proxy_sys_node_set_mode(Node*sproxy, char*mode);
int rest_get_ous(Node*node);
int rest_get_ins(Node*node);
int rest_get_sys(Node*node);
int rest_get_pam(Node*node);
int rest_get(Node*node);
int rests_get(Node*node);
SoupURI*soup_uri_from_os(OlapServer*os);
char*node_status_to_string(NodeStatus status);
int rest_delete(Node*node);
int rest_put(Node*node);
int rest_update(Node*node);
int rest_get_file(Node*node);
int rest_put_sys(Node*node);
int rest_delete_sys(Node*node);
int rest_delete_pam(Node*node);

void soup_callback_auth(SoupSession *session, SoupMessage *msg, SoupAuth *auth, gboolean retrying, gpointer user_data);
OlapServer*os_init_default();
void node_check_version(Node*ne);
char*node_pam_get_version(Node*ne);
void node_set_defaults(Node*node);
void node_reset_id(Node*node);
char*node_pam_get_compat_string(Node*ne);
void node_set_id_from_string(Node*node, int in_id, char*in_str);

char*node_get_out_conn_file(Node*node, int index, char*pam_id);
GString*node_fill_out_conn_to_cmd(Node*node, char*pam_id, int val_flag, GString*command, gchar*olapfs, char cepar);
void node_fill_out_conn_flag(Node*node, int val_flag);
void node_fill_out_conn(Node*node, char*pam_id, int val_flag);
int node_pam_check_outputs(Node*node);

Node *node_new (int preallocate);

char door_pam_get_scepar ( int pam_type );
char door_pam_get_cepar (int pam_type);
gchar*door_pam_conversion_helper(gchar*pam_cmd, Node*node, int *data_need_conversion_flag, int *data_conversion_impossible, char cepar, char scepar);

#ifdef __cplusplus
}
#endif

#endif /* _NODE_H */
