/***************************************************************************
 *            Connection.c
 *
 *  Wed Feb 15 12:40:44 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <Connection.h>

Connection*newConnection(int sourceNodeID, int sourcePortID, int destNodeID, int destPortID)
{
	Connection*conn;
	conn = (Connection*)malloc(sizeof(Connection));
	conn->sourceNodeID = sourceNodeID;
	conn->sourcePortID = sourcePortID;
	conn->destNodeID = destNodeID;
	conn->destPortID = destPortID;
	conn->file = (char*)NULL;
	conn->host = (char*)NULL;
	conn->proto = (char*)NULL;
	conn->address = (char*)NULL;
	conn->flag = 0;
	conn->ptype = unk;
	conn->meta_ptype = unk;
	g_mutex_init(&(conn->connMutex));
	return conn;
}

Connection*dupConnection(Connection*p)
{
	Connection*conn;
	conn = (Connection*)malloc(sizeof(Connection));
	conn->sourceNodeID = p->sourceNodeID;
	conn->sourcePortID = p->sourcePortID;
	conn->destNodeID = p->destNodeID;
	conn->destPortID = p->destPortID;
	conn->flag = p->flag;
	if ( conn->flag ) {
		conn->file = strdup(p->file);
		conn->host = strdup(p->host);
		conn->proto = strdup(p->proto);
		conn->address = strdup(p->address);
		conn->port = p->port;
	} else {
		conn->file = (char*)NULL;
		conn->host = (char*)NULL;
		conn->proto = (char*)NULL;
		conn->address = (char*)NULL;
	}
	conn->ptype = p->ptype;
	conn->meta_ptype = p->meta_ptype;
	g_mutex_init(&(conn->connMutex));
	return conn;
}

void deleteConnection(Connection*conn)
{
	g_mutex_clear(&(conn->connMutex));
	if (conn->file)
		free(conn->file);
	if (conn->host)
		free(conn->host);
	if (conn->proto)
		free(conn->proto);
	if (conn->address)
		free(conn->address);
	free(conn);
}

int connection_equal(Connection*conn1, Connection*conn2)
{
	if ( conn1->destNodeID == conn2->destNodeID && conn1->destPortID == conn2->destPortID && conn1->sourceNodeID == conn2->sourceNodeID && conn1->sourcePortID == conn2->sourcePortID ) {
		return 1;
	}
	return 0;
}
