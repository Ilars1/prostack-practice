/***************************************************************************
 *            VInfo.c
 *
 *  Wed Feb 15 12:21:26 2006
 *  Copyright  2006  mackoel
 *  Email
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <VInfo.h>

VInfo*readVInfo(FILE*f)
{
	VInfo*vi;
	int i;
	char *buffer;
	char c;
	vi = (VInfo*)malloc(sizeof(VInfo));
	vi->n = 0;
	vi->array = (int*)NULL;
	fscanf(f, "%c", &c);
	while ( c != '\r' && c != '\n' ) {
		buffer = (char*)NULL;
		i = 0;
		while ( isdigit((int)c) ) {
			buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
			buffer[i] = c;
			i++;
			fscanf(f, "%c", &c);
		}
		if ( i > 0 ) {
			buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
			buffer[i] = '\0';
			i++;
			vi->array = (int*)realloc(vi->array, (vi->n + 1) * sizeof(int));
			vi->array[vi->n] = atoi(buffer);
			vi->n++;
			free(buffer);
		}
		while ( !isdigit((int)c) && c != '\r' && c != '\n' )
			fscanf(f, "%c", &c);
	}
	if ( vi->n > 2 ) {
		i = vi->array[1];
		vi->array[1] = vi->array[2];
		vi->array[2] = i;
	}
	return vi;
}

void deleteVInfo(VInfo*info)
{
	free(info->array);
	free(info);
}

void vinfoSave(VInfo*in, FILE*f)
{
	int i;
	if ( in->n > 2 ) {
		i = in->array[1];
		in->array[1] = in->array[2];
		in->array[2] = i;
	}
	for( i = 0; i < in->n; i++)
		fprintf(f, " %d", in->array[i]);
	if ( in->n > 2 ) {
		i = in->array[1];
		in->array[1] = in->array[2];
		in->array[2] = i;
	}
	fprintf(f, "\r\n");
}


void vinfoPrint(VInfo*in, char*str)
{
	int i;
	if ( in->n > 2 ) {
		i = in->array[1];
		in->array[1] = in->array[2];
		in->array[2] = i;
	}
	for( i = 0; i < in->n; i++)
		sprintf(str, " %d", in->array[i]);
	if ( in->n > 2 ) {
		i = in->array[1];
		in->array[1] = in->array[2];
		in->array[2] = i;
	}
	sprintf(str, "\r\n");
}
