#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <door.h>
#include <Kimono.h>
#include <glib.h>
#include <unistd.h>
#include <komet.h>

static int isSetError = 0;
static void (*error)(char*msg);

OlapServer*newOlapServerFromFile(FILE*f)
{
	OlapServer*os;
	char *buffer;
	char c;
	int i;
	buffer = (char*)NULL;
	i = 0;
	fscanf(f, "%c", &c);
	while ( c != '\n' && c != '\r' && c != '#' && c != ':' ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
/*	os = (OlapServer*)malloc(sizeof(OlapServer));*/
	os = newOlapServerV();
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
/*		os->hname = (char*)calloc(i, sizeof(char));*/
		os->hname = strcpy(os->hname, buffer);
		free(buffer);
	} else {
/*		os->hname = (char*)calloc(10, sizeof(char));*/
		os->hname = strcpy(os->hname, "localhost");
		os->address = g_strdup("/");
		os->proto = g_strdup("door");
		os->nicname = g_strdup("default");
		os->port = 7778;
		return os;
	}
	buffer = (char*)NULL;
	i = 0;
	fscanf(f, "%c", &c);
	while ( c != '\n' && c != '\r' && c != '#' && c != '/' ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		os->port = atoi(buffer);
		free(buffer);
	}
	os->address = g_strdup("/");
	if ( !strcmp(os->hname, "localhost") ) {
		os->proto = g_strdup("door");
		os->nicname = g_strdup("default");
	} else {
		os->proto = g_strdup("http");
		os->nicname = g_strdup(os->hname);
	}
	return os;
}


void readNode(Node *n, FILE *f)
{
	int i;
	char *buffer;
	char c;
/*	int nInPorts;
	int nOutPorts;*/
	fscanf(f, "%c", &c);
	while ( !isdigit((int)c) ) 
		fscanf(f, "%c", &c);
	buffer = (char*)NULL;
	i = 0;
	while ( isdigit((int)c) ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		n->ID = atoi(buffer);
		free(buffer);
	}
	while ( c == '\0' || c == '\r' || c == '\n' || isspace((int)c) ) 
		fscanf(f, "%c", &c);
	fseek(f, -1, SEEK_CUR);
	n->info = readVInfo(f);
	fscanf(f, "%c", &c);
	while ( c == '\0' || c == '\r' || c == '\n' || isspace((int)c) ) 
		fscanf(f, "%c", &c);
	buffer = (char*)NULL;
	i = 0;
	while ( c != '|' ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		n->id = (char*)realloc(n->id, i * sizeof(char));
		n->id = strcpy(n->id, buffer);
		free(buffer);
	}
	fscanf(f, "%c", &c);
	buffer = (char*)NULL;
	i = 0;
	while ( c != '\n' && c != '\r' && c != '#' && c != '|' ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		n->file = (char*)realloc(n->file, i * sizeof(char));
		n->file = strcpy(n->file, buffer);
		free(buffer);
	}
	n->oServer = newOlapServerFromFile(f);
	fscanf(f, "%c", &c);
	while ( c != '#' && c != ':' )
		fscanf(f, "%c", &c);
	fscanf(f, "%c", &c);
	while ( c != '#' && !isalpha((int)c) && c != ':')
		fscanf(f, "%c", &c);
	if ( c != ':' ) {
		buffer = (char*)NULL;
		i = 0;
		while ( isalpha((int)c) ) {
			buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
			buffer[i] = c;
			i++;
			fscanf(f, "%c", &c);
		}
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		if ( !strcmp(buffer,"PAM") ) {
			n->type = pam;
			free(buffer);
			while ( !isalpha((int)c) && c != '_' )
				fscanf(f, "%c", &c);
			buffer = (char*)NULL;
			i = 0;
			while ( isalnum((int)c) || c == '_' ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = c;
				i++;
				fscanf(f, "%c", &c);
			}
			if ( i > 0 ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = '\0';
				i++;
				n->pam = newOlapPAM(buffer);
				setNodePorts(n);
			}
		} else if ( !strcmp(buffer,"DISPLAY") ) {
			n->type = display;
			n->nInConns = 1;
			n->inConn = (Port*)calloc(1, sizeof(Port));
			n->inConn[0].ID = 1;
			n->inConn[0].label = (char*)calloc(6, sizeof(char));
			n->inConn[0].label = strcpy(n->inConn[0].label, "image");
			n->inConn[0].type = tif;
			n->inConn[0].nConn = 0;
			n->inConn[0].channel = (Connection**)NULL;
			if ( g_strrstr(n->id, "%d") ) {
				char**margv;
				int i;
				char *nid;
				margv = g_strsplit(n->id, " ", MAXTOKENS);
				for ( i = 0; margv && margv[i]; i++ ) {
					if ( g_strrstr(margv[i], "%d") ) {
						margv[i] = strcpy(margv[i], "%s");
					}
				}
				nid = g_strjoinv(" ", margv);
				n->id = strcpy(n->id, nid);
				g_free(nid);
				g_strfreev(margv);
			}
		} else if ( !strcmp(buffer,"IM") ) {
			n->type = display;
		} else if ( !strcmp(buffer,"") ) {
			n->type = file;
			n->nOutConns = 1;
			n->outConn = (Port*)calloc(1, sizeof(Port));
			n->outConn[0].ID = 1;
			n->outConn[0].label = (char*)calloc(6, sizeof(char));
			n->outConn[0].label = strcpy(n->outConn[0].label, "image");
			n->outConn[0].type = tif;
			n->outConn[0].nConn = 0;
			n->outConn[0].channel = (Connection**)NULL;
		} else if ( !strcmp(buffer,"INS") ) {
			n->type = ins;
			n->nOutConns = 1;
			n->outConn = (Port*)calloc(1, sizeof(Port));
			n->outConn[0].ID = 1;
			n->outConn[0].label = (char*)calloc(6, sizeof(char));
			n->outConn[0].label = strcpy(n->outConn[0].label, "image");
			n->outConn[0].type = tif;
			n->outConn[0].nConn = 0;
			n->outConn[0].channel = (Connection**)NULL;
		} else if ( !strcmp(buffer,"OUS") ) {
			n->type = ous;
			n->nInConns = 1;
			n->inConn = (Port*)calloc(1, sizeof(Port));
			n->inConn[0].ID = 1;
			n->inConn[0].label = (char*)calloc(6, sizeof(char));
			n->inConn[0].label = strcpy(n->inConn[0].label, "image");
			n->inConn[0].type = tif;
			n->inConn[0].nConn = 0;
			n->inConn[0].channel = (Connection**)NULL;
		} else if ( !strcmp(buffer,"MACRO") ) {
			n->type = macro;
			node_macro_set_in_ports(n);
			node_macro_set_out_ports(n);
/*			n->nInConns = 1;
			n->inConn = (Port*)calloc(1, sizeof(Port));
			n->inConn[0].ID = 1;
			n->inConn[0].label = (char*)calloc(6, sizeof(char));
			n->inConn[0].label = strcpy(n->inConn[0].label, "image");
			n->inConn[0].type = tif;
			n->inConn[0].nConn = 0;
			n->inConn[0].channel = (Connection**)NULL;*/
		} else {
			if (isSetError)
				error("unknown pam node");
		}
		free(buffer);
	} else {
		n->type = file;
		n->nOutConns = 1;
		n->outConn = (Port*)calloc(1, sizeof(Port));
		n->outConn[0].ID = 1;
/*		n->outConn[0].label
		n->outConn[0].type*/
		n->outConn[0].nConn = 0;
		n->outConn[0].channel = (Connection**)NULL;
	}
	
	while ( c != '#' && !isdigit((int)c) ) 
		fscanf(f, "%c", &c);
	buffer = (char*)NULL;
	i = 0;
	while ( isdigit((int)c) ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		n->timeDelay = atoi(buffer);
		free(buffer);
	}
	fscanf(f, "%c", &c);
	while ( c == '\0' || c == '\r' || c == '\n' || isspace((int)c) ) {
		fscanf(f, "%c", &c);
	}
	buffer = (char*)NULL;
	i = 0;
	while ( c != '#' && c != '\0' && c != '\r' && c != '\n' ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = c;
		i++;
		fscanf(f, "%c", &c);
	}
	if ( i > 0 ) {
		buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
		buffer[i] = '\0';
		i++;
		if ( n->type == pam ) {
			n->label = strcpy(n->label, &buffer[8]);
		} else if ( n->type == display ) {
			n->label = strcpy(n->label, &buffer[12]);
		} else {
			n->label = strcpy(n->label, buffer);
		}
		free(buffer);
	}
}


void readConnectionsFromFile(ListOfNodes *lon, FILE*f)
{
	Connection *conn;
	int sourceNodeID;
	int sourcePortID;
	int destNodeID;
	int destPortID;
	int n;
	int i;
	int j;
	char *buffer;
	char c;
	fscanf(f, "%c", &c);
	while ( !isdigit((int)c) && !feof(f) )
		fscanf(f, "%c", &c);
	while ( !feof(f) ) {
		i = 0;
		buffer = (char*)NULL;
		while ( isdigit((int)c) ) {
			buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
			buffer[i] = c;
			i++;
			fscanf(f, "%c", &c);
		}
		if ( i > 0 ) {
			buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
			buffer[i] = '\0';
			i++;
			n = atoi(buffer);
			free(buffer);
		}
		while ( !isdigit((int)c) && !feof(f) )
			fscanf(f, "%c", &c);
		for ( j = 0; j < n; j++ ) {
			i = 0;
			buffer = (char*)NULL;
			while ( isdigit((int)c) ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = c;
				i++;
				fscanf(f, "%c", &c);
			}
			if ( i > 0 ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = '\0';
				i++;
				destNodeID = atoi(buffer);
				free(buffer);
			}
			fscanf(f, "%c", &c);
			i = 0;
			buffer = (char*)NULL;
			while ( isdigit((int)c) ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = c;
				i++;
				fscanf(f, "%c", &c);
			}
			if ( i > 0 ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = '\0';
				i++;
				destPortID = atoi(buffer);
				free(buffer);
			}
			while ( c != '-' && !feof(f) )
				fscanf(f, "%c", &c);
			while ( !isdigit(c) && !feof(f) )
				fscanf(f, "%c", &c);
			i = 0;
			buffer = (char*)NULL;
			while ( isdigit((int)c) ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = c;
				i++;
				fscanf(f, "%c", &c);
			}
			if ( i > 0 ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = '\0';
				i++;
				sourceNodeID = atoi(buffer);
				free(buffer);
			}
			fscanf(f, "%c", &c);
			i = 0;
			buffer = (char*)NULL;
			while ( isdigit((int)c) ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = c;
				i++;
				fscanf(f, "%c", &c);
			}
			if ( i > 0 ) {
				buffer = (char*)realloc(buffer, ( i + 1 )* sizeof(char));
				buffer[i] = '\0';
				i++;
				sourcePortID = atoi(buffer);
				free(buffer);
			}
			if ( sourceNodeID == destNodeID && sourcePortID == destPortID && isSetError) {
				error("wrong connection");
			} else {
				conn = newConnection(sourceNodeID, sourcePortID, destNodeID, destPortID);
				installConnection(lon, conn);
			}
			while ( !isdigit((int)c) && !feof(f) )
				fscanf(f, "%c", &c);
		}
	}
}


ListOfNodes *initFromFileToListOfNodes(FILE*f)
{
	ListOfNodes *list;
	list = newListOfNodes();
	list->next = (ListOfNodes*)NULL;
	list->prev = (ListOfNodes*)NULL;
	list->node = newNode();
	readNode(list->node, f);
	return list;
}

void addFromFileToListOfNodes(ListOfNodes *list, FILE*f)
{
	ListOfNodes *l1, *l2;
	l1 = list;
	l2 = list;
	while ( l1 ) {
		l2 = l1;
		l1 = l1->next;
	}
	l1 = newListOfNodes();
	l2->next = l1;
	l1->prev = l2;
	l1->node = newNode();
	readNode(l1->node, f);
	l1->next = (ListOfNodes*)NULL;
}

Workspace*workspaceFromFile(char*filename)
{
	Workspace*ws;
	FILE*f;
	int i;
	char *record;
	int c;
	f = fopen(filename, "r");
	if (!f && isSetError)
		error("workspaceFromFile: no file");
	record = (char*)calloc(MAX_RECORD, sizeof(char));
	record = fgets( record, MAX_RECORD, f);
	c = (int)(*record);
	while ( record != NULL && !isdigit(c) ) {
		record = fgets( record, MAX_RECORD, f);
		i = 0;
		while( isspace((int)record[i]) )
			i++;
		c = record[i];
	}
/*	while ( ( (record = fgets( record, MAX_RECORD, f)) != NULL ) && !isdigit((int)(*record)) ) 
		continue;*/
	if ( record == NULL && isSetError)
		error("workspaceFromFile: error in file");
	ws = workspaceCreate();
	ws->numberOfNodes = atoi(record);
	ws->listOfNodes = initFromFileToListOfNodes(f);
	if ( (record = fgets( record, MAX_RECORD, f)) == NULL && isSetError ) {
		error("workspaceFromFile: error in file");
		return NULL;
	}
	for ( i = 1; i < ws->numberOfNodes; i++ ) {
		addFromFileToListOfNodes(ws->listOfNodes, f);
		if ( (record = fgets( record, MAX_RECORD, f)) == NULL && isSetError ) {
			error("workspaceFromFile: error in file");
			return NULL;
		}
	}
	free(record);
	readConnectionsFromFile(ws->listOfNodes, f);
	ws->numberOfMacro = macroNodes(ws->listOfNodes);
	return ws;
}


