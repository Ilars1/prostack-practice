/***************************************************************************
 *            operations.c
 *
 *  Fri Aug  6 09:51:23 2010
 *  Copyright  2010  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fenv.h>
#include <math.h>
#include <glib.h>

#include <operations.h>
#include <imgproc/imgproc_c.hpp>
#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_thread_num() 0
	#define omp_get_num_threads() 1
#endif

int psocv_canny(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage* image;
	IplImage* edges;
	double threshold1;
	double threshold2;
	int aperture_size = 3;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_GRAYSCALE;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 2 ) {
		return 1;
	}
	threshold1 = atof(opts[0]);
	threshold2 = atof(opts[1]);
	if ( n_opts > 2 ) {
		aperture_size = atoi(opts[2]);
	}
	g_strfreev(opts);
	image = cvLoadImage((const char*) input_pointers[0], iscolor);
	edges = cvCloneImage (image);
	cvCanny((const CvArr* )image, (CvArr* )edges, (double) threshold1, (double) threshold2, (int) aperture_size);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) edges, NULL);
	cvReleaseImage(&image);
	cvReleaseImage(&edges);
	return res;
}

int psocv_harris(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage* image;
	IplImage* harris_dst;
	double k = 0.04;
	int aperture_size = 3;
	int blockSize;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_GRAYSCALE;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 1 ) {
		return 1;
	}
	blockSize = atoi(opts[0]);
	if ( n_opts > 1 ) {
		aperture_size = atoi(opts[1]);
	}
	if ( n_opts > 2 ) {
		k = atof(opts[2]);
	}
	g_strfreev(opts);
	image = cvLoadImage((const char*) input_pointers[0], iscolor);
	harris_dst = cvCreateImage( cvGetSize(image), IPL_DEPTH_32F, 1 );
	cvCornerHarris((const CvArr* )image, (CvArr* )harris_dst, (int) blockSize, (int) aperture_size, (double) k);
	cvConvertScale((const CvArr* )harris_dst, (CvArr* )image, 255.0, 0.0);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) image, NULL);
	cvReleaseImage(&image);
	cvReleaseImage(&harris_dst);
	return res;
}

int psocv_split(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage* image;
	IplImage *split_dst;
	CvRect roi;
	CvSize size;
	int r, c;
	int blockSize = 256;
	int nthreads = 1;
	int n_opts, res;
	char **opts;
	gchar*of;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 1 ) {
		return 1;
	}
	blockSize = atoi(opts[0]);
	g_strfreev(opts);
	cvSetNumThreads(nthreads);
	image = cvLoadImage((const char*) input_pointers[0], iscolor);
	size = cvGetSize(image);
	for (r = 0; r < size.height; r += blockSize) {
		for (c = 0; c < size.width; c += blockSize) {
			roi.x = c;
			roi.y = r;
			roi.width = (c + blockSize > size.width) ? (size.width - c) : blockSize;
			roi.height = (r + blockSize > size.height) ? (size.height - r) : blockSize;
			cvSetImageROI(image, roi);
			split_dst = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
			cvCopy(image, split_dst, NULL);
			of = g_strdup_printf("%d-%d-%s", r, c, output_pointers[0]);
			res = cvSaveImage((const char*) of, (const CvArr*) split_dst, NULL);
			cvResetImageROI(image);
			cvReleaseImage(&split_dst);
			g_free(of);
		}
	}
	cvResetImageROI(image);
	cvReleaseImage(&image);
	return res;
}

int psocv_color_convert(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage *image;
	IplImage *color_convert_dst;
	int code = CV_RGB2GRAY;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 1 ) {
		return 1;
	}
	if ( !g_strcmp0(opts[0], "RGB2GRAY") ) {
		code = CV_RGB2GRAY;
	} else if ( !g_strcmp0(opts[0], "GRAY2RGB") ) {
		code = CV_GRAY2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2XYZ") ) {
		code = CV_BGR2XYZ;
	} else if ( !g_strcmp0(opts[0], "RGB2XYZ") ) {
		code = CV_RGB2XYZ;
	} else if ( !g_strcmp0(opts[0], "XYZ2BGR") ) {
		code = CV_XYZ2BGR;
	} else if ( !g_strcmp0(opts[0], "XYZ2RGB") ) {
		code = CV_XYZ2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2YCrCb") ) {
		code = CV_BGR2YCrCb;
	} else if ( !g_strcmp0(opts[0], "RGB2YCrCb") ) {
		code = CV_RGB2YCrCb;
	} else if ( !g_strcmp0(opts[0], "YCrCb2BGR") ) {
		code = CV_YCrCb2BGR;
	} else if ( !g_strcmp0(opts[0], "YCrCb2RGB") ) {
		code = CV_YCrCb2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2HSV") ) {
		code = CV_BGR2HSV;
	} else if ( !g_strcmp0(opts[0], "RGB2HSV") ) {
		code = CV_RGB2HSV;
	} else if ( !g_strcmp0(opts[0], "HSV2BGR") ) {
		code = CV_HSV2BGR;
	} else if ( !g_strcmp0(opts[0], "HSV2RGB") ) {
		code = CV_HSV2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2HLS") ) {
		code = CV_BGR2HLS;
	} else if ( !g_strcmp0(opts[0], "RGB2HLS") ) {
		code = CV_RGB2HLS;
	} else if ( !g_strcmp0(opts[0], "HLS2BGR") ) {
		code = CV_HLS2BGR;
	} else if ( !g_strcmp0(opts[0], "HLS2RGB") ) {
		code = CV_HLS2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2Lab") ) {
		code = CV_BGR2Lab;
	} else if ( !g_strcmp0(opts[0], "RGB2Lab") ) {
		code = CV_RGB2Lab;
	} else if ( !g_strcmp0(opts[0], "Lab2BGR") ) {
		code = CV_Lab2BGR;
	} else if ( !g_strcmp0(opts[0], "Lab2RGB") ) {
		code = CV_Lab2RGB;
	} else if ( !g_strcmp0(opts[0], "BGR2Luv") ) {
		code = CV_BGR2Luv;
	} else if ( !g_strcmp0(opts[0], "RGB2Luv") ) {
		code = CV_RGB2Luv;
	} else if ( !g_strcmp0(opts[0], "Luv2BGR") ) {
		code = CV_Luv2BGR;
	} else if ( !g_strcmp0(opts[0], "Luv2RGB") ) {
		code = CV_Luv2RGB;
	}
	g_strfreev(opts);
	image = cvLoadImage((const char*) input_pointers[0], iscolor);
	if ( code == CV_RGB2GRAY ) {
		color_convert_dst = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 1 );
	} else if ( code == CV_GRAY2RGB ) {
		color_convert_dst = cvCreateImage( cvGetSize(image), IPL_DEPTH_8U, 3 );
	} else {
		color_convert_dst = cvCloneImage (image);
	}
	cvCvtColor( (const CvArr*) image, color_convert_dst, code);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) color_convert_dst, NULL);
	cvReleaseImage(&image);
	cvReleaseImage(&color_convert_dst);
	return res;
}

/* Modified from 
http://svn.openmicroscopy.org.uk/svn/ome/trunk/src/matlab/OME/Filters/colour_deconvolution/colour_deconvolution.c
*/
int psocv_color_transform(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage *image;
	IplImage *color_transform_dst;
	double MODx[3];
	double MODy[3];
	double MODz[3];
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 1 ) {
		return 1;
	}
	if ( !g_strcmp0(opts[0], "HE") ) {
/* GL Haem matrix */
		MODx[0]= 0.644211;
		MODy[0]= 0.716556;
		MODz[0]= 0.266844;
/* GL Eos matrix */
		MODx[1]= 0.092789;
		MODy[1]= 0.954111;
		MODz[1]= 0.283111;
/*  Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "HE2") ) {
/* GL Haem matrix */
		MODx[0]= 0.650;
		MODy[0]= 0.704;
		MODz[0]= 0.286;
/* GL Eos matrix */
		MODx[1]= 0.072;
		MODy[1]= 0.990;
		MODz[1]= 0.105;
/*  Zero rix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "HDAB") ) {
/* 3,3-diamino-benzidine tetrahydrochloride
	   Haem matrix */
		MODx[0]= 0.650;
		MODy[0]= 0.704;
		MODz[0]= 0.286;
/* DAB matrix */
		MODx[1]= 0.268;
		MODy[1]= 0.570;
		MODz[1]= 0.776;
/* Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "FastRedFastBlueDAB") ) {
/* fast red */
		MODx[0]= 0.21393921;
		MODy[0]= 0.85112669;
		MODz[0]= 0.47794022;
/* fast blue */
		MODx[1]= 0.74890292;
		MODy[1]= 0.60624161;
		MODz[1]= 0.26731082;
/* dab */
		MODx[2]= 0.268;
		MODy[2]= 0.570;
		MODz[2]= 0.776;
	} else if ( !g_strcmp0(opts[0], "MethylGreenDAB") ) {
/* MG matrix (GL) */
		MODx[0]= 0.98003;
		MODy[0]= 0.144316;
		MODz[0]= 0.133146;
/* DAB matrix */
		MODx[1]= 0.268;
		MODy[1]= 0.570;
		MODz[1]= 0.776;
/* Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "HEDAB") ) {
/* Haem matrix */
		MODx[0]= 0.650;
		MODy[0]= 0.704;
		MODz[0]= 0.286;
/* Eos matrix */
		MODx[1]= 0.072;
		MODy[1]= 0.990;
		MODz[1]= 0.105;
/* DAB matrix */
		MODx[2]= 0.268;
		MODy[2]= 0.570;
		MODz[2]= 0.776;
	} else if ( !g_strcmp0(opts[0], "HAEC") ) {
/* 3-amino-9-ethylcarbazole
	   Haem matrix */
		MODx[0]= 0.650;
		MODy[0]= 0.704;
		MODz[0]= 0.286;
/* AEC matrix */
		MODx[1]= 0.2743;
		MODy[1]= 0.6796;
		MODz[1]= 0.6803;
/* Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "Azan-Mallory") ) {
/* Azocarmine and Aniline Blue (AZAN)
	   GL Blue matrix */
		MODx[0]= .853033;
		MODy[0]= .508733;
		MODz[0]= .112656;
/* GL Red matrix */
		MODx[1]= 0.070933;
		MODy[1]= 0.977311;
		MODz[1]= 0.198067;
/* Orange matrix (not set yet, currently zero) */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "Alcian_blueH") ) {
/* GL Alcian Blue matrix */
		MODx[0]= 0.874622;
		MODy[0]= 0.457711;
		MODz[0]= 0.158256;
/* GL Haematox after PAS matrix */
		MODx[1]= 0.552556;
		MODy[1]= 0.7544;
		MODz[1]= 0.353744;
/* Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "HPAS") ) {
/* GL Haem matrix */
		MODx[0]= 0.644211; /* 0.650; */
		MODy[0]= 0.716556; /* 0.704; */
		MODz[0]= 0.266844; /* 0.286; */
/* GL PAS matrix */
		MODx[1]= 0.175411;
		MODy[1]= 0.972178;
		MODz[1]= 0.154589;
/* Zero matrix */
		MODx[2]= 0.0;
		MODy[2]= 0.0;
		MODz[2]= 0.0;
	} else if ( !g_strcmp0(opts[0], "Custom") ) {
		MODx[0]= atof(opts[1]);
		MODy[0]= atof(opts[2]);
		MODz[0]= atof(opts[3]);
		MODx[1]= atof(opts[4]);
		MODy[1]= atof(opts[5]);
		MODz[1]= atof(opts[6]);
		MODx[2]= atof(opts[7]);
		MODy[2]= atof(opts[8]);
		MODz[2]= atof(opts[9]);
	}
	g_strfreev(opts);
/**********************************************************************
*  Convert the stain vectors into the 'q' vector
***********************************************************************/
	double leng, A, V, C;
	double cosx[3];
	double cosy[3];
	double cosz[3];
	double len[3];
	double q[9];
	int i;
	for (i=0; i<3; i++){
/* normalise vector length */
		cosx[i]=cosy[i]=cosz[i]=0.0;
		len[i]=sqrt(MODx[i]*MODx[i] + MODy[i]*MODy[i] + MODz[i]*MODz[i]);
		if (len[i] != 0.0) {
			cosx[i]= MODx[i]/len[i];
			cosy[i]= MODy[i]/len[i];
			cosz[i]= MODz[i]/len[i];
		}
	}
/* translation matrix */
	if (cosx[1]==0.0){ /* 2nd colour is unspecified */
		if (cosy[1]==0.0){
			if (cosz[1]==0.0){
				cosx[1]=cosz[0];
				cosy[1]=cosx[0];
				cosz[1]=cosy[0];
			}	
		}
	}
	if (cosx[2]==0.0){ /* 3rd colour is unspecified */
		if (cosy[2]==0.0){
			if (cosz[2]==0.0){
				if ((cosx[0]*cosx[0] + cosx[1]*cosx[1])> 1) {
					cosx[2]=0.0;
				} else {
					cosx[2]=sqrt(1.0-(cosx[0]*cosx[0])-(cosx[1]*cosx[1]));
				}
				if ((cosy[0]*cosy[0] + cosy[1]*cosy[1])> 1) {
					cosy[2]=0.0;
				} else {
					cosy[2]=sqrt(1.0-(cosy[0]*cosy[0])-(cosy[1]*cosy[1]));
				}
				if ((cosz[0]*cosz[0] + cosz[1]*cosz[1])> 1) {
					cosz[2]=0.0;
				} else {
					cosz[2]=sqrt(1.0-(cosz[0]*cosz[0])-(cosz[1]*cosz[1]));
				}
			}
		}
	}
	leng= sqrt(cosx[2]*cosx[2] + cosy[2]*cosy[2] + cosz[2]*cosz[2]);
	cosx[2]= cosx[2]/leng;
	cosy[2]= cosy[2]/leng;
	cosz[2]= cosz[2]/leng;
/* matrix inversion */
	A = cosy[1] - cosx[1] * cosy[0] / cosx[0];
	V = cosz[1] - cosx[1] * cosz[0] / cosx[0];
	C = cosz[2] - cosy[2] * V/A + cosx[2] * (V/A * cosy[0] / cosx[0] - cosz[0] / cosx[0]);
	q[2] = (-cosx[2] / cosx[0] - cosx[2] / A * cosx[1] / cosx[0] * cosy[0] / cosx[0] + cosy[2] / A * cosx[1] / cosx[0]) / C;
	q[1] = -q[2] * V / A - cosx[1] / (cosx[0] * A);
	q[0] = 1.0 / cosx[0] - q[1] * cosy[0] / cosx[0] - q[2] * cosz[0] / cosx[0];
	q[5] = (-cosy[2] / A + cosx[2] / A * cosy[0] / cosx[0]) / C;
	q[4] = -q[5] * V / A + 1.0 / A;
	q[3] = -q[4] * cosy[0] / cosx[0] - q[5] * cosz[0] / cosx[0];
	q[8] = 1.0 / C;
	q[7] = -q[8] * V / A;
	q[6] = -q[7] * cosy[0] / cosx[0] - q[8] * cosz[0] / cosx[0];
	image = cvLoadImage((const char*) input_pointers[0], iscolor);
	color_transform_dst = cvCloneImage (image);
	psocvMatColorTransform( (const CvArr*) image, color_transform_dst, q);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) color_transform_dst, NULL);
	cvReleaseImage(&image);
	cvReleaseImage(&color_transform_dst);
	return res;
}

void psocvMatColorTransform(const CvArr* srcarr, CvArr* dstarr, double q[9])
{
	uchar*img, *trm;
	CvMat *src, sstub;
	CvMat *dst, dstub;
	int rows, cols;
	int i, j;
	int dstep, istep;
	double log255 = log(255.0);
	src = cvGetMat( srcarr, &sstub, NULL, 0);
	dst = cvGetMat( dstarr, &dstub, NULL, 0);
	if( CV_MAT_TYPE(src->type) != CV_8UC3 ) {
		cvError( (CV_StsUnsupportedFormat), "psocvMatColorTransform", "Only 8-bit, 3-channel input images are supported", __FILE__, __LINE__);
	}
	if( CV_MAT_TYPE(dst->type) != CV_8UC3 ) {
		cvError( (CV_StsUnsupportedFormat), "psocvMatColorTransform", "Only 8-bit, 3-channel output images are supported", __FILE__, __LINE__);
	}
	if( !CV_ARE_SIZES_EQ( src, dst )) {
		cvError( (CV_StsUnmatchedSizes), "psocvMatColorTransform", "The input and output images must have the same size", __FILE__, __LINE__);
	}
	istep = src->step;
	img = src->data.ptr;
	dstep = dst->step;
	trm = dst->data.ptr;
	rows = src->rows;
	cols = src->cols;
#pragma omp parallel for schedule(static) collapse(2) default(none) shared(rows,cols,img,trm,q,log255,istep,dstep)
	for ( i = 0; i < rows; i++) {
		for ( j = 0; j < cols; j++) {
			/* log transform the RGB data */
#ifdef _OPENMP
			uchar R = img[ i * istep + j * 3 ];
			uchar G = img[ i * istep + j * 3 + 1];
			uchar B = img[ i * istep + j * 3 + 2];
#else
			uchar R = img[0];
			uchar G = img[1];
			uchar B = img[2];
#endif
			double Rlog = -( ( 255.0 * log( ((double)R + 1.0 ) / 255.0 ) ) / log255);
			double Glog = -( ( 255.0 * log( ((double)G + 1.0 ) / 255.0 ) ) / log255);
			double Blog = -( ( 255.0 * log( ((double)B + 1.0 ) / 255.0 ) ) / log255);
			double Rscaled = Rlog * q[0];
			double Gscaled = Glog * q[1];
			double Bscaled = Blog * q[2];
			double output = MIN ( exp(-((Rscaled + Gscaled + Bscaled) - 255.0) * log255 / 255.0), 255.0);
#ifdef _OPENMP
			trm[ i * dstep + j * 3 ] = (uchar)cvRound(output);
#else
			trm[0] = (uchar)cvRound(output);
#endif
			Rscaled = Rlog * q[3];
			Gscaled = Glog * q[4];
			Bscaled = Blog * q[5];
			output = MIN ( exp(-((Rscaled + Gscaled + Bscaled) - 255.0) * log255 / 255.0), 255.0);
#ifdef _OPENMP
			trm[ i * dstep + j * 3 + 1] = (uchar)cvRound(output);
#else
			trm[1] = (uchar)cvRound(output);
#endif
			Rscaled = Rlog * q[6];
			Gscaled = Glog * q[7];
			Bscaled = Blog * q[8];
			output = MIN ( exp(-((Rscaled + Gscaled + Bscaled) - 255.0) * log255 / 255.0), 255.0);
#ifdef _OPENMP
			trm[ i * dstep + j * 3 + 2] = (uchar)cvRound(output);
#else
			trm[2] = (uchar)cvRound(output);
			img += 3;
			trm += 3;
#endif
		}
	}
}

int psocv_pyrmeanshift(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage* src;
	IplImage* dst;
	double sp, sr;
	int max_level = 1;
	CvTermCriteria termcrit;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 3 ) {
		return 1;
	}
	sp = atof(opts[0]);
	sr = atof(opts[1]);
	max_level = atoi(opts[2]);
	g_strfreev(opts);
	termcrit = cvTermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 5, 1);
	src = cvLoadImage((const char*) input_pointers[0], iscolor);
	dst = cvCloneImage (src);
	cvPyrMeanShiftFiltering((const CvArr*) src, (CvArr*) dst, sp, sr, max_level, termcrit);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) dst, NULL);
	cvReleaseImage(&src);
	cvReleaseImage(&dst);
	return res;
}

int psocv_pyrsegm(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	IplImage* src;
	IplImage* dst;
	double threshold1, threshold2;
	int max_level = 1;
	int block_size = 1000;
	CvMemStorage* storage;
	CvSeq* comp;
	CvRect roi;
	CvSize size;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 3 ) {
		return 1;
	}
	threshold1 = atof(opts[0]);
	threshold2 = atof(opts[1]);
	max_level = atoi(opts[2]);
	g_strfreev(opts);
	src = cvLoadImage((const char*) input_pointers[0], iscolor);
	dst = cvCloneImage (src);
	storage = cvCreateMemStorage(block_size);
	size = cvGetSize(src);
	roi.x = 0;
	roi.y = 0;
	roi.width = size.width;
	roi.height = size.height;
	cvSetImageROI(src, roi);
	cvPyrSegmentation(src, dst, storage, &comp, max_level, threshold1, threshold2);
	res = cvSaveImage((const char*) output_pointers[0], (const CvArr*) dst, NULL);
	cvReleaseMemStorage(&storage);
	cvResetImageROI(src);
	cvReleaseImage(&src);
	cvReleaseImage(&dst);
	return res;
}
