/***************************************************************************
 *            operations.h
 *
 *  Fri Aug  6 09:52:53 2010
 *  Copyright  2010  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef PSOCV_OPERATIONS_H
#define PSOCV_OPERATIONS_H

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fenv.h>
#include <math.h>
#include <glib.h>

#include <cv.h>
//#include <opencv2/core/core_c.h>
//#include <cxerror.h>
//#include <highgui.h>
#include <cvaux.h>

#include <opencv/cxcore.hpp>

#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc_c.h>


G_BEGIN_DECLS

int psocv_canny(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_harris(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_split(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_color_convert(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

/* Modified from 
http://svn.openmicroscopy.org.uk/svn/ome/trunk/src/matlab/OME/Filters/colour_deconvolution/colour_deconvolution.c
*/
int psocv_color_transform(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

void psocvMatColorTransform(const CvArr* srcarr, CvArr* dstarr, double q[9]);

int psocv_pyrmeanshift(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_pyrmeanshift(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_pyrsegm(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

G_END_DECLS

#endif /* PSOCV_OPERATIONS_H */
