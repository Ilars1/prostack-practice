//           psocvp_grabcut.cxx
//  Mon May 21 17:35:23 2012
//  Copyright  2012  Konstantin Kozlov
//  <kozlov@spbcas.ru>

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fenv.h>
#include <glib.h>

#ifndef MAX_RECORD
#define MAX_RECORD 2048
#endif

#include <highgui.h>
#include <cv.h>
#include <iostream>

using namespace std;
using namespace cv;

static gchar*oper;
static gchar*ostring;
static int rounding_mode_old, rounding_mode_new, rounding_mode_res;

static GOptionEntry entries[] = 
{
	{ "operation", 'o', 0, G_OPTION_ARG_STRING, &oper, "Operation", NULL },
	{ "options", 's', 0, G_OPTION_ARG_STRING, &ostring, "Options", NULL },
	{ NULL }
};

int psocv_grabcut(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers);

int psocv_grabcut(gchar*ostring, int n_input_pointers, gchar**input_pointers, int n_output_pointers, gchar**output_pointers)
{
	Mat src;
	Mat mask, marker, fgmarker;
	Mat bgdModel, fgdModel;
	Rect rect;
	int iterCount = 1;
	int n_opts, res;
	char **opts;
	int iscolor = CV_LOAD_IMAGE_UNCHANGED;
	double threshold1;
	double threshold2;
	opts = g_strsplit(ostring, ",", MAX_RECORD);
	if ( !opts ) {
		return 1;
	}
	n_opts = g_strv_length(opts);
	if ( n_opts < 3 ) {
		return 1;
	}
	threshold1 = atof(opts[0]);
	threshold2 = atof(opts[1]);
	iterCount = atoi(opts[2]);
	g_strfreev(opts);
	marker = imread( input_pointers[1], CV_LOAD_IMAGE_GRAYSCALE );
	src.create( marker.size(), CV_8UC1);
	fgmarker.create( marker.size(), CV_8UC1);
	threshold(marker, src, threshold1, GC_PR_FGD, THRESH_BINARY);
	threshold(marker, fgmarker, threshold2, GC_FGD, THRESH_BINARY);
	mask = max(src, fgmarker);
	src.release();
	fgmarker.release();
	marker.release();
	src = imread( input_pointers[0], iscolor );
	grabCut( src, mask, rect, bgdModel, fgdModel, iterCount, GC_INIT_WITH_MASK );
	marker = mask > 0;
	mask.release();
	res = imwrite( output_pointers[0], marker );
	src.release();
	marker.release();
	return res;
}

int main( int argc, char** argv )
{
	GError *gerror = NULL;
	GOptionContext *context;
	char*inputs_list = NULL, *outputs_list = NULL;
	char**input_pointers = NULL, **output_pointers = NULL;
	int n_input_pointers = 0, n_output_pointers = 0;
	context = g_option_context_new (" - Use OpenCV operations in ProStack");
	g_option_context_add_main_entries (context, entries, "psocv");
	if ( ! g_option_context_parse(context, &argc, &argv, &gerror) ) {
		g_warning(gerror->message);
		g_error_free(gerror);
		gerror = NULL;
	}
	g_option_context_free(context);
	if ( argc < 2 ) {
		g_error("No enough input files");
	}
	if ( oper == NULL ) {
		g_error("Operation not given");
	}
	rounding_mode_new = FE_TOWARDZERO;
	rounding_mode_old = fegetround();
	rounding_mode_res = fesetround(rounding_mode_new);
	inputs_list = g_strdup(argv[argc - 2]);
	input_pointers = g_strsplit(inputs_list, ",", MAX_RECORD);
	if ( input_pointers ) {
		n_input_pointers = g_strv_length(input_pointers);
	}
	outputs_list = g_strdup(argv[argc - 1]);
	output_pointers = g_strsplit(outputs_list, ",", MAX_RECORD);
	if ( output_pointers ) {
		n_output_pointers = g_strv_length(output_pointers);
	}
	if ( !g_strcmp0(oper, "grabcut") ) {
		psocv_grabcut(ostring, n_input_pointers, input_pointers, n_output_pointers, output_pointers);
	} else {
		g_error("psocv unknown operation %s", oper);
	}
	g_free(oper);
	g_free(ostring);
	rounding_mode_res = fesetround(rounding_mode_old);
	return (0);
}