/***************************************************************************
 *            lazyrasql.c
 *
 *  Fri Feb  3 14:37:02 2012
 *  Copyright  2012  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
/*#include <glib/gthread.h>*/
#include <gio/gio.h>
#include <unistd.h>
#include <lazyrasql.h>

static char*operation;
static char*opoptions;

static GOptionEntry entries[] = 
{
	{ "operation", 'o', 0, G_OPTION_ARG_STRING, &operation, "Operation to perform", "Name of the operation" },
	{ "options", 's', 0, G_OPTION_ARG_STRING, &opoptions, "Optional parameters", "Parameters" },
	{ NULL }
};


gchar*lazy_rasql_get_sdom(const gchar*collection_name, const gchar*id, const gchar*oid, int*x1, int*x2, int*y1, int*y2, int*nn)
{
	gchar**margv;
	gchar*response, *stdout_buff, **stdout_parts;
	int argcp, n_stdout_parts,i;
	GString *command;
	GError*gerror = NULL;
	int flaggs, child_exit_status;
	command = g_string_new("rasql -q");
	g_string_append_printf(command, " 'select sdom(%s) from %s as %s where oid(%s) = %s' --quiet --out string", id, collection_name, id, id, oid);
	if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_error("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
		}
	}
	g_string_free(command, TRUE);
	flaggs = G_SPAWN_SEARCH_PATH;/* | G_SPAWN_STDOUT_TO_DEV_NULL;*/
/*	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;*/
/*	flaggs = G_SPAWN_SEARCH_PATH;*/
	if ( !g_spawn_sync (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, &stdout_buff, NULL, &child_exit_status, &gerror) ) {
		if ( gerror ) {
			g_error("g_spawn_sync failed for %s\nwith %s", margv[0], gerror->message);
		}
	}
	g_strfreev(margv);
	stdout_parts = g_strsplit_set(stdout_buff, "[:,]\n", -1);
	g_free(stdout_buff);
	n_stdout_parts = g_strv_length (stdout_parts);
	(*nn) = (int)(0.5 * (double)n_stdout_parts + 0.5 );
	(*x1) = (int)g_strtod(stdout_parts[1], NULL);
	(*x2) = (int)g_strtod(stdout_parts[2], NULL);
	(*y1) = (int)g_strtod(stdout_parts[3], NULL);
	(*y2) = (int)g_strtod(stdout_parts[4], NULL);
	g_strfreev(stdout_parts);
	command = g_string_new("");
	for ( i = 0; i < (*nn) - 2; i++ ) {
		g_string_append(command, ",*:*");
	}
	response = command->str;
	g_string_free(command, FALSE);
	return response;
}

GString*lazy_rasql_get_mdd_from_parms(const gchar*collection_name, const gchar*id, const gchar*oid, gint has_roi, gint has_crop, gint X1, gint X2, gint Y1, gint Y2, LazyRasqlColorType lrqlct, GError**err)
{
	GString *mdd;
	GError *gerror = NULL;
	gchar *other_dims;
	int x1, x2, y1, y2, nn;
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);
	mdd = g_string_new(id);
	other_dims = lazy_rasql_get_sdom((const gchar*)collection_name, (const gchar*)id, (const gchar*)oid, &x1, &x2, &y1, &y2, &nn);
	if ( has_roi == 1 ) {
		if ( nn > 2 ) {
			g_string_append_printf(mdd, "[%d:%d,%d:%d]", X1, X2, Y1, Y2);
		} else {
			g_string_append_printf(mdd, "[%d:%d,%d:%d%s]", X1, X2, Y1, Y2, other_dims);
		}
	} else if ( has_crop == 1 ) {
		if ( nn > 2 ) {
			g_string_append_printf(mdd, "[%d:%d,%d:%d]", X1, x2 - X2 + 1, Y1, y2 - Y2 + 1);
		} else {
			g_string_append_printf(mdd, "[%d:%d,%d:%d%s]", X1, x2 - X2 + 1, Y1, y2 - Y2 + 1, other_dims);
		}
	}
	g_free(other_dims);
	if ( lrqlct == LAZY_RASQL_COLOR_GRAY ) {
		gchar*prev_mdd = g_strdup(mdd->str);
		mdd = g_string_assign(mdd, "");
		g_string_append_printf(mdd, "(0.299 * %s.red + 0.587 * %s.green +0.114 * %s.blue)", prev_mdd, prev_mdd, prev_mdd);
		g_free(prev_mdd);
	} else if ( lrqlct == LAZY_RASQL_COLOR_GREY ) {
		gchar*prev_mdd = g_strdup(mdd->str);
		mdd = g_string_assign(mdd, "");
		g_string_append_printf(mdd, "(0.2126 * %s.red + 0.7152 * %s.green +0.0721 * %s.blue)", prev_mdd, prev_mdd, prev_mdd);
		g_free(prev_mdd);
	} else if ( lrqlct == LAZY_RASQL_COLOR_RED ) {
		gchar*prev_mdd = g_strdup(mdd->str);
		mdd = g_string_assign(mdd, "");
		g_string_append_printf(mdd, "%s.red", prev_mdd);
		g_free(prev_mdd);
	} else if ( lrqlct == LAZY_RASQL_COLOR_GREEN ) {
		gchar*prev_mdd = g_strdup(mdd->str);
		mdd = g_string_assign(mdd, "");
		g_string_append_printf(mdd, "%s.green", prev_mdd);
		g_free(prev_mdd);
	} else if ( lrqlct == LAZY_RASQL_COLOR_BLUE ) {
		gchar*prev_mdd = g_strdup(mdd->str);
		mdd = g_string_assign(mdd, "");
		g_string_append_printf(mdd, "%s.blue", prev_mdd);
		g_free(prev_mdd);
	}
	return mdd;
}

GString*lazy_rasql_get_mdd_from_group(GKeyFile*gkf, const gchar*group, gchar**collection_name, gchar**oid, gchar**id, GError**err)
{
	gchar *group_name_crop, *group_name_roi, *str, *group_name;
	GString*mdd;
	int X1, X2, Y1, Y2, int_val, has_roi = 0, has_crop = 0;
	LazyRasqlColorType lrqlct;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);
	str = g_key_file_get_string(gkf, group, "OID", &gerror);
	if ( gerror ) {
		g_propagate_error (err, gerror);
		g_error("lazy_rasql_write error:%s", gerror->message);
	}
	(*oid) = str;
	str = g_key_file_get_string(gkf, group, "Collection", &gerror);
	if ( gerror ) {
		g_propagate_error (err, gerror);
		g_error("lazy_rasql_write error:%s", gerror->message);
	}
	(*collection_name) = str;
	str = g_key_file_get_string(gkf, group, "Id", &gerror);
	if ( gerror ) {
		g_propagate_error (err, gerror);
		g_error("lazy_rasql_write error:%s", gerror->message);
	}
	(*id) = str;
	group_name_roi = g_strdup_printf("Roi:%s", (*id));
	group_name_crop = g_strdup_printf("Crop:%s", (*id));
	group_name = NULL;
	if ( g_key_file_has_group(gkf, group_name_roi) == TRUE ) {
		has_roi = 1;
		group_name = group_name_roi;
	} else if ( g_key_file_has_group(gkf, group_name_crop) == TRUE ) {
		has_crop = 1;
		group_name = group_name_crop;
	}
	if ( group_name != NULL ) {
		int_val = g_key_file_get_integer(gkf, (const gchar *)group_name, "X1", &gerror);
		if ( gerror ) {
			g_propagate_error (err, gerror);
			g_error("lazy_rasql_write error:%s", gerror->message);
		}
		X1 = int_val;
		int_val = g_key_file_get_integer(gkf, (const gchar *)group_name, "X2", &gerror);
		if ( gerror ) {
			g_propagate_error (err, gerror);
			g_error("lazy_rasql_write error:%s", gerror->message);
		}
		X2 = int_val;
		int_val = g_key_file_get_integer(gkf, (const gchar *)group_name, "Y1", &gerror);
		if ( gerror ) {
			g_propagate_error (err, gerror);
			g_error("lazy_rasql_write error:%s", gerror->message);
		}
		Y1 = int_val;
		int_val = g_key_file_get_integer(gkf, (const gchar *)group_name, "Y2", &gerror);
		if ( gerror ) {
			g_propagate_error (err, gerror);
			g_error("lazy_rasql_write error:%s", gerror->message);
		}
		Y2 = int_val;
	}
	g_free(group_name_crop);
	g_free(group_name_roi);
	str = g_key_file_get_string(gkf, group, "type", &gerror);
	if ( gerror ) {
		g_propagate_error (err, gerror);
		g_error("lazy_rasql_write error:%s", gerror->message);
	}
	if ( !g_strcmp0(str, "gray") ) {
		lrqlct = LAZY_RASQL_COLOR_GRAY;
	} else if ( !g_strcmp0(str, "rgb") ) {
		lrqlct = LAZY_RASQL_COLOR_RGB;
	} else if ( !g_strcmp0(str, "grey") ) {
		lrqlct = LAZY_RASQL_COLOR_GREY;
	} else if ( !g_strcmp0(str, "red") ) {
		lrqlct = LAZY_RASQL_COLOR_RED;
	} else if ( !g_strcmp0(str, "green") ) {
		lrqlct = LAZY_RASQL_COLOR_GREEN;
	} else if ( !g_strcmp0(str, "blue") ) {
		lrqlct = LAZY_RASQL_COLOR_BLUE;
	}
	g_free(str);
	mdd = lazy_rasql_get_mdd_from_parms((const gchar*)(*collection_name), (const gchar*)(*id), (const gchar*)(*oid), has_roi, has_crop, X1, X2, Y1, Y2, lrqlct, &gerror);
	if ( gerror ) {
		g_propagate_error (err, gerror);
		g_error("lazy_rasql_write error:%s", gerror->message);
	}
	return mdd;
}

gchar*lazy_rasql_get_id(gchar*file_pointer)
{
	gchar*id, *bname, **pts;
	bname = g_path_get_basename(file_pointer);
	pts = g_strsplit(bname, ".", MAXTOKENS);
	id = g_strdup(pts[0]);
	g_strfreev(pts);
	g_free(bname);
	return id;
}

int lazy_rasql_run(gchar*request, gchar*output_pointer)
{
	gchar**margv;
	int argcp;
	GString *command;
	GError*gerror = NULL;
	int flaggs, child_exit_status;
	command = g_string_new("rasql -q");
	g_string_append_printf(command, " '%s' --out file --outfile %s", request, output_pointer);
	if ( !g_shell_parse_argv(command->str, &argcp, &margv, &gerror) ) {
		if ( gerror ) {
			g_error("g_shell_parse failed for %s\nwith %s", command->str, gerror->message);
		}
	}
	g_string_free(command, TRUE);
	flaggs = G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL;
/*	flaggs |= G_SPAWN_STDERR_TO_DEV_NULL;
	flaggs = G_SPAWN_SEARCH_PATH;*/
	if ( !g_spawn_sync (NULL, margv, NULL, (GSpawnFlags)flaggs, NULL, NULL, NULL, NULL, &child_exit_status, &gerror) ) {
		if ( gerror ) {
			g_error("g_spawn_sync failed for %s\nwith %s", margv[0], gerror->message);
		}
	}
	g_strfreev(margv);
	return child_exit_status;
}

int lazy_rasql_exec(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0, nops, has_crop = 0, X1, X2, Y1, Y2;
	gchar**ops, *collection_name, *oid, *id;
	LazyRasqlColorType lrqlct;
	GString *mdd, *request;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	ops = g_strsplit(opoptions, ",", MAXTOKENS);
	nops = g_strv_length(ops);
	if ( nops == 3 ) {
		collection_name = g_strdup(ops[0]);
		oid = g_strdup(ops[1]);
		if ( !g_strcmp0(ops[2], "gray") ) {
			lrqlct = LAZY_RASQL_COLOR_GRAY;
		} else if ( !g_strcmp0(ops[2], "rgb") ) {
			lrqlct = LAZY_RASQL_COLOR_RGB;
		} else if ( !g_strcmp0(ops[2], "grey") ) {
			lrqlct = LAZY_RASQL_COLOR_GREY;
		} else if ( !g_strcmp0(ops[2], "red") ) {
			lrqlct = LAZY_RASQL_COLOR_RED;
		} else if ( !g_strcmp0(ops[2], "green") ) {
			lrqlct = LAZY_RASQL_COLOR_GREEN;
		} else if ( !g_strcmp0(ops[2], "blue") ) {
			lrqlct = LAZY_RASQL_COLOR_BLUE;
		}
	} else if ( nops == 7 ) {
		collection_name = g_strdup(ops[0]);
		oid = g_strdup(ops[1]);
		if ( !g_strcmp0(ops[2], "gray") ) {
			lrqlct = LAZY_RASQL_COLOR_GRAY;
		} else if ( !g_strcmp0(ops[2], "rgb") ) {
			lrqlct = LAZY_RASQL_COLOR_RGB;
		} else if ( !g_strcmp0(ops[2], "grey") ) {
			lrqlct = LAZY_RASQL_COLOR_GREY;
		} else if ( !g_strcmp0(ops[2], "red") ) {
			lrqlct = LAZY_RASQL_COLOR_RED;
		} else if ( !g_strcmp0(ops[2], "green") ) {
			lrqlct = LAZY_RASQL_COLOR_GREEN;
		} else if ( !g_strcmp0(ops[2], "blue") ) {
			lrqlct = LAZY_RASQL_COLOR_BLUE;
		}
		X1 = (int)g_strtod(ops[3], NULL);
		X2 = (int)g_strtod(ops[4], NULL);
		Y1 = (int)g_strtod(ops[5], NULL);
		Y2 = (int)g_strtod(ops[6], NULL);
		has_crop = 1;
	}
	g_strfreev(ops);
	id = lazy_rasql_get_id(output_pointers[0]);
	request = g_string_new("select tiff((char)(");
	mdd = lazy_rasql_get_mdd_from_parms((const gchar*)collection_name, (const gchar*)id, (const gchar*)oid, 0, has_crop, X1, X2, Y1, Y2, lrqlct, &gerror);
	g_string_append_printf(request, "%s", mdd->str);
	g_string_free(mdd, TRUE);
	g_string_append_printf(request, ")) from %s as %s where oid(%s) = %s", collection_name, id, id, oid);
	g_free(oid);
	g_free(collection_name);
	retval = lazy_rasql_run(request->str, output_pointers[0]);
	g_string_free(request, TRUE);
	g_free(id);
	return retval;
}

int lazy_rasql_write(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0, kounter = 0, has_crop1 = 0, has_crop2 = 0, has_roi1 = 0, has_roi2 = 0;
	GKeyFile*gkf;
	GFile*gfile;
	LazyRasqlCombineType lazy_rasql_combine_type;
	LazyRasqlColorType lrqlct1, lrqlct2;
	char *etag;
	char*data;
	gsize size;
	GString*request, *mdd1, *mdd2;
	gchar *collection_name1, *oid1, *collection_name2, *oid2, **groups, *id1, *id2, *group_name_crop, *group_name_roi, *str;
	int X11, X12, Y11, Y12, X21, X22, Y21, Y22, int_val, i;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	gfile = g_file_new_for_commandline_arg(input_pointers[0]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &data, &size, &etag, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_object_unref(gfile);
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)data, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_free(data);
	groups = g_key_file_get_groups(gkf, &size);
	for ( i = 0; i < (int)size; i++ ) {
		if ( g_str_has_prefix(groups[i], "Get") ) {
			if ( kounter == 0 ) {
				mdd1 = lazy_rasql_get_mdd_from_group(gkf, (const gchar *)groups[i], &collection_name1, &oid1, &id1, &gerror);
				kounter++;
			} else if ( kounter == 1 ) {
				mdd2 = lazy_rasql_get_mdd_from_group(gkf, (const gchar *)groups[i], &collection_name2, &oid2, &id2, &gerror);
				kounter++;
			}
		} else if ( g_str_has_prefix(groups[i], "Combine") ) {
			str = g_key_file_get_string(gkf, (const gchar *)groups[i], "Method", &gerror);
			if ( gerror ) {
				g_propagate_error (err, gerror);
				g_error("lazy_rasql_write error:%s", gerror->message);
			}
			if ( !g_strcmp0(str, "minusabs") ) {
				lazy_rasql_combine_type = LAZY_RASQL_COMBINE_MINUSABS;
			} else if ( !g_strcmp0(str, "avg") ) {
				lazy_rasql_combine_type = LAZY_RASQL_COMBINE_AVG;
			}
			g_free(str);
		}
	}
	g_strfreev(groups);
	g_key_file_free(gkf);
/* select png(m[$X1:$X2,$Y1:$Y2]) from $ColName as m where oid(m) = $OID */
/* select png(0.5 * m[$X11:$X12,$Y11:$Y12] + 0.5 * n[$X21:$X22,$Y21:$Y22]) from $ColName1,$ColName2 as m, n where oid(m) = $OID1 and oid(n) = $OID2 */
	request = g_string_new("select tiff((char)(");
/*
30% of the red value, 59% of the green value, and 11
mono = (0.2125 * color.r) + (0.7154 * color.g) + (0.0721 * color.b);
L = 0.2126·R + 0.7152·G + 0.0722·B 
Y´ = 0.299 * R´ + 0.587 * G´ + 0.114 * B´
Gray = 0.29900*R+0.58700*G+0.11400*B
*/
	if ( kounter > 1 ) {
		switch (lazy_rasql_combine_type) {
			case LAZY_RASQL_COMBINE_AVG:
				g_string_append_printf(request, "0.5 * %s + 0.5 * %s", mdd1->str, mdd2->str);
			break;
			case LAZY_RASQL_COMBINE_MINUSABS:
				g_string_append_printf(request, "abs(%s - %s)", mdd1->str, mdd2->str);
			break;
		}
		g_string_append_printf(request, ")) from %s as %s, %s as %s where oid(%s) = %s AND oid(%s) = %s", collection_name1, id1, collection_name2, id2, id1, oid1, id2, oid2);
	} else if ( kounter > 0 ) {
		g_string_append_printf(request, "%s", mdd1->str);
		g_string_append_printf(request, ")) from %s as %s where oid(%s) = %s", collection_name1, id1, id1, oid1);
	}
	if ( kounter > 0 ) {
		g_string_free(mdd1, TRUE);
		g_free(oid1);
		g_free(collection_name1);
		g_free(id1);
	}
	if ( kounter > 1 ) {
		g_string_free(mdd2, TRUE);
		g_free(oid2);
		g_free(collection_name2);
		g_free(id2);
	}
	retval = lazy_rasql_run(request->str, output_pointers[0]);
	g_string_free(request, TRUE);
	return retval;
}

int lazy_rasql_get(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0;
	gchar**ops;
	gchar*grp_name, *id;
	GKeyFile*gkf;
	GFile*gfile;
	char *etag, *new_etag;
	char*data;
	gsize size;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	id = lazy_rasql_get_id(output_pointers[0]);
	grp_name = g_strdup_printf("Get:%s", id);
	gkf = g_key_file_new();
	ops = g_strsplit(opoptions, ",", MAXTOKENS);
	g_key_file_set_value(gkf, grp_name, "Collection", ops[0]);
	g_key_file_set_value(gkf, grp_name, "OID", ops[1]);
	g_key_file_set_value(gkf, grp_name, "type", ops[2]);
	g_key_file_set_value(gkf, grp_name, "Id", id);
	g_free(id);
	g_strfreev(ops);
	g_free(grp_name);
	data = g_key_file_to_data(gkf, &size, &gerror);
	g_key_file_free(gkf);
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	etag = NULL;
	new_etag = NULL;
	if ( !g_file_replace_contents(gfile, data, size, etag, FALSE, G_FILE_CREATE_NONE, &new_etag, (GCancellable*)NULL, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	if ( new_etag ) {
		g_free(new_etag);
	}
	g_object_unref(gfile);
	g_free(data);
	return retval;
}

int lazy_rasql_roi(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0;
	gchar**ops;
	gchar*grp_name, *arg1, *id;
	GKeyFile*gkf;
	GFile*gfile;
	char *etag, *new_etag;
	char*data;
	gsize size;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	id = lazy_rasql_get_id(output_pointers[0]);
	arg1 = lazy_rasql_get_id(input_pointers[0]);
	grp_name = g_strdup_printf("Roi:%s", arg1);
	gfile = g_file_new_for_commandline_arg(input_pointers[0]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &data, &size, &etag, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_object_unref(gfile);
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)data, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_free(data);
	ops = g_strsplit(opoptions, ",", MAXTOKENS);
	g_key_file_set_value(gkf, grp_name, "X1", ops[0]);
	g_key_file_set_value(gkf, grp_name, "X2", ops[1]);
	g_key_file_set_value(gkf, grp_name, "Y1", ops[2]);
	g_key_file_set_value(gkf, grp_name, "Y2", ops[3]);
	g_key_file_set_value(gkf, grp_name, "Arg1", arg1);
	g_key_file_set_value(gkf, grp_name, "Id", id);
	g_strfreev(ops);
	g_free(grp_name);
	data = g_key_file_to_data(gkf, &size, &gerror);
	g_key_file_free(gkf);
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	etag = NULL;
	new_etag = NULL;
	if ( !g_file_replace_contents(gfile, data, size, etag, FALSE, G_FILE_CREATE_NONE, &new_etag, (GCancellable*)NULL, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	if ( new_etag ) {
		g_free(new_etag);
	}
	g_object_unref(gfile);
	g_free(data);
	return retval;
}

int lazy_rasql_crop(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0;
	gchar**ops;
	gchar*grp_name, *arg1, *id;
	GKeyFile*gkf;
	GFile*gfile;
	char *etag, *new_etag;
	char*data;
	gsize size;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	id = lazy_rasql_get_id(output_pointers[0]);
	arg1 = lazy_rasql_get_id(input_pointers[0]);
	grp_name = g_strdup_printf("Crop:%s", arg1);
	gfile = g_file_new_for_commandline_arg(input_pointers[0]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &data, &size, &etag, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_object_unref(gfile);
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)data, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_free(data);
	ops = g_strsplit(opoptions, ",", MAXTOKENS);
	g_key_file_set_value(gkf, grp_name, "X1", ops[0]);
	g_key_file_set_value(gkf, grp_name, "X2", ops[1]);
	g_key_file_set_value(gkf, grp_name, "Y1", ops[2]);
	g_key_file_set_value(gkf, grp_name, "Y2", ops[3]);
	g_key_file_set_value(gkf, grp_name, "Arg1", arg1);
	g_key_file_set_value(gkf, grp_name, "Id", id);
	g_strfreev(ops);
	g_free(grp_name);
	data = g_key_file_to_data(gkf, &size, &gerror);
	g_key_file_free(gkf);
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	etag = NULL;
	new_etag = NULL;
	if ( !g_file_replace_contents(gfile, data, size, etag, FALSE, G_FILE_CREATE_NONE, &new_etag, (GCancellable*)NULL, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	if ( new_etag ) {
		g_free(new_etag);
	}
	g_object_unref(gfile);
	g_free(data);
	return retval;
}

int lazy_rasql_combine(gchar*oper, gchar**input_pointers, gchar**output_pointers, gchar*opoptions, GError**err)
{
	int retval = 0;
	gchar**ops;
	gchar*grp_name, *arg1, *arg2, *id;
	GKeyFile*gkf;
	GFile*gfile;
	char *etag, *new_etag;
	char*data, *data1, *data2;
	gsize size1, size2, size;
	GError *gerror = NULL;
	g_return_val_if_fail (err == NULL || *err == NULL, retval);
	id = lazy_rasql_get_id(output_pointers[0]);
	grp_name = g_strdup_printf("Combine:%s", id);
	arg1 = lazy_rasql_get_id(input_pointers[0]);
	arg2 = lazy_rasql_get_id(input_pointers[1]);
	gfile = g_file_new_for_commandline_arg(input_pointers[0]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &data1, &size1, &etag, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_object_unref(gfile);
	gfile = g_file_new_for_commandline_arg(input_pointers[1]);
	if ( !g_file_load_contents(gfile, (GCancellable*)NULL, &data2, &size2, &etag, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_object_unref(gfile);
	data = g_strconcat ( data1, data2, NULL );
	size = size1 + size2;
	g_free(data1);
	g_free(data2);
	gkf = g_key_file_new();
	if ( !g_key_file_load_from_data(gkf, (const gchar*)data, size, G_KEY_FILE_NONE, &gerror ) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	g_free(data);
	g_key_file_set_value(gkf, grp_name, "Arg1", arg1);
	g_key_file_set_value(gkf, grp_name, "Arg2", arg2);
	g_key_file_set_value(gkf, grp_name, "Method", oper);
	g_key_file_set_value(gkf, grp_name, "Id", id);
	g_free(grp_name);
	data = g_key_file_to_data(gkf, &size, &gerror);
	g_key_file_free(gkf);
	gfile = g_file_new_for_commandline_arg(output_pointers[0]);
	etag = NULL;
	new_etag = NULL;
	if ( !g_file_replace_contents(gfile, data, size, etag, FALSE, G_FILE_CREATE_NONE, &new_etag, (GCancellable*)NULL, &gerror) ) {
		g_propagate_error (err, gerror);
		retval = 1;
	}
	if ( new_etag ) {
		g_free(new_etag);
	}
	g_object_unref(gfile);
	g_free(data);
	return retval;
}

int main(int argc,char **argv)
{
	gchar**input_pointers, **output_pointers;
	int i, retval;
	int flaggs;
	GOptionContext *context;
	GError *gerror = NULL;
	context = g_option_context_new ("- convert data to correct type and call the operator");
	g_option_context_add_main_entries(context, (const GOptionEntry *)entries, "door");
	g_option_context_set_ignore_unknown_options(context, TRUE);
	if (!g_option_context_parse (context, &argc, &argv, &gerror)) {
		g_error ("option parsing failed: %s\n", gerror->message);
	}
	g_option_context_free (context);
	if ( operation == NULL ) {
		g_error("%s called with wrong arguments", argv[0]);
	}
	if ( 0 == g_strcmp0(operation, "write") ) {
		input_pointers = g_strsplit(argv[argc - 2], ",", MAXTOKENS);
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( input_pointers == NULL || output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_write(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "get") ) {
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_get(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "roi") ) {
		input_pointers = g_strsplit(argv[argc - 2], ",", MAXTOKENS);
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( input_pointers == NULL || output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_roi(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "crop") ) {
		input_pointers = g_strsplit(argv[argc - 2], ",", MAXTOKENS);
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( input_pointers == NULL || output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_crop(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "avg") ) {
		input_pointers = g_strsplit(argv[argc - 2], ",", MAXTOKENS);
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( input_pointers == NULL || output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_combine(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "minusabs") ) {
		input_pointers = g_strsplit(argv[argc - 2], ",", MAXTOKENS);
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( input_pointers == NULL || output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_combine(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else if ( 0 == g_strcmp0(operation, "exec") ) {
		output_pointers = g_strsplit(argv[argc - 1], ",", MAXTOKENS);
		if ( output_pointers == NULL ) {
			g_error("%s called with wrong arguments", argv[0]);
		}
		retval = lazy_rasql_exec(operation, input_pointers, output_pointers, opoptions, &gerror);
	} else {
		g_error("%s wrong operation %s", argv[0], operation);
	}
	return retval;
}
