/***************************************************************************
 *            lazyrasql.h
 *
 *  Fri Feb  3 14:37:02 2012
 *  Copyright  2012  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 

#ifndef LAZY_RASQL_H
#define LAZY_RASQL_H 1

#ifndef MAXTOKENS
#define MAXTOKENS 256
#endif

typedef enum {
	LAZY_RASQL_COMBINE_AVG,
	LAZY_RASQL_COMBINE_MINUSABS,
	LAZY_RASQL_COMBINE_ALL
} LazyRasqlCombineType;

typedef enum {
	LAZY_RASQL_COLOR_RGB,
	LAZY_RASQL_COLOR_GRAY,
	LAZY_RASQL_COLOR_GREY,
	LAZY_RASQL_COLOR_RED,
	LAZY_RASQL_COLOR_GREEN,
	LAZY_RASQL_COLOR_BLUE,
	LAZY_RASQL_COLOR_ALL
} LazyRasqlColorType;

#endif