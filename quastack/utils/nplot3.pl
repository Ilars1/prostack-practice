#!/usr/bin/perl

use strict;

my $term;
my $columns;
my @column;
my $titles;
my @title;
my $i;
my $k;
my $infile;
my $outfile;
my $scfile;
my $lw;
my $pflag;
my $xcolumn;
my $xrange;
my $yrange;

if ( $#ARGV != 9 ) {
    print STDOUT "This is nplot3. Usage: xmin:xmax ymin:ymax xcolumn column,column,..,column title,title,...,title term lw pflag inFile outFile\n";
    exit(0);
}

$xrange = $ARGV[0];
$yrange = $ARGV[1];
$xcolumn = $ARGV[2];
$columns = $ARGV[3];
@column = split /\,/,$columns;
$titles = $ARGV[4];
@title = split /\,/,$titles;
$term = $ARGV[5];
$lw = $ARGV[6];
$pflag = $ARGV[7];
$infile = $ARGV[8];
$infile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;
$outfile = $ARGV[9];
$outfile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;


$scfile = $infile . ".sc";
open( OUT ,">$scfile");
select OUT;
#	printf("set key outside title \"Legend\" noautotitle\n");
	print("set xrange [$xrange]\n");
	print("set yrange [$yrange]\n");
	print("set key outside width 1 reverse\n");
	print("set term $term\n");
	print("set output \'$outfile\'\n");
	printf("plot");
	for($i = 0; $i < $#column; $i++) {
		$k = $i + 1;
		print(" \'$infile\' using $xcolumn:$column[$i] title \"$title[$i]\" with $pflag lt $k lw $lw,"); 
	}
	$i = $#column;
	$k = $i + 1;
	print(" \'$infile\' using $xcolumn:$column[$i] title \"$title[$i]\" with $pflag lt $k lw $lw"); 
close(OUT);
system("gnuplot $scfile");
unlink $scfile;

