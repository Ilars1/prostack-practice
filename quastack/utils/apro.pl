#!/usr/bin/perl

use strict;

my $infile;
my $outfile;
my $xcol;
my $ycol;
my $xmin;
my $xmax;
my @data;
my @line;
my @sorted;
my $k;

# Сортировка строк по полям, разделенным символом.
# Например, хочу отсортировать строки, разделенные на поля запятой, сначала по второму полю по числам, затем по первому полю по алфавиту в порядке убывания.


sub fieldsort {
	my ($sep, $cols);
	if (ref $_[0]) {
		$sep = '\\s+'
	} else {
		$sep = shift;
	}
	unless (ref($cols = shift) eq 'ARRAY') {
		die "fieldsort columns must be in anon array";
	}
	my (@sortcode, @col);
	my $col = 1;
	for (@$cols) {
		my ($a, $b) = /^-/ ? qw(b a) : qw(a b);
		my $op = /n$/ ? '<=>' : 'cmp';
		push @col, (/(\d+)/)[0] - 1;
		push @sortcode, "\$${a}->[$col] $op \$${b}->[$col]";
		$col++;
	}
	my $sortfunc = eval "sub { " . join (" or ", @sortcode) . " } ";
	my $splitfunc = eval 'sub { (split /$sep/o, $_)[@col] } ';
	return
		map $_->[0],
		sort { $sortfunc->() }
		map [$_, $splitfunc->($_)],
		@_;
	}
                 
#Примеры:

# Как сказано выше
# @sorted = fieldsort ':', ['2n', -1], @data;

# по 2-му затем по 1-му полю, по алфавиту, разделены пробелами
# @sorted = fieldsort [2, 1], @data;

# по 1-му полю по числам в порядке убывания, затем по 3-му полю
# по алфавиту и по 2-му по числам, поля разделены '+'
# @sorted = fieldsort '+', ['-1n', 3, 2], @data;

# На самом деле большая часть приведенного выше кода - это препроцессор, который готовит данные для дальнейшей сортировки Шварца. 


if ( $#ARGV != 5 ) {
    print STDOUT "This is apron. Usage: xmin xmax inFile outFile\n";
    exit(0);
}

$xcol = $ARGV[0];
$ycol = $ARGV[1];
$xmin = $ARGV[2];
$xmax = $ARGV[3];
$infile = $ARGV[4];
$outfile = $ARGV[5];

$infile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;
$outfile =~ s{ ^ ~ ( [^/]* ) }           # substitute tilde so that open can
	{ $1                        # understand it
	? (getpwnam($1))[7]
	: ( $ENV{HOME} || $ENV{LOGDIR}
	|| (getpwuid($>))[7]
	)
	}ex;

open( IN , "<$infile" );
@data = <IN>;
close( IN );

@sorted = fieldsort ['3n'], @data;

open( OUT ,">$outfile");
select OUT;

for ($k = 0; $k <= $#sorted; $k++) {
	@line = split /\s+/,$sorted[$k];
#printf("%4d %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n", $k, $r,$line[1], $r + $rstep, $a,$line[2], $a + $astep);
	if ( $xmin < $line[3] && $line[3] < $xmax ) {
		print "$line[$xcol] $line[$ycol]\n";
	}
}
close(OUT);

