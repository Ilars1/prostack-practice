/* Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin N.Kozlov 2008 <kozlov@freya>
 * 
 * main.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * main.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <config.h>
#include <glib.h>
#include <gio/gio.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
/*#include <glade/glade.h>*/



/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif



#include "callbacks.h"

/* For testing propose use the local (not installed) glade file */
/*#define GLADE_FILE "/prutik/glade/prutik.glade"*/
#define GTK_BUILDER_FILE "/prutik/glade/prutik.glade"
#define LOCALE_DIR "/share/locale"
/*#define GLADE_FILE "prutik.glade"*/

#include <goocanvas.h>

static GtkWidget *canvas;
static GdkPixbuf *pixbuf;
static int bits_per_sample;
static int colorspace;
static int has_alpha;
static int height;
static int n_channels;
static int width;
static GooCanvasItem *root, *rect_item, *text_item, *image_item, *line_1_item, *line_2_item;
static int mode;
static int halfway_1;
static int halfway_2;
static int line_1_x_1;
static int line_1_x_2;
static int line_1_y_1;
static int line_1_y_2;
static int line_2_x_1;
static int line_2_x_2;
static int line_2_y_1;
static int line_2_y_2;
static double line_1;
static double line_2;
/*static GladeXML *gxml;*/
static GtkBuilder *gtkbuild;

#ifndef MAX_RECORD
#define MAX_RECORD 256
#endif

/*gboolean func(GladeXML*gxml, GdkEventMotion*event)*/
gboolean func(GtkBuilder *gtkbuild, GdkEventMotion*event)
{
	int x, y;
	char*buf;
	GdkModifierType state;
	GtkWidget*w;
	int rowstride;
	guchar *pixels, *p;
	double lower;
	double upper;
	double position;
	double max_size;
	buf = (char*)calloc(128, sizeof(char));
	if (event->is_hint)
		gdk_window_get_pointer (event->window, &x, &y, &state);
	else {
		x = event->x;
		y = event->y;
	}
/*	w =  glade_xml_get_widget (gxml, "image1");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "image1"));
	rowstride = gdk_pixbuf_get_rowstride (pixbuf);
	if ( x >= 0 && y < height && x < width && y >= 0 ) {
		pixels = gdk_pixbuf_get_pixels (pixbuf);
		p = pixels + y * rowstride + x * n_channels;
/*		w =  glade_xml_get_widget (gxml, "label20");*/
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label20"));
		if ( n_channels == 1 )
			sprintf(buf, "%d", (int)p[0]);
		else if ( n_channels == 3 )
			sprintf(buf, "%d,%d,%d", (int)p[0], (int)p[1], (int)p[2]);
		else if ( n_channels == 4 )
			sprintf(buf, "%d,%d,%d,%d", (int)p[0], (int)p[1], (int)p[2], (int)p[3]);
		gtk_label_set_text((GtkLabel*)w, buf);
/*		w =  glade_xml_get_widget (gxml, "label16");*/
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label16"));
		sprintf(buf, "%d", y);
		gtk_label_set_text((GtkLabel*)w, buf);
/*		w = glade_xml_get_widget (gxml, "label18");*/
		w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label18"));
		sprintf(buf, "%d", x);
		gtk_label_set_text((GtkLabel*)w, buf);
	}
	free(buf);
	return TRUE;
}

/* This handles button presses in item views. We simply output a message to the console. */
static gboolean
on_rect_button_press (GooCanvasItem  *item, GooCanvasItem  *target, GdkEventButton *event, gpointer data)
{
	g_print ("rect item received button press event\n");
	return TRUE;
}

/* This handles button presses in item views. We simply output a message to the console. */
static gboolean
on_button_press (GooCanvasItem  *item, GooCanvasItem  *target, GdkEventButton *event, gpointer data)
{
	int x = event->x;
	int y = event->y;
	g_print ("image received button press event at %d,%d\n", x, y);
	return TRUE;
}

double
ll (int x_1, int y_1, int x_2, int y_2)
{
	return sqrt((x_1 - x_2) * (x_1 - x_2) + (y_1 - y_2) * (y_1 - y_2));
}

/* This handles button presses in item views. We simply output a message to the console. */
static gboolean
on_button_release (GooCanvasItem  *item, GooCanvasItem  *target, GdkEventButton *event, gpointer data)
{
	int x = event->x;
	int y = event->y;
	gchar *path_data, *buf;
	GtkWidget *w;
	g_print ("image received button release event at %d,%d\n", x, y);
	if ( mode == 0 ) {
		if ( halfway_1 == -1 ) {
			line_1_x_1 = x;
			line_1_y_1 = y;
			halfway_1 = 0;
		} else if ( halfway_1 == 0 ) {
			line_1_x_2 = x;
			line_1_y_2 = y;
			path_data = g_strdup_printf("M %d %d L %d %d", line_1_x_1, line_1_y_1, line_1_x_2, line_1_y_2);
			line_1_item = goo_canvas_path_new (root, (const gchar *)path_data, 
											"stroke-color", "red",
											"line-width", 2.0, NULL);
			g_free(path_data);
			goo_canvas_item_raise (line_1_item, NULL);
			halfway_1 = 1;
/*			w =  glade_xml_get_widget (gxml, "label2");*/
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label2"));
			line_1 = ll(line_1_x_1, line_1_y_1, line_1_x_2, line_1_y_2);
			buf = g_strdup_printf("%7.3f", line_1);
			gtk_label_set_text((GtkLabel*)w, buf);
			g_free(buf);
			if ( halfway_2 == 1 && fabs(line_1) > 0 ) {
/*				w =  glade_xml_get_widget (gxml, "label4");*/
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label4"));
				buf = g_strdup_printf("%7.3f", line_2 / line_1);
				gtk_label_set_text((GtkLabel*)w, buf);
				g_free(buf);
			}
		} else if ( halfway_1 == 1 ) {
			goo_canvas_item_remove (line_1_item);
			halfway_1 = -1;
/*			w =  glade_xml_get_widget (gxml, "label2");*/
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label2"));
			buf = g_strdup_printf("-");
			gtk_label_set_text((GtkLabel*)w, buf);
			g_free(buf);
		}
	} else if ( mode == 1 ) {
		if ( halfway_2 == -1 ) {
			line_2_x_1 = x;
			line_2_y_1 = y;
			halfway_2 = 0;
		} else if ( halfway_2 == 0 ) {
			line_2_x_2 = x;
			line_2_y_2 = y;
			path_data = g_strdup_printf("M %d %d L %d %d", line_2_x_1, line_2_y_1, line_2_x_2, line_2_y_2);
			line_2_item = goo_canvas_path_new (root, (const gchar *)path_data, 
											"stroke-color", "yellow",
											"line-width", 2.0, NULL);
			g_free(path_data);
			goo_canvas_item_raise (line_2_item, NULL);
			halfway_2 = 1;
/*			w =  glade_xml_get_widget (gxml, "label3");*/
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label3"));
			line_2 = ll(line_2_x_1, line_2_y_1, line_2_x_2, line_2_y_2);
			buf = g_strdup_printf("%7.3f", line_2);
			gtk_label_set_text((GtkLabel*)w, buf);
			g_free(buf);
			if ( halfway_1 == 1 && fabs(line_1) > 0 ) {
/*				w =  glade_xml_get_widget (gxml, "label4");*/
				w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label4"));
				buf = g_strdup_printf("%7.3f", line_2 / line_1);
				gtk_label_set_text((GtkLabel*)w, buf);
				g_free(buf);
			}
		} else if ( halfway_2 == 1 ) {
			goo_canvas_item_remove (line_2_item);
			halfway_2 = -1;
/*			w =  glade_xml_get_widget (gxml, "label3");*/
			w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label3"));
			buf = g_strdup_printf("-");
			gtk_label_set_text((GtkLabel*)w, buf);
			g_free(buf);
		}
	}
	return TRUE;
}

static gboolean 
on_ok_press(char*bname)
{
	GtkWidget*w;
	FILE*fp;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
	GError *gerror = NULL;
#endif

#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)bname, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Prutik: Can't convert output name: %s\nat main.c:261", gerror->message);
	fp = g_fopen(converted, "w");
	if ( !fp )
		g_error("Can't open: %s", converted);
	g_free(converted);
#else
	fp = g_fopen(bname, "w");
	if ( !fp )
		g_error("Can't open: %s", bname);
#endif
/*	w =  glade_xml_get_widget (gxml, "label2");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label2"));
	g_fprintf(fp, "%s\n", gtk_label_get_text((GtkLabel*)w));
/*	w =  glade_xml_get_widget (gxml, "label3");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label3"));
	g_fprintf(fp, "%s\n", gtk_label_get_text((GtkLabel*)w));
/*	w =  glade_xml_get_widget (gxml, "label4");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label4"));
	g_fprintf(fp, "%s\n", gtk_label_get_text((GtkLabel*)w));
	fclose(fp);
	gtk_main_quit();
	return FALSE;
}

static gboolean 
on_line_press(gchar*bname)
{
	if ( !strcmp(bname, "Line 1") ) {
		mode = 0;
		g_print ("Doing line 1\n");
	} else if ( !strcmp(bname, "Line 2") ) {
		mode = 1;
		g_print ("Doing line 2\n");
	}
	return FALSE;
}

/*
    * Mac OS X: _NSGetExecutablePath() (man 3 dyld)
    * Linux: readlink /proc/self/exe
    * Solaris: getexecname()
    * FreeBSD: sysctl CTL_KERN KERN_PROC KERN_PROC_PATHNAME -1
    * BSD with procfs: readlink /proc/curproc/file
    * Windows: GetModuleFileName() with hModule = NULL
*/

gchar*subst_data_dir(gchar*ORIGIN)
{
	static gchar *src_path;
	gchar*data_basename;
	char*runtime_install_prefix;
#ifdef G_OS_WIN32
	runtime_install_prefix = g_win32_get_package_installation_directory_of_module(NULL);
#else
	int ret = -1, ret_size = MAX_RECORD;
	char *buf;
	runtime_install_prefix = (char*)calloc(ret_size, sizeof(char));
#ifdef G_OS_DARWIN
	ret = _NSGetExecutablePath(runtime_install_prefix, &ret_size);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. On Mac OS X the size may be = %d", ret_size);
	runtime_install_prefix[ret_size - 1] = '\0';
#else
	ret = readlink("/proc/self/exe", runtime_install_prefix, ret_size - 1);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. The returned size may be = %d", ret);
	runtime_install_prefix[ret] = '\0';
#endif
	buf = g_path_get_dirname(runtime_install_prefix);
	g_free(runtime_install_prefix);
	runtime_install_prefix = g_path_get_dirname(buf);
#endif
	if ( runtime_install_prefix == NULL ) {
		return ORIGIN;
	}
	if ( src_path != NULL )
		g_free(src_path);
	if ( PACKAGE_DATA_DIR != NULL ) {
		data_basename = g_path_get_basename(PACKAGE_DATA_DIR);
	} else {
		return ORIGIN;
	}
	src_path = g_build_filename(runtime_install_prefix, data_basename, ORIGIN, NULL);
	g_free(data_basename);
	return src_path;
}

GtkWidget*
create_window (char*fn, char *fn1)
{
	GtkWidget *window, *w, *image;
	GError *gerror = NULL;
	char*buf;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
#endif
	buf = (char*)calloc(128, sizeof(char));
/*	gxml = glade_xml_new (subst_data_dir(GLADE_FILE), NULL, NULL);*/
	gtkbuild = gtk_builder_new ();
	if ( !gtk_builder_add_from_file (gtkbuild, subst_data_dir(GTK_BUILDER_FILE), &gerror)) {
		g_warning ("Couldn't load builder file: %s", gerror->message);
		g_error_free (gerror);
	}
/* This is important */
/*	glade_xml_signal_autoconnect (gxml);*/
	gtk_builder_connect_signals (gtkbuild, NULL);
/*	window = glade_xml_get_widget (gxml, "window");*/
	window = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "window"));
/*	w = glade_xml_get_widget (gxml, "button1");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button1"));
	g_signal_connect_swapped (w, "clicked", G_CALLBACK(on_line_press), g_strdup("Line 1"));
/*	w = glade_xml_get_widget (gxml, "button2");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button2"));
	g_signal_connect_swapped (w, "clicked", G_CALLBACK(on_line_press), g_strdup("Line 2"));
/*	w = glade_xml_get_widget (gxml, "button4");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button4"));
	g_signal_connect_swapped (w, "clicked", G_CALLBACK(on_ok_press), fn1);
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)fn, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert image name: %s", gerror->message);
	image = gtk_image_new_from_file((const gchar *)converted);
	gtk_window_set_title((GtkWindow*)window, converted);
#else
	image = gtk_image_new_from_file((const gchar *)fn);
	gtk_window_set_title((GtkWindow*)window, fn);
#endif
	pixbuf = gtk_image_get_pixbuf((GtkImage *)image);
	n_channels = gdk_pixbuf_get_n_channels((const GdkPixbuf *)pixbuf);
	height = gdk_pixbuf_get_height((const GdkPixbuf *)pixbuf);
	width = gdk_pixbuf_get_width((const GdkPixbuf *)pixbuf);
/*	w = glade_xml_get_widget (gxml, "viewport3");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "viewport3"));
	canvas = goo_canvas_new ();
	gtk_widget_set_size_request (canvas, width, height);
	gtk_widget_set_size_request (window, width, height);
	goo_canvas_set_bounds (GOO_CANVAS (canvas), 0, 0, 1000, 1000);
	gtk_widget_show (canvas);
	gtk_container_add (GTK_CONTAINER (w), canvas);
	g_signal_connect_swapped(G_OBJECT(canvas), "motion_notify_event", G_CALLBACK(func), gtkbuild);
	sprintf(buf, "%d", height);
/*	w =  glade_xml_get_widget (gxml, "label12");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label12"));
	gtk_label_set_text((GtkLabel*)w, buf);
	sprintf(buf, "%d", width);
/*	w =  glade_xml_get_widget (gxml, "label14");*/
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "label14"));
	gtk_label_set_text((GtkLabel*)w, buf);
	g_free(buf);
	root = goo_canvas_get_root_item (GOO_CANVAS (canvas));
/* Add a few simple items. 
	rect_item = goo_canvas_rect_new (root, 100, 100, 400, 400,
										"line-width", 10.0,
										"radius-x", 20.0,
										"radius-y", 10.0,
										"stroke-color", "yellow",
										"fill-color", "red", NULL);
  
	text_item = goo_canvas_text_new (root, "Hello World", 300, 300, -1,
										GTK_ANCHOR_CENTER,
										"font", "Sans 24", NULL);
	goo_canvas_item_rotate (text_item, 45, 300, 300);
 Connect a signal handler for the rectangle item. 
	g_signal_connect (rect_item, "button_press_event", (GtkSignalFunc) on_rect_button_press, NULL);*/

	image_item = goo_canvas_image_new (root, pixbuf,
										0.0,
										0.0, NULL);

	g_signal_connect (image_item, "button_press_event", (GtkSignalFunc) on_button_press, NULL);
	g_signal_connect (image_item, "button_release_event", (GtkSignalFunc) on_button_release, NULL);

	return window;
}


int
main (int argc, char *argv[])
{
	GtkWidget *window;
	gchar *of;

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	
	gtk_set_locale ();
	gtk_init (&argc, &argv);
	mode = -1;
	halfway_1 = -1;
	halfway_2 = -1;
	if ( argc <= 1 )
		g_error("No input file");
	if ( argc <= 2 ) {
		of = g_strdup_printf("%s.membr", argv[1]);
	} else {
		of = g_strdup(argv[2]);
	}
	gtk_window_set_default_icon_from_file (subst_data_dir("/icons/gnome/48x48/mimetypes/gnome-mime-application-x-prostack.png"), NULL);
	window = create_window (argv[1], of);
	gtk_widget_show (window);

	gtk_main ();
	return 0;
}
