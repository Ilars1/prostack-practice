/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin Kozlov 2008 <kozlov@freya>
 * 
 * main.c is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * main.c is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with main.c.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#include <stdio.h>
#include <httpd/httpd.h>
#include <httpd/http_config.h>

#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_script.h"
#include "http_connection.h"
#include "ap_config.h"
#include "apr_strings.h"

#include "door.h"

/* ### what is the best way to set this? */
#define PROSTACK_DEFAULT_PROVIDER "door"

/* used to denote that mod_prostack will be handling this request */
#define PROSTACK_HANDLER_NAME "prostack-handler"

enum {
	PROSTACK_ENABLED_UNSET = 0,
	PROSTACK_ENABLED_OFF,
	PROSTACK_ENABLED_ON
};

/* per-dir configuration */
typedef struct {
	char *provider_name;
	char *full_uri;
	char *dir;
	int timeout;
	int allow_install;
	int allow_library;
} prostack_dir_conf;

/* per-server configuration */
typedef struct {
	char *location;
	char *conf_dir;
	char *user;
	char *kimono_db;
	int port_num;
} prostack_server_conf;

#define PROSTACK_INHERIT_VALUE(parent, child, field) ((child)->field ? (child)->field : (parent)->field)

extern module AP_MODULE_DECLARE_DATA prostack_module;

/*
 * Locate our directory configuration record for the current request.
 */
static prostack_dir_conf *our_dconfig(const request_rec *r)
{
	return (prostack_dir_conf *) ap_get_module_config(r->per_dir_config, &prostack_module);
}

/*
 * Locate our server configuration record for the specified server.
 */
static prostack_server_conf *our_sconfig(const server_rec *s)
{
	return (prostack_server_conf *) ap_get_module_config(s->module_config, &prostack_module);
}


static void *prostack_create_server_config(apr_pool_t *p, server_rec *s)
{
	prostack_server_conf *newconf;
	char *sname;
	newconf = (prostack_server_conf *)apr_pcalloc(p, sizeof(*newconf));
/* ### this isn't used at the moment... */
	sname = s->server_hostname;
	sname = (sname != NULL) ? sname : "";
	newconf->location = apr_pstrdup(p, sname);
	newconf->kimono_db = apr_pstrdup(p, "kimono-db.db");
	newconf->port_num = 0;
	newconf->conf_dir = apr_pstrdup(p, "/var/www/prostack");
	newconf->user = apr_pstrdup(p, "lims");
	if( !g_thread_supported()) {
		g_thread_init (NULL);
		g_type_init();
	}
	return newconf;
}

static void *prostack_merge_server_config(apr_pool_t *p, void *base, void *overrides)
{
	prostack_server_conf *child = overrides;
	prostack_server_conf *newconf;
	newconf = (prostack_server_conf *)apr_pcalloc(p, sizeof(*newconf));
/* ### nothing to merge right now... */
	newconf->location = (char*)apr_pstrdup(p, child->location);
	return newconf;
}

static void *prostack_create_dir_config(apr_pool_t *p, char *dir)
{
/* NOTE: dir==NULL creates the default per-dir config */
	prostack_dir_conf *conf;
	conf = (prostack_dir_conf *)apr_pcalloc(p, sizeof(*conf));
/* clean up the directory to remove any trailing slash */
	if (dir != NULL) {
		char *d;
		apr_size_t l;
		d = apr_pstrdup(p, dir);
		l = strlen(d);
		if (l > 1 && d[l - 1] == '/')
			d[l - 1] = '\0';
		conf->dir = d;
	}
	return conf;
}

static void *prostack_merge_dir_config(apr_pool_t *p, void *base, void *overrides)
{
	prostack_dir_conf *parent = base;
	prostack_dir_conf *child = overrides;
	prostack_dir_conf *newconf = (prostack_dir_conf *)apr_pcalloc(p, sizeof(*newconf));
/* DBG3("dav_merge_dir_config: new=%08lx  base=%08lx  overrides=%08lx",
 (long)newconf, (long)base, (long)overrides); */
	newconf->full_uri = PROSTACK_INHERIT_VALUE(parent, child, full_uri);
	newconf->provider_name = PROSTACK_INHERIT_VALUE(parent, child, provider_name);
	if (parent->provider_name != NULL) {
		if (child->provider_name == NULL) {
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL, "\"ProStack Off\" cannot be used to turn off a subtree");
		} else if (strcasecmp(child->provider_name, parent->provider_name) != 0) {
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL, "A subtree cannot specify a different ProStack provider than its parent.");
		}
	}
	newconf->timeout = PROSTACK_INHERIT_VALUE(parent, child, timeout);
	newconf->dir = PROSTACK_INHERIT_VALUE(parent, child, dir);
	newconf->allow_install = PROSTACK_INHERIT_VALUE(parent, child, allow_install);
	newconf->allow_library = PROSTACK_INHERIT_VALUE(parent, child, allow_library);
	return newconf;
}


/*
 * All our process-death routine does is add its trace to the log.
 */
static apr_status_t prostack_child_exit(void *data)
{
	char *note;
	server_rec *s = data;
	char *sname = s->server_hostname;
/*
* The arbitrary text we add to our trace entry indicates for which server
* we're being called.
*/
	kimono_shutdown();
	sname = (sname != NULL) ? sname : "";
	fprintf(stderr,"prostack_child_exit: server config:%s;\n", sname);
	fflush(stderr);
	return APR_SUCCESS;
}

/*
 * All our process initialiser does is add its trace to the log.
 */
static void prostack_child_init(apr_pool_t *p, server_rec *s)
{
//	char *note;
	int nargs = 0;
	char**args = NULL;
	prostack_server_conf *sconf;
	sconf = our_sconfig(s);
/*fprintf(stderr,"prostack_child_init: sconf->conf_dir:%s;\n", sconf->conf_dir);
fprintf(stderr,"prostack_child_init: sconf->user:%s;\n", sconf->user);
fprintf(stderr,"prostack_child_init: sconf->kimono_db:%s;\n", sconf->kimono_db);
fprintf(stderr,"prostack_child_init: sconf->port_num:%d;\n", sconf->port_num);
fflush(stderr);*/
//	note = apr_psprintf(p, "--database=%s --default_port_num=%d --conf_dir=%s --user=%s", sconf->kimono_db, sconf->port_num, sconf->conf_dir, sconf->user);
//	apr_tokenize_to_argv(note, &args, p); 
//	while ( args[nargs] ) nargs++;
//	nargs++;
	nargs = 5;
	args = (char**)g_new0(char**, nargs+1);
	args[0] = g_strdup_printf("mod_prostack");
	args[1] = g_strdup_printf("--database=%s", sconf->kimono_db);
	args[2] = g_strdup_printf("--default_port_num=%d", sconf->port_num);
	args[3] = g_strdup_printf("--conf_dir=%s", sconf->conf_dir);
	args[4] = g_strdup_printf("--user=%s", sconf->user);
	args[5] = NULL;
/*fprintf(stderr,"prostack_child_init: args[0]:%s;\n", args[0]);
fprintf(stderr,"prostack_child_init: args[1]:%s;\n", args[1]);
fprintf(stderr,"prostack_child_init: args[2]:%s;\n", args[2]);
fprintf(stderr,"prostack_child_init: args[3]:%s;\n", args[3]);
fprintf(stderr,"prostack_child_init: args[4]:%s;\n", args[4]);
fprintf(stderr,"prostack_child_init: nargs:%d;\n", nargs);
fflush(stderr);*/
	kimono_init(&nargs, &args, FALSE);
	apr_pool_cleanup_register(p, s, prostack_child_exit, prostack_child_exit);
	g_strfreev(args);
}


/*
 * Command handler for the ProStackFullUri directive, which is TAKE1.
 */
static const char *prostack_cmd_fulluri(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_dir_conf *conf = (prostack_dir_conf *)config;
	conf->full_uri = apr_pstrdup(cmd->pool, arg1);
	return NULL;
}

static const char *prostack_cmd_kimono_db(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_server_conf *s_cfg = our_sconfig(cmd->server);
	s_cfg->kimono_db = apr_pstrdup(cmd->pool, arg1);
	return NULL;
}


static const char *prostack_cmd_conf_dir(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_server_conf *s_cfg = our_sconfig(cmd->server);
	s_cfg->conf_dir = apr_pstrdup(cmd->pool, arg1);
	return NULL;
}


static const char *prostack_cmd_user(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_server_conf *s_cfg = our_sconfig(cmd->server);
	s_cfg->user = apr_pstrdup(cmd->pool, arg1);
	return NULL;
}

/*
 * Command handler for the ProStackFullUri directive, which is TAKE1.
 */
static const char *prostack_cmd_prostack(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_dir_conf *conf = (prostack_dir_conf *)config;
	if (strcasecmp(arg1, "on") == 0) {
		conf->provider_name = PROSTACK_DEFAULT_PROVIDER;
	} else if (strcasecmp(arg1, "off") == 0) {
		conf->provider_name = NULL;
	} else {
		conf->provider_name = apr_pstrdup(cmd->pool, arg1);
	}
	return NULL;
}


/*
 * Command handler for the ProStackInstall directive, which is FLAG.
 */
static const char *prostack_cmd_install(cmd_parms *cmd, void *config, int arg)
{
	prostack_dir_conf *conf = (prostack_dir_conf *)config;
	if (arg)
		conf->allow_install = PROSTACK_ENABLED_ON;
	else
		conf->allow_install = PROSTACK_ENABLED_OFF;
	return NULL;
}


/*
 * Command handler for the ProStackLibrary directive, which is FLAG.
 */
static const char *prostack_cmd_library(cmd_parms *cmd, void *config, int arg)
{
	prostack_dir_conf *conf = (prostack_dir_conf *)config;
	if (arg)
		conf->allow_library = PROSTACK_ENABLED_ON;
	else
		conf->allow_library = PROSTACK_ENABLED_OFF;
	return NULL;
}


/*
 * Command handler for Timeout directive, which is TAKE1
 */
static const char *prostack_cmd_timeout(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_dir_conf *conf = (prostack_dir_conf *)config;
	conf->timeout = atoi(arg1);
	if (conf->timeout < 0)
		return "Timeout requires a non-negative integer.";
	return NULL;
}

static const char *prostack_cmd_port_num(cmd_parms *cmd, void *config, const char *arg1)
{
	prostack_server_conf *s_cfg = our_sconfig(cmd->server);
	s_cfg->port_num = atoi(arg1);
	if (s_cfg->port_num < 0)
		return "port_num requires a non-negative integer.";
	return NULL;
}

/*
 * This function is registered as a handler for HTTP methods and will
 * therefore be invoked for all GET requests (and others).  Regardless
 * of the request type, this function simply sends a message to
 * STDERR (which httpd redirects to logs/error_log).  A real module
 * would do *alot* more at this point.
 */
static int prostack_method_handler (request_rec *r)
{
	// Send a message to stderr (apache redirects this to the error log)
	fprintf(stderr,"apache2_mod_prostack: A request was made.\n");

	// We need to flush the stream so that the message appears right away.
	// Performing an fflush() in a production system is not good for
	// performance - don't do this for real.
	fflush(stderr);

	// Return DECLINED so that the Apache core will keep looking for
	// other modules to handle this request.  This effectively makes
	// this module completely transparent.
	return DECLINED;
}

static int prostack_read_request_body(request_rec *r, int status, char**error_message, char**body, int *length)
{
	int retval = 0;
	apr_bucket_brigade *bb;
	apr_bucket *b;
	int seen_eos = 0;
	bb = apr_brigade_create(r->pool, r->connection->bucket_alloc);
	(*body) = (char*)NULL;
	(*length) = 0;
	do {
		apr_status_t rc;
		rc = ap_get_brigade(r->input_filters, bb, AP_MODE_READBYTES, APR_BLOCK_READ, 2048);
		if (rc != APR_SUCCESS) {
			status = HTTP_INTERNAL_SERVER_ERROR;
			(*error_message) = g_strdup("Could not get next bucket brigade");
			retval = -1;
			break;
		}
		for ( b = APR_BRIGADE_FIRST(bb); b != APR_BRIGADE_SENTINEL(bb); b = APR_BUCKET_NEXT(b)) {
			const char *data;
			apr_size_t len;
			if (APR_BUCKET_IS_EOS(b)) {
				seen_eos = 1;
				break;
			}
			if (APR_BUCKET_IS_METADATA(b)) {
				continue;
			}
			rc = apr_bucket_read(b, &data, &len, APR_BLOCK_READ);
			if (rc != APR_SUCCESS) {
				status = HTTP_BAD_REQUEST;
				(*error_message) = g_strdup("An error occurred while reading the request body.");
				retval = -1;
				break;
			}
			(*body) = (char*)realloc((*body), ((*length) + (int)len + 1) * sizeof(char));
			memcpy((*body) + (*length), data, len);
			(*length) += len;
		}
		apr_brigade_cleanup(bb);
	} while (!seen_eos);
	apr_brigade_destroy(bb);
	(*body)[ (*length) - 1 ] = '\0';
	return retval;
}

static int prostack_put_sys(request_rec *r)
{
	prostack_dir_conf *dconf;
	char **args;
	int nargs, res;
	Node*node;
	char*body, *err;
	int length, status;
	dconf = our_dconfig(r);
	args = g_strsplit(r->args, "&", 4);
	nargs = g_strv_length(args);
	if ( nargs < 3 || !args || !args[0] || !args[1] || !args[2] )
		return DECLINED;
	node = proxy_sys_node(args[0]);
	node->label = strcpy(node->label, args[1]);
	node->file = (char*)realloc(node->file, MAX_RECORD * sizeof(char) );
	node->file = strcpy(node->file, args[2]);
	if ( nargs == 4 ) {
		node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char) );
		node->id = strcpy(node->id, args[3]);
	}
	if ( nargs == 5 ) {
		node->parent_name = g_strdup (args[4]);
	}
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	body = NULL;
	err = NULL;
	res = prostack_read_request_body(r, status, &err, &body, &length);
	if ( res == -1 ) {
		if ( body ) {
			free(body);
		}
		r->status = status;
		if ( err ) {
			ap_rprintf(r, "%s", err);
			free ( err );
		}
		return DONE;
	}
	node->buf = body;
	res = door_put_sys(node);
	ap_set_content_type(r, "text/html");
	if ( res == 0 ) {
		ap_rprintf(r, "%s", node->buf);
		free(node->buf);
	} else {
		ap_rprintf(r, "Error");
	}
	proxy_sys_node_del(node);
	g_strfreev(args);
	return OK;
}

/* handle the PUT method */
static int prostack_method_put(request_rec *r)
{
	int status;
/* set up the HTTP headers for the response */
/* Handle conditional requests */
	status = ap_meets_conditions(r);
	if (status) {
		return status;
	}
	if ( strcasestr(r->uri, "SYS") )
		return prostack_put_sys(r);
	return OK;
}

/* handle the PUT method */
static int prostack_method_options(request_rec *r)
{
	int status;
/* set up the HTTP headers for the response */
/* Handle conditional requests */
	status = ap_meets_conditions(r);
	if (status) {
		return status;
	}
	return OK;
}

static int prostack_delete_sys(request_rec *r)
{
	prostack_dir_conf *dconf;
	char **args;
	int nargs, res;
	Node*node;
	dconf = our_dconfig(r);
	args = g_strsplit(r->args, "&", 4);
	nargs = g_strv_length(args);
	if ( nargs < 3 || !args || !args[0] || !args[1] || !args[2] )
		return DECLINED;
	node = proxy_sys_node(args[0]);
	node->label = strcpy(node->label, args[1]);
	node->file = (char*)realloc(node->file, MAX_RECORD * sizeof(char) );
	node->file = strcpy(node->file, args[2]);
	if ( nargs == 4 ) {
		node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char) );
		node->id = strcpy(node->id, args[3]);
	}
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	res = door_delete_sys(node);
	ap_set_content_type(r, "text/html");
	ap_rprintf(r, "%s", node->buf);
	free(node->buf);
	proxy_sys_node_del(node);
	g_strfreev(args);
	return OK;
}

/* handle the PUT method */
static int prostack_method_delete(request_rec *r)
{
	int status;
/* set up the HTTP headers for the response */
/* Handle conditional requests */
	status = ap_meets_conditions(r);
	if (status) {
		return status;
	}
	if ( strcasestr(r->uri, "SYS") )
		return prostack_delete_sys(r);
	return OK;
}

/* handle the GET method */
static int prostack_get_sys(request_rec *r)
{
	prostack_dir_conf *dconf;
	char **args;
	int nargs, res;
	Node*node;
	dconf = our_dconfig(r);
	args = g_strsplit(r->args, "&", 4);
	nargs = g_strv_length(args);
	if ( nargs < 3 || !args || !args[0] || !args[1] || !args[2] )
		return DECLINED;
	node = proxy_sys_node(args[0]);
	node->label = strcpy(node->label, args[1]);
	node->file = (char*)realloc(node->file, MAX_RECORD * sizeof(char) );
	node->file = strcpy(node->file, args[2]);
	if ( nargs == 4 ) {
		node->id = (char*)realloc(node->id, MAX_RECORD * sizeof(char) );
		node->id = strcpy(node->id, args[3]);
	}
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	res = door_get_sys(node);
	ap_set_content_type(r, "text/html");
/*	ap_rprintf(r, "%d ", res);
	ap_rprintf(r, "%s %s", dconf->full_uri, node->buf);*/
	ap_rprintf(r, "%s", node->buf);
/*	ap_rputs(DOCTYPE_HTML_3_2, r);
	ap_rputs("<HTML>\n", r);
	ap_rputs(" <HEAD>\n", r);
	ap_rputs("  <TITLE>mod_prostack Module Content-Handler Output\n", r);
	ap_rputs("  </TITLE>\n", r);
	ap_rputs(" </HEAD>\n", r);
	ap_rputs(" <BODY>\n", r);
	ap_rputs("  <H1><SAMP>mod_example</SAMP> Module Content-Handler Output\n", r);
	ap_rputs("  </H1>\n", r);
	ap_rprintf(r, "   <LI>URI: <SAMP>%s</SAMP>\n   </LI>\n", r->uri);
	ap_rprintf(r, "   <LI>ARGS: <SAMP>%s</SAMP>\n   </LI>\n", r->args);
	ap_rprintf(r, "   <LI>Request for server: <SAMP>%s</SAMP>\n   </LI>\n", r->server->server_hostname);
	ap_rprintf(r, "   <LI>Timeout: <SAMP>%d</SAMP>\n   </LI>\n", dconf->timeout);
	ap_rprintf(r, "   <LI>ProStackFullUri: <SAMP>%s</SAMP>\n   </LI>\n", dconf->full_uri);
	if (args && args[0]) ap_rprintf(r, "   <LI>method: <SAMP>%s</SAMP>\n   </LI>\n", args[0]);
	if (args && args[1]) ap_rprintf(r, "   <LI>uris: <SAMP>%s</SAMP>\n   </LI>\n", args[1]);
	if (args && args[2]) ap_rprintf(r, "   <LI>parms: <SAMP>%s</SAMP>\n   </LI>\n", args[2]);
	for ( i = 0; i < node->nInConns; i++ ) {
		ap_rprintf(r, "   <LI>Input[%d]: <SAMP>%s://%s:%d/%s</SAMP>\n   </LI>\n", i, node->inConn[i].channel[0]->proto, node->inConn[i].channel[0]->host, node->inConn[i].channel[0]->port, node->inConn[i].channel[0]->file);
	}
	ap_rprintf(r, "   <LI>Result: <SAMP>%d</SAMP>\n   </LI>\n", res);
	ap_rprintf(r, "   <LI>Status: <SAMP>%d</SAMP>\n   </LI>\n", node->status);
	ap_rprintf(r, "   <LI>Response: <SAMP>%s/%s</SAMP>\n   </LI>\n", dconf->full_uri, node->uid);
	ap_rputs(" </BODY>\n", r);
	ap_rputs("</HTML>\n", r);*/
	free(node->buf);
	proxy_sys_node_del(node);
	g_strfreev(args);
	return OK;
}

/* handle the GET method */
static int prostack_get_pam(request_rec *r)
{
	prostack_dir_conf *dconf;
	char **uris, **args, *buf;
	int nargs, i, nuris, res, node_parent_signal, uri_flag;
	Node*node;
	dconf = our_dconfig(r);
	args = g_strsplit(r->args, "&", 4);
	if ( !args )
		return DECLINED;
	setNotifyNode(notifyNode);
	node = newNode();
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	node->type = pam;
	node->pam = newOlapPAM(args[0]);
	setNodePorts(node);
	if ( args[1] && strlen(args[1]) > 0 ) {
		uris = g_strsplit(args[1], ",", 256);
		if ( uris ) {
			nuris = g_strv_length(uris);
		}
	}
	if ( node->nInConns != nuris )
		return DECLINED;
	for ( i = 0; i < node->nInConns; i++ ) {
		uri_flag = 0;
		node->inConn[i].channel = (Connection**)calloc(1, sizeof(Connection*));
		node->inConn[i].index = 0;
		node->inConn[i].channel[0] = newConnection(0, 0, 0, 0);
		node->inConn[i].channel[0]->host = g_strdup(node->oServer->hname);
		node->inConn[i].channel[0]->proto = g_strdup(node->oServer->proto);
		node->inConn[i].channel[0]->port = node->oServer->port;
		buf = g_uri_unescape_string((const char *)uris[i], (const char*)NULL);
		if ( buf ) {
			buf = g_strstrip(buf);
			if ( strlen(buf) > 0 ) {
				if ( g_str_has_prefix((const gchar*)buf, "omero://") ) {
					node->inConn[i].channel[0]->file = g_strdup(buf);
					uri_flag = 1;
				}
			}
			g_free(buf);
			buf = NULL;
		}
		if ( uri_flag == 0 ) {
			node->inConn[i].channel[0]->file = g_path_get_basename(uris[i]);
		}
/*fprintf(stderr,"prostack_get_pam: nargs:%s;\n", node->inConn[i].channel[0]->file);
fflush(stderr);*/
	}
	node->id = g_strdup(args[2]);
	node_parent_signal = 0;
	g_mutex_init(&(node->parent_mutex));
	node->parent_signal = &node_parent_signal;
	notifyNode(node, running);
	res = door_get_pam(node);
	ap_set_content_type(r, "text/html");
	ap_rprintf(r, "%d ", node->status);
	ap_rprintf(r, "%s %s", dconf->full_uri, node->uid);
	g_mutex_clear(&(node->parent_mutex));
	g_strfreev(uris);
	g_strfreev(args);
	return OK;
}

static int prostack_get_ins(request_rec *r)
{
	prostack_dir_conf *dconf;
	char*uri;
	int nargs, i, nuris, res, node_parent_signal;
	Node*node;
	dconf = our_dconfig(r);
	setNotifyNode(notifyNode);
	node = newNode();
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	node->type = ins;
	node->nOutConns = 1;
	node->outConn = (Port*)calloc(1, sizeof(Port));
	node->outConn[0].ID = 1;
	node->outConn[0].nConn = 0;
	node->outConn[0].channel = (Connection**)NULL;
	node->outConn[0].label = (char*)calloc(6, sizeof(char));
	node->outConn[0].label = strcpy(node->outConn[0].label, "image");
	node->outConn[0].type = tif;
	node_parent_signal = 0;
	g_mutex_init(&(node->parent_mutex));
	node->parent_signal = &node_parent_signal;
	node->file = g_strdup(r->args);
	notifyNode(node, running);
	res = door_get_ins(node);
	ap_set_content_type(r, "text/html");
	ap_rprintf(r, "%d ", node->status);
	ap_rprintf(r, "%s %s", dconf->full_uri, node->uid);
	g_mutex_clear(&(node->parent_mutex));
	return OK;
}

static int prostack_get_ous(request_rec *r)
{
	prostack_dir_conf *dconf;
	char**args;
	int nargs, i, nuris, res, node_parent_signal;
	Node*node;
	dconf = our_dconfig(r);
	args = g_strsplit(r->args, "&", 2);
	if ( !args )
		return DECLINED;
	setNotifyNode(notifyNode);
	node = newNode();
	node->timeDelay = dconf->timeout;
	node->oServer = os_init_default();
	node->type = ous;
	node->nInConns = 1;
	node->file = g_strdup(args[0]);
	node->inConn = (Port*)calloc(1, sizeof(Port));
	node->inConn[0].ID = 1;
	node->inConn[0].nConn = 0;
	node->inConn[0].channel = (Connection**)NULL;
	node->inConn[0].label = (char*)calloc(6, sizeof(char));
	node->inConn[0].label = strcpy(node->inConn[0].label, "image");
	node->inConn[0].type = tif;
	node->inConn[0].channel = (Connection**)calloc(1, sizeof(Connection*));
	node->inConn[0].index = 0;
	node->inConn[0].channel[0] = newConnection(0, 0, 0, 0);
	node->inConn[0].channel[0]->host = g_strdup(node->oServer->hname);
	node->inConn[0].channel[0]->proto = g_strdup(node->oServer->proto);
	node->inConn[0].channel[0]->port = node->oServer->port;
	node->inConn[0].channel[0]->file = g_path_get_basename(args[1]);
	node_parent_signal = 0;
	g_mutex_init(&(node->parent_mutex));
	node->parent_signal = &node_parent_signal;
	notifyNode(node, running);
	res = door_get_ous(node);
	ap_set_content_type(r, "text/html");
	ap_rprintf(r, "%d ", node->status);
	ap_rprintf(r, "%s %s", dconf->full_uri, node->uid);
	g_mutex_clear(&(node->parent_mutex));
	g_strfreev(args);
	return OK;
}


/* handle the GET method */
static int prostack_method_get(request_rec *r)
{
	int status;
	prostack_dir_conf *dconf;
//	prostack_server_conf *sconf;
	char*type, **uris, *id, *method;
	dconf = our_dconfig(r);
//	sconf = our_sconfig(r->server);
/* Handle conditional requests */
	status = ap_meets_conditions(r);
	if (status) {
		return status;
	}
/*
* We're all done, so cancel the timeout we set.  Since this is probably
* the end of the request we *could* assume this would be done during
* post-processing - but it's possible that another handler might be
* called and inherit our outstanding timer.  Not good; to each its own.
*/
/*
* We did what we wanted to do, so tell the rest of the server we
* succeeded.
*/
//	ap_rprintf(r, "   <LI>Server from conf: <SAMP>%s</SAMP>\n   </LI>\n", sconf->location);
	if ( strcasestr(r->uri, "PAM") )
		return prostack_get_pam(r);
	else if ( strcasestr(r->uri, "SYS") )
		return prostack_get_sys(r);
	else if ( strcasestr(r->uri, "INS") )
		return prostack_get_ins(r);
	else if ( strcasestr(r->uri, "OUS") )
		return prostack_get_ous(r);
	return DECLINED;
}


/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Now we declare our content handlers, which are invoked when the server   */
/* encounters a document which our module is supposed to have a chance to   */
/* see.  (See mod_mime's SetHandler and AddHandler directives, and the      */
/* mod_info and mod_status examples, for more details.)                     */
/*                                                                          */
/* Since content handlers are dumping data directly into the connection     */
/* (using the r*() routines, such as rputs() and rprintf()) without         */
/* intervention by other parts of the server, they need to make             */
/* sure any accumulated HTTP headers are sent first.  This is done by       */
/* calling send_http_header().  Otherwise, no header will be sent at all,   */
/* and the output sent to the client will actually be HTTP-uncompliant.     */
/*--------------------------------------------------------------------------*/
/*
 * Sample content handler.  All this does is display the call list that has
 * been built up so far.
 *
 * The return value instructs the caller concerning what happened and what to
 * do next:
 *  OK ("we did our thing")
 *  DECLINED ("this isn't something with which we want to get involved")
 *  HTTP_mumble ("an error status should be reported")
 */
static int prostack_handler(request_rec *r)
{
		prostack_server_conf *sconf;
	sconf = our_sconfig(r->server);
fprintf(stderr,"prostack_child_init: server config:%s;\n", sconf->conf_dir);
fprintf(stderr,"prostack_child_init: server config:%s;\n", sconf->user);
fprintf(stderr,"prostack_child_init: server config:%s;\n", sconf->kimono_db);
fprintf(stderr,"prostack_child_init: server config:%d;\n", sconf->port_num);
fflush(stderr);
	if (strcmp(r->handler, PROSTACK_HANDLER_NAME)) {
		return DECLINED;
	}
	if (r->parsed_uri.fragment != NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "buggy client used un-escaped hash in Request-URI");
//        return dav_error_response(r, HTTP_BAD_REQUEST, "The request was invalid: the URI included an un-escaped hash character");
		return DECLINED;
	}
/*
* If we're only supposed to send header information (HEAD request), we're
* already there.
*/
	if (r->header_only) {
		return OK;
	}
	if (!r->args) {
		return DECLINED;
	}
/*
* Now send our actual output.  Since we tagged this as being
* "text/html", we need to embed any HTML.
*/
/* ### do we need to do anything with r->proxyreq ?? */

/*
* ### anything else to do here? could another module and/or
* ### config option "take over" the handler here? i.e. how do
* ### we lock down this hierarchy so that we are the ultimate
* ### arbiter? (or do we simply depend on the administrator
* ### to avoid conflicting configurations?)
*/
/*
* Set up the methods mask, since that's one of the reasons this handler
* gets called, and lower-level things may need the info.
*
* First, set the mask to the methods we handle directly.  Since by
* definition we own our managed space, we unconditionally set
* the r->allowed field rather than ORing our values with anything
* any other module may have put in there.
*
* These are the HTTP-defined methods that we handle directly.
*/
	r->allowed = 0
		| (AP_METHOD_BIT << M_GET)
		| (AP_METHOD_BIT << M_PUT)
		| (AP_METHOD_BIT << M_DELETE)
		| (AP_METHOD_BIT << M_OPTIONS)
		| (AP_METHOD_BIT << M_INVALID);
/* dispatch the appropriate method handler */
	if (r->method_number == M_GET) {
		return prostack_method_get(r);
	}
	if (r->method_number == M_PUT) {
		return prostack_method_put(r);
	}
//	if (r->method_number == M_POST) {
//		return olap_method_post(r);
//	}
	if (r->method_number == M_DELETE) {
		return prostack_method_delete(r);
	}
	if (r->method_number == M_OPTIONS) {
		return prostack_method_options(r);
	}
	return DECLINED;
}

static int prostack_fixups(request_rec *r)
{
	prostack_dir_conf *conf;
/* quickly ignore any HTTP/0.9 requests which aren't subreqs. */
	if (r->assbackwards && !r->main) {
		return DECLINED;
	}
	conf = our_dconfig(r);
/* if ProStack is not enabled, then we've got nothing to do */
	if (conf->provider_name == NULL) {
		return DECLINED;
	}
	r->handler = PROSTACK_HANDLER_NAME;
	return OK;
}


/* This routine is used to actually process the connection that was received.
 * Only protocol modules should implement this hook, as it gives them an
 * opportunity to replace the standard HTTP processing with processing for
 * some other protocol.  Both echo and POP3 modules are available as
 * examples.
 *
 * The return VALUE is OK, DECLINED, or HTTP_mumble.  If we return OK, no
 * further modules are called for this phase.
 */
static int prostack_process_connection(conn_rec *c)
{
	apr_bucket_brigade *bb;
	apr_bucket *b;
	apr_status_t rv;
	int count;
/*	EchoConfig *pConfig = ap_get_module_config(c->base_server->module_config, &echo_module);
	if (!pConfig->bEnabled) {
		return DECLINED;
	}*/
	count = 0;
	do {
		bb = apr_brigade_create(c->pool, c->bucket_alloc);
/* Get a single line of input from the client */
		if (((rv = ap_get_brigade(c->input_filters, bb, AP_MODE_GETLINE, APR_BLOCK_READ, 0)) != APR_SUCCESS) || APR_BRIGADE_EMPTY(bb)) {
			apr_brigade_destroy(bb);
			break;
		} else {
			count++;
		}
/* Make sure the data is flushed to the client */
		fprintf(stderr,"apache2_mod_prostack: A connection was made.\n");
		fflush(stderr);
		b = apr_bucket_flush_create(c->bucket_alloc);
		APR_BRIGADE_INSERT_TAIL(bb, b);
/* Send back the data. */
		rv = ap_pass_brigade(c->output_filters, bb);
	} while ( count < 1 && rv == APR_SUCCESS);
	return OK;
}

/*
 * This function is a callback and it declares what other functions
 * should be called for request processing and configuration requests.
 * This callback function declares the Handlers for other events.
 */
static void prostack_register_hooks (apr_pool_t *p)
{
	ap_hook_child_init(prostack_child_init, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_handler(prostack_handler, NULL, NULL, APR_HOOK_MIDDLE);
//	ap_hook_process_connection(mod_prostack_process_connection, NULL, NULL, APR_HOOK_MIDDLE);
// I think this is the call to make to register a handler for method calls (GET PUT et. al.).
// We will ask to be last so that the comment has a higher tendency to
// go at the end.
//	ap_hook_handler(prostack_method_handler, NULL, NULL, APR_HOOK_LAST);
	ap_hook_fixups(prostack_fixups, NULL, NULL, APR_HOOK_MIDDLE);
}


/*---------------------------------------------------------------------------
 *
 * Configuration info for the module
 */

static const command_rec prostack_cmds[] =
{
/* per directory/location */
	AP_INIT_TAKE1("ProStack", prostack_cmd_prostack, NULL, ACCESS_CONF, "specify the ProStack provider for a directory or location"),
	AP_INIT_TAKE1("ProStackFullUri", prostack_cmd_fulluri, NULL, ACCESS_CONF, "specify the ProStack full uri"),
	AP_INIT_TAKE1("ProStackKimonoDb", prostack_cmd_kimono_db, NULL, ACCESS_CONF|RSRC_CONF, "specify the ProStack kimono db"),
	AP_INIT_TAKE1("ProStackConfDir", prostack_cmd_conf_dir, NULL, ACCESS_CONF|RSRC_CONF, "specify the ProStack conf dir"),
	AP_INIT_TAKE1("ProStackPortNum", prostack_cmd_port_num, NULL, ACCESS_CONF|RSRC_CONF, "specify port num"),
	AP_INIT_TAKE1("ProStackUser", prostack_cmd_user, NULL, ACCESS_CONF|RSRC_CONF, "specify the ProStack user"),
/* per directory/location, or per server */
	AP_INIT_TAKE1("ProStackTimeout", prostack_cmd_timeout, NULL, ACCESS_CONF|RSRC_CONF, "specify allowed timeout"),
/* per directory/location, or per server */
	AP_INIT_FLAG("ProStackInstall", prostack_cmd_install, NULL, ACCESS_CONF|RSRC_CONF, "allow install"),
	AP_INIT_FLAG("ProStackLibrary", prostack_cmd_library, NULL, ACCESS_CONF|RSRC_CONF, "allow library"),
	{ NULL }
};


/*
 * Declare and populate the module's data structure.  The
 * name of this structure ('tut1_module') is important - it
 * must match the name of the module.  This structure is the
 * only "glue" between the httpd core and the module.
 */
module AP_MODULE_DECLARE_DATA prostack_module =
{
// Only one callback function is provided.  Real
// modules will need to declare callback functions for
// server/directory configuration, configuration merging
// and other tasks.
	STANDARD20_MODULE_STUFF,
	prostack_create_dir_config,      /* dir config creater */
	prostack_merge_dir_config,       /* dir merger --- default is to override */
	prostack_create_server_config,   /* server config */
	prostack_merge_server_config,    /* merge server config */
	prostack_cmds,                   /* command table */
	prostack_register_hooks,         /* callback for registering hooks */
};
