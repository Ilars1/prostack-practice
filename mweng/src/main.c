/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin N.Kozlov 2007 <kozlov@freya.spbcas.ru>
 * 
 * main.c is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * main.c is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with main.c.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */
#include <time.h>
#include <glib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <engine.h>
#define OPTS ":ho:s:v"
#define  MAX_RECORD 256
#define  MAX_TOKENS 256

static char*oper;
static char*opts;
static char *inputs;
static char *outputs;

static GOptionEntry entries[] = 
{
	{ "inputs", 'i', 0, G_OPTION_ARG_STRING, &inputs, "comma separated list of inputs", NULL },
	{ "function", 'f', 0, G_OPTION_ARG_STRING, &oper, "name of Matlab function", NULL },
	{ "outputs", 'o', 0, G_OPTION_ARG_STRING, &outputs, "comma separated list of outputs", NULL },
	{ "option_string", 's', 0, G_OPTION_ARG_STRING, &opts, "options of Matlab function", NULL },
	{ NULL }
};

int main (int argc, char **argv)
{
	int kounter, n_inputs, n_outputs;
	GError *error = NULL;
	GOptionContext *context;
	FILE*fp;
	char**moptions,**minputs,**moutputs;
	GString*mcommand;
	Engine *ep;
	mxArray *T = NULL, *result = NULL;
	char buffer[MAX_RECORD+1];
	double time[10] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };

	context = g_option_context_new (" - Run Matlab function");
	g_option_context_add_main_entries (context, entries, "mweng");
	if ( ! g_option_context_parse(context, &argc, &argv, &error) ) {
		fprintf(stderr, "Unable to read file: %s\n", error->message);
		g_error_free(error);
	}
	g_option_context_free(context);
	if ( argc > 1 )
		g_error("Too many arguments!\n");
	if ( !oper )
		g_error("Operation is not defined\n");
/*	if ( !opts )
		g_error("Options are not defined\n");*/
/*	fprintf(stdout, "operation %s\n", oper);
	fprintf(stdout, "options %s\n", opts);
	fprintf(stdout, "inputs %s\n", inputs);
	fprintf(stdout, "outputs %s\n", outputs);*/
	if ( opts )
		moptions = g_strsplit(opts, ",", MAX_TOKENS);
	else
		moptions = NULL;
	if ( inputs ) 
		minputs = g_strsplit(inputs, ",", MAX_TOKENS);
	else
		minputs = NULL;
	if ( outputs )
		moutputs = g_strsplit(outputs, ",", MAX_TOKENS);
	else
		moutputs = NULL;
/*
* Start the MATLAB engine locally by executing the string
* "matlab"
*
* To start the session on a remote host, use the name of
* the host as the string rather than \0
*
* For more complicated cases, use any string with whitespace,
* and that string will be executed literally to start MATLAB
*/
	if (!(ep = engOpen("matlab -nodisplay\0"))) {
		g_error("\nCan't start MATLAB engine\n");
	}
	mcommand = g_string_new("\0");
	g_printf("init: <%s>\n", mcommand->str);
	fflush(stdout);
	if ( !minputs )
		n_inputs = 0;
	else {
		for ( kounter = 0; minputs[kounter]; kounter++ ) {
			g_string_printf(mcommand,"I_%d = imread('%s');", kounter, minputs[kounter]);
			g_printf("%s\n", mcommand->str);
			engEvalString(ep, mcommand->str);
			n_inputs = kounter;
		}
		n_inputs++;
	}
	mcommand = g_string_assign(mcommand, "");
	if ( !outputs )
		n_outputs = 0;
	else {
		for ( kounter = 0; moutputs[kounter]; kounter++) {
			g_string_append_printf(mcommand,"O_%d ", kounter);
			g_printf("%s\n", mcommand->str);
			n_outputs = kounter;
		}
		n_outputs++;
	}
	if ( n_outputs > 1 ) {
		mcommand = g_string_append(mcommand, "]");
		mcommand = g_string_prepend(mcommand, "[");
	}
	if ( n_outputs > 0 ) {
		mcommand = g_string_append(mcommand, " = ");
	}
	g_string_append_printf(mcommand,"%s(", oper);
	if ( n_inputs > 0 ) {
		for ( kounter = 0; kounter < n_inputs - 1; kounter++) {
			g_string_append_printf(mcommand,"I_%d,", kounter);
		}
		g_string_append_printf(mcommand,"I_%d", n_inputs - 1);
	}
	if ( moptions ) {
		if ( n_inputs > 0 )
			mcommand = g_string_append(mcommand,",");
		for ( kounter = 0; moptions[kounter]; kounter++)  {
			g_string_append_printf(mcommand,"%s,", moptions[kounter]);
			g_printf("%s\n", mcommand->str);
		}
		mcommand = g_string_truncate(mcommand, mcommand->len - 1);
	}
	mcommand = g_string_append(mcommand,");");
	g_printf("Matlab << %s\n", mcommand->str);
	engEvalString(ep, mcommand->str);
	for ( kounter = 0; kounter < n_outputs; kounter++) {
		g_string_printf(mcommand,"imwrite(O_%d, '%s');", kounter, moutputs[kounter]);
		g_printf("%s\n", mcommand->str);
		engEvalString(ep, mcommand->str);
	}
	
	
	engClose(ep);
	if ( inputs )
		free(inputs);
	if ( outputs )
		free(outputs);
	if ( oper )
		free(oper);
	if ( opts )
		free(opts);
	if ( moptions )
		g_strfreev(moptions);
	if ( moutputs )
		g_strfreev(moutputs);
	if ( minputs )
		g_strfreev(minputs);
	return EXIT_SUCCESS;
}


