/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * main.c
 * Copyright (C) Konstantin N.Kozlov 2007 <kozlov@spbcas.ru>
 * 
 * main.c is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * main.c is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with main.c.  If not, write to:
 * 	The Free Software Foundation, Inc.,
 * 	51 Franklin Street, Fifth Floor
 * 	Boston, MA  02110-1301, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <config.h>

#include <gtk/gtk.h>



/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif



#include "callbacks.h"

/* For testing propose use the local (not installed) glade file */
/*#define GLADE_FILE "/iapee/glade/iapee.glade"*/
#define GTK_BUILDER_FILE "/iapee/glade/iapee.glade"
#define LOCALE_DIR "locale"
/*#define GLADE_FILE "glaz.glade"*/


#include <CharImage.h>

static CharImage*ci;
static CharImage*di;
static int reverserows = 0;
static int reversecolumns = 0;
static GtkBuilder *gtkbuild;

static char *title;

static GOptionEntry entries[] = 
{
	{ "title", 't', 0, G_OPTION_ARG_STRING, &title, "Title", NULL },
	{ NULL }
};

void update()
{
	GtkWidget*w;
	GdkColor fg;
	GdkColor bg;
	GdkPixbuf *pixbuf;
	int rowstride;
	int height;
	int n_channels;
	gboolean has_alpha = FALSE;
	int width;
	int column, row, x, y, z;
	GtkTextBuffer*buffer;
	char*buf;
	guchar *pixels, *p;
	di = CharImageCopy(ci);
	buf = (char*)calloc(4096, sizeof(char));
	buf = strcpy(buf, "-\n");
	if ( reversecolumns == 1 ) {
		di = CharImageReverseColumn(di);
		buf = strcat(buf, "reversecolumn\n");
	} else {
		buf = strcat(buf, "-\n");
	}
	if ( reverserows == 1 ) {
		di = CharImageReverseRow(di);
		buf = strcat(buf, "reverserow\n");
	} else {
		buf = strcat(buf, "-\n");
	}
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "image1"));
	pixbuf = gtk_image_get_pixbuf((GtkImage*)w);
	rowstride = gdk_pixbuf_get_rowstride (pixbuf);
	n_channels = gdk_pixbuf_get_n_channels (pixbuf);
	height = gdk_pixbuf_get_height((const GdkPixbuf *)pixbuf);
	width = gdk_pixbuf_get_width((const GdkPixbuf *)pixbuf);
	pixels = gdk_pixbuf_get_pixels (pixbuf);
	has_alpha = gdk_pixbuf_get_has_alpha ( (const GdkPixbuf *)pixbuf);
	for ( x = 0, column = 0; x < width && column < di->columns; x++, column++ ) {
		for ( y = 0, row = 0; y < height && row < di->rows; y++, row++ ) {
			p = pixels + y * rowstride + x * n_channels;
/*			for ( z = 0; z < n_channels - 1; z++ ) {
				p[z] = di->pixels[row * di->columns + column];
			}*/
			p[0] =  di->pixels[row * di->columns + column];
			for ( z = 1; z < n_channels - 1; z++ ) {
				p[z] = di->pixels[row * di->columns + column];
			}
			if ( !has_alpha ) {
				p[ n_channels - 1 ] = di->pixels[row * di->columns + column];
			}
		}
	}
	gtk_widget_queue_draw(w);
	CharImageDelete(di);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "textview1"));
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (w));
	gtk_text_buffer_set_text (buffer, buf, -1);
	free(buf);
}

gboolean cb(int*flag)
{
	if ( *flag > 0 ) {
		*flag = 0;
	} else {
		*flag = 1;
	}
	update();
	return FALSE;
}

/*
    * Mac OS X: _NSGetExecutablePath() (man 3 dyld)
    * Linux: readlink /proc/self/exe
    * Solaris: getexecname()
    * FreeBSD: sysctl CTL_KERN KERN_PROC KERN_PROC_PATHNAME -1
    * BSD with procfs: readlink /proc/curproc/file
    * Windows: GetModuleFileName() with hModule = NULL
*/

gchar*subst_data_dir(gchar*ORIGIN)
{
	static gchar *src_path;
	gchar*data_basename;
	char*runtime_install_prefix;
#ifdef G_OS_WIN32
	runtime_install_prefix = g_win32_get_package_installation_directory_of_module(NULL);
#else
	int ret = -1, ret_size = MAX_RECORD;
	char *buf;
	runtime_install_prefix = (char*)calloc(ret_size, sizeof(char));
#ifdef G_OS_DARWIN
	ret = _NSGetExecutablePath(runtime_install_prefix, &ret_size);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. On Mac OS X the size may be = %d", ret_size);
	runtime_install_prefix[ret_size - 1] = '\0';
#else
	ret = readlink("/proc/self/exe", runtime_install_prefix, ret_size - 1);
	if ( ret == -1 || ret >= ret_size - 1)
		g_error("Can't determine install path. The returned size may be = %d", ret);
	runtime_install_prefix[ret] = '\0';
#endif
	buf = g_path_get_dirname(runtime_install_prefix);
	g_free(runtime_install_prefix);
	runtime_install_prefix = g_path_get_dirname(buf);
#endif
	if ( runtime_install_prefix == NULL ) {
		return ORIGIN;
	}
	if ( src_path != NULL )
		g_free(src_path);
	if ( PACKAGE_DATA_DIR != NULL ) {
		data_basename = g_path_get_basename(PACKAGE_DATA_DIR);
	} else {
		return ORIGIN;
	}
	src_path = g_build_filename(runtime_install_prefix, data_basename, ORIGIN, NULL);
	g_free(data_basename);
	return src_path;
}

GtkWidget*
create_window (char*fn)
{
	GtkWidget *window;
	GtkWidget *image;
	GdkPixbuf *pixbuf;
	GtkWidget*w;
	GError *gerror = NULL;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
#endif
	gtkbuild = gtk_builder_new ();
	if ( !gtk_builder_add_from_file (gtkbuild, subst_data_dir(GTK_BUILDER_FILE), &gerror)) {
		g_warning ("Couldn't load builder file: %s", gerror->message);
		g_error_free (gerror);
	}
/* This is important */
	gtk_builder_connect_signals (gtkbuild, NULL);
	window = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "dialog1"));
	image = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "image1"));
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)fn, len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert image name: %s", gerror->message);
/*	gtk_image_set_from_file((GtkImage *)image, (const gchar *)converted);*/
	pixbuf = gdk_pixbuf_new_from_file ((const gchar *)converted, &gerror);
#else
/*	gtk_image_set_from_file((GtkImage *)image, (const gchar *)fn);*/
	pixbuf = gdk_pixbuf_new_from_file ((const gchar *)fn, &gerror);
#endif
	if ( gerror ) {
		g_error(gerror->message);
	}
	gtk_image_set_from_pixbuf ((GtkImage *)image, pixbuf);
	g_object_unref ( pixbuf );
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button3"));
	g_signal_connect_swapped(G_OBJECT(w), "clicked", G_CALLBACK(cb), &reversecolumns);
	w = GTK_WIDGET (gtk_builder_get_object (gtkbuild, "button4"));
	g_signal_connect_swapped(G_OBJECT(w), "clicked", G_CALLBACK(cb), &reverserows);
	if ( !title ) {
#ifdef G_OS_WIN32
		gtk_window_set_title((GtkWindow*)window, converted);
#else
		gtk_window_set_title((GtkWindow*)window, fn);
#endif
	} else {
		gtk_window_set_title((GtkWindow*)window, title);
	}
	return window;
}

int
main (int argc, char *argv[])
{
	GtkWidget *window;
	GError *gerror = NULL;
	GOptionContext *context;
	char **rules;
	char*buf;
	gsize length;
#ifdef G_OS_WIN32
	gchar*converted;
	gssize len;
	gsize bytes_read;
	gsize bytes_written;
#endif
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, subst_data_dir(LOCALE_DIR));
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	gtk_set_locale ();
	context = g_option_context_new (" - Check orientation");
	g_option_context_add_main_entries (context, entries, "iapee");
	if ( ! g_option_context_parse(context, &argc, &argv, &gerror) ) {
		g_warning(gerror->message);
		g_error_free(gerror);
		gerror = NULL;
	}
	g_option_context_free(context);
	if ( argc <= 2 ) {
		g_error("No input file");
	}
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)argv[2], len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert file name: %s", gerror->message);
	if ( !g_file_get_contents(converted, &buf, &length, &gerror) ) {
		if ( gerror )
			g_error(gerror->message);
	}
#else
	if ( !g_file_get_contents(argv[2], &buf, &length, &gerror) ) {
		if ( gerror )
			g_error(gerror->message);
	}
#endif
	rules = g_strsplit(buf, "\n", 6);
	g_free(buf);
	if ( !rules || !rules[2] || !rules[3] ) {
		g_error("Wrong input\n");
	}
	if ( gtk_init_check (&argc, &argv) ) {
		ci = CharImageCreate(argv[1]);
		window = create_window (argv[1]);
		if ( gtk_dialog_run((GtkDialog*)window) == GTK_RESPONSE_OK ) {
			if ( reversecolumns == 1 ) {
				if ( !strcmp(rules[2], "-") ) {
					g_free(rules[2]);
					rules[2] = g_strdup("reversecolumn");
				} else {
					g_free(rules[2]);
					rules[2] = g_strdup("-");
				}
			}/* else {
				g_free(rules[2]);
				rules[2] = g_strdup("-");
			}*/
			if ( reverserows == 1 ) {
				if ( !strcmp(rules[3], "-") ) {
					g_free(rules[3]);
					rules[3] = g_strdup("reverserow");
				} else {
					g_free(rules[3]);
					rules[3] = g_strdup("-");
				}
			}/* else {
				g_free(rules[3]);
				rules[3] = g_strdup("-");
			}*/
		}
	}
	buf = g_strjoinv("\n", rules);
	g_strfreev(rules);
#ifdef G_OS_WIN32
	len = -1;
	converted = g_locale_to_utf8((const gchar *)argv[3], len, &bytes_read, &bytes_written, &gerror);
	if ( gerror )
		g_error("Can't convert file name: %s", gerror->message);
	if ( !g_file_set_contents(converted, buf, strlen(buf), &gerror) ) {
		if ( gerror )
			g_error(gerror->message);
	}
	g_free(converted);
#else
	if ( !g_file_set_contents(argv[3], buf, strlen(buf), &gerror) ) {
		if ( gerror )
			g_error(gerror->message);
	}
#endif
	g_free(buf);
	return 0;
}
